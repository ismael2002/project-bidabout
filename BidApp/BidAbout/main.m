//
//  main.m
//  BidAbout
//
//  Created by Waseem on 05/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BidAboutAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([BidAboutAppDelegate class]));
	}
}