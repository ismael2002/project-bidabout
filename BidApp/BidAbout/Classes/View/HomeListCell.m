//
//  HomeListCell.m
//  BidAbout
//
//  Created by Waseem on 30/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "HomeListCell.h"

@implementation HomeListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
	{
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (id) createCell
{
	NSArray* view =  [[NSBundle mainBundle] loadNibNamed:@"HomeList" owner:self options:nil];
	return [view objectAtIndex:0];
}

- (void) customizeCell
{
	// Set the background view of the
//	UIView* cellBg = [[UIView alloc] initWithFrame:self.frame];
//	self.backgroundView = cellBg;

	self.accessoryType = UITableViewCellAccessoryNone;
	self.selectionStyle = UITableViewCellSelectionStyleGray;
}

- (void) setTitleName:(NSString*)title
{
	_cellLabel.text = title;	
}

- (void) setImageAtIndex:(NSString*)imageName
{
    [_cellImageView setImage:[UIImage imageNamed:imageName]];
}

@end
