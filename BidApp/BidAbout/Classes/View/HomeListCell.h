//
//  HomeListCell.h
//  BidAbout
//
//  Created by Waseem on 30/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDBadgedCell.h"

@interface HomeListCell : TDBadgedCell
{
	__weak IBOutlet UIImageView*	_cellImageView;
	__weak IBOutlet UILabel*		_cellLabel;
}

+ (id) createCell;
- (void) customizeCell;
- (void) setTitleName:(NSString*)title;
- (void) setImageAtIndex:(NSString*)imageName;

@end
