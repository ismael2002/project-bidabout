//
//  UIImageBaseView.m
//  Misc
//

#define kOriginY  ([UIUtils isiPhone]) ? 0 : 0

#import <QuartzCore/QuartzCore.h>
#import "UIImageBaseView.h"

@implementation UIImageBaseView

- (void) drawRect:(CGRect)rect
{
	CGFloat yOffSet = kOriginY;
    
    NSString* imageName = [UIUtils iPhone5ImageName:@"login-background.png"];
    
    if (self.tag == 2)
        imageName = [UIUtils iPhone5ImageName:@"signup-background.png"];
    else if(self.tag == 2)
        imageName = [UIUtils iPhone5ImageName:@"settings-background.png"];
        
    
    UIImage* bgImage = ImageWithPath(ResourcePath(imageName));

	CGRect drawRect = rect;
	drawRect.origin.y = yOffSet;
    [bgImage drawInRect:rect];
}

@end

#pragma mark -

@implementation UIImageBaseWindow

- (void) drawRect:(CGRect)rect
{
	UIImage* bgImage = ImageWithPath(ResourcePath( @"HomeScreen.png"));
	[bgImage drawAtPoint:CGPointZero];
}

@end

@implementation BorderImageView

- (id) initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
	if (self)
	{
		[self.layer setBorderColor:[UIColor grayColor].CGColor];
		[self.layer setBorderWidth:1.0];
		[self.layer setCornerRadius:10.0];
		[self setBackgroundColor:[UIColor clearColor]];
		self.clipsToBounds = YES;
	}
	return self;
}

@end

@implementation BorderView

- (id) initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
	if (self)
	{
		[self.layer setBorderColor:[UIColor blackColor].CGColor];
		[self.layer setBorderWidth:1.0];
		[self.layer setCornerRadius:10.0];
		self.clipsToBounds = YES;
	}
	return self;
}

@end

@implementation TopBar

- (void) drawRect:(CGRect)rect
{
	UIImage* bgImage = ImageWithPath(ResourcePath( @"Topborder.png"));
	[bgImage drawInRect:rect];
    
    bgImage = ImageWithPath(ResourcePath( @"Borderline.png"));
    [bgImage drawAtPoint:CGPointMake(0, 78)];
}

@end

@implementation BorderWebView

- (id) initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
	if (self)
	{
        [[self layer] setBorderWidth:4.0];  
        [[self layer] setBorderColor:[[UIColor grayColor] CGColor]];
        
        UIImageView* background = [[UIImageView alloc] initWithFrame:self.bounds];
        background.tag = 1000;
        background.image = [UIImage imageNamed:@"HomeScreen.png"];
        [self addSubview:background];
	}
	return self;
}

@end


