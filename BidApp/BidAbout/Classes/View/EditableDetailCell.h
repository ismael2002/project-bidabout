


@interface EditableDetailCell : UITableViewCell
{
}

@property (nonatomic, retain) UITextField *textField;
@property (nonatomic, retain) UILabel* titleLabel;

@end
