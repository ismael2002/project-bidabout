
#import "EditableDetailCell.h"

@implementation EditableDetailCell

@synthesize textField = _textField;
@synthesize titleLabel = _titleLabel;

#pragma mark -


- (id) initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)identifier
{
    self = [super initWithStyle:style reuseIdentifier:identifier];
    
    if (self == nil)
    { 
        return nil;
    }
    
    CGRect bounds = [[self contentView] bounds];
    CGRect textFieldRect;
    
    if ([UIUtils isiPhone] )
    {
        textFieldRect = CGRectInset(bounds, 120.0, 12.0);
        textFieldRect.size.width = 190.0;
    }
    else
    {
        textFieldRect = CGRectInset(bounds, -200, 10.0);
        textFieldRect.size.width = 500.0;
    }
    
    CGRect labelRect;
    if ([UIUtils isiPhone] )
    {
        labelRect = CGRectInset(bounds, 5.0, 10.0);
    }
    else
    {
        labelRect = CGRectInset(bounds, 20.0, 10.0);
    }
    UITextField *textField = [[UITextField alloc] initWithFrame:textFieldRect];
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:labelRect];
    
    //  Set the keyboard's return key label to 'Next'.
    //
    [textField setReturnKeyType:UIReturnKeyDone];
    
    //  Make the clear button appear automatically.
    [textField setClearButtonMode:UITextFieldViewModeAlways];
    [textField setBackgroundColor:[UIColor clearColor]];
    [textField setOpaque:YES];
	[textField setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
	[textField setFont:[UIFont boldSystemFontOfSize:14.0]];
    
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setOpaque:YES];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:14.0]];
    
    
    CGRect rect;
    if ([UIUtils isiPhone] )
    {
        rect = CGRectMake(0, self.contentView.frame.size.height - 1.0, self.contentView.frame.size.width, .5);
    }
    else
    {
        rect = CGRectMake(0, self.contentView.frame.size.height - 1.0, 768, .5);
    }
    UIView* lineView = [[UIView alloc] initWithFrame:rect];
    
    UIColor *navColor = [UIColor colorWithRed:(114.0f/255.0f) green:(170.0f/255.0f) blue:(68.0f/255.0f) alpha:1.0f];
    lineView.backgroundColor = navColor;
    
    [[self contentView] addSubview:textField];
    [[self contentView] addSubview:titleLabel];
    if (KVersion >= 7)
        [[self contentView] addSubview:lineView];
    
    [self setTextField:textField];
    [self setTitleLabel:titleLabel];
    
    return self;
}

//  Disable highlighting of currently selected cell.
//
- (void) setSelected:(BOOL)selected
           animated:(BOOL)animated 
{
    [super setSelected:selected animated:NO];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

@end
