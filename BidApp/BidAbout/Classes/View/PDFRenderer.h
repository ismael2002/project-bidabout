//
//  PDFRenderer.h
//  
//

//

#import <Foundation/Foundation.h>
#import "Customers.h"

@interface PDFRenderer : NSObject


+ (void) drawPDF:(NSString*)fileName dataArray:(NSArray*)productArray withTitle:(NSString*)title withClient:(Customers*)clients withDetail:(NSArray*)detaillist header:(NSArray*)headerlist withDate:(NSString *)date type:(NSString *)estimate photoList:(NSArray *)photoList isinvoice: (BOOL) isInvoice showQuantity:(BOOL)displayQuantity showRate:(BOOL)displayRate showSubtotalsOnly: (BOOL)subtotals showTotalsOnly:(BOOL) totalsOnly;

//+ (NSArray*) getManagedData:(id)productObj forName:(BOOL)forname;
+ (NSArray*) getManagedData:(id)productObj forName:(BOOL)forname showQuantity:(BOOL)showQuantity showRate:(BOOL)showRate showSubtotalsOnly:(BOOL)subtotals showTotalsOnly:(BOOL)totals;

@property(nonatomic,assign) BOOL isInvoice;
@property(nonatomic,assign) BOOL isSecondPage;

@end
