//
//  PDFRenderer.m
//  
//
//


#import "PDFRenderer.h"
#import "EstimateItem.h"
#import "CompanySetting.h"

@implementation PDFRenderer

+ (void) drawPDF:(NSString*)fileName dataArray:(NSArray*)productArray withTitle:(NSString*)title withClient:(Customers*)clients withDetail:(NSArray*)detaillist header:(NSArray*)headerlist withDate:(NSString *)date type:(NSString *)estimate photoList:(NSArray *)photoList isinvoice:(BOOL)isInvoice showQuantity:(BOOL)showQuantity showRate:(BOOL)showRate showSubtotalsOnly:(BOOL)subtotals showTotalsOnly:(BOOL)totalsOnly
{
    NSString *terms = @"";
   
    
    // Create the PDF context using the default page size of 612 x 812.
    UIGraphicsBeginPDFContextToFile(fileName, CGRectZero, nil);

	CGRect frame = CGRectMake(0, -100, 660, 812);
	
	NSInteger yOrigin = 100;
	
	NSInteger currentPage = 0;
    NSInteger itemIndex = 0;
	BOOL isEstimate = ([estimate  caseInsensitiveCompare:@"ESTIMATE"] == NSOrderedSame)?YES:NO;
    
    

    NSLog(@"The product array is: %@", productArray);
    
    
    for (id obj in productArray)
    {
        // Mark the beginning of a new page.
        BOOL isCustMobile = NO;
        
        NSInteger xOrigin = 10;
        
//		NSInteger rowHeight = 80;
        NSInteger numberOfColumns = 4;
		NSInteger numberOfRows = 1;
        
		
        //-------- issue with 750 being too much if too many
		//CGFloat totalTextYCompare = (isEstimate)?650:700;
        //changed so that more items could fit on the page
        CGFloat totalTextYCompare = 750;
        
        if (productArray.count > 5) {
            totalTextYCompare = 650;
        }
        
		CGFloat newRowHeight = [self rowHeight:obj tableHeight:400];
		CGSize tableSize = CGSizeMake(660, newRowHeight * numberOfRows);
		CGFloat newYorigin = yOrigin + newRowHeight-80;
		//if ((currentPage % 6 == 0) || currentPage == 0 || ((itemIndex == productArray.count-1) && (yOrigin > totalTextYCompare)) || newYorigin > totalTextYCompare)	// start new page
    
        NSLog(@"The y origin is: %ld and the total textYcompare is: %f", (long)yOrigin, totalTextYCompare);
        
        //NSLog(@"current page is: %@", currentPage);
		if (currentPage == 0 || ((itemIndex == productArray.count-1) && (yOrigin > totalTextYCompare)) || newYorigin > totalTextYCompare)	// start new page
		{
			UIGraphicsBeginPDFPageWithInfo(frame, nil);
			yOrigin = 100;
            
            // Draw Title
            CGRect rect = CGRectMake(0.0, 50.0, 660, 50);
            
            [PDFRenderer drawTitleText:estimate inFrame:rect alignment:NSTextAlignmentCenter isBold:YES];
            
            rect.origin.y += 60.0;
            rect.origin.x = 10;
            rect.size.height = 812;
            rect.size.width = 640;
            
            //setting up the attributes - for all
            UIFont *font = [UIFont boldSystemFontOfSize:14];
            
            NSMutableParagraphStyle *paragraphStyleLeft = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            //truncating
            paragraphStyleLeft.lineBreakMode = NSLineBreakByWordWrapping;
            paragraphStyleLeft.alignment = NSTextAlignmentLeft;
            
            NSDictionary *attributesLeft = @{ NSFontAttributeName: font,
                                          NSParagraphStyleAttributeName: paragraphStyleLeft };
            
            NSLog(@"current page is: %ld", (long)currentPage);
            NSLog(@"used text2, which contains client info");
            NSString* text2 = [NSString stringWithFormat:@"%@ %@ \n%@\n%@ %@\n%@ %@ %@",clients.customer_firstname, clients.customer_lastname, clients.customer_company, clients.customer_addres1, clients.customer_addres2, clients.customer_city,clients.customer_state,clients.customer_zip];
            
            //CGSize size1 = [text2 drawInRect:rect withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentLeft];
            
//            NSLog(@"the size height is: %f and the width is: %f", size1.height, size1.width);
//            NSLog(@"The rect height is: %f and the text width is: %f", rect.size.height, rect.size.width);
            
            
//            CGSize size1;
//            size1.height = rect.size.height;
//            size1.width = rect.size.width;
//            
           [text2 drawInRect:rect withAttributes:attributesLeft];
            CGSize size1 = [text2 sizeWithAttributes:attributesLeft];
            
            NSLog(@"the size height is: %f and the width is: %f", size1.height, size1.width);
            NSLog(@"The rect height is: %f and the text width is: %f", rect.size.height, rect.size.width);
            
            
            NSString* email = @"";
            NSArray* companSettingData = [_gAppData getCompanyData];
            if (companSettingData.count > 0)
            {
                CompanySetting* obj = [companSettingData objectAtIndex:0];
                
                UIImage* image = [UIUtils getimageFromDocumentDirectoryWithName:obj.company_logo];
                if(image)
                    [self drawImage:image inRect:CGRectMake(rect.origin.x + rect.size.width - 200, 20, 200, 80) proportionally:YES];
                
                
                terms = obj.company_terms;
                text2 = [NSString stringWithFormat:@"%@\n%@\n%@ %@ %@\n%@",obj.company_name, obj.company_address, obj.company_city,obj.company_state,obj.company_zip,obj.company_country];
                
                UIFont *font = [UIFont boldSystemFontOfSize:14];
                
                NSMutableParagraphStyle *paragraphStyleRight = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
                //truncating
                paragraphStyleRight.lineBreakMode = NSLineBreakByWordWrapping;
                paragraphStyleRight.alignment = NSTextAlignmentRight;
                NSDictionary *attributesRight = @{ NSFontAttributeName: font,
                                              NSParagraphStyleAttributeName: paragraphStyleRight };
                
                [text2 drawInRect:rect withAttributes:attributesRight];
                //[text2 drawInRect:rect withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentRight];
                
                if (obj.company_mobilePhone.length > 0)
                {
                    rect.origin.y += size1.height+24;
                    text2 = [NSString stringWithFormat:@"Mobile: %@ ",obj.company_mobilePhone];
                    //size1 = [text2 drawInRect:rect withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentRight];
                    [text2 drawInRect:rect withAttributes:attributesRight];
                    size1 = [text2 sizeWithAttributes:attributesRight];
                    
                    
                    isCustMobile = YES;
                }
                email = obj.company_email;
            }
            
            if (clients.customer_mobilePhone.length > 0)
            {
                if (!isCustMobile)
                    rect.origin.y += size1.height+10;
                
                text2 = [NSString stringWithFormat:@"Mobile: %@ ",clients.customer_mobilePhone];
                //size1 = [text2 drawInRect:rect withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentLeft];
                
                [text2 drawInRect:rect withAttributes:attributesLeft];
                size1 = [text2 sizeWithAttributes:attributesLeft];
            }
            if (email.length > 0)
            {
                rect.origin.y += size1.height;
                text2 = [NSString stringWithFormat:@"Email: %@ ",email];
                //size1 = [text2 drawInRect:rect withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentRight];
                
                UIFont *font = [UIFont boldSystemFontOfSize:14];
                
                NSMutableParagraphStyle *paragraphStyleRight = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
                //truncating
                paragraphStyleRight.lineBreakMode = NSLineBreakByWordWrapping;
                paragraphStyleRight.alignment = NSTextAlignmentRight;
                NSDictionary *attributesRight = @{ NSFontAttributeName: font,
                                                   NSParagraphStyleAttributeName: paragraphStyleRight };
                
                
                [text2 drawInRect:rect withAttributes:attributesRight];
                size1 = [text2 sizeWithAttributes:attributesRight];
            }
            //allignment left -----------
            NSMutableParagraphStyle *paragraphStyleRight = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            //truncating
            paragraphStyleRight.lineBreakMode = NSLineBreakByWordWrapping;
            paragraphStyleRight.alignment = NSTextAlignmentRight;
            NSDictionary *attributesRight = @{ NSFontAttributeName: font,
                                               NSParagraphStyleAttributeName: paragraphStyleRight };
            
            
            rect.origin.y+=+size1.height+10;
            //size1 = [title drawInRect:rect withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentRight];
            [title drawInRect:rect withAttributes:attributesRight];
            size1 = [title sizeWithAttributes:attributesRight];
            
            rect.origin.y+=+size1.height;
            //size1 = [[NSString stringWithFormat:@"Date %@", date] drawInRect:rect withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentRight];
            
            [[NSString stringWithFormat:@"Date %@", date] drawInRect:rect withAttributes:attributesRight];
            size1 = [[NSString stringWithFormat:@"Date %@", date] sizeWithAttributes:attributesRight];
            
            yOrigin = rect.origin.y+size1.height+20;
            
            [self drawTableDataAt:CGPointMake(xOrigin, yOrigin) tableSize:CGSizeMake(tableSize.width, 80) andRowCount:numberOfRows andColumnCount:numberOfColumns andProduct:obj isAddFirstLine:YES showQuantity:showQuantity showRate:showRate showSubtotalsOnly:subtotals showTotalsOnly:totalsOnly];
            yOrigin += 80-40;
			
			CGFloat lTableSize = 0;
			for (NSInteger jj=itemIndex; jj<productArray.count; jj++)
			{
				CGFloat lNewRowHeight = [self rowHeight:[productArray objectAtIndex:jj] tableHeight:(frame.size.height - yOrigin - (frame.size.height - totalTextYCompare))];
				CGFloat lNewYorigin = lTableSize + lNewRowHeight;
				
                //changed from 400, so that it could reach the bottom of the page
				if ( lNewYorigin <= 500)
					lTableSize+=lNewRowHeight;
				else
					break;
			}
			
            for (NSInteger j=0; j<4; j++)
            {
                /*NSInteger totalCount = (NSInteger)productArray.count-(itemIndex);

                NSInteger rowHeight = 80*5;
                if (totalCount < 5)
                    rowHeight = 80*totalCount;*/

                CGRect frame = [self getRectOnIndex:j xCord:10.0 yCord:yOrigin height:lTableSize];
                [PDFRenderer drawRoundRect:frame cornerRadius:0 mode:kCGPathStroke];
            }
            currentPage += 1;
		}
        
        //end of IF STATEMENT
        
        NSLog(@"the table size is: %f", tableSize.height);

        
		// Draw table data - individual item
        [self drawTableDataAt:CGPointMake(xOrigin, yOrigin) tableSize:tableSize andRowCount:numberOfRows andColumnCount:numberOfColumns andProduct:obj isAddFirstLine:NO showQuantity:showQuantity showRate:showRate showSubtotalsOnly:subtotals showTotalsOnly:totalsOnly];
        
        NSLog(@"draw table data at, table height is is: %f, and the table width is: %f, number of rows is: %ld, number of colums are: %ld", tableSize.height, tableSize.width, (long)numberOfRows, (long)numberOfColumns);
        
        NSLog(@"THe object is: %@", obj);
        NSLog(@"===== the current page is: %ld", (long)currentPage);
        
		yOrigin += tableSize.height;
		currentPage += 1;
        itemIndex   += 1;
        
        // to draw total at last.
        if (itemIndex ==  productArray.count)
        {
			CGFloat yOrgH = yOrigin+10;
            
            //setting up the attributes - for all
            UIFont *font = [UIFont boldSystemFontOfSize:14];
            
            NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            //trunkcating
            paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
            paragraphStyle.alignment = NSTextAlignmentRight;
            
            NSDictionary *attributes = @{ NSFontAttributeName: font,
                                                  NSParagraphStyleAttributeName: paragraphStyle };
            
            // be able to toggle the totals of the estimate or proposal
            if (totalsOnly)
            {
                for (NSInteger i = 0; i< headerlist.count; i++)
                {
                    NSString* deatilTxt = [NSString stringWithFormat:@"%@",[detaillist objectAtIndex:i]];
                    NSString* headertxt = [NSString stringWithFormat:@"%@:",[headerlist objectAtIndex:i]];
                    if (i == headerlist.count -1)
                    {
                        
                        //[@"(Accepted)" drawInRect:CGRectMake(0, yOrgH, 100, 812) withAttributes:attributes];
                        //[@"(Accepted)" drawInRect:CGRectMake(0, yOrgH, 100, 812) withFont:[UIFont boldSystemFontOfSize:16] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentRight];
                    }
                    
                    [deatilTxt drawAtPoint:CGPointMake(540, yOrgH) withAttributes:attributes];
                    [headertxt drawAtPoint:CGPointMake(420, yOrgH) withAttributes:attributes];
                    
                    //				yOrgH = yOrgH+headsize.height+5;
                    yOrgH = yOrgH + 25;
                    
                }
            }

            
            // start
            
            UIFont *newFont = [UIFont boldSystemFontOfSize:14];
            
            NSMutableParagraphStyle *newParagraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            //truncating
            paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
            paragraphStyle.alignment = NSTextAlignmentLeft;
            
            NSDictionary *newAttributes = @{ NSFontAttributeName: newFont,
                                          NSParagraphStyleAttributeName: newParagraphStyle,
                                          };
            
                    yOrgH = yOrigin+140;

            NSString* signature = @"    Signature ______________________________________________     Date ______________";
            NSString* print =     @"    Print Full Name _________________________________________     Date ______________";
            

            [signature drawAtPoint:CGPointMake(0, yOrgH) withAttributes:newAttributes];
            
            [print drawAtPoint:CGPointMake(0, yOrgH + 30) withAttributes:newAttributes];
            
                    //[headertxt drawInRect:CGRectMake(0, yOrgH, 550, 912) withAttributes:attributes];
            
                    //?
                    //yOrgH = yOrgH+print.height+5;
            
            //end
            
            //figuring out
            NSLog(@"header text is: %@ and the detail text is: %@", headerlist, detaillist);
            
            NSNumber* termsOn = [NSNumber numberWithBool:_gAppPrefData.isIncludeTermsOn];
            
            if([termsOn integerValue] > 0 && !isInvoice)
            {
                UIGraphicsBeginPDFPageWithInfo(frame, nil);
            
                //[@"Terms and Conditions:" drawInRect:CGRectMake(20, 100, 450, 0) withFont:[UIFont boldSystemFontOfSize:20] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentLeft];
            
                //terms = terms.length == 0 ? kTerms : terms;
                
                //[terms drawInRect:CGRectMake(20, 140, 450, 800) withFont:[UIFont systemFontOfSize:14] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentLeft];
                

                //setting up terms and conditions to the PDF
                font = [UIFont systemFontOfSize:12];
            
                paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
                paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
                paragraphStyle.alignment = NSTextAlignmentLeft;
                
                NSDictionary *TermsAndConditionsAttributes = @{ NSFontAttributeName: font,
                                              NSParagraphStyleAttributeName: paragraphStyle };
            
            
            
                [terms drawInRect:CGRectMake(20, 140, 550, 800) withAttributes:TermsAndConditionsAttributes];
                
                //terms = terms.length == 0 ? kTerms : terms;
            
            
                //Setting up Terms and Conditions title on PDF
            
                UIFont *titleFont = [UIFont systemFontOfSize:28];
                NSDictionary *TermsAndConditionsTitleAttributes = @{ NSFontAttributeName: titleFont,
                                                            NSParagraphStyleAttributeName: paragraphStyle };
                
                [@"Terms and Conditions" drawInRect:CGRectMake(20, 40, 550, 50) withAttributes:TermsAndConditionsTitleAttributes];
            }
            
        }
    }
		// Close the PDF context and write the contents out.

    for (NSString* imagePath in photoList)
    {
        UIGraphicsBeginPDFPageWithInfo(frame, nil);
        
        NSString* path = [NSString stringWithFormat:@"%@",imagePath];
        NSString* filePath = [UIUtils pathForDocumentNamed:path];
        UIImage* image = [UIImage imageWithContentsOfFile:filePath];
        if (image)
            [self drawImage:[UIImage imageWithContentsOfFile:filePath] inRect:CGRectMake(10, 10, 640, 772) proportionally:YES];
        
    }
    UIGraphicsEndPDFContext();
}

+ (void) drawRoundRect:(CGRect) frame cornerRadius:(CGFloat) radius mode:(CGPathDrawingMode) mode
{
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGFloat minx = CGRectGetMinX(frame), midx = CGRectGetMidX(frame), maxx = CGRectGetMaxX(frame);
	CGFloat miny = CGRectGetMinY(frame), midy = CGRectGetMidY(frame), maxy = CGRectGetMaxY(frame);
	
	CGContextMoveToPoint(context, minx, midy);							// Start at 1
	CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);	// Add an arc through 2 to 3
	CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);	// Add an arc through 4 to 5
	CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);	// Add an arc through 6 to 7
	CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);	// Add an arc through 8 to 9
	CGContextClosePath(context);										// Close the path
	
	CGContextDrawPath(context, mode);							// Fill & stroke the path
}

+ (void) drawLineFromPoint:(CGPoint)from toPoint:(CGPoint)to
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2.0);
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = {(20.0f/255.0f),(130.0f/255.0f),(20.0f/255.0f), 1.0};
    
    CGColorRef color = CGColorCreate(colorspace, components);
    
    CGContextSetStrokeColorWithColor(context, color);
    
    
    CGContextMoveToPoint(context, from.x, from.y);
    CGContextAddLineToPoint(context, to.x, to.y);
    
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
    
}

+ (void) drawTitleText:(NSString*)textToDraw inFrame:(CGRect)frameRect alignment:(NSTextAlignment)alignment isBold:(BOOL)makeBold
{
	[[UIColor grayColor] set];
    
	//CGRect textRect = CGRectInset(frameRect, 10, 4);
	
	//[[UIColor grayColor] set];
	
	if ([textToDraw rangeOfString:@"http"].length > 0)
    {
		//[[UIColor blueColor] set];
        
		CGRect linkFrame = frameRect;
		linkFrame.origin.y = 812 - linkFrame.origin.y - linkFrame.size.height;
        
        NSURL* url = [NSURL URLWithString:[textToDraw stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        UIGraphicsSetPDFContextURLForRect(url, linkFrame);
        
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    //truncating
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = alignment;
    
	UIFont* font = (makeBold)?[UIFont boldSystemFontOfSize:20.0]:[UIFont systemFontOfSize:20.0];
    
    [textToDraw drawInRect:frameRect withAttributes:@{NSFontAttributeName: font,
                                            NSParagraphStyleAttributeName: paragraphStyle}];

    [[UIColor blackColor] set];
}

+ (void) drawText:(NSString*)textToDraw inFrame:(CGRect)frameRect alignment:(NSTextAlignment)alignment isBold:(BOOL)makeBold
{
	[[UIColor grayColor] set];

	CGRect textRect = CGRectInset(frameRect, 10, 4);
	
	[[UIColor blackColor] set];
	
	if ([textToDraw rangeOfString:@"http"].length > 0)
    {
		//[[UIColor blueColor] set];

		CGRect linkFrame = frameRect;
		linkFrame.origin.y = 812 - linkFrame.origin.y - linkFrame.size.height;

        NSURL* url = [NSURL URLWithString:[textToDraw stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        UIGraphicsSetPDFContextURLForRect(url, linkFrame);
        
    }

	UIFont* font = (makeBold)?[UIFont boldSystemFontOfSize:12.0]:[UIFont systemFontOfSize:12.0];
	//[textToDraw drawInRect:textRect withFont:font lineBreakMode:NSLineBreakByTruncatingTail alignment:alignment];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    //truncating
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = alignment;
    
    NSDictionary *attributes = @{ NSFontAttributeName: font,
                                       NSParagraphStyleAttributeName: paragraphStyle };
    
    
    [textToDraw drawInRect:textRect withAttributes:attributes];
    
}

+ (CGRect) drawImage:(UIImage*) image inRect:(CGRect) rect proportionally:(BOOL)proportionally
{
	assert(image);
	
	if (proportionally)
	{
		CGSize sz = rect.size;
		CGSize imgSz = image.size;
		
		CGFloat dx = sz.width / imgSz.width;
		CGFloat dy = sz.height / imgSz.height;
		
		CGFloat minScale = MIN(dx, dy);
		sz.width = imgSz.width * minScale;
		sz.height = imgSz.height * minScale;
		
		rect.origin.x += (rect.size.width - sz.width) * 0.5 + ((rect.size.width - sz.width)* 0.5);
		rect.origin.y += (rect.size.height - sz.height) * 0.5;
		
		rect.size = sz;
	}
	
	[image drawInRect:rect];
	return rect;
}

+ (CGFloat)rowHeight:(id)productObj tableHeight:(CGFloat)height
{
    //height = height + 200;
    
    NSLog(@"TABLE HEIGHT ============= is: %f", height);
	NSString* productDesc =[NSString stringWithFormat:@"%@",[productObj item_desc]];
	//CGSize newSize = [productDesc sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(330-12, height) lineBreakMode:NSLineBreakByWordWrapping];
    
    

    
    
    UIFont* font = [UIFont systemFontOfSize:10.0];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{ NSFontAttributeName: font,
                                  NSParagraphStyleAttributeName: paragraphStyle };

    CGSize newSize = [productDesc sizeWithAttributes:attributes];
////    [productDesc boundingRectWithSize:newSize options:<#(NSStringDrawingOptions)#> attributes:attributes context:<#(NSStringDrawingContext *)#>];
//    
    
    CGSize maximumLabelSize = CGSizeMake(330-12, height);
    [productDesc boundingRectWithSize:maximumLabelSize
                                             options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                          attributes:attributes
                                             context:nil];
    
    
	if (newSize.height > 60)
		return newSize.height+20+10;
	else
		return 80;
}

+ (void) drawTableDataAt:(CGPoint)origin
           tableSize:(CGSize)size
             andRowCount:(NSInteger)numberOfRows
          andColumnCount:(NSInteger)numberOfColumns
              andProduct:(id)productObj
          isAddFirstLine:(BOOL)isFirstLine
            showQuantity:(BOOL)showQuantity
                showRate:(BOOL)showRate
       showSubtotalsOnly:(BOOL)subtotals
          showTotalsOnly:(BOOL)totals
{
    NSLog(@"draw table data at width is: %f, and the height is: %f", size.width, size.height);
    NSLog(@"Number of rows are: %ld and the number of columns are: %ld", (long)numberOfRows, (long)numberOfColumns);
    
    NSArray* nameArray = [self getManagedData:productObj forName:YES showQuantity:showQuantity showRate:showRate showSubtotalsOnly:subtotals showTotalsOnly:totals];
    NSArray* valueArray = [self getManagedData:productObj forName:NO showQuantity:showQuantity showRate:showRate showSubtotalsOnly:subtotals showTotalsOnly:totals];

    if (nameArray.count < 1 || valueArray.count < 1)
		return;
    
	NSInteger rowHeight = (size.height /  numberOfRows);
	//NSInteger fisrtColWidth = size.width/4;//(size.width * 22) / 100;
    if (isFirstLine)
    {
        for (NSInteger j = 0; j < nameArray.count; j++)
        {
            /*NSInteger newOriginX = origin.x + ((j) * fisrtColWidth);
            CGRect frame = CGRectMake(newOriginX, origin.y, fisrtColWidth, rowHeight-40);*/
            
            CGRect frame = [self getRectOnIndex:j xCord:origin.x yCord:origin.y height:rowHeight-40];
            if (j == 0 )
                [self drawText:[nameArray objectAtIndex:j] inFrame:frame alignment:NSTextAlignmentLeft isBold:YES];
            else if (j == 3 )
                [self drawText:[nameArray objectAtIndex:j] inFrame:frame alignment:NSTextAlignmentRight isBold:YES];
            else
                [self drawText:[nameArray objectAtIndex:j] inFrame:frame alignment:NSTextAlignmentCenter isBold:YES];

            [PDFRenderer drawRoundRect:frame cornerRadius:0 mode:kCGPathStroke];
        }
    }
    else
    {
        for (NSInteger j = 0; j < valueArray.count; j++)
        {
            /*NSInteger newOriginX = origin.x + ((j) * fisrtColWidth);
            CGRect frame = CGRectMake(newOriginX, origin.y, fisrtColWidth, rowHeight);*/
            
            CGRect frame = [self getRectOnIndex:j xCord:origin.x yCord:origin.y height:rowHeight];
            if (j == 0 )
            {
                [self drawText:[valueArray objectAtIndex:j] inFrame:frame alignment:NSTextAlignmentLeft isBold:NO];
                
                frame.origin.y +=20.0;
                frame.size.height-=20.0;
                NSString* productDesc =[NSString stringWithFormat:@"%@",[productObj item_desc]];
                [self drawText:productDesc inFrame:frame alignment:NSTextAlignmentLeft isBold:NO];
            }
            else if (j == 3 )
                [self drawText:[valueArray objectAtIndex:j] inFrame:frame alignment:NSTextAlignmentRight isBold:NO];
            else
                [self drawText:[valueArray objectAtIndex:j] inFrame:frame alignment:NSTextAlignmentCenter isBold:NO];
        }
    }
}

//all data is managed here for the table
+ (NSArray*) getManagedData:(id)productObj forName:(BOOL)forname showQuantity:(BOOL)showQuantity showRate:(BOOL)showRate showSubtotalsOnly:(BOOL)subtotals showTotalsOnly:(BOOL)totals
{
    NSMutableArray* nameArray = [[NSMutableArray alloc] init];
    NSMutableArray* valueArray = [[NSMutableArray alloc] init];
    
    NSString* productDesc =[NSString stringWithFormat:@"%@",[productObj item_title]]; ;

    NSString* productQuant;
    NSString* productRate;
    NSString* productTotal;
    
//    if (totals) {
//        productQuant = @"";
//        productRate = @"";
//        productTotal = @"";
//    }
//        else
//        {
            if (showQuantity) {
                productQuant = [NSString stringWithFormat:@"%@", [productObj item_quantity]];
            } else {
                productQuant = @"";
            }
            
            
            if (showRate) {
                productRate = [NSString stringWithFormat:@"%.02f", [[productObj item_rate] floatValue]];
            } else{
                productRate = @"";
            }
    
            
            
            if (subtotals) {
                productTotal = [NSString stringWithFormat:@"%.02f",[[productObj item_amount] floatValue]];
            }else{
                productTotal = @"";
            }
       // }

    [nameArray addObject:@"Description"];
    [nameArray addObject:@"Quantity"];
    [nameArray addObject:@"Price"];
    [nameArray addObject:@"Total"];

    [valueArray addObject:(productDesc.length>0)?productDesc:@""];
    [valueArray addObject:(productQuant.length>0)?productQuant:@""];
    [valueArray addObject:(productRate.length>0)?productRate:@""];
    [valueArray addObject:(productTotal.length>0)?productTotal:@""];

    return forname ? nameArray : valueArray;
}

+ (CGRect)getRectOnIndex:(NSUInteger)index xCord:(CGFloat)xCord yCord:(CGFloat)yCord height:(CGFloat)height
{
    //320+80+120+140
    CGRect rect = CGRectZero;
    switch (index)
    {
        case 0:
        {
            rect = CGRectMake(xCord, yCord, 330.0, height);
            break;
        }
        case 1:
        {
            rect = CGRectMake(xCord+330, yCord, 80.0, height);
            break;
        }
        case 2:
        {
            rect = CGRectMake(xCord+330+80, yCord, 110.0, height);
            break;
        }
        case 3:
        {
            rect = CGRectMake(xCord+330+80+110, yCord, 120.0, height);
            break;
        }
        default:
            break;
    }
    return rect;
}

@end
