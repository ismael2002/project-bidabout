//
//  BaseViewController.h
//  BidAbout
//
//  Created by Ratnesh Singh on 16/10/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@end
