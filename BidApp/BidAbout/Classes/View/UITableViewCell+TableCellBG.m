//
//  UITableViewCell+TableCellBG.m
//  BidAbout
//
//  Created by Waseem Ahmad on 01/10/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "UITableViewCell+TableCellBG.h"

@implementation UITableViewCell (TableCellBG)

-  (id)init
{
	if (self = [super init])
	{
		self.backgroundColor = [UIColor clearColor];
	}
	return self;
	
}
@end
