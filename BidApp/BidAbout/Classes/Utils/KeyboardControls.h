//
//  KeyboardControls.m
//  KeyBoardProject
//
//  Copyright (c) 2012 Min. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KeyboardControlsDelegate;

typedef enum 
{
    KeyboardControlsDirectionPrevious,
    KeyboardControlsDirectionNext
} KeyboardControlsDirection;

@interface KeyboardControls : UIView
{
    UINavigationBar* _navBar;
    UISegmentedControl* _segmentedPreviousNext;
    UIBarButtonItem* _buttonDone;
    UINavigationItem* _navItem;
}

@property (nonatomic, retain) id <KeyboardControlsDelegate> delegate;

/* The text fields the KeyboardControls will handle */
@property (nonatomic, retain) NSArray *textFieldsArray;

/* The currently active text field */
@property (nonatomic, retain) id activeTextField;

@property (nonatomic, assign) UIBarStyle barStyle;
@property (nonatomic, retain) UIColor* previousNextTintColor;
@property (nonatomic, retain) UIColor* doneTintColor;
@property (nonatomic, retain) NSString* previousTitle;
@property (nonatomic, retain) NSString* nextTitle;
@property (nonatomic, retain) NSString* barTitle;


@end

@protocol KeyboardControlsDelegate <NSObject>
@required
/* Called when the user presses either the "Previous" or the "Next" button */
- (void)keyboardControlsPreviousNextPressed:(KeyboardControls *)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField;

/* Called when the user pressed the "Done" button */
- (void)keyboardControlsDonePressed:(KeyboardControls *)controls;

@end
