//
//  UIUtils.m
//  ValsList
//
//  Created by Waseem on 02/01/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIUtils.h"
#import "SecureUDID.h"
#import "SWRevealViewController.h"
#import "SBJson.h"
#include <sys/xattr.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <netinet/in.h>

@implementation UIUtils

#pragma --
#pragma Connectivity

#pragma --
#pragma Messagel Alert

+ (void) messageAlert:(NSString*)msg title:(NSString*)title delegate:(id)delegate
{
	NSString* localizedOkay = NSLocalizedString(@"OK", nil);
    
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message: msg
												   delegate: delegate cancelButtonTitle: localizedOkay otherButtonTitles: nil];
	[alert show];
}

+ (void) messageAlert:(NSString*)msg title:(NSString*)title delegate:(id)delegate withTag:(NSInteger) tag
{
	NSString* localizedOkay = NSLocalizedString(@"OK", nil);
    
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message: msg
												   delegate: delegate cancelButtonTitle: localizedOkay otherButtonTitles: nil];
    [alert setTag:tag];
	[alert show];
}


+ (UIAlertView*) messageAlertWithOkCancel:(NSString*)msg title:(NSString*)title delegate:(id)delegate
{
	NSString* localizedCancel = NSLocalizedString(@"Cancel", nil);
	NSString* localizedOkay = NSLocalizedString(@"OK", nil);
	
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message: msg
												   delegate: delegate cancelButtonTitle: localizedCancel otherButtonTitles:localizedOkay, nil];
	[alert show];
	return alert;
}

+ (UIAlertView*) messageAlertWithOkCancel:(NSString*)msg title:(NSString*)title delegate:(id)delegate withTag:(NSInteger)tag
{
	NSString* localizedCancel = NSLocalizedString(@"Cancel", nil);
	NSString* localizedOkay = NSLocalizedString(@"OK", nil);
	
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message: msg
												   delegate: delegate cancelButtonTitle: localizedCancel otherButtonTitles:localizedOkay, nil];
    [alert setTag:tag];
	[alert show];
	return alert;
}
#pragma mark -

+ (BOOL) validateForEmptyString: (NSString *)inputString andFieldName: (NSString *)theFieldName
{
    BOOL isValid = NO;
    if([inputString length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: kApplicationName
                                                        message: [NSString stringWithFormat:@"%@ %@", theFieldName ,@"field can not be blank!"]
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        isValid = YES;
        
    }
    return isValid;
}

+ (BOOL)setFormatForPhoneNumber:(UITextField*)phoneNoTextField OfLength:(NSInteger)phoneNumberLength
{
	if(phoneNumberLength > phoneNoTextField.text.length)
	{
		switch (phoneNoTextField.text.length)
		{
			case 3:
				phoneNoTextField.text = [phoneNoTextField.text stringByAppendingString:@"-"];
				break;
			case 7:
				phoneNoTextField.text = [phoneNoTextField.text stringByAppendingString:@"-"];
				break;
			default:
				break;
		}
        return YES;
	}
	else
        return NO;
}

+ (BOOL) validateEmail: (NSString *)theEmailAddress
{
    BOOL isValid = NO;
    
    if(![self validateForEmptyString: theEmailAddress andFieldName:@"Email"])
    {
        //isValid = YES;
        NSString *email = theEmailAddress;
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        //Valid email address
        if ([emailPredicate evaluateWithObject:email] == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @""
                                                            message: [NSString stringWithFormat:@"Please enter a valid email address and try again"]
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            isValid = YES;
        }
    }
    else
    {
        isValid = YES;
    }
    return isValid;
}

+ (BOOL) isPhoneNumbervalid:(NSString*)phoneNumber
{
    phoneNumber = [phoneNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phoneText = [phoneNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    if (phoneText.length != 10)
    {
        [UIUtils messageAlert:@"Mobile number must be of 10 digits. Please provide a valid mobile number." title:@"" delegate:nil];
        
        return TRUE;
    }
    
    NSString* phoneRegex = @"^[2-9]{1}[0-9]{9}$";
    NSPredicate* phoneFormat = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    if([phoneFormat evaluateWithObject:phoneText]==NO)
    {
        [UIUtils messageAlert:@"Please provide a valid mobile number." title:@"" delegate:nil];
        return TRUE;
    }
    
    return  FALSE;
}

+ (NSString*) checkNil:(NSString*)string
{
    return (string == nil) ? @"" : string;
}

+ (NSString*) checknilAndWhiteSpaceinString:(NSString*)string
{
    return (string == nil || string.length <1)?@"":[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (BOOL) isiPhone
{
	return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

+ (BOOL) isiPhone5
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f)
        return YES;
    
    return NO;
}

+ (BOOL) isiPhone6
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if (screenHeight == 1080) {
        return YES;
    }
    return NO;
}

+ (NSString*) iPhone5ImageName:(NSString*)imageName
{
    NSMutableString* imageNameMutable = [NSMutableString stringWithString:imageName];
    NSRange retinaAtSymbol = [imageName rangeOfString:@"@"];
    if (retinaAtSymbol.location != NSNotFound)
    {
        [imageNameMutable insertString:@"-568h" atIndex:retinaAtSymbol.location];
    }
    else
    {
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        if ([UIScreen mainScreen].scale == 2.f && screenHeight == 568.0f)
        {
            NSRange dot = [imageName rangeOfString:@"."];
            if (dot.location != NSNotFound)
            {
                [imageNameMutable insertString:@"-568h@2x" atIndex:dot.location];
            }
            else
            {
                [imageNameMutable appendString:@"-568h@2x"];
            }
        }
    }
    return [NSString stringWithString:imageNameMutable];
}

#pragma mark -

+ (NSDictionary*) plistItem:(NSString*)name
{
	NSString* path = [[NSBundle mainBundle] pathForResource:name ofType:nil];
	return [NSDictionary dictionaryWithContentsOfFile:path];
}

#pragma mark -

+ (UIImage*) cropImage:(UIImage*)inImage ofSize:(CGSize)inSize
{
	if (inImage)
	{
		CGRect thumbRect = CGRectMake(0, 0, inSize.width, inSize.height);
		UIGraphicsBeginImageContext(inSize);
		[inImage drawInRect:thumbRect];
		UIImage *thumbImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		return thumbImage;
	}
	else
		return nil;
}

+ (CGRect) drawImage:(UIImage*) image inRect:(CGRect) rect proportionally:(BOOL)proportionally
{
	if (image == nil)
		return CGRectZero;
	
	if (proportionally)
	{
		CGSize sz = rect.size;
		CGSize imgSz = image.size;
		
		CGFloat dx = sz.width / imgSz.width;
		CGFloat dy = sz.height / imgSz.height;
		
		CGFloat minScale = MIN(dx, dy);
		sz.width = imgSz.width * minScale;
		sz.height = imgSz.height * minScale;
		
		rect.origin.x += (rect.size.width - sz.width) * 0.5;
		rect.origin.y += (rect.size.height - sz.height) * 0.5;
		
		rect.size = sz;
	}
	
	[image drawInRect:rect];
	return rect;
}

+ (void) drawRoundRect:(CGRect) frame cornerRadius:(CGFloat) radius mode:(CGPathDrawingMode) mode
{
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGFloat minx = CGRectGetMinX(frame), midx = CGRectGetMidX(frame), maxx = CGRectGetMaxX(frame);
	CGFloat miny = CGRectGetMinY(frame), midy = CGRectGetMidY(frame), maxy = CGRectGetMaxY(frame);
	
	CGContextMoveToPoint(context, minx, midy);							// Start at 1
	CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);	// Add an arc through 2 to 3
	CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);	// Add an arc through 4 to 5
	CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);	// Add an arc through 6 to 7
	CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);	// Add an arc through 8 to 9
	CGContextClosePath(context);										// Close the path
	
	CGContextDrawPath(context, mode);							// Fill & stroke the path
}

+ (NSString*) documentDirectoryWithSubpath:(NSString*)subpath
{
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	if (paths.count <= 0)
		return nil;
	
	NSString* dirPath = [paths objectAtIndex:0];
	if (subpath)
		dirPath = [dirPath stringByAppendingFormat:@"/%@", subpath];
	
	return dirPath;
}


+ (UIImage*) giveSnapShotOfView:(UIView*) view
{
	if (view == nil)
		return nil;
	
	UIGraphicsBeginImageContext(view.bounds.size);
	UIImage* bgImaage = [UIImage imageNamed:@""];
	[bgImaage drawAtPoint:CGPointMake(0, 0)];
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

+ (BOOL) isConnectedToNetwork
{
	// Create zero addy
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	
	// synchronous model
    
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	
	if (!didRetrieveFlags)
	{
		NSLog(@"Error. Could not recover network reachability flags\n");
		return 0;
	}
	
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
	//return (isReachable && !needsConnection) ? YES : NO;
	//BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
	
	return (isReachable && !needsConnection);
}

+ (NSString*) getUDID;
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *defaultRODeviceSecureUDID = [def stringForKey:@"RustOleumSecureUDID"];
    if(defaultRODeviceSecureUDID == nil) {
        NSString *domain     = [NSBundle mainBundle].bundleIdentifier;
		//        NSString *key        = [self getLastRunDateString];
        NSString *key        = @"PAINTPRO";
        defaultRODeviceSecureUDID = [SecureUDID UDIDForDomain:domain usingKey:key];
        [def setObject:defaultRODeviceSecureUDID forKey:@"RustOleumSecureUDID"];
        [def synchronize];
    }
	return defaultRODeviceSecureUDID;
}

+ (id) jsonData:(NSData*)data
{
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    SBJsonParser* jsonParser = [SBJsonParser new];
    NSError* error = nil;
    id resp = [jsonParser objectWithString:jsonString];
    if (error)
    {
        return nil;
    }
    return resp;
}

+ (id) createJSONToPost:(NSDictionary*)dict
{
    SBJsonWriter* jsonWritter = [SBJsonWriter new];
    NSData* jsondata = [jsonWritter dataWithObject:dict];
    return jsondata;
}


// This method is used to add reveal button as left bar button on navigation bar.

+ (UIButton*) getRevelButtonItem:(UIViewController*)viewController
{
    SWRevealViewController *revealController = [viewController revealViewController];
    [viewController.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIImage* image3 = [UIImage imageNamed:@"btn-menu.png"];
    CGRect frameimg = CGRectMake(0, 0, image3.size.width, image3.size.height);
    UIButton* barButton = [[UIButton alloc] initWithFrame:frameimg];
    [barButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [barButton addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [barButton setShowsTouchWhenHighlighted:YES];
    
    return barButton;
}

+ (UIColor*) getTintColor
{
    UIColor *tintColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    return tintColor;
}

+ (NSString*) getCurrencyValue:(NSString*)value
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
    [numberFormatter setLocale:locale];
    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[value doubleValue]]];
    
    return numberAsString;
}

+ (NSString*) documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+ (NSString *)pathForDocumentNamed:(NSString*)name
{
    return [NSString stringWithFormat:@"%@/%@", [self documentsDirectory], name];
}

+ (void) createDirectoryAtPath:(NSString*)path
{
	NSFileManager* fileMgr = [NSFileManager defaultManager];
	[fileMgr createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (BOOL) saveData:(NSData*)data path:(NSString*)filePath
{
    NSString* folderPath = [filePath stringByDeletingLastPathComponent];
    [self createDirectoryAtPath:folderPath];
    return [data writeToFile:filePath atomically:NO];
}

+ (void) deleteItemAtPath:(NSString*)filePath
{
    NSFileManager* fileMgr = [NSFileManager defaultManager];
    [fileMgr removeItemAtPath:filePath error:nil];
}

+ (UIImage*) getimageFromDocumentDirectoryWithName:(NSString*)imagename
{
    NSString* path = [self pathForDocumentNamed:imagename];
    UIImage *cellImage =[UIImage imageWithContentsOfFile:path];
    return cellImage;
}
@end
