//
//  UIUtils.h
//  ValsList
//
//  Created by Waseem on 02/01/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIUtils : NSObject

// Message Alert Function
+ (void) messageAlert:(NSString*)msg title:(NSString*)title delegate:(id)delegate;
+ (void) messageAlert:(NSString*)msg title:(NSString*)title delegate:(id)delegate withTag:(NSInteger) tag;
+ (UIAlertView*) messageAlertWithOkCancel:(NSString*)msg title:(NSString*)title delegate:(id)delegate;
+ (UIAlertView*) messageAlertWithOkCancel:(NSString*)msg title:(NSString*)title delegate:(id)delegate withTag:(NSInteger)tag;

+ (BOOL) validateForEmptyString: (NSString *)inputString andFieldName: (NSString *)theFieldName;
+ (BOOL) validateEmail: (NSString *)theEmailAddress;
+ (BOOL) isPhoneNumbervalid:(NSString*)phoneNumber;
+ (BOOL)setFormatForPhoneNumber:(UITextField*)phoneNoTextField OfLength:(NSInteger)phoneNumberLength;
+ (NSString*) checkNil:(NSString*)string;
+ (NSString*) checknilAndWhiteSpaceinString:(NSString*)string;
+ (BOOL) isiPhone;
+ (BOOL) isiPhone5;
+ (BOOL) isiPhone6;

+ (NSString*) iPhone5ImageName:(NSString*)imageName;

+ (NSDictionary*) plistItem:(NSString*)name;

+ (UIImage*) cropImage:(UIImage*)inImage ofSize:(CGSize)inSize;
+ (CGRect) drawImage:(UIImage*) image inRect:(CGRect) rect proportionally:(BOOL)proportionally;

+ (void) drawRoundRect:(CGRect) frame cornerRadius:(CGFloat) radius mode:(CGPathDrawingMode) mode;
+ (NSString*) documentDirectoryWithSubpath:(NSString*)subpath;

+ (UIImage*) giveSnapShotOfView:(UIView*) view;

+ (NSString*) getUDID;
+ (BOOL) isConnectedToNetwork;

+ (id) jsonData:(NSData*)data;
+ (id) createJSONToPost:(NSDictionary*)dict;

+ (UIButton*) getRevelButtonItem:(UIViewController*)viewController;
+ (UIColor*) getTintColor;
+ (NSString*) getCurrencyValue:(NSString*)value;

+ (NSString *)pathForDocumentNamed:(NSString*)name;
+ (BOOL) saveData:(NSData*)data path:(NSString*)filePath;
+ (void) deleteItemAtPath:(NSString*)filePath;
+ (UIImage*) getimageFromDocumentDirectoryWithName:(NSString*)imagename;

@end
