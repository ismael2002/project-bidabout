//
//  WebServiceHelper.h
//  Rust_Oleum
//
//  Created by   on 01/11/12.
//  Copyright (c) 2012  . All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    WS_REQUEST_NONE = 100,
    WS_REQUEST_GET_REGISTRATION,
    WS_REQUEST_GET_LOGIN,
    WS_REQUEST_GET_LOGIN_CONTEXT,
    WS_REQUEST_GET_MEMBERS,
    WS_REQUEST_GET_CUSTOMERS,
    WS_REQUEST_GET_CUSTOMERSWITHID,
    WS_REQUEST_GET_TEMPLATETYPE,
    WS_REQUEST_GET_TEMPLATES,
    WS_REQUEST_GET_TEMPLATESWITHID,
    WS_REQUEST_GET_QUOTES,
    WS_REQUEST_POST_CUSTOMER,
} WEB_SERVICE_REQUEST_TYPE;

typedef enum
{
    WEB_SERVICE_STATUS_NONE=0,
    WEB_SERVICE_STATUS_SUCCESS,
    WEB_SERVICE_STATUS_FAIL
}WEB_SERVICE_OPERATION_STATUS;

@protocol WebServiceHelperDelegate

- (void) getResponseFromServer: (NSString*) responseStr
                   requestType: (WEB_SERVICE_REQUEST_TYPE) theRequestType
                 requestStatus: (WEB_SERVICE_OPERATION_STATUS) theRequestStatus;
@end


@interface WebServiceHelper : NSObject

@property (nonatomic, assign) WEB_SERVICE_REQUEST_TYPE       requestType;
@property (nonatomic, assign) WEB_SERVICE_OPERATION_STATUS  requestStatus;

@property (nonatomic, retain) NSMutableData*                responseData;
@property (nonatomic, retain) NSURLConnection*              activeConnection;

@property (nonatomic, assign) id<WebServiceHelperDelegate> delegate;

- (BOOL) setUpConnectionForRequest:(NSURLRequest*)request;
- (void) getAppRegistrionResponce:(NSData*)dataForPostBody;
- (void) getLogInResponce;
- (void) getLogInServiceContext;
- (void) getMemberListResponse;
- (void) getCustomerListResponse;
- (void) getCustomerListWithID:(NSString *)memberId;
- (void) getTemplateTypesResponse;
- (void) getTemplatesResponse;
- (void) getTemplatesWith:(NSString *)memberId andTemplate:(NSInteger)templateId;
- (void) getQuotes;

- (void) postNewCustomerData:(NSData*)dataForPost;

@end
