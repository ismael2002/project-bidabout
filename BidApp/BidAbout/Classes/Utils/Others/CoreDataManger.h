//
//  CoreDataManger.h
//
//  Created by Bolder Image 
//  Copyright (c) 2013 Bolder Image Ahmad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customers;
@class Invoice;
@class Estimate;
@class Item;
@class Templates;
@class InvoiceItem;

@interface CoreDataManger : NSObject
{
	NSManagedObjectModel*           _managedObjectModel;
	NSManagedObjectContext*         _managedObjectContext;
	NSPersistentStoreCoordinator*	_persistentStoreCoordinator;
    
    NSMutableArray*         _freeKeys;
    NSInteger               _tracker;
    NSInteger               _primaryKey;
    NSMutableArray*         _listOfItemIDs;
}

@property(nonatomic, readonly, retain) NSPersistentStoreCoordinator*    persistentStoreCoordinator;
@property(nonatomic, readonly, retain) NSManagedObjectModel*    managedObjectModel;
@property(nonatomic, readonly, retain) NSManagedObjectContext*  managedObjectContext;


- (void) clearDatabase;

- (NSArray*) getItems;
- (void) insertNewItem:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary;
- (void) insertNewEstimateItem:(NSDictionary*)itemDict;
- (void) replaceEstimateItem:(NSDictionary*)itemDict withNewEstimateID: (NSNumber*) estimateID;
- (void) updatePaypalUserName:(NSString*)userName emailProvider:(NSString*) email domain:(NSString*)domain;
- (void) insertNewItemFromTempalte:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary newTemplateItem: (BOOL) isNewtemplate;
- (void) insertNewInvoiceItem:(NSDictionary*)itemDict;
- (void) insertNewEstimate:(NSDictionary*)estimateDict replaceEstimateID: (BOOL) replaceOldID;
- (void) insertNewInvoice:(NSDictionary*)invoiceDict replaceTemplateID: (BOOL) replaceOldID;
- (void) estimateAcceptedFromParse: (PFObject*) estimate;
- (void) addUpdateCompanySettingData:(NSDictionary*)companySettingDict;
- (void) insertNewTemplate:(NSDictionary*) templateDict replaceTemplateID: (BOOL) replaceOldID newTemplateID: (NSNumber*) newID;
- (void) addNewInvoiceItem:(NSDictionary*) itemDict withNewInvoiceID: (NSNumber*) invoiceID;
- (void) addInvoiceItemToProductList: (NSDictionary*) itemDict;

- (void) changeTermsAndConditions: (NSString*) terms;
- (void) updateTemplateItem: (id) oldTemplateItem withUpdateTemplateItem: (NSMutableDictionary*) updatedTemplate;
- (void) updateCustomerWithID:(NSDictionary*)custDict;
- (void) updateProductwithItem:(NSDictionary*)itemDict;
- (void) updateEstimateItemwithDict:(NSDictionary*)itemDict;
- (void) updateInvoiceItemWithDict:(NSDictionary*)itemDict;
- (void) changeActiveInactiveTempalte: (NSNumber*) template_ID;
- (void) convertEstimateToInvoice: (NSDictionary*) estimateDictionary withItems: (NSArray*) itemArray;

- (void) deleteInvoiceItemsFromInvoice:(NSNumber*) invoiceID;
- (void) deleteProductItemWithID:(NSString*)itemID;
- (void) deleteEstimateItemWithID:(NSString*)itemID andTitle: (NSString*) title;
- (void) deleteEstimate:(NSString*)estimateID;
- (void) deleteInvoiceItem:(InvoiceItem*)itemID;
- (void) deleteInvoice:(Invoice*)invoiceObject;
- (void) deleteCompanyData;
- (void) deleteObejctFromClientList:(Customers*) customerObject;
- (void) deleteItemsFromCurrentTemplate;
- (void) deleteItemFromTemplate: (NSNumber*) templateID ItemTitle: (NSString*) itemTitle;
- (void) deleteItemsFromCurrentEstimate;
- (void) deleteItemsFromCurrentInvoice;

- (NSArray*) getFilteredItemResults;
- (NSArray*) getEstimateItemsFromCurrentEstimate;
- (NSArray*) getEstimateItemsWithEstimateID: (NSNumber*) estimateID;
- (NSArray*) getInvoiceItemsFromCurrentInvoice;
- (NSArray*) getInvoiceItemsWithInvoiceID: (NSNumber*) invoiceID;
- (double) getTaxWithInvoiceID: (NSNumber*) invoiceID;
- (NSDictionary*) getPaypalInformation;

- (void) storeCustomerDetail:(NSArray*)customerList;

- (NSArray*) getCustomerdetailsWithQuotes:(BOOL)isQuoteYes andID:(NSString*)customerId;
- (NSArray*) getTemplatedetails;
- (NSArray*) getItemWithId:(NSString*)itemID;
- (NSArray*) getEstimateItemWithId:(NSString*)itemID;
- (NSArray*) getInvoiceItemWithId:(NSString*)itemID;
- (NSArray*) getEstimatelistWith_ID:(NSString*)estimateID;
- (NSArray*) getEstimatelistWith_ClientID:(NSString*)clientID;
- (NSArray*) getInvoielistWith_ClientID:(NSString*)clientID;
- (NSArray*) getItemsWithCurrentTemplate;
- (NSArray*) getItemsFromExistingTemplate: (NSNumber*) templateID;
- (NSArray*) getInvoicelistWith_ID:(NSString*)invoiceId;
- (NSArray*) getCompanySettingData;
- (NSArray*) getItemResults;
- (NSArray*) getInvoiceWithID: (NSString*) invoiceID;
- (BOOL) isCustomerAlreadyExist:(NSString*)customerId;

- (void) storeTemplateFromLocalDB:(NSArray*)templateList;

@end