//
//  WebServiceHelper.m
//  Rust_Oleum
//
//  Created by   on 01/11/12.
//  Copyright (c) 2012  . All rights reserved.
//

#import "WebServiceHelper.h"
#import "BidAboutAppDelegate.h"

@implementation WebServiceHelper

@synthesize requestStatus       = _requestStatus;
@synthesize requestType         = _requestType;
@synthesize delegate            = _delegate;
@synthesize responseData        = _responseData;
@synthesize activeConnection    = _activeConnection;


- (BOOL) setUpConnectionForRequest:(NSURLRequest*)request
{
    if (![UIUtils isConnectedToNetwork])
    {
        [UIUtils messageAlert:kNetworkErrorMsg title:kNetworkErroTitle delegate:nil];
        return FALSE;
    }
    
    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection)
    {
        self.activeConnection = connection;
        
        [_gAppDelegate showLoadingView:YES];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        return TRUE;
    }

    self.activeConnection = nil;
    return FALSE;
}

#pragma mark - Url Connection Delegates

- (void) connection:(NSURLConnection*) connection didReceiveResponse:(NSURLResponse*) response
{
	[_responseData setLength: 0];
}

- (void) connection:(NSURLConnection*) connection didReceiveData:(NSData*) data
{
	[_responseData appendData:data];
}

- (void) connection:(NSURLConnection*) connection didFailWithError:(NSError*) error
{
    self.activeConnection = nil;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [_gAppDelegate showLoadingView:NO];
    
    [UIUtils messageAlert:error.description title:kApplicationName delegate:nil];
}

- (void) connectionDidFinishLoading:(NSURLConnection*) connection
{
    _requestStatus = WEB_SERVICE_STATUS_SUCCESS;
    
	NSString* XMLstring = [[NSString alloc]
						   initWithBytes: [_responseData mutableBytes]
						   length:[_responseData length]
						   encoding:NSUTF8StringEncoding];
    [_delegate getResponseFromServer:XMLstring requestType:_requestType requestStatus:_requestStatus];
	
	NSLog(@"RESPONSE : %@", XMLstring);

    self.activeConnection = nil;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [_gAppDelegate showLoadingView:NO];
}

#pragma mark --
#pragma mark App LogIn service

- (void) getLogInResponce
{
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@",kTestOuthServerUrl];
    _requestType = WS_REQUEST_GET_LOGIN;
	[request setURL:[NSURL URLWithString:urlStr]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
	[request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[self getLogInBody] dataUsingEncoding:NSUTF8StringEncoding]];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}

#pragma mark --
#pragma mark App Registration service

- (void) getAppRegistrionResponce:(NSData*)dataForPostBody
{
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@/%@",kTestRestServerUrl,@"RegisterUser"];
    _requestType = WS_REQUEST_GET_REGISTRATION;
	[request setURL:[NSURL URLWithString:urlStr]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:dataForPostBody];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}

#pragma mark --
#pragma mark Get member service

- (void) getLogInServiceContext
{
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@%@",kTestRestServerUrl,@"GetServiceContext"];
    _requestType = WS_REQUEST_GET_LOGIN_CONTEXT;
	[request setURL:[NSURL URLWithString:urlStr]];
    [request setValue:[self getAuthToken] forHTTPHeaderField:@"Authorization"];
	[request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPMethod:@"GET"];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}

#pragma mark --
#pragma mark Get member service

- (void) getMemberListResponse
{
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@/%@",kTestRestServerUrl,@"Members"];
    _requestType = WS_REQUEST_GET_MEMBERS;
	[request setURL:[NSURL URLWithString:urlStr]];
    
    [request setValue:[self getAuthToken] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  
    
	[request setHTTPMethod:@"GET"];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}

#pragma mark --
#pragma mark CustomerList service

- (BOOL) checkNetworkConnection
{
    if (![UIUtils isConnectedToNetwork])
    {
        [UIUtils messageAlert:kNetworkErrorMsg title:kNetworkErroTitle delegate:nil];
        return NO;
    }
    return YES;
}

- (NSURLRequest*) getRequestWithUrlStr:(NSString*)urlString
{
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:urlString]];
    [request setValue:[self getAuthToken] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
	
	[request setHTTPMethod:@"GET"];
    return request;
}

- (void) getCustomerListResponse
{
    if ([self checkNetworkConnection] == NO)
        return;
    
    [_gAppDelegate showLoadingView:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@%@",kTestRestServerUrl,@"Customers"];
    NSURLRequest* request = [self getRequestWithUrlStr:urlStr];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse* response, NSData* responseData, NSError* error)
     {
         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
         NSUInteger responseStatusCode = [httpResponse statusCode];

         WEB_SERVICE_REQUEST_TYPE requestType = WS_REQUEST_GET_CUSTOMERS;

         [_gAppDelegate showLoadingView:NO];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

         if (responseStatusCode != 200 || error)
         {             
             [UIUtils messageAlert:@"Connection Failure." title:kApplicationName delegate:nil];
         }
         else
         {
             WEB_SERVICE_OPERATION_STATUS requestStatus = WEB_SERVICE_STATUS_SUCCESS;

             NSString* XMLstring = [[NSString alloc] initWithData:responseData
                                                         encoding:NSUTF8StringEncoding];
                                    
             [_delegate getResponseFromServer:XMLstring requestType:requestType requestStatus:requestStatus];
         }
     }];
}

#pragma mark --
#pragma mark CustomerList with ID service

- (void) getCustomerListWithID:(NSString *)memberId
{
    
    if ([self checkNetworkConnection] == NO)
        return;
    
    [_gAppDelegate showLoadingView:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@Customers?MemberId=%@",kTestRestServerUrl,memberId];
    NSURLRequest* request = [self getRequestWithUrlStr:urlStr];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse* response, NSData* responseData, NSError* error)
     {
         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
         NSUInteger responseStatusCode = [httpResponse statusCode];
         
         WEB_SERVICE_REQUEST_TYPE requestType = WS_REQUEST_GET_CUSTOMERSWITHID;
         
         [_gAppDelegate showLoadingView:NO];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         if (responseStatusCode != 200 || error)
         {
             [UIUtils messageAlert:@"Connection Failure." title:kApplicationName delegate:nil];
         }
         else
         {
             WEB_SERVICE_OPERATION_STATUS requestStatus = WEB_SERVICE_STATUS_SUCCESS;
             
             NSString* XMLstring = [[NSString alloc] initWithData:responseData
                                                         encoding:NSUTF8StringEncoding];
             
             [_delegate getResponseFromServer:XMLstring requestType:requestType requestStatus:requestStatus];
         }
     }];
}

#pragma mark --
#pragma mark TemplateTypes service

- (void) getTemplateTypesResponse
{
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@/TemplateTypes",kTestRestServerUrl];
    _requestType = WS_REQUEST_GET_TEMPLATETYPE;
	[request setURL:[NSURL URLWithString:urlStr]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"OAuth" forHTTPHeaderField:@"Authorization"];
	[request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];

	[request setHTTPMethod:@"GET"];
    [request setHTTPBody:[[self getLogInBody] dataUsingEncoding:NSUTF8StringEncoding]];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}

#pragma mark --
#pragma mark Templates service

- (void) getTemplatesResponse
{
    if ([self checkNetworkConnection] == NO)
        return;
    
    [_gAppDelegate showLoadingView:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@/Templates",kTestRestServerUrl];
    NSURLRequest* request = [self getRequestWithUrlStr:urlStr];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse* response, NSData* responseData, NSError* error)
     {
         WEB_SERVICE_REQUEST_TYPE requestType = WS_REQUEST_GET_TEMPLATES;

         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
         NSUInteger responseStatusCode = [httpResponse statusCode];

         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [_gAppDelegate showLoadingView:NO];
         
         if (responseStatusCode != 200 || error)
         {
             [UIUtils messageAlert:@"Connection Failure." title:kApplicationName delegate:nil];
         }
         else
         {
             WEB_SERVICE_OPERATION_STATUS requestStatus = WEB_SERVICE_STATUS_SUCCESS;
             
             NSString* XMLstring = [[NSString alloc] initWithData:responseData
                                                         encoding:NSUTF8StringEncoding];
             [_delegate getResponseFromServer:XMLstring requestType:requestType requestStatus:requestStatus];
         }
     }];
}

#pragma mark --
#pragma mark Templates with member service

- (void) getTemplatesWith:(NSString *)memberId andTemplate:(NSInteger)templateId
{
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@/Templates?MemberId=%@&TemplateTypeId=%ld",kTestRestServerUrl,memberId,(long)templateId];
    
    _requestType = WS_REQUEST_GET_TEMPLATESWITHID;
	[request setURL:[NSURL URLWithString:urlStr]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"OAuth" forHTTPHeaderField:@"Authorization"];
	[request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
    
	[request setHTTPMethod:@"GET"];
    [request setHTTPBody:[[self getLogInBody] dataUsingEncoding:NSUTF8StringEncoding]];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}

#pragma mark --
#pragma mark get Quote service

- (void) getQuotes
{
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
	//http://services.bidabout.com/rest/Quotes?MemberId=5&CustomerId=1&TemplateId=1&Templat eType=1
	
	//NSString* urlStr = [NSString stringWithFormat:@"%@/Quotes",kTestRestServerUrl];
	NSString* urlStr = [NSString stringWithFormat:@"%@Quotes?MemberId=%@&CustomerId=1&TemplateId=1&TemplateType=1",kTestRestServerUrl, _gAppPrefData.memberID];
    
    _requestType = WS_REQUEST_GET_QUOTES;
	[request setURL:[NSURL URLWithString:urlStr]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[self getAuthToken] forHTTPHeaderField:@"Authorization"];
	[request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
    
	[request setHTTPMethod:@"GET"];
  //  [request setHTTPBody:[[self getLogInBody] dataUsingEncoding:NSUTF8StringEncoding]];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}


#pragma mark --
#pragma mark Post Customer Service

- (void) postNewCustomerData:(NSData*)dataForPost
{
    
    //@^@ Working
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    
    // BUILD URL STRING BY APPENDING URL PARTS
    NSString* urlStr = [NSString stringWithFormat:@"%@%@",kTestRestServerUrl,@"customer/0"];
    
    _requestType = WS_REQUEST_POST_CUSTOMER;
	[request setURL:[NSURL URLWithString:urlStr]];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"SmartDevice Application" forHTTPHeaderField:@"Genexus-Agent"];
    [request setValue:[self getAuthToken] forHTTPHeaderField:@"Authorization"];
    
	[request setHTTPMethod:@"POST"];
    [request setHTTPBody:dataForPost];
    if ([self setUpConnectionForRequest:request])
        _responseData = [[NSMutableData alloc]init];
}

#pragma mark --
#pragma mark SetBody method

- (NSString*) setRegistrationBody
{
    return nil;
}

- (NSString*) getLogInBody
{
    NSString* grantType = @"password";
    NSString* scope = @"FullControl";

    NSString* httpBody = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=%@&scope=%@&username=%@&password=%@",kClientID,kClientSecrect,grantType, scope,_gAppPrefData.emailId,_gAppPrefData.password ];

    return httpBody;
}

- (NSString*) getAuthToken
{
    NSString* authTokenStr = [NSString stringWithFormat:@"OAuth %@",_gAppPrefData.accesToken ];
    return authTokenStr;
}

@end
