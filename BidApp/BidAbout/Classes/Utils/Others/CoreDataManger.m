//
//  CoreDataManger.m
//
//  Created by Bolder Image
//  Copyright (c) 2013 Bolder Image Ahmad. All rights reserved.
//

#import "UIUtils.h"
#import "Customers.h"
#import "Templates.h"
#import "Estimate.h"
#import "Item.h"
#import "EstimateItem.h"
#import "CompanySetting.h"
#import "InvoiceItem.h"
#import "Invoice.h"
#import "CoreDataManger.h"
#import "PaypalSettings.h"

@implementation CoreDataManger

- (id) init
{
    if(self = [super init])
    {
        [self managedObjectContext];
        
        _primaryKey = 0;
        _tracker = [[[NSUserDefaults standardUserDefaults] valueForKey:@"Tracker"] integerValue];
        _freeKeys = [[NSMutableArray alloc] init];
        _listOfItemIDs = [[NSMutableArray alloc] init];
    }
    return self;
}

/*
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */

- (NSManagedObjectContext*) managedObjectContext
{
    if (_managedObjectContext)
        return _managedObjectContext;
    NSPersistentStoreCoordinator* coordinator = self.persistentStoreCoordinator;
    if (coordinator)
    {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */

- (NSManagedObjectModel*) managedObjectModel
{
    if (_managedObjectModel)
        return _managedObjectModel;
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */

- (NSPersistentStoreCoordinator*) persistentStoreCoordinator
{
    if (_persistentStoreCoordinator)
        return _persistentStoreCoordinator;
    
    NSURL* storeUrl = [NSURL fileURLWithPath:[[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"Bid-AboutDB.sqlite"]];
    NSError* error;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

- (NSString*)applicationDocumentsDirectory
{
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory = [NSString stringWithFormat:@"%@",documentsDirectory];
    return documentsDirectory;// [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void) deleteAllObjects:(NSString*)entityDescription
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    NSArray* items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject* managedObject in items)
    {
        [_managedObjectContext deleteObject:managedObject];
        NSLog(@"%@ object deleted",entityDescription);
    }
    if (![_managedObjectContext save:&error])
    {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
}

- (void) clearDatabase
{
    //Erase the persistent store from coordinator and also file manager.
    NSPersistentStore *store = [self.persistentStoreCoordinator.persistentStores lastObject];
    NSError *error = nil;
    NSURL *storeURL = store.URL;
    [self.persistentStoreCoordinator removePersistentStore:store error:&error];
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
    
    NSLog(@"Data Reset");
    
    //Make new persistent store for future saves   (Taken From Above Answer)
    if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        [UIUtils messageAlert:error.description title:@"Error" delegate:nil];
        return;
    }
}

- (BOOL) save
{
    NSError* error = nil;
    if (_managedObjectContext != nil)
    {
        if ([_managedObjectContext hasChanges] && ![_managedObjectContext save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            return YES;
        }
    }
    return NO;
}

#pragma mark --
#pragma mark Genrate Primary key

- (NSNumber*) getPrimaryKeys
{
    if(_freeKeys.count > 0)
    {
        NSNumber* x = [_freeKeys objectAtIndex:0];
        [_freeKeys removeObjectAtIndex:0];
        return x;
    }
    else
    {
        NSNumber* keyNumber = [NSNumber numberWithUnsignedInteger:(++_tracker)];
        [[NSUserDefaults standardUserDefaults] setInteger:_tracker forKey:@"Tracker"];
        
        return keyNumber;
    }
}

- (void) addTofreekeys:(NSNumber*)unusedKey
{
    [_freeKeys addObject:unusedKey];
}

#pragma Mark --
#pragma Mark Add and Save Methods

-(void) changeTermsAndConditions: (NSString*) terms
{
    NSLog(@"Starting to change terms and conditions");
    
    NSMutableDictionary* oldTermsAndConditions = [NSMutableDictionary new];
    BOOL includeTerms;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CompanySetting"];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    NSLog(@"Checking results");
    if (results.count > 0)
    {
        NSLog(@"Results greater than 0");
        CompanySetting* companySettings = results[0];
        
        NSLog(@"The company settings are: %@, terms are: %@", companySettings, terms);
        
        if (terms.length < 1)
        {
            terms = @"";
        }
        
        includeTerms = companySettings.include_terms;
        NSDictionary *oldTerms = @{
                                       @"company_address" : companySettings.company_address,
                                       @"company_city" : companySettings.company_city,
                                       @"company_country" : companySettings.company_country,
                                       @"company_email" : companySettings.company_email,
                                       @"company_logo" : companySettings.company_logo,
                                       @"company_mobilePhone" : companySettings.company_mobilePhone,
                                       @"company_name" : companySettings.company_name,
                                       @"company_state" : companySettings.company_state,
                                       @"company_tax" : companySettings.company_tax,
                                       @"company_terms" : terms,
                                       @"company_zip" : companySettings.company_zip,
                                       };
        
        [oldTermsAndConditions addEntriesFromDictionary:oldTerms];
        
    }
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }

    NSLog(@"setting up new company settings");
    CompanySetting* companySettingsObj = [NSEntityDescription
                              insertNewObjectForEntityForName:@"CompanySetting"
                              inManagedObjectContext:_managedObjectContext];
    
    companySettingsObj.company_address = [oldTermsAndConditions objectForKey:@"company_address"];
    companySettingsObj.company_city = [oldTermsAndConditions objectForKey:@"company_city"];
    companySettingsObj.company_country = [oldTermsAndConditions objectForKey:@"company_country"];
    
    companySettingsObj.company_email = [oldTermsAndConditions objectForKey:@"company_email"];
    companySettingsObj.company_logo = [oldTermsAndConditions objectForKey:@"company_logo"];
    companySettingsObj.company_mobilePhone = [oldTermsAndConditions objectForKey:@"company_mobilePhone"];
    
    companySettingsObj.company_name = [oldTermsAndConditions objectForKey:@"company_name"];
    companySettingsObj.company_state = [oldTermsAndConditions objectForKey:@"company_state"];
    companySettingsObj.company_tax = [oldTermsAndConditions objectForKey:@"company_tax"];
    
    companySettingsObj.company_terms = [oldTermsAndConditions objectForKey:@"company_terms"];
    companySettingsObj.company_zip = [oldTermsAndConditions objectForKey:@"company_zip"];
    companySettingsObj.include_terms = includeTerms;
    
    [self save];
    
    NSLog(@"The company settings object that is created is: %@", companySettingsObj);
    NSLog(@"the company settings dictionary are: %@", oldTermsAndConditions);
    
}

- (void) insertNewItem:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary
{
    NSDictionary* itemDict = [itemDictCopy copy];
    NSLog(@"The item dictionary is: %@", itemDict);
    
    Item* itemobj = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Item"
                     inManagedObjectContext:_managedObjectContext];

    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    //NSNumber* templateID = [itemDict objectForKey:@"templateId"];
    
    //invoiceObj.invoice_id = [self getPrimaryKeys];
    itemobj.item_id = [itemDict objectForKey:@"ItemID"];
    itemobj.item_desc = (itemDesc) ? itemDesc:@"";
    itemobj.item_title = titleStr ? titleStr:@"";
    itemobj.item_quantity = (qtyStr) ? qtyStr:@"0";
    
    //itemobj.item_quantity = [NSString stringWithFormat:@"%d", [[itemDict objectForKey:@"Quantity"] integerValue]];
    itemobj.item_rate = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Rate"] doubleValue]];
    itemobj.item_amount = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Amount"] doubleValue]];
    itemobj.item_taxAllow = (taxAllow) ? taxAllow: [NSNumber numberWithInteger:0];
    
    NSNumber* tempId;
    
    //if (itemobj.item_id.length < 1)
    if ([itemobj.item_teplateID integerValue] > 1)
    {
        NSLog(@"less than one"); // even though adding a template item, its going to this one
        itemobj.item_id = [[self getPrimaryKeys] stringValue];
    }
    else {
        //if ([itemDict objectForKey:@"templateId"]) //so that products could be updated
        if ([itemobj.item_teplateID integerValue] > 1)
        {
            NSLog(@"changed template id");
            tempId = [NSNumber numberWithInteger:888888888];
            itemobj.item_teplateID = tempId;
        }else
        {
            tempId = [NSNumber numberWithInteger:0];
            itemobj.item_teplateID = tempId;
            
            //if this item is replacing an old item, this will delete the old item
            NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
            
            NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", oldItemDictionary.item_teplateID];
            NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", oldItemDictionary.item_title];
            NSPredicate *notePredicate = [NSPredicate predicateWithFormat:@"item_desc == %@", oldItemDictionary.item_desc];
            NSPredicate *taxPredicate = [NSPredicate predicateWithFormat:@"item_taxAllow == %@", oldItemDictionary.item_taxAllow];
            NSPredicate *ratePredicate = [NSPredicate predicateWithFormat:@"item_rate == %@", oldItemDictionary.item_rate];
            
            NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, notePredicate, taxPredicate, ratePredicate, nil]];
            request.predicate = compoundPredicate;
            
            NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
            for (NSManagedObject* managedObject in results)
            {
                [_managedObjectContext deleteObject:managedObject];
            }
        }
    }
    [self save];
}

- (void) insertNewItemFromTempalte:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary newTemplateItem: (BOOL) isNewtemplate
{
    
    NSDictionary* itemDict = [itemDictCopy copy];
    NSLog(@"The item dictionary is: %@", itemDict);
    
    Item* itemobj = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Item"
                     inManagedObjectContext:_managedObjectContext];
    
    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    NSNumber* templateID = [itemDict objectForKey:@"templateId"];
    
    //invoiceObj.invoice_id = [self getPrimaryKeys];
    itemobj.item_id = [itemDict objectForKey:@"ItemID"];
    itemobj.item_desc = (itemDesc) ? itemDesc:@"";
    itemobj.item_title = titleStr ? titleStr:@"";
    itemobj.item_quantity = (qtyStr) ? qtyStr:@"0";
    itemobj.item_rate = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Rate"] doubleValue]];
    itemobj.item_amount = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Amount"] doubleValue]];
    itemobj.item_taxAllow = (taxAllow) ? taxAllow: [NSNumber numberWithInteger:0];
    itemobj.item_id = [[self getPrimaryKeys] stringValue];
    
    NSNumber* tempId;
    
    if (isNewtemplate)
    {
        tempId = [NSNumber numberWithInteger:888888888];
        itemobj.item_teplateID = tempId;
    } else {
        itemobj.item_teplateID = templateID;
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
        
        NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", oldItemDictionary.item_teplateID];
        NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", oldItemDictionary.item_title];
        NSPredicate *notePredicate = [NSPredicate predicateWithFormat:@"item_desc == %@", oldItemDictionary.item_desc];
        NSPredicate *taxPredicate = [NSPredicate predicateWithFormat:@"item_taxAllow == %@", oldItemDictionary.item_taxAllow];
        NSPredicate *ratePredicate = [NSPredicate predicateWithFormat:@"item_rate == %@", oldItemDictionary.item_rate];
        
        NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, notePredicate, taxPredicate, ratePredicate, nil]];
        request.predicate = compoundPredicate;
        
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
        for (NSManagedObject* managedObject in results)
        {
            [_managedObjectContext deleteObject:managedObject];
        }
    }
    
    NSLog(@"The item created is: %@", itemobj);

    [self save];
}

-(NSArray*) getItemsWithCurrentTemplate
{
    NSNumber* tempId;
    
    tempId = [NSNumber numberWithInteger:888888888];

    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"item_title" ascending:YES]];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
}

-(NSArray*) getEstimateItemsFromCurrentEstimate
{
    NSNumber* tempId = [NSNumber numberWithInteger:999999999];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"EstimateItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"item_title" ascending:YES]];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];

    return results;
}

- (NSArray*) getEstimateItemsWithEstimateID: (NSNumber*) estimateID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"EstimateItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", estimateID];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"item_title" ascending:YES]];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
}

-(NSArray*) getInvoiceItemsFromCurrentInvoice
{
    NSNumber* tempId = [NSNumber numberWithInteger:777777777];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"InvoiceItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];

    return results;
}

-(NSArray*) getItemsFromExistingTemplate: (NSNumber*) templateID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", templateID];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"item_title" ascending:YES]];
    
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
    
}

- (void) deleteItemsFromCurrentTemplate
{
    NSNumber* tempId;
    
    tempId = [NSNumber numberWithInteger:888888888];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }

    [self save];
}

- (void) deleteItemFromTemplate: (NSNumber*) templateID ItemTitle: (NSString*) itemTitle
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    
    //request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    //request.predicate = [NSPredicate predicateWithFormat:@"item_title == %@", itemTitle];
    
    NSLog(@"The template id used is: %@ and the item title used is: %@", templateID, itemTitle);
    
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", templateID];
    NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", itemTitle];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, nil]];
    
    request.predicate = compoundPredicate;
    
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    NSLog(@"The item from the template that was deleted is: %@", results);
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
}

- (void) deleteItemsFromCurrentEstimate
{
    NSNumber* tempId;
    
    tempId = [NSNumber numberWithInteger:999999999];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"EstimateItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    [self save];
}

- (void) deleteItemsFromCurrentInvoice
{
    NSNumber* tempId;
    
    tempId = [NSNumber numberWithInteger:777777777];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"InvoiceItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    [self save];
}


- (NSArray*) getActiveTemplateIDs
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Templates"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"template_name" ascending:YES]];
    
    NSArray* allTemplates = [_managedObjectContext executeFetchRequest:request error:nil];
    NSMutableArray* activeTemplates = [[NSMutableArray alloc] init];
    NSNumber* templateID = 0;
    
    for (NSInteger x = 0; x < allTemplates.count; x++)
    {
        Templates* template = allTemplates[x];
        NSNumber* isActive = template.active;
        
        if ([isActive integerValue] == 0)
        {
            templateID = template.template_id;
            [activeTemplates addObject:templateID];
        }
    }
    return activeTemplates;
}

-(NSArray*) getFilteredItemResults
{
    NSArray* allItems = [self getItemResults];
    NSArray* listOfTemplateIDs = [self getActiveTemplateIDs];
    NSMutableArray* filteredItems = [[NSMutableArray alloc] init];
    
    for (NSInteger x = 0; x < allItems.count; x++)
    {
        Item* item = allItems[x];
        NSNumber *templateID = item.item_teplateID;
        
        if ([templateID integerValue] > 0)
        {
            
            for (NSInteger y = 0; y < listOfTemplateIDs.count; y++)
            {
                NSNumber* checkingTemplateID = listOfTemplateIDs[y];
                
                if ([templateID integerValue] == [checkingTemplateID integerValue])
                {
                    [filteredItems addObject:item];
                }
            }
        } else{
            [filteredItems addObject:item];
            }
    }
    return filteredItems;
}


-(NSArray*) getItemResults
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"item_title" ascending:YES]];
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
}

- (NSArray*) getItems
{
    return _listOfItemIDs;
}

-(NSDictionary*) getPaypalInformation
{
    PaypalSettings* settings;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Paypal"];
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    if (results.count > 0)
    {
        settings = results[0];
        
        NSDictionary *settingsResults = @{
                                          @"userName" : settings.userName,
                                          @"emailService" : settings.emailService,
                                          @"domain" : settings.domain,
                                          };
        return settingsResults;
    } else
    {
        NSDictionary *settingsResults = @{
                                          @"userName" : @"",
                                          @"emailService" : @"",
                                          @"domain" : @"",
                                          };
        return settingsResults;
    }
}

// need to update the settings for paypal.
- (void) updatePaypalUserName:(NSString*)userName emailProvider:(NSString*) email domain:(NSString*)domain
{
    // deleting previous info
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Paypal"];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
    
    
    PaypalSettings* itemobj = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Paypal"
                            inManagedObjectContext:_managedObjectContext];
    
    itemobj.userName = userName;
    itemobj.emailService = email;
    itemobj.domain = domain;
    
    [self save];
}


- (void) insertNewEstimateItem:(NSDictionary*)itemDict
{
    NSLog(@"the estimate item is: %@", itemDict);
    EstimateItem* estimateItemObj = [NSEntityDescription
                                     insertNewObjectForEntityForName:@"EstimateItem"
                                     inManagedObjectContext:_managedObjectContext];
    
    NSNumber* tempId = [NSNumber numberWithInteger:999999999];
    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    

    estimateItemObj.item_teplateID = [self getPrimaryKeys];
    estimateItemObj.item_teplateID = tempId;
    estimateItemObj.item_desc = (itemDesc) ? itemDesc:@"";
    estimateItemObj.item_title = titleStr ? titleStr:@"";
    estimateItemObj.item_quantity = (qtyStr) ? qtyStr:@"0";
    estimateItemObj.item_rate = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Rate"] doubleValue]];
    estimateItemObj.item_amount = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Amount"] doubleValue]];
    estimateItemObj.item_taxAllow = (taxAllow) ? taxAllow: [NSNumber numberWithInteger:0];
    
    [self save];
}

- (void) replaceEstimateItem:(NSDictionary*)itemDict withNewEstimateID: (NSNumber*) estimateID
{
    NSLog(@"starting replace estimate item");
    NSLog(@"to replace the old item, the dictionary used is: %@", itemDict);
    
    EstimateItem* estimateItemObj = [NSEntityDescription
                                     insertNewObjectForEntityForName:@"EstimateItem"
                                     inManagedObjectContext:_managedObjectContext];
    
    NSNumber* tempId = [NSNumber numberWithInteger:[[itemDict objectForKey:@"item_tempID"]integerValue]];
    NSString* itemDesc = [itemDict objectForKey:@"item_desc"];
    NSNumber* taxAllow = [itemDict objectForKey:@"item_taxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"item_title"];
    NSString* qtyStr = [itemDict objectForKey:@"item_quantity"];
    
    estimateItemObj.item_teplateID = tempId;
    estimateItemObj.item_desc = (itemDesc) ? itemDesc:@"";
    estimateItemObj.item_title = titleStr ? titleStr:@"";
    estimateItemObj.item_quantity = (qtyStr) ? qtyStr:@"0";
    //added quantity
    estimateItemObj.item_quantity = [NSString stringWithFormat:@"%ld", (long)[[itemDict objectForKey:@"item_quantity"] integerValue]];
    estimateItemObj.item_rate = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"item_rate"] doubleValue]];
    estimateItemObj.item_amount = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"item_amount"] doubleValue]];
    estimateItemObj.item_taxAllow = (taxAllow) ? taxAllow: [NSNumber numberWithInteger:0];
    
    NSLog(@"The estimate item made is: %@", estimateItemObj);
    
    [self save];
}

//when an item is added to an invoice, this is what adds the item
- (void) insertNewInvoiceItem:(NSDictionary*)itemDict
{
    InvoiceItem* itemobj = [NSEntityDescription
                     insertNewObjectForEntityForName:@"InvoiceItem"
                     inManagedObjectContext:_managedObjectContext];
    
    //adding an invoice item to the current invoice items
    NSNumber* tempId = [NSNumber numberWithInteger:777777777];
    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    
    itemobj.item_id = [itemDict objectForKey:@"ItemID"];
    itemobj.item_teplateID = tempId;
    itemobj.item_desc = (itemDesc) ? itemDesc:@"";
    itemobj.item_title = titleStr ? titleStr:@"";
    itemobj.item_quantity = (qtyStr) ? qtyStr:@"0";
    
    itemobj.item_rate = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Rate"] doubleValue]];
    itemobj.item_amount = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Amount"] doubleValue]];
    itemobj.item_taxAllow = (taxAllow) ? taxAllow: [NSNumber numberWithInteger:0];
    
    [self save];
}

- (void) addNewInvoiceItem:(NSDictionary*) itemDict withNewInvoiceID: (NSNumber*) invoiceID
{
    InvoiceItem* itemobj = [NSEntityDescription
                            insertNewObjectForEntityForName:@"InvoiceItem"
                            inManagedObjectContext:_managedObjectContext];
    
    //adding an invoice item to the current invoice items
    
    NSString* itemDesc = [itemDict objectForKey:@"item_desc"];
    NSNumber* taxAllow = [itemDict objectForKey:@"item_taxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"item_title"];
    NSString* qtyStr = [itemDict objectForKey:@"item_quantity"];
    NSString* amount = [itemDict objectForKey:@"item_amount"];
    NSString* rate = [itemDict objectForKey:@"item_rate"];
    NSString* itemID = [itemDict objectForKey:@"item_id"];
    //NSNumber*
    
    itemobj.item_teplateID = invoiceID;
    itemobj.item_desc = itemDesc;
    itemobj.item_taxAllow = taxAllow;
    itemobj.item_title = titleStr;
    itemobj.item_quantity = qtyStr;
    itemobj.item_amount = amount;
    itemobj.item_rate = rate;
    itemobj.item_id = itemID;

    [self save];
}

//error on the pdf options
- (void) estimateAcceptedFromParse: (PFObject*) estimate
{
    NSLog(@"estimate accepted started");
    
    Estimate* localCopy;
    NSDictionary* oldEstimate;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Estimate"];
    
    

    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"estimate_ClientId == %@", [estimate objectForKey:@"clientId"]];
    NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"estimate_id == %@", [estimate objectForKey:@"estimateId"]];
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, nil]];
    
    request.predicate = compoundPredicate;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    NSLog(@"The Estimate that connects to the local estimate is: %@", results);
    
    if (results.count > 0)
    {
        Estimate* localEstimate = results[0];
        localCopy.estimate_ClientId = localEstimate.estimate_ClientId;
        localCopy.estimate_id = localEstimate.estimate_id;
        
        NSLog(@"The Estimate that connects to the local estimate is: %@", localEstimate);
        
        oldEstimate = @{
                        @"clientId" : localEstimate.estimate_ClientId,
                        @"comment" : localEstimate.estimate_comment,
                        @"date" : localEstimate.estimate_date,
                        @"direction" : localEstimate.estimate_direction,
                        @"id" : localEstimate.estimate_id,
                        @"item" : localEstimate.estimate_item,
                        @"photo" : localEstimate.estimate_photo,
                        @"tax" : localEstimate.estimate_tax,
                        };
    }
    
    NSLog(@"The local copy of the Estimate is: %@", oldEstimate);
    
    for (NSManagedObject* managedObject in results)
    {
        NSLog(@"Going to delete %@", managedObject);
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
    
    NSLog(@"The local copy of the Estimate is: %@", oldEstimate);
    
    
    if (results.count > 0)
    {
    
    Estimate* estimateObj = [NSEntityDescription
                             insertNewObjectForEntityForName:@"Estimate"
                             inManagedObjectContext:_managedObjectContext];
    
    if ([oldEstimate objectForKey:@"clientId"])
    {
        estimateObj.estimate_ClientId = [oldEstimate objectForKey:@"clientId"];
    }
    
    if ([oldEstimate objectForKey:@"comment"])
    {
        estimateObj.estimate_comment = [oldEstimate objectForKey:@"comment"];
    }
    
    if ([oldEstimate objectForKey:@"date"])
    {
        estimateObj.estimate_date = [oldEstimate objectForKey:@"date"];
    }
    
    if ([oldEstimate objectForKey:@"direction"])
    {
        estimateObj.estimate_direction = [oldEstimate objectForKey:@"direction"];
    }
    
    if ([oldEstimate objectForKey:@"id"])
    {
        estimateObj.estimate_id = [oldEstimate objectForKey:@"id"];
    }
    
    if ([oldEstimate objectForKey:@"item"])
    {
        estimateObj.estimate_item = [oldEstimate objectForKey:@"item"];
    }
    
    if ([oldEstimate objectForKey:@"photo"])
    {
        estimateObj.estimate_photo = [oldEstimate objectForKey:@"photo"];
    }
    
    if ([oldEstimate objectForKey:@"tax"])
    {
        estimateObj.estimate_tax = [oldEstimate objectForKey:@"tax"];
    }
    
    estimateObj.accepted = [NSNumber numberWithInteger:1];
        
    // make sure that the pdf options are not nil
        
        if ([oldEstimate objectForKey:@"PDFQuantity"] == nil) {
            estimateObj.estimate_pdf_quantity = [NSNumber numberWithInteger:1];
        } else {
            estimateObj.estimate_pdf_quantity = [oldEstimate objectForKey:@"PDFQuantity"];
        }
        
        if ([oldEstimate objectForKey:@"PDFRate"] == nil) {
            estimateObj.estimate_pdf_rate = [NSNumber numberWithInteger:1];
        }else {
            estimateObj.estimate_pdf_rate = [oldEstimate objectForKey:@"PDFRate"];
        }
        
        if ([oldEstimate objectForKey:@"PDFSubtotal"] == nil) {
            estimateObj.estimate_pdf_subtotal = [NSNumber numberWithInteger:1];
        }else {
            estimateObj.estimate_pdf_subtotal = [oldEstimate objectForKey:@"PDFSubtotal"];
        }
        
        if ([oldEstimate objectForKey:@"PDFTotal"] == nil) {
            estimateObj.estimate_pdf_total = [NSNumber numberWithInteger:0];
        }else {
            estimateObj.estimate_pdf_total = [oldEstimate objectForKey:@"PDFTotal"];
        }
    
    [self save];
    
    NSLog(@"The estimate that saved that will replace the local estimate is: %@", estimateObj);
        
    }
}

//need to add an accepted portion
- (void) insertNewEstimate:(NSDictionary*)estimateDict replaceEstimateID: (BOOL) replaceOldID
{
    NSLog(@"c %@", estimateDict);
    Estimate* estimateObj = [NSEntityDescription
                             insertNewObjectForEntityForName:@"Estimate"
                             inManagedObjectContext:_managedObjectContext];
    
    NSString* commmntstr = [estimateDict objectForKey:@"Comment"];
    NSString* estimateDateStr = [estimateDict objectForKey:@"EstimateDate"];
    NSString* directionStr = [estimateDict objectForKey:@"Direction"];
    NSString* itemsIDStr = [estimateDict objectForKey:@"Items"];
    NSString* clientIdstr = [estimateDict objectForKey:@"Client"];
    NSString* photoList = [estimateDict objectForKey:@"PhotoList"];
    NSNumber* taxValue = [estimateDict objectForKey:@"Tax"];
    NSNumber* estimateID = [self getPrimaryKeys];
    NSNumber* oldEstimateID = [estimateDict objectForKey:@"EstimateId"];
    //need to use this to move previous items
    
    //trying not to change the estimate number
    NSNumber* estimateNumber;
    if ([estimateDict objectForKey:@"EstimateNumber"])
    {
        NSLog(@"reusing the same estimate id");
        estimateNumber = [estimateDict objectForKey:@"EstimateNumber"];
    } else {
        NSLog(@"giving a new estimate id");
        estimateNumber = estimateID;
    }
    
    NSNumber* pdfQuantity = [estimateDict objectForKey:@"PDFQuantity"];
    NSNumber* pdfRate = [estimateDict objectForKey:@"PDFRate"];
    NSNumber* pdfSubtotal = [estimateDict objectForKey:@"PDFSubtotal"];
    NSNumber* pdfTotal = [estimateDict objectForKey:@"PDFTotal"];
    
    estimateObj.estimate_number = estimateNumber;
    estimateObj.estimate_pdf_quantity = pdfQuantity;
    estimateObj.estimate_pdf_rate = pdfRate;
    estimateObj.estimate_pdf_subtotal = pdfSubtotal;
    estimateObj.estimate_pdf_total = pdfTotal;
    estimateObj.estimate_id = estimateID;
    
    //chg prevented the estimate id to change
    NSInteger estimateId  = [estimateObj.estimate_id integerValue];
    _gAppPrefData.estimatelastID = [NSNumber numberWithInteger:estimateId];
    [_gAppPrefData saveAllData];
    
    if (replaceOldID)
    {
        estimateObj.accepted = [estimateDict objectForKey:@"Accepted"];
    } else {
        estimateObj.accepted = [NSNumber numberWithInteger:2];
    }

    estimateObj.estimate_comment = (commmntstr)? commmntstr:@"";
    estimateObj.estimate_date = (estimateDateStr)? estimateDateStr:@"";
    estimateObj.estimate_direction = (directionStr)? directionStr:@"";
    estimateObj.estimate_item = (itemsIDStr)? itemsIDStr:@"";
    estimateObj.estimate_ClientId = (clientIdstr)? clientIdstr:@"";
    estimateObj.estimate_photo = (photoList.length > 0)?photoList:@"";
    estimateObj.estimate_tax = (taxValue)? taxValue:[NSNumber numberWithDouble:0.0];
    
    PFObject *estimate = [PFObject objectWithClassName:@"Estimate"];
    [estimate setObject:[NSNumber numberWithBool:NO] forKey:@"accepted"];
    [estimate setObject:estimateObj.estimate_ClientId forKey:@"clientId"];
    [estimate setObject:[estimateObj.estimate_id stringValue] forKey:@"estimateId"];
    [estimate setObject:[PFUser currentUser].objectId forKey:@"userId"];
    
    [estimate saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        // Error handling someday?
    }];
    
    [self save];
    
    NSLog(@"The estimate that was saved is: %@", estimateObj);
    
    
    if (replaceOldID)
    {
        // changing tempID of old invoices to match the new one
        NSArray* itemsFromOldEstimate = [self getEstimateItemsWithEstimateID:oldEstimateID];
        NSLog(@"estimate items retrieved are: %@", itemsFromOldEstimate);
        
        for (NSInteger x = 0; x < itemsFromOldEstimate.count; x++)
        {
            EstimateItem* existingEstimateItem = itemsFromOldEstimate[x];
            
            NSLog(@"the existing Estimate item is: %@", existingEstimateItem);
            
            if (!existingEstimateItem.item_id)
            {
                existingEstimateItem.item_id = @"";
            }
            
            NSDictionary *existingItem = @{
                                           @"item_amount" : existingEstimateItem.item_amount,
                                           @"item_desc" : existingEstimateItem.item_desc,
                                           @"item_id" : existingEstimateItem.item_id,
                                           @"item_rate" : existingEstimateItem.item_rate,
                                           @"item_taxAllow" : existingEstimateItem.item_taxAllow,
                                           @"item_tempID" : estimateID,
                                           @"item_title" : existingEstimateItem.item_title,
                                           @"item_quantity" : existingEstimateItem.item_quantity,
                                           };
            
            NSLog(@"checking old estimate id");
            NSLog(@"old estimate id is: %@", oldEstimateID);
            NSLog(@"existing item is: %@", existingItem);
            
            
            [self replaceEstimateItem:existingItem withNewEstimateID:oldEstimateID];
        }
        
        [self deleteEstimateItems:oldEstimateID];
        [self deleteEstimate:[oldEstimateID stringValue]];
    }
    
    //connecting the current estimate items to the newly created estimate
 
    NSArray* currentEstimateItems = [self getEstimateItemsFromCurrentEstimate];
    
    if (currentEstimateItems.count > 0)
    {
        for (NSInteger x = 0; x < currentEstimateItems.count; x++)
        {
            EstimateItem* item = currentEstimateItems [x];
            
            if (!item.item_id)
            {
                NSLog(@"Item %@ does not have a current item id", item.item_title);
                item.item_id = @"";
            }
            
            NSDictionary *oldItems = @{
                                       @"item_amount" : item.item_amount,
                                       @"item_desc" : item.item_desc,
                                       @"item_id" : item.item_id,
                                       @"item_rate" : item.item_rate,
                                       @"item_taxAllow" : item.item_taxAllow,
                                       @"item_tempID" : estimateID,
                                       @"item_title" : item.item_title,
                                       @"item_quantity" : item.item_quantity,
                                       };
            
            [self saveItemsToEstimate:oldItems];
        }
        [self deleteItemsFromCurrentEstimate];
    }
    
    NSLog(@"The final estimate that was saved is: %@", estimateObj);
    
    [self save];
}

- (void) saveItemsToEstimate: (NSDictionary*) estimateDict
{
    EstimateItem* estimateObj = [NSEntityDescription
                             insertNewObjectForEntityForName:@"EstimateItem"
                             inManagedObjectContext:_managedObjectContext];
    
    estimateObj.item_desc = [estimateDict objectForKey:@"item_desc"];
    estimateObj.item_amount = [estimateDict objectForKey:@"item_amount"];
    estimateObj.item_id = [estimateDict objectForKey:@"item_id"];
    estimateObj.item_rate = [estimateDict objectForKey:@"item_rate"];
    estimateObj.item_taxAllow = [estimateDict objectForKey:@"item_taxAllow"];
    estimateObj.item_teplateID = [estimateDict objectForKey:@"item_tempID"];
    estimateObj.item_title = [estimateDict objectForKey:@"item_title"];
    estimateObj.item_quantity = [estimateDict objectForKey:@"item_quantity"];
    
    [self save];
}

- (void) insertNewInvoice:(NSDictionary*)invoiceDict replaceTemplateID: (BOOL) replaceOldID
{
    NSLog(@"insert new invoice, the dictionary is: %@", invoiceDict);
    
    Invoice* invoiceObj = [NSEntityDescription
                           insertNewObjectForEntityForName:@"Invoice"
                           inManagedObjectContext:_managedObjectContext];
    
    NSString* commmntstr = [invoiceDict objectForKey:@"Comment"];
    NSString* invoiceDateStr = [invoiceDict objectForKey:@"InvoiceDate"];
    NSString* directionStr = [invoiceDict objectForKey:@"Direction"];
    NSString* itemsIDStr = [invoiceDict objectForKey:@"Items"];
    NSString* clientIdstr = [invoiceDict objectForKey:@"Client"];
    NSNumber* paidAmount = [invoiceDict objectForKey:@"PaidAmount"];
    NSString* photoList = [invoiceDict objectForKey:@"PhotoList"];
    NSNumber* taxValue = [invoiceDict objectForKey:@"Tax"];
    NSNumber* invoiceID = [self getPrimaryKeys];
    NSNumber* oldInvoiceID = [invoiceDict objectForKey:@"InvoiceId"];
    
    //trying not to change the invoice number
    NSNumber* invoiceNumber;
    if ([invoiceDict objectForKey:@"InvoiceNumber"])
    {
        NSLog(@"reusing the same invoice id");
        invoiceNumber = [invoiceDict objectForKey:@"InvoiceNumber"];
    } else {
        NSLog(@"giving a new invoice id");
        invoiceNumber = invoiceID;
    }
    
    
    NSNumber* pdfQuantity = [invoiceDict objectForKey:@"PDFQuantity"];
    NSNumber* pdfRate = [invoiceDict objectForKey:@"PDFRate"];
    NSNumber* pdfSubtotal = [invoiceDict objectForKey:@"PDFSubtotal"];
    NSNumber* pdfTotal = [invoiceDict objectForKey:@"PDFTotal"];
    
    
    invoiceObj.invoice_pdf_quantity = pdfQuantity;
    invoiceObj.invoice_pdf_rate = pdfRate;
    invoiceObj.invoice_pdf_subtotal = pdfSubtotal;
    invoiceObj.invoice_pdf_total = pdfTotal;

    invoiceObj.invoice_number = invoiceNumber;
    invoiceObj.invoice_id = invoiceID;
    invoiceObj.invoice_comment = (commmntstr)? commmntstr:@"";
    invoiceObj.invoice_date = (invoiceDateStr)? invoiceDateStr:@"";
    invoiceObj.invoice_direction = (directionStr)? directionStr:@"";
    invoiceObj.invoice_item = (itemsIDStr)? itemsIDStr:@"";
    invoiceObj.invoice_ClientId = (clientIdstr)? clientIdstr:@"";
    invoiceObj.invoice_photo = (photoList.length > 0)?photoList:@"";
    invoiceObj.invoice_paidAmount = (paidAmount)? paidAmount:[NSNumber numberWithDouble:0.0];
    invoiceObj.invoice_tax = (taxValue)? taxValue:[NSNumber numberWithDouble:0.0];
    
    [self save];
    
    
    if (replaceOldID)
    {
        // changing tempID of old invoices to match the new one
        NSArray* itemsFromOldInvoice = [self getInvoiceItemsWithInvoiceID:oldInvoiceID];
        
        for (NSInteger x = 0; x < itemsFromOldInvoice.count; x++)
        {
            InvoiceItem* existingInvoiceItem = itemsFromOldInvoice[x];
            
            NSLog(@"the existing invoice item is: %@", existingInvoiceItem);
            
            if (!existingInvoiceItem.item_id)
            {
                existingInvoiceItem.item_id = @"";
            }
            
            NSDictionary *existingItem = @{
                                       @"item_amount" : existingInvoiceItem.item_amount,
                                       @"item_desc" : existingInvoiceItem.item_desc,
                                       @"item_id" : existingInvoiceItem.item_id,
                                       @"item_rate" : existingInvoiceItem.item_rate,
                                       @"item_taxAllow" : existingInvoiceItem.item_taxAllow,
                                       @"item_tempID" : invoiceID,
                                       @"item_title" : existingInvoiceItem.item_title,
                                       @"item_quantity" : existingInvoiceItem.item_quantity,
                                       };
            
            
            [self addNewInvoiceItem:existingItem withNewInvoiceID:invoiceID];
        }
        
        [self deleteInvoiceItemsFromInvoice:oldInvoiceID];
    }
    
    // tie the current items to the invoice
    NSLog(@"Tie the current items to the invoice");
    
    NSArray* currentInvoiceItems = [self getInvoiceItemsFromCurrentInvoice];
    
    if (currentInvoiceItems.count > 0)
    {
        for (NSInteger x = 0; x < currentInvoiceItems.count; x++)
        {
            InvoiceItem* item = currentInvoiceItems [x];
            
            NSDictionary *currentItem = @{
                                       @"item_amount" : item.item_amount,
                                       @"item_desc" : item.item_desc,
                                       @"item_id" : item.item_id,
                                       @"item_rate" : item.item_rate,
                                       @"item_taxAllow" : item.item_taxAllow,
                                       @"item_tempID" : invoiceID,
                                       @"item_title" : item.item_title,
                                       @"item_quantity" : item.item_quantity,
                                       };

            [self addNewInvoiceItem:currentItem withNewInvoiceID:invoiceID];
        }
        
        [self deleteItemsFromCurrentInvoice];
    }

    //delete old invoice
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Invoice"];
    request.predicate = [NSPredicate predicateWithFormat:@"invoice_id == %@", oldInvoiceID];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    if (results.count > 0)
    {
        NSLog(@"the old invoice id is: %@, the result is: %@", oldInvoiceID, results[0]);
    } else {
        NSLog(@"there were no previous invoices");
    }
    
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    NSLog(@"The invoice object that is being saved is number: %@, and the invoice id is: %@", invoiceObj.invoice_number, invoiceObj.invoice_id);
    
    [self save];
}

- (void) insertNewTemplate:(NSDictionary*) templateDict replaceTemplateID: (BOOL) replaceOldID newTemplateID: (NSNumber*) newID
{
    Templates* templateObj = [NSEntityDescription
                           insertNewObjectForEntityForName:@"Templates"
                           inManagedObjectContext:_managedObjectContext];
    
    NSString* templateName = [templateDict objectForKey:@"template_name"];
    NSNumber* templateID;
    templateID = [self getPrimaryKeys];

    templateObj.template_name = templateName;
    templateObj.template_id = templateID;
    templateObj.active = [templateDict objectForKey:@"active"];
    templateObj.tempProductList = [templateDict objectForKey:@"comment"];
    
    NSLog(@"the class for the active object is: %@", [[templateDict objectForKey:@"comment"] class]);
    NSLog(@"the object within the active dictionary is: %@",[templateDict objectForKey:@"comment"]);
    NSLog(@"before the template active alteration, the template is: %@", templateObj);
    
    if([templateDict objectForKey:@"active"])
    {
        templateObj.active = [templateDict objectForKey:@"active"];
    } else {
         templateObj.active = [NSNumber numberWithInteger:0];
    }
    
    NSLog(@"The template that will be created is: %@", templateObj);
    
    NSArray* currentTemplateItems = [self getItemsWithCurrentTemplate];
    
    if (currentTemplateItems.count > 0)
    {
        
        for (NSInteger x = 0; x < currentTemplateItems.count; x++)
        {
            Item* item = currentTemplateItems [x];
            
            if (!item.item_id)
            {
                item.item_id = @"";
            }
            
            NSDictionary *oldItems = @{
                                        @"item_amount" : item.item_amount,
                                        @"item_desc" : item.item_desc,
                                        @"item_id" : item.item_id,
                                        @"item_rate" : item.item_rate,
                                        @"item_taxAllow" : item.item_taxAllow,
                                        @"item_tempID" : templateID,
                                        @"item_title" : item.item_title,
                                        @"item_quantity" : item.item_quantity,
                                        };
            
            [self insertNewTemplateItem:oldItems newTemplateID:templateID];
        }

        [self deleteItemsFromCurrentTemplate];

    }
    [self save];
    
    
    //this should be the update section, transfer old items to new template, then delete all previous items and template
    if (replaceOldID)
    {
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
        request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", newID];
        
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
        
        NSInteger section = 0;
        for (NSManagedObject* managedObject in results)
        {
            Item* templateItem = results[section];
            
            NSNumber* itemID = [self getPrimaryKeys];
            NSString* itemIDString = [itemID stringValue];
            
            NSDictionary *oldItems = @{
                                       @"item_amount" : templateItem.item_amount,
                                       @"item_desc" : templateItem.item_desc,
                                       @"item_id" : itemIDString,
                                       @"item_rate" : templateItem.item_rate,
                                       @"item_taxAllow" : templateItem.item_taxAllow,
                                       @"item_tempID" : templateItem.item_teplateID,
                                       @"item_title" : templateItem.item_title,
                                       @"item_quantity" : templateItem.item_quantity,
                                       };
            
            
            
            NSMutableDictionary* mutableOldItem = [oldItems mutableCopy];
            NSMutableDictionary* mutableNewItem = [oldItems mutableCopy];
            
            [mutableNewItem setObject:templateID forKey:@"item_tempID"];
            
            [self updateTemplateItem:mutableOldItem withUpdateTemplateItem:mutableNewItem];
            
            [_managedObjectContext deleteObject:managedObject];
            [self save];
            
            section++;
        }
        templateID = newID;
        
        request = [[NSFetchRequest alloc] initWithEntityName:@"Templates"];
        request.predicate = [NSPredicate predicateWithFormat:@"template_id == %@", newID];
        
        NSArray *templateResults = [_managedObjectContext executeFetchRequest:request error:nil];
        
        for (NSManagedObject* managedObject in templateResults)
        {
            [_managedObjectContext deleteObject:managedObject];
        }
    }
}

- (void) insertNewTemplateItem:(NSDictionary*)itemDict newTemplateID: (NSNumber*) templateID
{
        Item* itemobj = [NSEntityDescription
                         insertNewObjectForEntityForName:@"Item"
                         inManagedObjectContext:_managedObjectContext];
        
        itemobj.item_id = [itemDict objectForKey:@"item_ID"];
        itemobj.item_desc = [itemDict objectForKey:@"item_desc"];
        itemobj.item_teplateID = templateID;
        itemobj.item_title = [itemDict objectForKey:@"item_title"];
        itemobj.item_quantity = [itemDict objectForKey:@"item_quantity"];
        itemobj.item_rate = [itemDict objectForKey:@"item_rate"];
        itemobj.item_amount = [itemDict objectForKey:@"item_amount"];
        itemobj.item_taxAllow = [itemDict objectForKey:@"item_taxAllow"];

        [self save];
}

- (void) updateTemplateItem: (id) oldTemplateItem withUpdateTemplateItem: (NSMutableDictionary*) itemDict
{
    NSNumber* oldTemplateID;
    NSString* oldAmount;
    NSString* oldItemTitle;
    NSString* oldItemID;
    NSString* oldDescription;
    NSString* oldQuantity;
    NSString* oldRate;
    
    if ( [oldTemplateItem isKindOfClass:[Item class]] )
    {
        Item* oldTemplate = oldTemplateItem;
        
        oldTemplateID = oldTemplate.item_teplateID;
        oldAmount = oldTemplate.item_amount;
        oldItemTitle = oldTemplate.item_title;
        oldItemID = oldTemplate.item_id;
        oldDescription = oldTemplate.item_desc;
        oldQuantity = oldTemplate.item_quantity;
        oldRate = oldTemplate.item_rate;
        
    } else if ([oldTemplateItem isKindOfClass:[NSMutableDictionary class]])
    {
        oldTemplateID = [oldTemplateItem objectForKey:@"item_teplateID"];
        oldAmount = [oldTemplateItem objectForKey:@"item_amount"];
        oldItemTitle = [oldTemplateItem objectForKey:@"item_title"];
        oldItemID = [oldTemplateItem objectForKey:@"item_id"];
        oldDescription = [oldTemplateItem objectForKey:@"item_desc"];
        oldQuantity = [oldTemplateItem objectForKey:@"item_quantity"];
        oldRate = [oldTemplateItem objectForKey:@"item_rate"];
    }
    
    NSNumber* newTemplateID;
    
    Item* itemobj = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Item"
                     inManagedObjectContext:_managedObjectContext];
    
    if ([itemDict objectForKey:@"item_tempID"])
    {
        newTemplateID = [itemDict objectForKey:@"item_tempID"];
    } else {
        newTemplateID = [itemDict objectForKey:@"item_teplateID"];
    }
    
    NSNumber* rateNumber = [itemDict objectForKey:@"item_rate"];
    NSNumber* quantityNumber = [itemDict objectForKey:@"item_quantity"];
    
    itemobj.item_id = oldItemID;
    itemobj.item_desc = [itemDict objectForKey:@"item_desc"];
    itemobj.item_teplateID = newTemplateID;
    itemobj.item_title = [itemDict objectForKey:@"item_title"];
    itemobj.item_quantity = [NSString stringWithFormat:@"%@", quantityNumber];
    itemobj.item_rate = [NSString stringWithFormat:@"%@", rateNumber];
    itemobj.item_amount = [itemDict objectForKey:@"item_amount"];
    itemobj.item_taxAllow = [itemDict objectForKey:@"item_taxAllow"];
    
    [self save];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", oldTemplateID];
    NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", oldItemTitle];
    NSPredicate *amoutPredicate = [NSPredicate predicateWithFormat:@"item_amount == %@", oldAmount];
    NSPredicate *descriptionPredicate = [NSPredicate predicateWithFormat:@"item_desc == %@", oldDescription];
    NSPredicate *quantityPredicate = [NSPredicate predicateWithFormat:@"item_quantity == %@", oldQuantity];
    NSPredicate *ratePredicate = [NSPredicate predicateWithFormat:@"item_rate == %@", oldRate];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, amoutPredicate, descriptionPredicate, quantityPredicate, ratePredicate, nil]];
    
    request.predicate = compoundPredicate;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    NSLog(@"The items that will be deleted are: %@", results);
    
    //check if any changes were made, if there were no changes delete the one created, if there were, delete the old version
    
    if (![oldDescription isEqual:itemobj.item_desc] || ![oldItemTitle isEqual:itemobj.item_title] || ![oldRate isEqual:itemobj.item_rate] || ![oldTemplateID isEqual:itemobj.item_teplateID] || ![oldAmount isEqual:itemobj.item_amount])
    {
        NSLog(@"different Description");
        
        for (NSManagedObject* managedObject in results)
        {
            [_managedObjectContext deleteObject:managedObject];
            [self save];
        }
    } else
        {
            for (NSManagedObject* managedObject in results)
            {
                [_managedObjectContext deleteObject:managedObject];
                [self save];
                return;
            }
        }
}

- (void) addUpdateCompanySettingData:(NSDictionary*)companySettingDict
{
    CompanySetting* companySettingObj = nil;
    
    NSArray* arrayCompanySetting = [self getCompanySettingData];
    
    if (arrayCompanySetting.count > 0)
        companySettingObj  = [arrayCompanySetting objectAtIndex:0];
    else
        companySettingObj = [NSEntityDescription
                             insertNewObjectForEntityForName:@"CompanySetting"
                             inManagedObjectContext:_managedObjectContext];
    
    NSString* companyNameStr = [companySettingDict objectForKey:@"CompanyName"];
    NSString* companyAddStr = [companySettingDict objectForKey:@"CompanyAddress"];
    NSString* companyCityStr = [companySettingDict objectForKey:@"CompanyCity"];
    NSString* companyStateStr = [companySettingDict objectForKey:@"CompanyState"];
    NSString* companyZipStr = [companySettingDict objectForKey:@"CompanyZip"];
    NSString* companyCountryStr = [companySettingDict objectForKey:@"CompanyCountry"];
    NSString* companyTaxStr = [companySettingDict objectForKey:@"CompanyTax"];
    NSString* companyLogoStr = [companySettingDict objectForKey:@"CompanyLogo"];
    NSString* companyMobileStr = [companySettingDict objectForKey:@"CompanyMobile"];
    NSString* companyEmailStr = [companySettingDict objectForKey:@"CompanyEmail"];
    NSString* companyTermsString = [companySettingDict objectForKey:@"CompanyTerms"];
    NSNumber* includeTermsNumber = [companySettingDict objectForKey: @"IncludeTerms"];
    
    
    companySettingObj.company_name = (companyNameStr.length >0)? companyNameStr:@"";
    companySettingObj.company_address = (companyAddStr.length >0)? companyAddStr:@"";
    companySettingObj.company_city = (companyCityStr.length >0)? companyCityStr:@"";
    companySettingObj.company_state = (companyStateStr.length >0)? companyStateStr:@"";
    companySettingObj.company_zip = (companyZipStr.length >0)? companyZipStr:@"";
    companySettingObj.company_country = (companyCountryStr.length > 0)?companyCountryStr:@"";
    companySettingObj.company_tax = (companyTaxStr.length >0)? companyTaxStr:@"";
    companySettingObj.company_logo = (companyLogoStr.length >0)? companyLogoStr:@"";
    companySettingObj.company_mobilePhone = (companyMobileStr.length >0)? companyMobileStr:@"";
    companySettingObj.company_email = (companyEmailStr.length >0)? companyEmailStr:@"";
    companySettingObj.company_terms = (companyTermsString.length >0)? companyTermsString:@"";
    companySettingObj.include_terms = [includeTermsNumber boolValue];
    
    [self save];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateCompanyinfoSettingNotification object:nil];
}

- (void) changeActiveInactiveTempalte: (NSNumber*) template_ID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Templates"];
    request.predicate = [NSPredicate predicateWithFormat:@"template_id == %@", template_ID];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    NSMutableDictionary* templateDict = [[NSMutableDictionary alloc]init];
    
    for (NSManagedObject* managedObject in results)
    {
        Templates* template = results[0];
        
        [templateDict setObject:template.template_id forKey:@"template_id"];
        [templateDict setObject:template.template_name forKey:@"template_name"];
        [templateDict setObject:[NSNumber numberWithInteger:0] forKey:@"active"];
        
        //mutated dictionary
        
        NSNumber* active = template.active;
        NSNumber* isActive = [NSNumber numberWithInteger:0];;
        NSNumber* isNotActive = [NSNumber numberWithInteger:1];
        
        if ([active integerValue] == 0) {
            [templateDict setValue:isNotActive forKey:@"active"];
        } else {
            [templateDict setValue:isActive forKey:@"active"];
        }

        
        BOOL OldID = YES;
        NSNumber* newID = [self getPrimaryKeys];
        
        [self insertNewTemplate:templateDict replaceTemplateID:OldID newTemplateID:newID];
        
        [_managedObjectContext deleteObject:managedObject];
        [self connectItemsToNewTemplate:template.template_id newID:newID];
    }
    [self save];
}

- (void) convertEstimateToInvoice: (NSDictionary*) estimateDictionary withItems: (NSArray*) itemArray
{
    Invoice* invoiceObj = [NSEntityDescription
                           insertNewObjectForEntityForName:@"Invoice"
                           inManagedObjectContext:_managedObjectContext];
    

    if ([estimateDictionary valueForKey:@"Comment"])
    {
        invoiceObj.invoice_comment = [estimateDictionary valueForKey:@"Comment"];
    }
    
    if ([estimateDictionary valueForKey:@"EstimateDate"])
    {
        invoiceObj.invoice_date = [estimateDictionary valueForKey:@"EstimateDate"];
    }
    
    if ([estimateDictionary valueForKey:@"Direction"])
    {
        invoiceObj.invoice_direction = [estimateDictionary valueForKey:@"Direction"];
    }
    
    if ([estimateDictionary valueForKey:@"Items"])
    {
        invoiceObj.invoice_item = [estimateDictionary valueForKey:@"Items"];
    }
    
    if ([estimateDictionary valueForKey:@"Client"])
    {
        invoiceObj.invoice_ClientId = [estimateDictionary valueForKey:@"Client"];
    }
    
    if ([estimateDictionary valueForKey:@"Tax"])
    {
        invoiceObj.invoice_tax = [estimateDictionary valueForKey:@"Tax"];
    }
    
    NSNumber* invoiceID = [self getPrimaryKeys];
    invoiceObj.invoice_id = invoiceID;
    
//    NSNumber* pdfQuantity = [estimateDictionary objectForKey:@"PDFQuantity"];
//    NSNumber* pdfRate = [estimateDictionary objectForKey:@"PDFRate"];
//    NSNumber* pdfSubtotal = [estimateDictionary objectForKey:@"PDFSubtotal"];
//    NSNumber* pdfTotal = [estimateDictionary objectForKey:@"PDFTotal"];
    
    if ([estimateDictionary objectForKey:@"PDFQuantity"] != nil)
    {
        NSLog(@"IT WAS NULL");
        invoiceObj.invoice_pdf_quantity = [estimateDictionary objectForKey:@"PDFQuantity"];
    } else {
        invoiceObj.invoice_pdf_quantity = NO;
    }
    
    if ([estimateDictionary objectForKey:@"PDFRate"] != nil)
    {
        NSLog(@"IT WAS NULL");
        invoiceObj.invoice_pdf_rate = [estimateDictionary objectForKey:@"PDFQuantity"];
    } else {
        invoiceObj.invoice_pdf_rate = NO;
    }
    
    if ([estimateDictionary objectForKey:@"PDFSubtotal"] != nil)
    {
        NSLog(@"IT WAS NULL");
        invoiceObj.invoice_pdf_subtotal = [estimateDictionary objectForKey:@"PDFQuantity"];
    } else {
        invoiceObj.invoice_pdf_subtotal = NO;
    }
    
    if ([estimateDictionary objectForKey:@"PDFTotal"] != nil)
    {
        NSLog(@"IT WAS NULL");
        invoiceObj.invoice_pdf_total = [estimateDictionary objectForKey:@"PDFQuantity"];
    } else {
        invoiceObj.invoice_pdf_total = NO;
    }
    

    //invoiceObj.invoice_photo = (photoList.length > 0)?photoList:@"";
    [self save];
    
    NSLog(@"The invoice that was saved is: %@", invoiceObj);
    NSLog(@"The item array is: %@", itemArray);
    
    //after saving the invoice, need to convert the estimate items into invoice items
    
    
    for (NSInteger x = 0; x < itemArray.count; x++)
    {
        EstimateItem* estimateItem = itemArray[x];
        
        InvoiceItem* invoiceItemObj = [NSEntityDescription
                               insertNewObjectForEntityForName:@"InvoiceItem"
                               inManagedObjectContext:_managedObjectContext];
        
        invoiceItemObj.item_amount = estimateItem.item_amount;
        invoiceItemObj.item_taxAllow = estimateItem.item_taxAllow;
        invoiceItemObj.item_desc = estimateItem.item_desc;
        invoiceItemObj.item_id = estimateItem.item_id;
        invoiceItemObj.item_quantity = estimateItem.item_quantity;
        invoiceItemObj.item_rate = estimateItem.item_rate;
        invoiceItemObj.item_teplateID = invoiceID;
        invoiceItemObj.item_title = estimateItem.item_title;
        
        [self save];
    }
}

- (void) connectItemsToNewTemplate: (NSNumber*) oldTemplateID newID: (NSNumber*) newTemplateID
{
    NSArray* itemsFromTemplate = [self getItemsFromExistingTemplate:oldTemplateID];
    
    if (itemsFromTemplate.count > 0)
    {
        for (NSInteger x = 0; x < itemsFromTemplate.count; x++)
        {
            Item* item = itemsFromTemplate [x];
            
            Item* itemobj = [NSEntityDescription
                             insertNewObjectForEntityForName:@"Item"
                             inManagedObjectContext:_managedObjectContext];
            
            itemobj.item_id = item.item_id;
            itemobj.item_desc = item.item_desc;
            itemobj.item_title = item.item_title;
            itemobj.item_quantity = item.item_quantity;
            itemobj.item_rate = item.item_rate;
            itemobj.item_amount = item.item_amount;
            itemobj.item_taxAllow = item.item_taxAllow;
            itemobj.item_teplateID = newTemplateID;

            [self save];
        }
        
        // after the new items are added with the updated template id, i delete the items with the old template id
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
        request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", oldTemplateID];
        
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
        for (NSManagedObject* managedObject in results)
        {
            [_managedObjectContext deleteObject:managedObject];
            [self save];
        }
    }
}

#pragma mark --
#pragma mark Delete Methods

- (void) deleteInvoiceItemsFromInvoice:(NSNumber*) invoiceID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"InvoiceItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", invoiceID];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    NSLog(@"the invoice items that will be deleted are: %@, using the invoiceID of: %@", results, invoiceID);
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    NSLog(@"the invoice items were deleted");
    
}

- (void) deleteProductItemWithID:(NSString*)itemID
{
    NSArray* itemList = [self getItemWithId:itemID];
    for (NSManagedObject* managedObject in itemList)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [_listOfItemIDs removeObject:itemID];
    
    [self save];
}

- (void) deleteEstimateItemWithID:(NSString*)itemID andTitle: (NSString*) title
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"EstimateItem"];
    
//    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", itemID];
//    request.predicate = [NSPredicate predicateWithFormat:@"item_title == %@", title];
    
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", itemID];
    NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", title];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, nil]];
    
    request.predicate = compoundPredicate;
    
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
}

- (void) deleteEstimateItems:(NSNumber*)itemID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"EstimateItem"];
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", itemID];
    request.predicate = templateIDPredicate;
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
}

- (void) deleteEstimate:(NSString*)estimateID
{
    NSInteger estimateidInt = [estimateID integerValue];
    
    [self deleteEstimateItems: [NSNumber numberWithInteger:estimateidInt]];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Estimate"];
    request.predicate = [NSPredicate predicateWithFormat:@"estimate_id == %@", estimateID];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    NSLog(@"The estimate items that are going to be deleted are: %@", results);
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
}

//- (void) deleteInvoiceItemWithID:(NSString*)itemID
//{
//    NSArray* itemList = [self getInvoiceItemWithId:itemID];
//    for (NSManagedObject* managedObject in itemList)
//    {
//        [_managedObjectContext deleteObject:managedObject];
//        NSLog(@" Invoice Item object deleted");
//    }
//    [self save];
//}


- (void) deleteInvoiceItem:(InvoiceItem *)item
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"InvoiceItem"];
    
//    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", item.item_teplateID];
//    request.predicate = [NSPredicate predicateWithFormat:@"item_id == %@", item.item_id];
    
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", item.item_teplateID];
    NSPredicate *itemIDPredicate = [NSPredicate predicateWithFormat:@"item_id == %@", item.item_id];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, itemIDPredicate, nil]];
    
    request.predicate = compoundPredicate;
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
}

- (void) deleteInvoice:(Invoice*)invoiceObject
{
    NSLog(@"Delete invoice, the invoice object is: %@", invoiceObject);
    NSLog(@"The client id is: %@", invoiceObject.invoice_ClientId);
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Invoice"];
    NSPredicate *clientIDPredicate = [NSPredicate predicateWithFormat:@"invoice_ClientId == %@", invoiceObject.invoice_ClientId];
    request.predicate = clientIDPredicate;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    //first delete all of the items within the invoices
    for (NSInteger x = 0; x < results.count; x++)
    {
        Invoice* invoice = results[x];
        NSInteger clientIDInteger = [invoice.invoice_id integerValue];
        NSNumber* clientID = [NSNumber numberWithInteger:clientIDInteger];
        
        [self deleteInvoiceItemsFromInvoice:clientID];
    }
    
    
    // delete the actual invoices
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
}


- (void) deleteCompanyData
{
    NSArray* itemList = [self getCompanySettingData];
    for (NSManagedObject* managedObject in itemList)
    {
        [_managedObjectContext deleteObject:managedObject];
        NSLog(@" Company Item object deleted");
    }
    [self save];
}

// Delete Customers Object From client list.
- (void) deleteObejctFromClientList:(Customers*) customerObject
{
    NSString *id = customerObject.customer_external_id;
    NSManagedObjectContext*  context = _managedObjectContext;
    [context deleteObject:customerObject];
    [self save];
    
    if(id){
        PFObject *object = [PFObject objectWithoutDataWithClassName:@"Quote"
                                                           objectId:id];
        [object deleteEventually];
    }
    
    NSError* error;
    if(![context save:&error])
        NSLog(@"Error message: %@", [error localizedDescription]);
}

#pragma mark --
#pragma mark Update Methods

- (void) updateCustomerWithID:(NSDictionary*)custDict
{
    NSString* customerID = [custDict objectForKey:@"CustomerID"];
    NSArray* arryItem = [self getCustomerdetailsWithQuotes:NO andID:customerID];
    
    if (arryItem.count > 0)
    {
        Customers* customerObj = [arryItem objectAtIndex:0];
        
        NSString* customerFirstNameStr = [custDict objectForKey:@"CustomerFirstName"];
        NSString* customerLastNameStr = [custDict objectForKey:@"CustomerLastName"];
        NSString* customerCmpnyStr = [custDict objectForKey:@"CustomerMemberCompany"];
        NSString* customerMobilePhneStr = [custDict objectForKey:@"CustomerMobilePhone"];
        NSString* customerAddres1Str = [custDict objectForKey:@"CustomerAddress1"];
        NSString* customerAddres2Str = [custDict objectForKey:@"CustomerAddress2"];
        NSString* customerCityStr = [custDict objectForKey:@"CustomerCity"];
        NSString* customerStateStr = [custDict objectForKey:@"CustomerState"];
        NSString* customerZipStr = [custDict objectForKey:@"CustomerZip"];
        NSString* customerEmailStr = [custDict objectForKey:@"CustomerEmail"];
        NSString* customerNotesStr = [custDict objectForKey:@"CustomerNotes"];
        NSString* customerQuotesStr = [custDict objectForKey:@"CustomerQuotes"];
        
        customerObj.customer_firstname = (customerFirstNameStr != nil) ? customerFirstNameStr : @"";
        customerObj.customer_lastname = (customerLastNameStr != nil) ? customerLastNameStr : @"";
        customerObj.customer_company = (customerCmpnyStr != nil) ? customerCmpnyStr : @"";
        customerObj.customer_mobilePhone = (customerMobilePhneStr != nil) ? customerMobilePhneStr : @"";
        customerObj.customer_addres1 = (customerAddres1Str != nil) ? customerAddres1Str : @"";
        customerObj.customer_addres2 = (customerAddres2Str != nil) ? customerAddres2Str : @"";
        customerObj.customer_city = (customerCityStr != nil) ? customerCityStr : @"";
        customerObj.customer_state = (customerStateStr != nil) ? customerStateStr : @"";
        customerObj.customer_zip = (customerZipStr != nil) ? customerZipStr : @"";
        customerObj.customer_email = (customerEmailStr != nil) ? customerEmailStr : @"";
        customerObj.customer_notes = (customerNotesStr != nil) ? customerNotesStr : @"";
        
        BOOL allowQuotes = [((customerQuotesStr != nil) ? customerQuotesStr : @"") boolValue];
        customerObj.customer_quotes = [NSNumber numberWithBool:allowQuotes];
    }
    
    [self save];
}


- (void) updateProductwithItem:(NSDictionary*)itemDict
{
    NSLog(@"The item dictionary is: %@", itemDict);
    
    NSString* itemId;
    
    if ([itemDict objectForKey:@"ItemID"])
    {
        itemId = [itemDict objectForKey:@"ItemID"];
    } else {
        itemId = @"";
    }
        
    NSNumber* tempId = [NSNumber numberWithInteger:[[itemDict objectForKey:@"templateId"]integerValue]];
    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    
    NSLog(@"testing half");
    
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    NSNumber* nsNumberRate = [itemDict objectForKey:@"Rate"];
    
    NSLog(@"after rate");
    
   // NSString* rateString = [nsNumberRate stringValue];
    NSString *rate = [NSString stringWithFormat:@"%@", nsNumberRate];
    
    NSLog(@"The rate is: %@, the actual rate is: %@", rate, [itemDict objectForKey:@"Rate"]);
    
    NSString* amount = [itemDict objectForKey:@"Amount"];
    
    
    NSLog(@"The template id used to delete is: %@, along with the title: %@", tempId, titleStr);
    
    // ------
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", titleStr];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, nil]];
    
    request.predicate = compoundPredicate;
    
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    if (results.count > 0)
    {
        NSLog(@"the items deleted are: %@, the first one is: %@", results, results[0]);
    } else{
        NSLog(@"there were no results");
    }
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
    
    NSLog(@"About to set up the new invoice item, the reate is: %@", rate);
    
    
    Item* itemobj = [NSEntityDescription
                             insertNewObjectForEntityForName:@"Item"
                             inManagedObjectContext:_managedObjectContext];
    
    itemobj.item_teplateID = tempId;
    itemobj.item_desc = itemDesc;
    itemobj.item_title = titleStr;
    itemobj.item_quantity = qtyStr;
    itemobj.item_rate = rate;
    itemobj.item_amount = amount;
    itemobj.item_taxAllow = taxAllow;
    
    [self save];
    
    NSLog(@"The item used to update is: %@", itemobj);
    NSFetchRequest *requestItems = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    requestItems.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    NSArray* itemResults = [_managedObjectContext executeFetchRequest:request error:nil];
    
    NSLog(@"all of the items in this current estimate are: %@", itemResults);
    NSLog(@"the count is: %lu", (unsigned long)itemResults.count);
    
    
    
}

- (void) updateEstimateItemwithDict:(NSDictionary*)estimateDictionary
{
    
    NSLog(@"about to update the estimate item, dictionary is: %@", estimateDictionary);
    NSDictionary* itemDict = [estimateDictionary copy];
    NSLog(@"the copy is: %@", itemDict);
    
    NSNumber* tempId = [NSNumber numberWithInteger:[[itemDict objectForKey:@"templateId"]integerValue]];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    NSString* tempIdString = [tempId stringValue];
    
    [self deleteEstimateItemWithID:tempIdString andTitle:titleStr];
    
    
    
    EstimateItem* estimateItemObj = [NSEntityDescription
                               insertNewObjectForEntityForName:@"EstimateItem"
                               inManagedObjectContext:_managedObjectContext];
    
    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    
    estimateItemObj.item_teplateID = (tempId)? tempId:[NSNumber numberWithBool:NO];
    estimateItemObj.item_desc = (itemDesc) ? itemDesc:@"";
    estimateItemObj.item_title = titleStr ? titleStr:@"";
    estimateItemObj.item_quantity = (qtyStr) ? qtyStr:@"0";
    
    if ([itemDict objectForKey:@"ItemID"])
    {
        estimateItemObj.item_id = [itemDict objectForKey:@"ItemID"];
    }
        
    estimateItemObj.item_quantity = [NSString stringWithFormat:@"%ld", (long)[[itemDict objectForKey:@"Quantity"] integerValue]];
    estimateItemObj.item_rate = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Rate"] doubleValue]];
    estimateItemObj.item_amount = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Amount"] doubleValue]];
    estimateItemObj.item_taxAllow = (taxAllow) ? taxAllow: [NSNumber numberWithInteger:1];
    
    NSLog(@"the estimate item created is: %@", estimateItemObj);
    
    [self save];
}

-(void) addInvoiceItemToProductList: (NSDictionary*) itemDict
{
    NSLog(@"about to add item to product list");
    
    NSNumber* tempId = [NSNumber numberWithInteger:0];
    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    NSNumber* nsNumberRate = [itemDict objectForKey:@"Rate"];
    // NSString* rateString = [nsNumberRate stringValue];
    NSString *rate = [NSString stringWithFormat:@"%@", nsNumberRate];
    NSString* amount = [itemDict objectForKey:@"Amount"];
    
    Item* item = [NSEntityDescription
                               insertNewObjectForEntityForName:@"Item"
                               inManagedObjectContext:_managedObjectContext];
    
    item.item_teplateID = tempId;
    item.item_desc = itemDesc;
    item.item_taxAllow = taxAllow;
    item.item_title = titleStr;
    item.item_quantity = qtyStr;
    item.item_rate = rate;
    item.item_amount = amount;
    
    [self save];
    
}

- getPaidAmount: (NSNumber*) invoiceID invoiceTitle: (NSString*) invoiceTitle
{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Invoice"];
    
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", invoiceID];
    //NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", invoiceTitle];
    //NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, nil]];
    
    request.predicate = templateIDPredicate;
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    Invoice* item = results[0];
    
    NSNumber* paidAmount = item.invoice_paidAmount;
    
    NSLog(@"the paid amount is: %@", paidAmount);
    
    return paidAmount;
}

- (void) updateInvoiceItemWithDict:(NSDictionary*)itemDict
{
    NSLog(@"The item dictionary is: %@", itemDict);
    
    NSString* itemId;
    
    if ([itemDict objectForKey:@"ItemID"])
    {
        itemId = [itemDict objectForKey:@"ItemID"];
    } else {
        itemId = @"";
    }
    
    NSNumber* tempId = [NSNumber numberWithInteger:[[itemDict objectForKey:@"templateId"]integerValue]];
    NSString* itemDesc = [itemDict objectForKey:@"Note"];
    NSNumber* taxAllow = [itemDict objectForKey:@"TaxAllow"];
    NSString* titleStr = [itemDict objectForKey:@"Title"];
    NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
    NSNumber* nsNumberRate = [itemDict objectForKey:@"Rate"];
    // NSString* rateString = [nsNumberRate stringValue];
    NSString *rate = [NSString stringWithFormat:@"%@", nsNumberRate];
    NSString* amount = [itemDict objectForKey:@"Amount"];
    
    NSLog(@"The rate is: %@, the actual rate is: %@", rate, [itemDict objectForKey:@"Rate"]);
    NSLog(@"The template id used to delete is: %@, along with the title: %@", tempId, titleStr);
    
    // ------
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"InvoiceItem"];
    
    NSPredicate *templateIDPredicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"item_title == %@", titleStr];
    
    NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: templateIDPredicate, titlePredicate, nil]];
    
    request.predicate = compoundPredicate;
    
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    if (results.count > 0)
    {
        NSLog(@"the items deleted are: %@, the first one is: %@", results, results[0]);
    } else{
        NSLog(@"there were no results");
    }
    
    
    
    for (NSManagedObject* managedObject in results)
    {
        [_managedObjectContext deleteObject:managedObject];
    }
    
    [self save];
    
    

    
    NSLog(@"About to set up the new invoice item, the reate is: %@", rate);
    
    InvoiceItem* itemobj = [NSEntityDescription
                     insertNewObjectForEntityForName:@"InvoiceItem"
                     inManagedObjectContext:_managedObjectContext];
    
    itemobj.item_teplateID = tempId;
    itemobj.item_desc = itemDesc;
    itemobj.item_title = titleStr;
    itemobj.item_quantity = qtyStr;
    itemobj.item_rate = rate;
    itemobj.item_amount = amount;
    itemobj.item_taxAllow = taxAllow;
    
    [self save];
    
    NSLog(@"The item used to update is: %@", itemobj);
    NSFetchRequest *requestItems = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    requestItems.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", tempId];
    NSArray* itemResults = [_managedObjectContext executeFetchRequest:request error:nil];
    
    NSLog(@"all of the items in this current estimate are: %@", itemResults);
    NSLog(@"the count is: %lu", (unsigned long)itemResults.count);
}

#pragma mark --
#pragma mark Save Methods

- (void) storeCustomerDetail:(NSArray*)customerList
{
    for (NSDictionary* dict in customerList)
    {
        Customers* membersTable = [NSEntityDescription
                                   insertNewObjectForEntityForName:@"Customers"
                                   inManagedObjectContext:_managedObjectContext];
        
        NSNumber* customerId = [self getPrimaryKeys];
        NSString* customerMemberIdStr = [dict objectForKey:@"CustomerMemberId"];
        NSString* customerFirstNameStr = [dict objectForKey:@"CustomerFirstName"];
        NSString* customerLastNameStr = [dict objectForKey:@"CustomerLastName"];
        NSString* customerFullNameStr = [dict objectForKey:@"CustomerFullName"];
        NSString* customerCmpnyStr = [dict objectForKey:@"CustomerMemberCompany"];
        NSString* customerMobilePhoneStr = [dict objectForKey:@"CustomerMobilePhone"];
        NSString* customerHomePhoneStr = [dict objectForKey:@"CustomerHomePhone"];
        NSString* customerOfficePhoneStr = [dict objectForKey:@"CustomerOfficePhone"];
        NSString* customerFullAddresStr = [dict objectForKey:@"CustomerFullAddress"];
        NSString* customerAddres1Str = [dict objectForKey:@"CustomerAddress1"];
        NSString* customerAddres2Str = [dict objectForKey:@"CustomerAddress2"];
        NSString* customerCityStr = [dict objectForKey:@"CustomerCity"];
        NSString* customerStateStr = [dict objectForKey:@"CustomerState"];
        NSString* customerZipStr = [dict objectForKey:@"CustomerZip"];
        NSString* customerEmailStr = [dict objectForKey:@"CustomerEmail"];
        NSString* customerQuotes = [dict objectForKey:@"CustomerPendingQuote"];
        NSString* customerCretaedByStr = [dict objectForKey:@"CustomerCreatedBy"];
        NSString* customerCreatdeDateStr = [dict objectForKey:@"CustomerCreatedDate"];
        NSString* customerModifyByStr = [dict objectForKey:@"CustomerModifiedBy"];
        NSString* customerModifiedDateStr = [dict objectForKey:@"CustomerModifiedDate"];
        NSString* customerNotesStr = [dict objectForKey:@"CustomerNotes"];
        NSString* customerExternalIdStr = [dict objectForKey:@"CustomerExternalId"];
        NSString* customerLastnameIntialStr = [dict objectForKey:@"CustomerLastNameInitial"];
        
        NSLog(@"%@", customerCreatdeDateStr);
        
        membersTable.customer_id = customerId;
        membersTable.customer_memberId = customerMemberIdStr;
        membersTable.customer_external_id = (customerExternalIdStr != nil) ? customerExternalIdStr : @"";
        
        membersTable.customer_firstname = (customerFirstNameStr != nil) ? customerFirstNameStr : @"";
        membersTable.customer_lastname = (customerLastNameStr != nil) ? customerLastNameStr : @"";
        membersTable.customer_fullname = (customerFullNameStr != nil) ? customerFullNameStr : @"";
        membersTable.customer_company = (customerCmpnyStr != nil) ? customerCmpnyStr : @"";
        membersTable.customer_mobilePhone = (customerMobilePhoneStr != nil) ? customerMobilePhoneStr : @"";
        membersTable.customer_homePhone = (customerHomePhoneStr != nil) ? customerHomePhoneStr : @"";
        membersTable.customer_officePhone = (customerOfficePhoneStr != nil) ? customerOfficePhoneStr : @"";
        membersTable.customer_fullAddress = (customerFullAddresStr != nil) ? customerFullAddresStr : @"";
        membersTable.customer_addres1 = (customerAddres1Str != nil) ? customerAddres1Str : @"";
        membersTable.customer_addres2 = (customerAddres2Str != nil) ? customerAddres2Str : @"";
        membersTable.customer_city = (customerCityStr != nil) ? customerCityStr : @"";
        membersTable.customer_state = (customerStateStr != nil) ? customerStateStr : @"";
        membersTable.customer_zip = (customerZipStr != nil) ? customerZipStr : @"";
        membersTable.customer_email = (customerEmailStr != nil) ? customerEmailStr : @"";
        
        BOOL allowQuotes = [customerQuotes boolValue];
        membersTable.customer_quotes = [NSNumber numberWithBool:allowQuotes];
        
        membersTable.customer_createdBy = (customerCretaedByStr != nil) ? customerCretaedByStr : @"";
        membersTable.customer_createdDate = (customerCreatdeDateStr != nil) ? customerCreatdeDateStr : @"";
        membersTable.customer_modifyBy = (customerModifyByStr != nil) ? customerModifyByStr : @"";
        membersTable.customer_modifiedDate = (customerModifiedDateStr != nil) ? customerModifiedDateStr : @"";
        membersTable.customer_notes = (customerNotesStr != nil) ? customerNotesStr : @"";
        membersTable.customer_lastnameInitial = (customerLastnameIntialStr != nil) ? customerLastnameIntialStr : @"";
    }
    
    [self save];
    [[NSNotificationCenter defaultCenter] postNotificationName:kCustomerCreateServiceSucess object:nil];
}

//here is where the array is passed through, containing the information from the JSON file of the template.json
- (void) storeTemplateFromLocalDB:(NSArray*)templateList
{

    for (NSDictionary* dict in templateList)
    {
        Templates* membersTable = [NSEntityDescription
                                   insertNewObjectForEntityForName:@"Templates"
                                   inManagedObjectContext:_managedObjectContext];
        
        NSString* tempID = [dict objectForKey:@"TemplateId"];
        NSString* tempName = [dict objectForKey:@"TemplateName"];
        //NSNumber* templateActive = [dict objectForKey:@"Active"];
        
        membersTable.template_id = [NSNumber numberWithInteger:[tempID integerValue]];
        membersTable.template_name = (tempName != nil) ? tempName : @"";
        
        if ([dict objectForKey:@"Active"] == 0)
        {
            membersTable.active = [NSNumber numberWithInteger:0];
        } else{
                membersTable.active = [NSNumber numberWithInteger:1];
        }
        
        NSString* productListStr  = @"";
        
        NSArray* productList = [dict objectForKey:@"ProductList"];
        for (NSDictionary* itemDict in productList)
        {
            Item* itemobj = [NSEntityDescription
                             insertNewObjectForEntityForName:@"Item"
                             inManagedObjectContext:_managedObjectContext];
            
            //			NSNumber* tempId = [NSNumber numberWithInteger:[[itemDict objectForKey:@"templateId"]integerValue]];
            NSString* itemDesc = [itemDict objectForKey:@"Note"];
            NSString* taxAllow = [itemDict objectForKey:@"TaxAllow"];
            NSString* titleStr = [itemDict objectForKey:@"Title"];
            NSString* qtyStr = [itemDict objectForKey:@"Quantity"];
            
            itemobj.item_id = [NSString stringWithFormat:@"%@",[self getPrimaryKeys]];
            //			itemobj.item_teplateID = (tempId)? tempId:[NSNumber numberWithBool:NO];
            itemobj.item_desc = (itemDesc) ? itemDesc:@"";
            itemobj.item_title = titleStr ? titleStr:@"";
            itemobj.item_quantity = (qtyStr) ? qtyStr:@"0";
            itemobj.item_teplateID = [NSNumber numberWithInteger:[tempID integerValue]];
            
            NSLog(@"The template id for the item is %@, and the template id itself is: %@", membersTable.template_id, itemobj.item_teplateID);
            
            
            //itemobj.item_quantity = [NSString stringWithFormat:@"%d", [[itemDict objectForKey:@"Quantity"] integerValue]];
            itemobj.item_rate = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Rate"] doubleValue]];
            itemobj.item_amount = [NSString stringWithFormat:@"%f",[[itemDict objectForKey:@"Amount"] doubleValue]];
            itemobj.item_taxAllow = ([taxAllow caseInsensitiveCompare:@"YES"]) ? [NSNumber numberWithInteger:1]: [NSNumber numberWithInteger:0];
            //itemobj.item_teplateID = [itemDict objectForKey:@"TemplateID"];
            //trying to make sure that all of the items from the json file have the template id number stufk with them
            
            //productListStr = (productListStr.length < 1)? [NSString stringWithFormat:@"%@",itemobj.item_id]:[NSString stringWithFormat:@"%@,%@",productListStr,itemobj.item_id];
        }
        productListStr = kTerms;
        
        membersTable.tempProductList = productListStr;
    }
    
    [self save];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kTemplateListServiceSucess object:nil];
}

#pragma mark --
#pragma mark Get Methods

- (NSArray*) getCustomerdetailsWithQuotes:(BOOL)isQuoteYes andID:(NSString*)customerId
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Customers"
                                              inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    [request setReturnsDistinctResults:YES];
    if (isQuoteYes)
    {
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"customer_quotes = %@", [NSNumber numberWithBool:isQuoteYes]];
        [request setPredicate:predicate];
    }
    
    if (customerId)
    {
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"customer_id contains[cd] %@",customerId];
        [request setPredicate:predicate];
    }
    
    //NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"customer_firstname" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"customer_createdDate" ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
    
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
    NSError* error;
    NSArray* resultArray = [_managedObjectContext executeFetchRequest:request error:&error];
    return resultArray;
}

- (NSArray*) getTemplatedetails
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Templates"];
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
}

- (NSArray*) getItemWithId:(NSString*)itemID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_id == %@", itemID];
    
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
}



- (NSArray*) getEstimateItemWithId:(NSString*)itemID
{
    NSInteger intItemID = [itemID integerValue];
    NSNumber* nsnumberItemID = [NSNumber numberWithInteger:intItemID];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"EstimateItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", nsnumberItemID];

    NSArray *results = [_managedObjectContext executeFetchRequest:request error:nil];

    NSLog(@"The results are: %@", results);
    
    return results;
}

- (NSArray*) getInvoiceItemsWithInvoiceID: (NSNumber*) invoiceID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"InvoiceItem"];
    request.predicate = [NSPredicate predicateWithFormat:@"item_teplateID == %@", invoiceID];
    
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
}

//need to get tax from the invoiceID
- (double) getTaxWithInvoiceID: (NSNumber*) invoiceID
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Invoice"];
    request.predicate = [NSPredicate predicateWithFormat:@"invoice_id == %@", invoiceID];
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    Invoice* invoiceResult = results[0];
    
    return [invoiceResult.invoice_tax doubleValue];
}


- (NSArray*) getInvoiceItemWithId:(NSString*)itemID
{
    NSArray * splitIDs = [itemID componentsSeparatedByString:@","];
    NSMutableArray* cleanIDs = [[NSMutableArray alloc] init];
    NSString* tempString;
    
    for (NSInteger z = 0; z < splitIDs.count; z++) {
        tempString = splitIDs[z];
        tempString = [tempString stringByReplacingOccurrencesOfString:@" " withString:@""];
        tempString = [tempString stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        if (![tempString isEqualToString:@""]) {
            [cleanIDs addObject:tempString];
        }
    }

    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"InvoiceItem"];
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    InvoiceItem* result;
    NSString* temporaryItemID;
    NSArray* finalArray;
    NSMutableArray* multipleResults = [[NSMutableArray alloc] init];
    NSString* singleID;
    
    if (cleanIDs.count == 1) {
        singleID = cleanIDs[0];
        
        for (NSInteger x = 0; x < results.count; x++)
        {
            result = results[x];
            temporaryItemID = result.item_id;
            
            if ([temporaryItemID isEqualToString:singleID] )
            {
                finalArray = [NSArray arrayWithObjects:results[x], nil];

                return finalArray;
            }
        }
    }
    
    else
    {
        for (NSInteger y = 0; y < cleanIDs.count; y++)
        {
            for (NSInteger x = 0; x < results.count; x++)
            {
                singleID = cleanIDs[y];
                
                result = results[x];
                temporaryItemID = result.item_id;
                
                if ([temporaryItemID isEqualToString:singleID] )
                {
                    //finalArray = [NSArray arrayWithObjects:results[x], nil];
                    [multipleResults addObject:results[x]];
                }
            }
        }
        finalArray = [NSArray arrayWithArray:multipleResults];
        return finalArray;
    }
    return results;
}

- (NSArray*) getEstimatelistWith_ID:(NSString*)estimateID
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Estimate"
                                              inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    [request setReturnsDistinctResults:YES];
    if (estimateID)
    {
        NSLog(@"%@", NSStringFromClass([estimateID class]));
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"estimate_id contains[cd] %@",estimateID];
        [request setPredicate:predicate];
    }
    
    NSError* error;
    NSArray* resultArray = [_managedObjectContext executeFetchRequest:request error:&error];
    return resultArray;
}

- (NSArray*) getEstimatelistWith_ClientID:(NSString*)clientID
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Estimate"
                                              inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    [request setReturnsDistinctResults:YES];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"estimate_ClientId contains[cd] %@",clientID];
    [request setPredicate:predicate];
    
    NSError* error;
    NSArray* resultArray = [_managedObjectContext executeFetchRequest:request error:&error];
    return resultArray;
}

- (NSArray*) getInvoielistWith_ClientID:(NSString*)clientID
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Invoice"
                                              inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    [request setReturnsDistinctResults:YES];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"invoice_ClientId contains[cd] %@",clientID];
    [request setPredicate:predicate];
    
    NSError* error;
    NSArray* resultArray = [_managedObjectContext executeFetchRequest:request error:&error];
    return resultArray;
}


- (NSArray*) getInvoicelistWith_ID:(NSString*)invoiceId
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Invoice"];
    
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    return results;
}

- (NSArray*) getInvoiceWithID: (NSString*) invoiceID;
{
    NSInteger intID = [invoiceID integerValue];
    NSNumber* numberOfInvoice = [NSNumber numberWithInteger:intID];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Invoice"];
    request.predicate = [NSPredicate predicateWithFormat:@"invoice_id == %@", numberOfInvoice];
  
    NSArray* results = [_managedObjectContext executeFetchRequest:request error:nil];
    
    return results;
    
}

- (NSArray*) getCompanySettingData
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"CompanySetting"
                                              inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    [request setReturnsDistinctResults:YES];
    
    NSError* error;
    NSArray* resultArray = [_managedObjectContext executeFetchRequest:request error:&error];
    return resultArray;
}

- (BOOL) isCustomerAlreadyExist:(NSString*)customerId
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Customers"
                                              inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"customer_id contains[cd] %@",customerId];
    [request setPredicate:predicate];
    
    NSError* error;
    NSArray* resultArray = [_managedObjectContext executeFetchRequest:request error:&error];
    
    return (resultArray.count > 0);
}

// not used?
//- (void)updateTemplate:(Templates *)template
//{
//    
//    
//    [self save];
//}

@end