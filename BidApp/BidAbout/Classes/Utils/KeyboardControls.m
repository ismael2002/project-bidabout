//
//  KeyboardControls.m
//  KeyBoardProject
//
//  Copyright (c) 2012 Min. All rights reserved.
//

#import "KeyboardControls.h"

enum 
{
    KeyboardControlsIndexPrevious,
    KeyboardControlsIndexNext
};

@interface KeyboardControls (private)

- (IBAction)segmentedControlPreviousNextChangedValue:(id)sender;
- (IBAction)buttonDonePressed:(id)sender;
- (void)previous;
- (void)next;
- (void)done;
- (void)enableDisableButtonsForActiveTextField:(id)textField;

@end

@implementation KeyboardControls

@synthesize delegate;
@synthesize textFieldsArray;
@synthesize activeTextField = _activeTextField;
@synthesize barStyle = _barStyle;
@synthesize previousNextTintColor = _previousNextTintColor;
@synthesize doneTintColor = _doneTintColor;
@synthesize previousTitle = _previousTitle;
@synthesize nextTitle = _nextTitle;
@synthesize barTitle = _barTitle;

/* Initialize */
- (id)init
{
    if (self = [super init])
    {
        // Set frame
        CGRect frame = CGRectMake(0, 0, 320, 44);
        self.frame = frame;
        
        self.barStyle = UIBarStyleBlackTranslucent;
        self.previousNextTintColor = [UIColor blackColor];
        self.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
        
        self.previousTitle = @"Previous";
        self.nextTitle = @"Next";
        self.barTitle = @"";
        
        _navBar = [[UINavigationBar alloc] initWithFrame:self.frame];
        _navBar.barStyle = self.barStyle;
        _navBar.backgroundColor = [UIColor clearColor];
        
        _segmentedPreviousNext = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:self.previousTitle, self.nextTitle, nil]];
        _segmentedPreviousNext.tintColor = self.previousNextTintColor;
        //_segmentedPreviousNext.segmentedControlStyle = UISegmentedControlStyleBar;
        _segmentedPreviousNext.momentary = YES;
        [_segmentedPreviousNext addTarget:self action:@selector(segmentedControlPreviousNextChangedValue:) forControlEvents:UIControlEventValueChanged];
        
        _navItem = [[UINavigationItem alloc] initWithTitle:self.barTitle];
        
        UIBarButtonItem *barSegment = [[UIBarButtonItem alloc] initWithCustomView:_segmentedPreviousNext];
        
        _buttonDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(buttonDonePressed:)];
        
        _buttonDone.tintColor = self.doneTintColor;
        
        _navItem.rightBarButtonItem = _buttonDone;
        _navItem.leftBarButtonItem = barSegment;
        
        // Add items to bar
        _navBar.items  = [NSArray arrayWithObjects:_navItem, nil];
        
        // Set autoresizing (when phone rotates)
        _navBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        // Add toolbar to self
        [self addSubview:_navBar];
    }
    return self;
}

- (void) previous
{
    NSInteger index = (NSInteger)[self.textFieldsArray indexOfObject:self.activeTextField];
    
    if (index > 0)
    {
        NSInteger previous = index - 1;
        self.activeTextField = [self.textFieldsArray objectAtIndex:previous];
        
        [self enableDisableButtonsForActiveTextField:self.activeTextField];
        
        if ([self.delegate respondsToSelector:@selector(keyboardControlsPreviousNextPressed:withDirection:andActiveTextField:)])
            [self.delegate keyboardControlsPreviousNextPressed:self withDirection:KeyboardControlsDirectionPrevious andActiveTextField:self.activeTextField];
    }
}

- (void) next
{
    NSInteger index = (NSInteger)[self.textFieldsArray indexOfObject:self.activeTextField];

    if (index < self.textFieldsArray.count - 1)
    {
        NSInteger next = index + 1;
        self.activeTextField = [self.textFieldsArray objectAtIndex:next];
        
        [self enableDisableButtonsForActiveTextField:self.activeTextField];
        
        if ([self.delegate respondsToSelector:@selector(keyboardControlsPreviousNextPressed:withDirection:andActiveTextField:)])
            [self.delegate keyboardControlsPreviousNextPressed:self withDirection:KeyboardControlsDirectionNext andActiveTextField:self.activeTextField];
    }
}

- (void) done
{
    if ([self.delegate respondsToSelector:@selector(keyboardControlsDonePressed:)])
        [self.delegate keyboardControlsDonePressed:self];
}

- (void)enableDisableButtonsForActiveTextField:(id)textField
{
    NSInteger index = (NSInteger)[self.textFieldsArray indexOfObject:textField];
    
    // Check if "Previous" button should be enabled
    if (index > 0)
        [_segmentedPreviousNext setEnabled:YES forSegmentAtIndex:0];
    else
        [_segmentedPreviousNext setEnabled:NO forSegmentAtIndex:0];
    
    // Check if "Next" button should be enabled
    if (index < self.textFieldsArray.count - 1)
        [_segmentedPreviousNext setEnabled:YES forSegmentAtIndex:1];
    else
        [_segmentedPreviousNext setEnabled:NO forSegmentAtIndex:1];
}

#pragma mark -
#pragma mark Getters and Setters

- (void) setActiveTextField:(id)activeTextField
{
    _activeTextField = activeTextField;
    [self enableDisableButtonsForActiveTextField:self.activeTextField];
}

#pragma mark -
#pragma mark IBActions

/* Previous / Next segmented control changed value */
- (IBAction) segmentedControlPreviousNextChangedValue:(id)sender
{
    switch ([(UISegmentedControl *)sender selectedSegmentIndex]) {
        case KeyboardControlsIndexPrevious:
            [self previous];
            break;
        case KeyboardControlsIndexNext:
            [self next];
            break;
        default:
            break;
    }
}

- (void) buttonDonePressed:(id)sender
{
    [self done];
}

#pragma mark -
#pragma mark Settings

- (void)setBarStyle:(UIBarStyle)barStyle
{
    _barStyle = barStyle;
    _navBar.barStyle = self.barStyle;
}

- (void) setPreviousNextTintColor:(UIColor *)previousNextTintColor
{
    _previousNextTintColor = previousNextTintColor;
    _segmentedPreviousNext.tintColor = self.previousNextTintColor;
}

- (void) setDoneTintColor:(UIColor *)doneTintColor
{
    _doneTintColor = doneTintColor;
    _buttonDone.tintColor = self.doneTintColor;
}

- (void) setPreviousTitle:(NSString *)previousTitle
{
    _previousTitle = previousTitle;
    [_segmentedPreviousNext setTitle:self.previousTitle forSegmentAtIndex:KeyboardControlsIndexPrevious];
}

- (void) setNextTitle:(NSString *)nextTitle
{
    _nextTitle = nextTitle;
    [_segmentedPreviousNext setTitle:self.nextTitle forSegmentAtIndex:KeyboardControlsIndexNext];
}

- (void) setBarTitle:(NSString *)barTitle
{
    _barTitle = barTitle;
    [_navItem setTitle:self.barTitle];
}
@end
