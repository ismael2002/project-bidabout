//
//  AppPrefData.h
//
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppPrefData : NSObject 
{
}

@property (nonatomic, retain) NSString* emailId;
@property (nonatomic, retain) NSString* password;
@property (nonatomic, retain) NSString* accesToken;
@property (nonatomic, retain) NSString* memberID;
@property (nonatomic, retain) NSNumber* estimatelastID;
@property (nonatomic, retain) NSNumber* invoicelastID;
@property (nonatomic, retain) NSNumber* currentTax;

@property (nonatomic) BOOL isLogin;
@property (nonatomic) BOOL isFirstTimeLogedIn;
@property (nonatomic) BOOL isDefualttaxOn;
@property (nonatomic) BOOL isIncludeTermsOn;
@property (nonatomic) BOOL isRememberMe;

+ (AppPrefData*) sharedObject;

- (void)numberOfPendingRequests:(void (^)(NSInteger requestCount))success clear:(BOOL)clear;
- (void)numberOfAcceptedEstimates:(void (^)(NSInteger estimateCount))success clear:(BOOL)clear;
- (void) loadAllData;
- (void) saveAllData;

@end

#define _gAppPrefData [AppPrefData sharedObject]
