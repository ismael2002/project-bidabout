//
//  DataManager.h
//

#import "WebServiceHelper.h"
#import "CoreDataManger.h"
#import "Templates.h"
#import "InvoiceItem.h"

@class Item;
@class Invoice;
@class Estimate;

@interface DataManager : NSObject <WebServiceHelperDelegate>
{
    WebServiceHelper*  _webServiceHelper;
    CoreDataManger* _coreDataManager;
}

@property (strong, nonatomic) NSMutableArray* customerList;

+ (DataManager*) sharedObject;

- (void) deleteUserData;

- (void) sendRequestForSynsc;
- (void) sendRequestForRegistration:(NSData*)dataForPost;
- (void) sendRequestForLogIn;
- (void) sendRequestForLogInContext;
- (void) sendRequestForMemberList;
- (void) sendRequestForCustomerList;
- (void) sendRequestForCustomerWithID:memberId;
- (void) sendRequestForTempleatesTypes;
- (void) sendRequestForTemplates;
- (void) sendRequestForTempleatesWith:memberId andTemplate:(NSInteger)templateId;
- (void) sendRequestForQuotes;
- (void) sendRequestForCreateCustomer:(NSData*)dataForPost;

- (void) updatePaypalUserName:(NSString*)userName emailProvider:(NSString*) email domain:(NSString*)domain;
- (void) changeTermsAndConditions: (NSString*) terms;
- (void) insertNewItemFromTempalte:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary newTemplateItem: (BOOL) isNewtemplate;
- (void) addInvoiceItemToProductList: (NSDictionary*) itemDict;
//- (void) addNewItem:(NSDictionary*)itemDict;
- (void) addNewItem:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary;
- (void) addNewEstimateItem:(NSDictionary*)itemDict;
- (void) addNewInvoiceItem:(NSDictionary*)itemDict;
- (void) addNewInvoiceItem:(NSDictionary*) itemDict withNewInvoiceID: (NSNumber*) invoiceID;
//- (void) addNewEstimate:(NSDictionary*)estimateDict;
- (void) insertNewEstimate:(NSDictionary*)estimateDict replaceEstimateID: (BOOL) replaceOldID;
- (void) replaceEstimateItem:(NSDictionary*)itemDict withNewEstimateID: (NSNumber*) estimateID;
- (void) estimateAcceptedFromParse: (PFObject*) estimate;
- (void) addNewInvoice:(NSDictionary*)invoiceDict replaceTemplateID: (BOOL) replaceOldID;
- (void) addUpdateCompanyData:(NSDictionary*)companyinfoDict;
//- (void) addNewTemplate: (NSDictionary*) templateDict;
- (void) addNewTemplate:(NSDictionary*) templateDict replaceTemplateID: (BOOL) replaceOldID newTemplateID: (NSNumber*) newID;

- (void) updateTemplateItem: (Item*) oldTemplateItem withUpdateTemplateItem: (NSMutableDictionary*) updatedTemplate;

- (void) updateCustomerDetail:(NSDictionary*)custDict;
- (void) updateProductItem:(NSDictionary*)itemDict;
- (void) updateEstimateItem:(NSDictionary*)itemDict;
- (void) updateInvoiceItem:(NSDictionary*)itemDict;

//- (void) updateInvoice:(NSDictionary*)invoiceDict;
- (void) changeActiveInactiveTempalte: (NSNumber*) template_ID;
- (void) convertEstimateToInvoice: (NSDictionary*) estimateDictionary withItems: (NSArray*) itemArray;

- (void) deleteItemFromTemplate: (NSNumber*) templateID ItemTitle: (NSString*) itemTitle;
- (void) deleteInvoiceItemsFromInvoice:(NSNumber*) invoiceID;
- (void) deleteProductItem:(NSString*)itemID;
- (void) deleteEstimateItemWithID:(NSString*)itemID andTitle: (NSString*) title;
- (void) deleteEstimate:(Estimate *)estimate;
//- (void) deleteInvoiceItem:(NSString*)itemID;
- (void) deleteInvoice:(Invoice *)invoice;
- (void) deleteCompanydata;
- (void) deleteCustomerFromeClientList:(Customers*) custObj;
- (void) deleteItemsFromCurrentTemplate;
- (void) deleteItemsFromCurrentEstimate;
- (void) deleteItemsFromCurrentInvoice;
- (void) deleteInvoiceItem: (InvoiceItem*) item;

- (NSArray*) getCustomerDetails:(BOOL)isQuoteYes andCustomerId:(NSString*)customerIDStr;
- (NSArray*) getTemplatedetails;
- (NSArray*) getItemWithID:(NSString*)itemID;
- (NSArray*) getEstimateItemWithID:(NSString*)itemID;
- (NSArray*) getInvoiceItemWithID:(NSString*)itemID;
- (NSArray*) getEstimatelistWithID:(NSString*)estimateID;
- (NSArray*) getEstimatelistWithClientID:(NSString*)ClientID;
- (NSArray*) getInvoielistWithClientID:(NSString*)ClientID;
- (NSArray*) getItems;
- (NSArray*) getItemResults;
- (NSArray*) getFilteredItemResults;
- (NSArray*) getItemsFromCurrentTemplate;
- (NSArray*) getItemsFromExistingTemplate: (NSNumber*) templateID;
- (NSArray*) getEstimateItemsFromCurrentEstimate;
- (NSArray*) getInvoiceItemsFromCurrentInvoice;
- (NSArray*) getInvoiceItemsWithInvoiceID: (NSNumber*) invoiceID;
- (double) getTaxWithInvoiceID: (NSNumber*) invoiceID;
- (NSArray*) getInvoicelistWithId:(NSString*)invocieID;
- (NSArray*) getCompanyData;
- (NSArray*) getInvoiceWithID: (NSString*) invoiceID;
//- (NSNumber*) getPaidAmount: (NSNumber*) invoiceID invoiceItemTitle: (NSString*) invoiceItem;
-(NSDictionary*) getPaypalInformation;

- (void) saveCustomerDeail:(NSArray*) customerDetail;

- (NSArray*) getTemplatedetailsFromLocal;
- (void) toInitialTemplateListResponse;

//- (void)updateTemplate:(Templates *)template;

@end

#define _gAppData [DataManager sharedObject]
