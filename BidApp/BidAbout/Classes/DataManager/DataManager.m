//
//  DataManager.m
//

#import "DataManager.h"
#import "WebServiceHelper.h"
#import "SBJson.h"
#import "Item.h"

static DataManager* gDataMgr = nil;

@implementation DataManager

+ (DataManager*) sharedObject
{
	if (!gDataMgr)
	{
		gDataMgr = [[DataManager alloc] init];
	}
	return gDataMgr;
}

- (id) init
{
    self = [super init];
    {
        _webServiceHelper = [[WebServiceHelper alloc] init];
        _webServiceHelper.delegate = self;
        _coreDataManager = [[CoreDataManger alloc] init];
        _customerList = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark --
#pragma mark DeletuserData

- (void) deleteUserData
{
    [_coreDataManager clearDatabase];
}

#pragma mark -- 
#pragma mark WebServices request

- (void) sendRequestForSynsc
{
//    [self sendRequestForCustomerWithID:[_gAppPrefData.memberID  integerValue]];
//    [self sendRequestForTemplates];
}

- (void) sendRequestForRegistration:(NSData*)dataForPost
{
    [_webServiceHelper getAppRegistrionResponce:dataForPost];
}

- (void) sendRequestForLogIn
{
    [_webServiceHelper getLogInResponce];
}

- (void) sendRequestForLogInContext
{
    [_webServiceHelper getLogInServiceContext];
}

- (void) sendRequestForMemberList
{
    [_webServiceHelper getMemberListResponse];
}

- (void) sendRequestForCustomerList
{
//    [_webServiceHelper getCustomerListResponse];
}

- (void) sendRequestForCustomerWithID:(NSString *)memberId
{
//    [_webServiceHelper getCustomerListWithID:memberId];
}

- (void) sendRequestForTempleatesTypes
{
    [_webServiceHelper getTemplateTypesResponse];
}

- (void) sendRequestForTemplates 
{
//    [_webServiceHelper getTemplatesResponse];
//	[self toInitialTemplateListResponse];
}

- (void) sendRequestForTempleatesWith:(NSString *)memberId andTemplate:(NSInteger)templateId
{
    [_webServiceHelper getTemplatesWith:memberId  andTemplate:templateId];
}

- (void) sendRequestForQuotes
{
    [_webServiceHelper getQuotes];
}

- (void) sendRequestForCreateCustomer:(NSData*)dataForPost
{
    [_webServiceHelper postNewCustomerData:dataForPost];
}

#pragma mark -- 
#pragma mark Private Parsing Methods

- (void) toParseRegistrationResponse:(NSString*)response
{
    // Parse here response and set isSuccesfull or not
    NSDictionary* dict = [UIUtils jsonData:[response dataUsingEncoding:NSUTF8StringEncoding]];
    if (dict)
    {
        NSDictionary* errorDict = [dict objectForKey:@"error"];
        if (errorDict)
        {
            [UIUtils messageAlert:[errorDict objectForKey:@"message"] title:@"Error" delegate:nil];
            return;
        }
        
        NSDictionary* outComeDict = [dict objectForKey:@"Outcome"];
        if (outComeDict)
        {
            NSNumber* status = [outComeDict valueForKey:@"Success"];
            if ([status boolValue])
            {
//                _gAppPrefData.isLogin = YES;
//                [_gAppPrefData saveAllData];
                
                BOOL isSucessfull = YES; // For test YES
                
                [[NSNotificationCenter defaultCenter] postNotificationName:kSignUpSucessfullNotification object:[NSNumber numberWithBool:isSucessfull]];
            }
            else
            {
                NSArray* msgArray = [outComeDict valueForKey:@"Messages"];
                if (msgArray.count > 0)
                {
                    NSDictionary* dict = [msgArray objectAtIndex:0];
                    [UIUtils messageAlert:[dict objectForKey:@"Description"] title:@"Error" delegate:nil];
                    return;
                }
                [UIUtils messageAlert:kSignUpErrorMsg title:@"Error" delegate:nil];
            }
        }
        else
        {
            [UIUtils messageAlert:kSignUpErrorMsg title:@"Error" delegate:nil];
        }
        
    }
}

- (void) toParseLogInResponse:(NSString*)response
{
    // Parse here response and set isSuccesfull or not
    NSDictionary* dict = [UIUtils jsonData:[response dataUsingEncoding:NSUTF8StringEncoding]];
    if (dict)
    {
        NSDictionary* errorDict = [dict objectForKey:@"error"];
        if (errorDict)
        {
            [UIUtils messageAlert:[errorDict objectForKey:@"message"] title:@"Error" delegate:nil];
            return;
        }
        
        NSString* accesToken = [dict objectForKey:@"access_token"];
//        NSString* refreshToken = [dict objectForKey:@"refresh_token"];
//        NSString* userGuide = [dict objectForKey:@"user_guid"];
//        NSString* scope = [dict objectForKey:@"scope"];
        
        _gAppPrefData.accesToken = accesToken;
        [_gAppPrefData saveAllData];
        
        [self sendRequestForLogInContext];
    }
}

- (void) toParseGetLoginContextResponse:(NSString*)response
{
    // Parse here response and set isSuccesfull or not
    NSDictionary* dict = [UIUtils jsonData:[response dataUsingEncoding:NSUTF8StringEncoding]];
    if (dict)
    {
        NSDictionary* errorDict = [dict objectForKey:@"error"];
        if (errorDict)
        {
            [UIUtils messageAlert:[errorDict objectForKey:@"message"] title:@"Error" delegate:nil];
            return;
        }
        
        NSString* memberID = [dict objectForKey:@"MemberId"];
        if (memberID.length > 0)
        {                        
            _gAppPrefData.memberID = memberID;
//            _gAppPrefData.isLogin = YES;
//            [_gAppPrefData saveAllData];
            
            BOOL isSucessfull = YES; // For test YES
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kLogInSucessfullNotification object:[NSNumber numberWithBool:isSucessfull]];
        }
        else
        {
            [UIUtils messageAlert:kErrorMsg title:@"Error" delegate:nil];
        }

    }
    else
    {
        [UIUtils messageAlert:kErrorMsg title:@"Error" delegate:nil];
    }
}

- (void) toParseCustomerListResponse:(NSString*)response
{
    NSArray* memberList = [UIUtils jsonData:[response dataUsingEncoding:NSUTF8StringEncoding]];
    if (memberList.count > 0)
    {
        [_coreDataManager storeCustomerDetail:memberList];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCustomerListServiceSucess object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kCustomerListServiceFail object:nil];
    }
}

- (void) saveCustomerDeail:(NSArray*) customerDetail
{
	if (customerDetail.count > 0)
    {
        [_coreDataManager storeCustomerDetail:customerDetail];        
    }
}

- (void) toParseCreateCustomerServiceResponse:(NSString*)response
{
    NSDictionary* dict = [UIUtils jsonData:[response dataUsingEncoding:NSUTF8StringEncoding]];
    if (dict)
    {
        NSDictionary* errorDict = [dict objectForKey:@"error"];
        if (errorDict)
        {
            [UIUtils messageAlert:[errorDict objectForKey:@"message"] title:@"Error" delegate:nil];
            return;
        }
        
        NSArray* memberArray = [NSArray arrayWithObjects:dict,nil];
        [_coreDataManager storeCustomerDetail:memberArray];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCustomerCreateServiceSucess object:nil];

    }
    
//    if (dict)
//    {
//        NSDictionary* outComeDict = [dict objectForKey:@"Outcome"];
//        if (outComeDict)
//        {
//            NSNumber* status = [outComeDict valueForKey:@"Success"];
//            if ([status boolValue])
//            {
//                [[NSNotificationCenter defaultCenter] postNotificationName:kMemberCreateServiceSucess object:nil];
//            }
//            else
//            {
//                NSArray* msgArray = [outComeDict valueForKey:@"Messages"];
//                if (msgArray.count > 0)
//                {
//                    NSDictionary* dict = [msgArray objectAtIndex:0];
//                    [UIUtils messageAlert:[dict objectForKey:@"Description"] title:@"Error" delegate:nil];
//                    return;
//                }
//                [UIUtils messageAlert:kSignUpErrorMsg title:@"Error" delegate:nil];
//            }
//        }
//    }
}

- (void) toParseTemplateListResponse:(NSString*)response
{
    NSArray* templateList = [UIUtils jsonData:[response dataUsingEncoding:NSUTF8StringEncoding]];
    if (templateList.count > 0)
    {
//        [_coreDataManager storeTemplateDetails:templateList];
		[_coreDataManager storeTemplateFromLocalDB:templateList];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kTemplateListServiceFail object:nil];
    }
}

- (void) toInitialTemplateListResponse
{
    NSLog(@"To initial template list response");
    
	NSArray* templateList = [self getTemplatedetailsFromLocal];
    NSLog(@"The template list is: %@", templateList);
    
    if (templateList.count > 0)
    {
//        [_coreDataManager storeTemplateDetails:templateList];
		[_coreDataManager storeTemplateFromLocalDB:templateList];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kTemplateListServiceFail object:nil];
    }
}


#pragma mark --
#pragma mark Insert method

-(void) addInvoiceItemToProductList: (NSDictionary*) itemDict
{
    [_coreDataManager addInvoiceItemToProductList:itemDict];
}

- (void) insertNewItemFromTempalte:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary newTemplateItem: (BOOL) isNewtemplate
{
    [_coreDataManager insertNewItemFromTempalte:itemDictCopy oldItem:oldItemDictionary newTemplateItem:isNewtemplate];
}


//-(void) addNewItem:(NSDictionary*)itemDict
- (void) addNewItem:(NSDictionary*)itemDictCopy oldItem: (Item*) oldItemDictionary
{
    //[_coreDataManager insertNewItem:itemDict];
    [_coreDataManager insertNewItem:itemDictCopy oldItem:oldItemDictionary];
}

- (void) addNewEstimateItem:(NSDictionary*)itemDict
{
    [_coreDataManager insertNewEstimateItem:itemDict];
}

- (void) addNewInvoiceItem:(NSDictionary*)itemDict
{
    [_coreDataManager insertNewInvoiceItem:itemDict];
}

- (void) addNewInvoiceItem:(NSDictionary*) itemDict withNewInvoiceID: (NSNumber*) invoiceID;
{
    [_coreDataManager addNewInvoiceItem:itemDict withNewInvoiceID:invoiceID];
}

//- (void) addNewEstimate:(NSDictionary*)estimateDict
//{
//    [_coreDataManager insertNewEstimate:estimateDict];
//}

- (void) insertNewEstimate:(NSDictionary*)estimateDict replaceEstimateID: (BOOL) replaceOldID
{
    [_coreDataManager insertNewEstimate:estimateDict replaceEstimateID:replaceOldID];
}

- (void) estimateAcceptedFromParse: (PFObject*) estimate
{
    [_coreDataManager estimateAcceptedFromParse:estimate];
}

- (void) replaceEstimateItem:(NSDictionary*)itemDict withNewEstimateID: (NSNumber*) estimateID
{
    [_coreDataManager replaceEstimateItem:itemDict withNewEstimateID:estimateID];
}

- (void) addNewInvoice:(NSDictionary*)invoiceDict replaceTemplateID: (BOOL) replaceOldID
{
    [_coreDataManager insertNewInvoice:invoiceDict replaceTemplateID:replaceOldID];
}

- (void) addUpdateCompanyData:(NSDictionary*)companyinfoDict
{
    [_coreDataManager addUpdateCompanySettingData:companyinfoDict];
}

- (void) addNewTemplate:(NSDictionary*) templateDict replaceTemplateID: (BOOL) replaceOldID newTemplateID: (NSNumber*) newID
{
    [_coreDataManager insertNewTemplate:templateDict replaceTemplateID:replaceOldID newTemplateID:newID];
}

#pragma mark --
#pragma mark Update Methods

- (void) updatePaypalUserName:(NSString*)userName emailProvider:(NSString*) email domain:(NSString*)domain
{
    [_coreDataManager updatePaypalUserName:userName emailProvider:email domain:domain];
}

- (void) changeTermsAndConditions: (NSString*) terms
{
    [_coreDataManager changeTermsAndConditions:terms];
    
}

- (void) updateTemplateItem: (Item*) oldTemplateItem withUpdateTemplateItem: (NSMutableDictionary*) updatedTemplate
{
    [_coreDataManager updateTemplateItem:oldTemplateItem withUpdateTemplateItem:updatedTemplate];
}

//- (void)updateTemplate:(Templates *)template
//{
//    [_coreDataManager updateTemplate:template];
//}

- (void) updateCustomerDetail:(NSDictionary*)custDict
{
    [_coreDataManager updateCustomerWithID:custDict];
}

- (void) updateProductItem:(NSDictionary*)itemDict
{
    [_coreDataManager updateProductwithItem:itemDict];
}

- (void) updateEstimateItem:(NSDictionary*)itemDict
{
    [_coreDataManager updateEstimateItemwithDict:itemDict];
}

- (void) updateInvoiceItem:(NSDictionary*)itemDict
{
    [_coreDataManager updateInvoiceItemWithDict:itemDict];
}

//-(void) updateEstimate:(NSDictionary*)estimateDict
//{
//    [_coreDataManager updatedEstimateWithEstimate:estimateDict];
//}

//-(void) updateInvoice:(NSDictionary*)invoiceDict
//{
//    [_coreDataManager updatedInvoiceWithInvoice:invoiceDict];
//}

- (void) changeActiveInactiveTempalte: (NSNumber*) template_ID
{
    [_coreDataManager changeActiveInactiveTempalte:template_ID];
}

- (void) convertEstimateToInvoice: (NSDictionary*) estimateDictionary withItems: (NSArray*) itemArray
{
    [_coreDataManager convertEstimateToInvoice:estimateDictionary withItems:itemArray];
}


#pragma mark --
#pragma mark Delete  Methods

- (void) deleteInvoiceItemsFromInvoice:(NSNumber*) invoiceID
{
    [_coreDataManager deleteInvoiceItemsFromInvoice:invoiceID];
}

- (void) deleteProductItem:(NSString*)itemID
{
    [_coreDataManager deleteProductItemWithID:itemID];
}

- (void) deleteEstimateItemWithID:(NSString*)itemID andTitle: (NSString*) title
{
    [_coreDataManager deleteEstimateItemWithID:itemID andTitle:title];
}

- (void) deleteEstimate:(NSString*)estimateID
{
	[_coreDataManager deleteEstimate:estimateID];
}


- (void) deleteInvoiceItem:(InvoiceItem*)itemID
{
    [_coreDataManager deleteInvoiceItem:itemID];
}


- (void) deleteInvoice:(Invoice *)invoice
{
	[_coreDataManager deleteInvoice:invoice];
}

- (void) deleteCompanydata
{
    [_coreDataManager deleteCompanyData];
}

- (void) deleteCustomerFromeClientList:(Customers*) custObj
{
	[_coreDataManager deleteObejctFromClientList:custObj];
}

#pragma mark --
#pragma mark Get CoreData Detial Methods

- (NSArray*) getCustomerDetails:(BOOL)isQuoteYes andCustomerId:(NSString*)customerIDStr
{
    return [_coreDataManager getCustomerdetailsWithQuotes:isQuoteYes andID:customerIDStr];
}

- (NSArray*) getTemplatedetails
{
    return [_coreDataManager getTemplatedetails];
}

- (NSArray*) getItemWithID:(NSString*)itemID
{
    return [_coreDataManager getItemWithId:itemID];
}

-(NSArray*) getItemsFromCurrentTemplate
{
    return [_coreDataManager getItemsWithCurrentTemplate ];
}

- (void) deleteItemFromTemplate: (NSNumber*) templateID ItemTitle: (NSString*) itemTitle
{
    [_coreDataManager deleteItemFromTemplate:templateID ItemTitle:itemTitle];
}

- (void) deleteItemsFromCurrentTemplate
{
    return [_coreDataManager deleteItemsFromCurrentTemplate];
}

- (void) deleteItemsFromCurrentEstimate
{
    return [_coreDataManager deleteItemsFromCurrentEstimate];
}

- (void) deleteItemsFromCurrentInvoice
{
    return [_coreDataManager deleteItemsFromCurrentInvoice];
}



-(NSArray*) getItemsFromExistingTemplate: (NSNumber*) templateID
{
    return [_coreDataManager getItemsFromExistingTemplate:templateID];
}

- (NSArray*) getItems
{
    return [_coreDataManager getItems];
}

- (NSArray*) getEstimateItemWithID:(NSString*)itemID
{
    return [_coreDataManager getEstimateItemWithId:itemID];
}

- (NSArray*) getInvoiceItemWithID:(NSString*)itemID
{
    return [_coreDataManager getInvoiceItemWithId:itemID];
}




- (NSArray*) getInvoiceItemsWithInvoiceID: (NSNumber*) invoiceID
{
    return [_coreDataManager getInvoiceItemsWithInvoiceID:invoiceID];
}

//get tax info
- (double) getTaxWithInvoiceID: (NSNumber*) invoiceID
{
    return [_coreDataManager getTaxWithInvoiceID:invoiceID];
}

- (NSArray*) getEstimatelistWithID:(NSString*)estimateID
{
    return [_coreDataManager getEstimatelistWith_ID:estimateID];
}

- (NSArray*) getEstimatelistWithClientID:(NSString*)ClientID
{
    return [_coreDataManager getEstimatelistWith_ClientID:ClientID];
}

- (NSArray*) getInvoielistWithClientID:(NSString*)ClientID
{
    return [_coreDataManager getInvoielistWith_ClientID:ClientID];
}

- (NSArray*) getInvoicelistWithId:(NSString*)invocieID
{
    return [_coreDataManager getInvoicelistWith_ID:invocieID];
}

- (NSArray*) getCompanyData
{
    return [_coreDataManager getCompanySettingData];
}

- (NSArray*) getItemResults
{
    return [_coreDataManager getItemResults];
}

-(NSArray*) getFilteredItemResults
{
    return [_coreDataManager getFilteredItemResults];
}

- (NSArray*) getEstimateItemsFromCurrentEstimate
{
    return [_coreDataManager getEstimateItemsFromCurrentEstimate];
}

- (NSArray*) getInvoiceItemsFromCurrentInvoice
{
    return [_coreDataManager getInvoiceItemsFromCurrentInvoice];
}

- (NSArray*) getInvoiceWithID: (NSString*) invoiceID;
{
    return [_coreDataManager getInvoiceWithID:invoiceID];
    
}

//- (NSNumber*) getPaidAmount: (NSNumber*) invoiceID invoiceItemTitle: (NSString*) invoiceItem
//{
//    return [_coreDataManager getPaidAmount:invoiceID invoiceItemTitle:invoiceItem];
//}

-(NSDictionary*) getPaypalInformation
{
    return [_coreDataManager getPaypalInformation];
}

#pragma mark --
#pragma mark WebServiceDelegate

- (void) getResponseFromServer: (NSString*) responseStr
                   requestType: (WEB_SERVICE_REQUEST_TYPE) theRequestType
                 requestStatus: (WEB_SERVICE_OPERATION_STATUS) theRequestStatus
{
    NSLog(@"Get response from server");
    
    if (WEB_SERVICE_STATUS_FAIL == theRequestStatus)
	{
		[UIUtils messageAlert:@"Network error." title:kApplicationName delegate:nil];
	}
    else
    {
        switch(theRequestType)
        {
            case WS_REQUEST_NONE:
            {
                [UIUtils messageAlert:@"Network error." title:kApplicationName delegate:nil];
            }
                break;
                
            case WS_REQUEST_GET_REGISTRATION:
            {
				[self toParseRegistrationResponse:responseStr];
            }
                break;
                
            case WS_REQUEST_GET_LOGIN:
            {
                [self toParseLogInResponse:responseStr];
            }
                break;
                
            case WS_REQUEST_GET_LOGIN_CONTEXT:
            {
                [self toParseGetLoginContextResponse:responseStr];
            }
                break;
                
            case WS_REQUEST_GET_MEMBERS:
            {
                NSLog(@"Parsing Work needed");
            }
                break;
                
            case WS_REQUEST_GET_CUSTOMERS:
            {
//                [self toParseCustomerListResponse:responseStr];
            }
                break;
                
            case WS_REQUEST_GET_CUSTOMERSWITHID:
            {
				//[self toParseCustomerListResponse:responseStr];
            }
                break;
                
            case WS_REQUEST_GET_TEMPLATETYPE:
            {
                NSLog(@"Parsing Work needed");
            }
                break;
                
            case WS_REQUEST_GET_TEMPLATES:
            {
//                [self toParseTemplateListResponse:responseStr];
            }
                break;
                
            case WS_REQUEST_GET_TEMPLATESWITHID:
            {
                NSLog(@"Parsing Work needed");
            }
                break;
                
            case WS_REQUEST_GET_QUOTES:
            {
                NSLog(@"Parsing Work needed");
            }
                break;
                
            case WS_REQUEST_POST_CUSTOMER:
            {
                [self toParseCreateCustomerServiceResponse:responseStr];
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark -- 

- (NSArray*) getTemplatedetailsFromLocal
{
    NSLog(@"Get template details from local");
	NSString* path = [[NSBundle mainBundle] pathForResource:@"Template" ofType:@"json"];
	NSData* templetArray = [NSData dataWithContentsOfFile:path];
	NSDictionary* dict = [UIUtils jsonData:templetArray];
	NSArray* array = [dict valueForKey:@"Template"];
	
    NSLog(@"The template dict is: %@", dict);
    
    if (sizeof(void*) == 4) {
        NSLog(@"You're running in 32 bit getTemplateDetailsFromLocal");
        
    } else if (sizeof(void*) == 8) {
        NSLog( @"You're running in 64 bit getTemplateDetailsFromLocal");
    }
    
    
	if(array.count<1)
	{
//		[UIUtils messageAlert:@"There is no templete" title:@"Error" delegate:nil];

	}
	
	return array;
}

@end
