//
//  AppPrefData.m
//
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AppPrefData.h"

static AppPrefData*	sAppPrefData = nil;

@implementation AppPrefData

@synthesize emailId, password, isLogin,estimatelastID,invoicelastID,isDefualttaxOn,currentTax,isRememberMe;

+ (AppPrefData*) sharedObject
{
	if (sAppPrefData == nil)
	{
		sAppPrefData = [[AppPrefData alloc] init];
	}
	return sAppPrefData;
}

- (id) init
{
	if (self = [super init])
	{
		[self loadAllData];
	}
	return self;
}

- (void) loadAppPrefData
{
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	NSDictionary* dictionary = [defaults objectForKey:@"keyPreferenceData"];
	if (dictionary == nil)
	{
		self.emailId =  @"";
		self.password = @"";
        self.accesToken = @"";
		self.isLogin = NO;
        self.isFirstTimeLogedIn = YES;
        self.isDefualttaxOn = YES;
        self.isRememberMe = YES;
        self.memberID = @"";
        self.currentTax = [NSNumber numberWithDouble:kCurrentTax];
        self.estimatelastID = [NSNumber numberWithInteger:1];
        self.invoicelastID = [NSNumber numberWithInteger:1];
	}
	else
	{
		self.emailId = [dictionary objectForKey:@"keyEmailId"];
		self.password = [dictionary objectForKey:@"keyPassword"];
        self.accesToken = [dictionary objectForKey:@"keyAccesToken"];
        self.memberID = [dictionary objectForKey:@"keyMemberId"];
		self.isLogin = [[dictionary objectForKey:@"keyIsLogin"] boolValue];
        self.isRememberMe = [[dictionary objectForKey:@"keyisRememberMe"] boolValue];
        self.isFirstTimeLogedIn = [[dictionary objectForKey:@"keyIsFirstTimeLogIn"] boolValue];
        self.estimatelastID = [dictionary objectForKey:@"keyLastEstimateId"];
        self.invoicelastID = [dictionary objectForKey:@"keyLastInvoiceId"];
        self.currentTax = [dictionary objectForKey:@"keyCurrentTax"];
        self.isDefualttaxOn = [[dictionary objectForKey:@"keyIsDefaultTax"] boolValue];
        self.isIncludeTermsOn = [[dictionary objectForKey:@"includeTerms"] boolValue];
	}
}

- (NSDictionary*) dataAsDictionary
{
	NSMutableDictionary* dictionary = [NSMutableDictionary dictionary];
	[dictionary setObject:self.emailId forKey:@"keyEmailId"];
	[dictionary setObject:self.password forKey:@"keyPassword"];
    [dictionary setObject:self.accesToken forKey:@"keyAccesToken"];
    [dictionary setObject:self.memberID forKey:@"keyMemberId"];
	[dictionary setObject:[NSNumber numberWithBool:self.isLogin] forKey:@"keyIsLogin"];
    [dictionary setObject:[NSNumber numberWithBool:self.isFirstTimeLogedIn] forKey:@"keyIsFirstTimeLogIn"];
    [dictionary setObject:[NSNumber numberWithBool:self.isRememberMe] forKey:@"keyisRememberMe"];
    [dictionary setObject:self.estimatelastID forKey:@"keyLastEstimateId"];
    [dictionary setObject:self.invoicelastID forKey:@"keyLastInvoiceId"];
    [dictionary setObject:self.currentTax forKey:@"keyCurrentTax"];
    [dictionary setObject:[NSNumber numberWithBool:self.isDefualttaxOn]  forKey:@"keyIsDefaultTax"];
    [dictionary setObject:[NSNumber numberWithBool:self.isIncludeTermsOn] forKey:@"includeTerms"];
	return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (void) saveAppPrefData
{
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:[self dataAsDictionary] forKey: @"keyPreferenceData"];
	[defaults synchronize];
}

- (void)numberOfPendingRequests:(void (^)(NSInteger requestCount))success clear:(BOOL)clear
{
    NSLog(@"Number of pending requests");
    NSLog(@"The member id is: %@", _gAppPrefData.memberID);
    
    PFQuery *query = [PFQuery queryWithClassName:@"Quote"];
    [query whereKey:@"memberID" equalTo:_gAppPrefData.memberID];
    [query whereKey:@"isQuote" equalTo:@YES];
    [query whereKey:@"read" notEqualTo:@YES];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSInteger requests = [objects count];
            NSLog(@"%ld new requests", (long)requests);
            if(clear)
            {
                for (PFObject *object in objects) {
                    object[@"read"] = @YES;
                    [object saveInBackground];
                }
            }
            success(requests);
        } else {
            success(0);
        }
    }];
}




//    userEmail = [defaults objectForKey:@"email"];
//    userCode  = [defaults objectForKey:@"code"];
//    @try {
//        if (! [userEmail isEqualToString:@""] && ![userCode isEqualToString:@""]) {
//            PFQuery *query = [PFQuery queryWithClassName:@"Person"];
//            [query setLimit: 1000];
//            [query whereKey:@"username" equalTo:userEmail];
//            [query whereKey:@"key" equalTo:userCode];
//            
//            //[query setLimit: 1000];
//            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                if (!error && ![userCode isEqualToString:@""] && objects.count != 0) {
//                    
//                    [self login];
//                    
//                } else {
//                    NSLog(@"THERE IS NO PREVIOUS LOG IN");
//                }
//            }];
//        }
//    }
//    @catch (NSException *exception) {
//        
//    }



- (void)numberOfAcceptedEstimates:(void (^)(NSInteger estimateCount))success clear:(BOOL)clear
{
    NSLog(@"Number of accepted estimate ------- ");
    
    PFQuery *query = [PFQuery queryWithClassName:@"Estimate"];
    [query whereKey:@"userId" equalTo:[PFUser currentUser].objectId];
    [query whereKey:@"accepted" equalTo:@YES];
    [query whereKey:@"read" notEqualTo:@YES];
    
    NSLog(@"About to find objects");
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSInteger estimates = [objects count];
            NSLog(@"%ld new estimates", (long)estimates);
            NSLog(@"The object is: %@", objects);
            //NSLog(@"The first object is: %@", objects[0]);
            
            
            if (objects.count > 0)
            {
                PFObject* estimate = objects[0];
                
                NSLog(@"the class id of this is: %@",  estimate);
                //NSLog(@"the class id of this is: %@",  [estimate objectForKey:@"clientId"]);
                NSLog(@"about to change acceptance");
                [_gAppData estimateAcceptedFromParse:estimate];
                
                
            }
            
            // Chris or Scott, so sorry this is so lazy
            if(clear)
            {
                for (PFObject *object in objects) {
                    object[@"read"] = @YES;
                    [object saveInBackground];
                }
            }
            success(estimates);
        } else {
            success(0);
        }
    }];
    
}

#pragma mark -

- (void) loadAllData
{
	[self loadAppPrefData];
}

- (void) saveAllData
{
	[self saveAppPrefData];
}

@end
