//
//  EstimateItem.m
//  BidAbout
//
//  Created by Waseem  on 27/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "EstimateItem.h"


@implementation EstimateItem

@dynamic item_amount;
@dynamic item_desc;
@dynamic item_id;
@dynamic item_quantity;
@dynamic item_rate;
@dynamic item_taxAllow;
@dynamic item_teplateID;
@dynamic item_title;

@end
