//
//  EstimateItem.h
//  BidAbout
//
//  Created by Waseem  on 27/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EstimateItem : NSManagedObject

@property (nonatomic, retain) NSString * item_amount;
@property (nonatomic, retain) NSString * item_desc;
@property (nonatomic, retain) NSString * item_id;
@property (nonatomic, retain) NSString * item_quantity;
@property (nonatomic, retain) NSString * item_rate;
@property (nonatomic, retain) NSNumber * item_taxAllow;
@property (nonatomic, retain) NSNumber * item_teplateID;
@property (nonatomic, retain) NSString * item_title;

@end
