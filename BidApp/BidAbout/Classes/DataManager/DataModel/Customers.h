//
//  Customers.h
//  BidAbout
//
//  Created by     on 16/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Customers : NSManagedObject

@property (nonatomic, retain) NSString * customer_addres1;
@property (nonatomic, retain) NSString * customer_addres2;
@property (nonatomic, retain) NSString * customer_city;
@property (nonatomic, retain) NSString * customer_company;
@property (nonatomic, retain) NSString * customer_createdBy;
@property (nonatomic, retain) NSString * customer_createdDate;
@property (nonatomic, retain) NSString * customer_email;
@property (nonatomic, retain) NSString * customer_firstname;
@property (nonatomic, retain) NSString * customer_fullAddress;
@property (nonatomic, retain) NSString * customer_fullname;
@property (nonatomic, retain) NSString * customer_homePhone;
@property (nonatomic, retain) NSNumber * customer_id;
@property (nonatomic, retain) NSString * customer_external_id;
@property (nonatomic, retain) NSString * customer_lastname;
@property (nonatomic, retain) NSString * customer_lastnameInitial;
@property (nonatomic, retain) NSString * customer_memberId;
@property (nonatomic, retain) NSString * customer_mobilePhone;
@property (nonatomic, retain) NSString * customer_modifiedDate;
@property (nonatomic, retain) NSString * customer_modifyBy;
@property (nonatomic, retain) NSString * customer_notes;
@property (nonatomic, retain) NSString * customer_officePhone;
@property (nonatomic, retain) NSNumber * customer_quotes;
@property (nonatomic, retain) NSString * customer_state;
@property (nonatomic, retain) NSString * customer_zip;

@end
