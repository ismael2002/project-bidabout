//
//  PaypalSettings.m
//  BidAbout
//
//  Created by Marc Cain on 7/6/15.
//  Copyright (c) 2015 BolderImage. All rights reserved.
//

#import "PaypalSettings.h"

@implementation PaypalSettings

@dynamic userName;
@dynamic emailService;
@dynamic domain;

@end
