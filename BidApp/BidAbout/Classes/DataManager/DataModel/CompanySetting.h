//
//  CompanySetting.h
//  BidAbout
//
//  Created by Kapil on 10/01/14.
//  Copyright (c) 2014 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CompanySetting : NSManagedObject

@property (nonatomic, retain) NSString * company_address;
@property (nonatomic, retain) NSString * company_city;
@property (nonatomic, retain) NSString * company_country;
@property (nonatomic, retain) NSString * company_logo;
@property (nonatomic, retain) NSString * company_name;
@property (nonatomic, retain) NSString * company_state;
@property (nonatomic, retain) NSString * company_tax;
@property (nonatomic, retain) NSString * company_zip;
@property (nonatomic, retain) NSString * company_email;
@property (nonatomic, retain) NSString * company_mobilePhone;
@property (nonatomic, retain) NSString * company_terms;
@property (nonatomic) BOOL include_terms;

@end
