//
//  Invoice.m
//  BidAbout
//
//  Created by waseem  on 30/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "Invoice.h"


@implementation Invoice

@dynamic invoice_ClientId;
@dynamic invoice_comment;
@dynamic invoice_date;
@dynamic invoice_direction;
@dynamic invoice_id;
@dynamic invoice_item;
@dynamic invoice_paidAmount;
@dynamic invoice_tax;
@dynamic invoice_photo;
@dynamic invoice_pdf_quantity;
@dynamic invoice_pdf_rate;
@dynamic invoice_pdf_subtotal;
@dynamic invoice_pdf_total;
@dynamic invoice_number;

@end
