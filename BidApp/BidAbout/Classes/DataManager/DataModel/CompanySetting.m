//
//  CompanySetting.m
//  BidAbout
//
//  Created by Kapil on 10/01/14.
//  Copyright (c) 2014 BolderImage. All rights reserved.
//

#import "CompanySetting.h"


@implementation CompanySetting

@dynamic company_address;
@dynamic company_city;
@dynamic company_country;
@dynamic company_logo;
@dynamic company_name;
@dynamic company_state;
@dynamic company_tax;
@dynamic company_zip;
@dynamic company_email;
@dynamic company_mobilePhone;
@dynamic company_terms;
@dynamic include_terms;

@end
