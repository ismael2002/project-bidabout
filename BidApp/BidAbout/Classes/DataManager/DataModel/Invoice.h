//
//  Invoice.h
//  BidAbout
//
//  Created by waseem  on 30/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Invoice : NSManagedObject

@property (nonatomic, retain) NSString * invoice_ClientId;
@property (nonatomic, retain) NSString * invoice_comment;
@property (nonatomic, retain) NSString * invoice_date;
@property (nonatomic, retain) NSString * invoice_direction;
@property (nonatomic, retain) NSNumber * invoice_id;
@property (nonatomic, retain) NSNumber * invoice_number;
@property (nonatomic, retain) NSString * invoice_item;
@property (nonatomic, retain) NSNumber * invoice_paidAmount;
@property (nonatomic, retain) NSNumber * invoice_tax;
@property (nonatomic, retain) NSString * invoice_photo;

@property (nonatomic, retain) NSNumber * invoice_pdf_quantity;
@property (nonatomic, retain) NSNumber * invoice_pdf_rate;
@property (nonatomic, retain) NSNumber * invoice_pdf_subtotal;
@property (nonatomic, retain) NSNumber * invoice_pdf_total;

@end
