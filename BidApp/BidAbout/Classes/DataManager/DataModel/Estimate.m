//
//  Estimate.m
//  BidAbout
//
//  Created by waseem  on 30/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "Estimate.h"


@implementation Estimate

@dynamic estimate_ClientId;
@dynamic estimate_comment;
@dynamic estimate_date;
@dynamic estimate_direction;
@dynamic estimate_id;
@dynamic estimate_item;
@dynamic estimate_tax;
@dynamic estimate_photo;
@dynamic accepted;
@dynamic estimate_number;

@dynamic estimate_pdf_quantity;
@dynamic estimate_pdf_rate;
@dynamic estimate_pdf_subtotal;
@dynamic estimate_pdf_total;

@end
