//
//  InvoiceItem.m
//  BidAbout
//
//  Created by Waseem  on 04/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "InvoiceItem.h"


@implementation InvoiceItem

@dynamic item_amount;
@dynamic item_desc;
@dynamic item_id;
@dynamic item_quantity;
@dynamic item_rate;
@dynamic item_taxAllow;
@dynamic item_teplateID;
@dynamic item_title;

@end
