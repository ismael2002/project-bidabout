//
//  Item.h
//  BidAbout
//
//  Created by    on 05/08/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * item_amount;
@property (nonatomic, retain) NSString * item_desc;
@property (nonatomic, retain) NSString * item_id;
@property (nonatomic, retain) NSString * item_quantity;
@property (nonatomic, retain) NSString * item_rate;
@property (nonatomic, retain) NSNumber * item_taxAllow;
@property (nonatomic, retain) NSNumber * item_teplateID;
@property (nonatomic, retain) NSString * item_title;

@end
