//
//  Estimate.h
//  BidAbout
//
//  Created by waseem  on 30/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Estimate : NSManagedObject

@property (nonatomic, retain) NSString * estimate_ClientId;
@property (nonatomic, retain) NSString * estimate_comment;
@property (nonatomic, retain) NSString * estimate_date;
@property (nonatomic, retain) NSString * estimate_direction;
@property (nonatomic, retain) NSNumber * estimate_id;
@property (nonatomic, retain) NSString * estimate_item;
@property (nonatomic, retain) NSNumber * estimate_tax;
@property (nonatomic, retain) NSString * estimate_photo;
@property (nonatomic, retain) NSNumber * accepted;
@property (nonatomic, retain) NSNumber * estimate_number;

@property (nonatomic, retain) NSNumber * estimate_pdf_quantity;
@property (nonatomic, retain) NSNumber * estimate_pdf_rate;
@property (nonatomic, retain) NSNumber * estimate_pdf_subtotal;
@property (nonatomic, retain) NSNumber * estimate_pdf_total;

@end
