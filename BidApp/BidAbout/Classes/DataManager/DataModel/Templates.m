//
//  Templates.m
//  BidAbout
//
//  Created by Ratnesh Singh on 04/12/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "Templates.h"


@implementation Templates

@dynamic template_id;
@dynamic template_name;
@dynamic tempProductList;
@dynamic active;

@end