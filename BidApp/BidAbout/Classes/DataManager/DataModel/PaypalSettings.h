//
//  PaypalSettings.h
//  BidAbout
//
//  Created by Marc Cain on 7/6/15.
//  Copyright (c) 2015 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PaypalSettings : NSManagedObject

@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * emailService;
@property (nonatomic, retain) NSString * domain;

@end
