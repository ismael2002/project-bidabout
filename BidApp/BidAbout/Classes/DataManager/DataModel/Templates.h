//
//  Templates.h
//  BidAbout
//
//  Created by Ratnesh Singh on 04/12/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Templates : NSManagedObject

@property (nonatomic, retain) NSNumber * template_id;
@property (nonatomic, retain) NSString * template_name;
@property (nonatomic, retain) NSString * tempProductList;
@property (nonatomic, retain) NSNumber * active;

@end
