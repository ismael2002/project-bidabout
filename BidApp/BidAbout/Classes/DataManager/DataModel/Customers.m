//
//  Customers.m
//  BidAbout
//
//  Created by     on 16/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "Customers.h"


@implementation Customers

@dynamic customer_addres1;
@dynamic customer_addres2;
@dynamic customer_city;
@dynamic customer_company;
@dynamic customer_createdBy;
@dynamic customer_createdDate;
@dynamic customer_email;
@dynamic customer_firstname;
@dynamic customer_fullAddress;
@dynamic customer_fullname;
@dynamic customer_homePhone;
@dynamic customer_id;
@dynamic customer_external_id;
@dynamic customer_lastname;
@dynamic customer_lastnameInitial;
@dynamic customer_memberId;
@dynamic customer_mobilePhone;
@dynamic customer_modifiedDate;
@dynamic customer_modifyBy;
@dynamic customer_notes;
@dynamic customer_officePhone;
@dynamic customer_quotes;
@dynamic customer_state;
@dynamic customer_zip;

@end
