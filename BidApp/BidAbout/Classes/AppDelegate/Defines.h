//
//  Defines.h
//  ConductManager
//

#import "UIUtils.h"

#define KVersion                [[[UIDevice currentDevice] systemVersion] integerValue]

typedef enum serverResponseFileType
{
    EXmlFile = 500,
    EJsonFile
} EResponseFileType;

typedef enum serverRequestType
{
    EContactRequest = 1000,
    EProductRequest
} ERequestType;

#define kURLGenrater            @"http://bolderapps.net/bidabout/request/#%@"
#define kTestRestServerUrl      @"http://services.bidabout.com/rest/"
#define kTestOuthServerUrl      @"http://services.bidabout.com/oauth/access_token"
#define kGetMemberServiceUrl    @"http://services.bidabout.com/rest/Members"
//#define kCurrentTemplateItemsTemplateID 888888888

#define kClientID               @"a8fd8a85227644769c87f91492b61749"
#define kClientSecrect          @"20a589735f784e2098fe28e5c23cef8f"

//#define kClientID             @"62d65f9d30eb4086b59d8f527bc8551d"
//#define kClientSecrect        @"74acc88fbc0b4736aa3bd1ab227ac167"

//#define kURLGenrater          @"http://apps3.genexusx.com/Id0d90f8e5ee894ef1a1f7feb57f246dca/quoterequest.aspx?%@"
//#define kTestRestServerUrl    @"http://apps3.genexusx.com/Id0d90f8e5ee894ef1a1f7feb57f246dca/rest/"
//#define kTestOuthServerUrl    @"http://apps3.genexusx.com/Id0d90f8e5ee894ef1a1f7feb57f246dca/oauth/access_token"
//#define kGetMemberServiceUrl  @"http://apps3.genexusx.com/Id0d90f8e5ee894ef1a1f7feb57f246dca/rest/Members"

#define kMobileDeviceID  TARGET_IPHONE_SIMULATOR ?@"Id0d90f8e5ee894ef1a1f7feb57f246dca":[UIUtils getUDID]

#define kApplicationName @""
#define kNetworkErroTitle       @"Network Error"

//#define kClientID             @"b381e728cdbe41eaa820ae6c020bce82"
//#define kClientSecrect        @"ea9fa9c3d36d45ff9c0e5dfe89b8bd57"


#define kRequestFinishedSucessfullyNotification     @"kRequestFinishedSucessfullyNotification"
#define kRequestFailedNotification                  @"kRequestFailedNotification"
#define kLogInSucessfullNotification                @"LoginSucessFulNotification"
#define kLogInFailedNotification                    @"LogInFailNotiFication"
#define kSignUpSucessfullNotification               @"SignUpSucessFulNotification"
#define kSignUpFailNotification                     @"SignUpFailNotification"
#define kSaveditemNotification                      @"SavedItemNotificatiom"
#define kSavedInvoiceItemNotification               @"SavedInvoiceItemNotification"

#define kCustomerListServiceSucess                  @"CustomerListSucessNotification"
#define kCustomerListServiceFail                    @"CustomerListFailNotification"
#define kCustomerCreateServiceSucess                @"CustomerCreateSucessNotification"


#define kTemplateListServiceSucess                  @"TemplateListServiceSucessNotification"
#define kTemplateListServiceFail                    @"TemplateListServiceFailNotification"
#define kUpdateCompanyinfoSettingNotification       @"UpdateDoneNotification"

#define kMailMessage @"Hello, please find the attached"

/* Messages Alert */
#define kDataError                  @"Sorry, Data is not in valid format."
#define kPasswordReset              @"Please check your email for a link to reset your password."
#define kLoginError                 @"Incorrect email or password."
#define kBlankFieldMessage          @"field can't be blank"
#define kMessage                    @"Message"
#define kSignUpErrorMsg             @"An error ocuured during Registration Process. Try again!"
#define kErrorMsg                   @"Error occured durring process. Try again!"
#define kWorkInProgressMsg          @"Work in Progress"
#define kNetworkErrorMsg            @"Network not Available . Please check your network settings!"

#define kCurrentTax 0.00

// default terms and conditions
#define kDefaultTerms @"Terms and Conditions \n\nThese Terms and Conditions are provided as an integral part of the attached Proposal and are herein agreed to as part of the agreement:\n\n Seller agrees to provide all materials and perform all services described in a workmanlike manner.\n\nAny additions or changes to the scope of this work as described must be made in writing.\n\nProcedures used and materials provided will be made at the discretion of the Company unless specifically agreed to otherwise.\n\nAll materials and labor are warranted by the Company as defined by the Company Warranty policy.\n\nIn the event you fail to make a payment when due, the Company reserves the right to terminate this agreement. A late fee up to the maximum legal amount may be charged on any balance unpaid over 30 days. In the event that collection proceedings become necessary to collect amounts due under this agreement, the customer agrees to pay all costs of collection including reasonable attorney's fees and court costs. If payment is not received when due, any guarantees, express or implied, may be voided at the Company's discretion. It is mutually agreed that all materials furnished hereunder shall remain the property of Company until all payments specified herein have been made in full and that Company may regain possession thereof without notice to Purchaser. \n\nThe Company is authorized to check credit histories of any customer requesting credit terms."

#define kTerms @"Scheduling\nYou will be notified of your regular day of service. We make every attempt to service every customer on their scheduled day. If there is rain and/or a holiday at any time during the week, all service dates for that week will likely be effected accordingly, although we do make an attempt to catch up. If you wish to cancel your service for a week, you must call the office 24 hours prior to your service date. Our crews will not make a determination as to whether or not the grass needs mowing. Please do not make scheduling changes with the crew...all changes must be made directly to our office.\n\nPayments\nWe send invoices monthly and payment is due upon receipt. Payments received after 30 days aresubject to late payment fees, as will checks returned NSF. Please talk to our office about payment methods accepted.\n\nLiability and Insurance\nOur crews are trained to take necessary precautions. Accidents do occur and our company is fully insured for such incidences. However, it should be noted that we are being engaged to service your property and this includes utilizing mowers and other equipment. As such, we cannot be held liable for items unseen in the landscape, including unmarked wires, very small toys, invisible/dog fences, improperly buries cable and electric lines, and pop-up sprinkler heads that have not receded. If you see that damage has occurred for which you feel we are responsible, please inform our office no later than 24 hours after our service has been performed.\n\nService Term\nOur service agreement is Good Until Cancelled. In subsequent years, we will start up your service in Spring automatically unless we are told otherwise. This saves us a tremendous amount of paperwork and lets us concentrate on the upcoming season. If there are any price changes, you will be notified prior to service. Your agreement is cancellable at any time. To cancel or make any changes to your agreement, simply contact our office and we will help you."




//----------- Configuration Setting ----------

//---- Add this notification where you like to screen update ---------

//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadingFinished:) name:kRequestFinishedSucessfullyNotification object:nil];

//- (void) downloadingFinished:(NSNotification*)notification
//{
//    ERequestType type  = [[notification object] integerValue];
//    if (type == EContactRequest)
//        [self performSelector:@selector(fetchResult)];
//}


//[[NSNotificationCenter defaultCenter] removeObserver:self 
//                                                name:kRequestFinishedSucessfullyNotification object:nil];


// --------- Required Frameworks ------

/*
 
 - Quartz Core
 - System Configuration
 - libxml2.dylib
 
 Add header search path:    /usr/include/libxml2
 
*/

#define _gAppDelegate (BidAboutAppDelegate*)[[UIApplication sharedApplication] delegate]

//Functions

#define _IsSame_String(str1,str2)   [str1 isEqualToString:str2]
#define	ResourcePath(path)          [[NSBundle mainBundle] pathForResource:path ofType:nil]
#define	ImageWithPath(path)         [UIImage imageWithContentsOfFile:path]
#define KUserDefaults               [NSUserDefaults standardUserDefaults]
#define kDeviceVersion				[[[UIDevice currentDevice] systemVersion] integerValue]

#define GET_COLOR(r,g,b,alpha)    [UIColor colorWithRed:r green:g blue:b alpha:alpha]
#define GET_FONT(name,size)       [UIFont fontWithName:name size:size]

// Device Specific
#define kScreenHeight             [[UIScreen mainScreen] bounds].size.height

#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString: @"iPhone"])
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)



#define $alert(title, msg) [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show]



