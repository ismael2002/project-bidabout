//
//  BidAboutAppDelegate.m
//  BidAbout
//
//  Created by Waseem on 05/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "LoginViewController.h"
#import "BidAboutAppDelegate.h"
#import "HomeViewController.h"
#import "LeftViewController.h"
#import "CompanySettingsViewController.h"
#import "SWRevealViewController.h"
#import <Crashlytics/Crashlytics.h>

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#pragma mark -
#pragma mark Private Interface

@interface BidAboutAppDelegate () <SWRevealViewControllerDelegate>

@end

@implementation BidAboutAppDelegate

- (BOOL) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    [Parse setApplicationId:@"uBa2vqsijLlmpsDeIFSVzUE9UXaPjshfnaGsz33W"
                  clientKey:@"eXcDKSaSCuAjHhBox07T8iPOGFmRh2HlXtQy2GWM"];
    
    [self registerForRemoteNotification];
    
	[self setAppearance];

	if(![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirestTime"])
	{
		[_gAppData toInitialTemplateListResponse];
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirestTime"];
	}


	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    (_gAppPrefData.isLogin)? [self loginSuccessful]:[self displayLoginScreen];
    
	self.window.backgroundColor = [UIColor whiteColor];
	[self.window makeKeyAndVisible];
    
    [Crashlytics startWithAPIKey:@"67fecd92eb4862778a6fb64b2657d3a97d6f1e3d"];
	return YES;
}

- (void) initializeApplication:(UIViewController*)initialViewController
{
	LeftViewController* rearViewController = [[LeftViewController alloc] init];
	
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:initialViewController];
	
	SWRevealViewController* revealViewController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:navigationController];
    revealViewController.delegate = self;
    
	self.viewController = revealViewController;
	
	self.window.rootViewController = self.viewController;
}

- (UIViewController*)initialSetUpView
{
    if (_gAppPrefData.isFirstTimeLogedIn)
    {
        _gAppPrefData.isFirstTimeLogedIn = NO;
        [_gAppPrefData saveAllData];
        
        CompanySettingsViewController* compnaysettingVC = [[CompanySettingsViewController alloc] init];
        return compnaysettingVC;
    }
    else
    {
        HomeViewController* homeVC = [[HomeViewController alloc] init];
        return homeVC;
    }
}

#pragma mark -
#pragma mark Display login screen

- (void) displayLoginScreen
{
    NSLog(@"Display login screen");
    
	LoginViewController* loginViewControler= [[LoginViewController alloc] initWithNibName:@"LoginView" bundle:nil];
	self.window.rootViewController = loginViewControler;
}


//saving the push notification from parse?
- (void) loginSuccessful
{
    NSLog(@"login successful");
    
	[self initializeApplication:[self initialSetUpView]];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setObject:_gAppPrefData.memberID forKey:@"memberId"];
    
    //resetting badge number
    //currentInstallation.badge = 0;
    
    [currentInstallation saveInBackground];
//    [_gAppData sendRequestForSynsc];
}

-(void) applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"application did become active");
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    currentInstallation.badge = 0;
    [currentInstallation saveInBackground];
    
}

#pragma mark -
#pragma mark ActivityIndicatorMethod

- (void) showLoadingView:(BOOL)isShown
{
    NSLog(@"show loading view");
    
	if (isShown)
	{
        if (_loadingView == nil)
            _loadingView = [UILoadingView loadingView];
        [_loadingView showViewAnimated:YES onView:self.window];
	}
    else
    {
        [_loadingView removeViewAnimated:NO];
    }
}

- (void) setAppearance
{
    NSLog(@"Set appearance");
	
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 7
	if (KVersion >= 7)
	{
		UIColor *navColor = [UIColor colorWithRed:(141.0f/255.0f) green:(197.0f/255.0f) blue:(62.0f/255.0f) alpha:1.0f];
		[[UINavigationBar appearance] setBarTintColor:navColor];

		//[[UITableViewCell appearance] setBackgroundColor:[UIColor clearColor]];
	}
#endif
}


//gets notification from parse ==
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"Application, did register for remote push");
    // Store the deviceToken in the current Installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    
    //Setting up different channel for this user - set category to test push notifications
    //[currentInstallation addUniqueObject:@"Ismael-Test" forKey:@"channels"];
    
    
    
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSLog(@"Did fail for remote notifications");
    
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"%@",str);
}

- (void)registerForRemoteNotification
{
    NSLog(@"register for remote notification");
    
    if (!SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

//handles a push notification when the application is open
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"PUSH NOTIFICATION WHILE APP IS OPEN");
    [PFPush handlePush:userInfo];
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    NSLog(@"Application did register user notification settings");
    [application registerForRemoteNotifications];
}
#endif

@end

