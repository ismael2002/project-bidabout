//
//  BidAboutAppDelegate.h
//  BidAbout
//
//  Created by Waseem on 05/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "UILoadingView.h"


@class UILoadingView,SWRevealViewController;

@interface BidAboutAppDelegate : UIResponder <UIApplicationDelegate>
{
	UILoadingView* _loadingView;
}

@property(nonatomic, retain) UIWindow* window;
@property (strong, nonatomic) SWRevealViewController* viewController;

- (void) initializeApplication:(UIViewController*)initialViewController;
- (void) loginSuccessful;
- (void) showLoadingView:(BOOL)isShown;

@end

