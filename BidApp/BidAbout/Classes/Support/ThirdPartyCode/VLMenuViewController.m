//
//  VLMenuViewController.m
//  ValsList
//
//  Created by Ratnesh Singh on 14/01/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "ClientViewController.h"
#import "HomeViewController.h"
#import "VLMenuViewController.h"

@interface VLMenuViewController ()

@end

@implementation VLMenuViewController


#pragma mark-- ViewLifeCycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    NSString* sVersionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString* sBuildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString* sBuildInfo = [NSString stringWithFormat:@"Ver %@ Build %@", sVersionNumber, sBuildNumber];
    _versionInfo.text = sBuildInfo;

	//	self.navigationItem.title = [_gAppData.currentCategoryName uppercaseString];
	
	// Set the navigationTitle color.
	//	if (kDeviceVersion >= 5.0)
	//		self.navigationController.navigationBar.titleTextAttributes = kTitleTextAttribute;


	[self setIntialValue];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma mark--Set Menu Data

- (void) setIntialValue
{
	self.navigationItem.title = @"";
	
	_menuArrayList = [[NSArray alloc] initWithContentsOfFile:ResourcePath(@"MenuItem.plist")];
	NSLog(@"Menu list = %d", _menuArrayList.count);
}


#pragma mark - tableView dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_menuArrayList count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSArray* itemArray = [_menuArrayList objectAtIndex:section];
	return [itemArray count];
}


//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 44.0;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    CGRect frame = CGRectMake(0, 0, 320, 44);
//    UIImageView* imgView = [[UIImageView alloc] initWithFrame:frame];
//    NSString* imageName = nil;
//    if (section == 0)
//        imageName = @"music_header.png";
//    else if (section == 1)
//        imageName = @"val_header.png";
//    else
//        imageName = @"more_header.png";
//    [imgView setImage:[UIImage imageNamed:imageName]];
//    return  [imgView autorelease];
//}

//-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 55.0;
//}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempHeaderView=[[UIView alloc]initWithFrame:CGRectMake(10,4,160-10-10,22)];
//    tempHeaderView.backgroundColor = kMenuTitleBoxColor;
	
	UILabel *tempHeaderLabel=[[UILabel alloc] initWithFrame:CGRectMake(20,4,160-10-10,22)];
//    tempHeaderLabel.textColor = kMenuTitleTextColor;
	tempHeaderLabel.backgroundColor = [UIColor clearColor];

	if (section == 0)
        tempHeaderLabel.text=@"";
    else if (section == 1)		
		tempHeaderLabel.text=@"Company";

    else if (section == 2)
        tempHeaderLabel.text=@"Setting";
	else
		tempHeaderLabel.text = @"";

	tempHeaderLabel.textAlignment = UITextAlignmentCenter;
	[tempHeaderView addSubview:tempHeaderLabel];
	
    return tempHeaderView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellId = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

//		UIView *tableRowView=[[UIView alloc]initWithFrame:CGRectMake(10,4,140,34)];
//		tableRowView.backgroundColor = kMenuPageButtonBoxColor;
//		[cell addSubview:tableRowView];
//				
//		UILabel *tempHeaderLabel=[[[UILabel alloc] initWithFrame:CGRectMake(5,0,130,30)] autorelease];
//		tempHeaderLabel.textColor = kMenuTitleTextColor;
//		tempHeaderLabel.backgroundColor = [UIColor clearColor];
//		
//		[tableRowView addSubview:tempHeaderLabel];


//		NSArray* array = [dict objectForKey:@"Info"];
//		tempHeaderLabel.text = [array objectAtIndex:indexPath.row];
//		tempHeaderLabel.textAlignment = UITextAlignmentCenter;
//		
//		if(indexPath.section == 0)
//			tempHeaderLabel.textColor = kMenuMusicPageTextColor;
//		else if(indexPath.section == 1)
//			tempHeaderLabel.textColor = kMenuValsPageTextColor;
//		else if(indexPath.section == 2)
//			tempHeaderLabel.textColor = kMenuMorePageTextColor;
//
//		[cell autorelease];
    }

	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
	if(indexPath.section == 0 || indexPath.section == 1)
		cell.imageView.image = [UIImage imageNamed:@"Homelogo.png"];
	
	cell.textLabel.font = [UIFont systemFontOfSize:16];
	cell.textLabel.numberOfLines = 0;
	NSArray* array = [_menuArrayList objectAtIndex:indexPath.section];

	if(indexPath.section == 2)
		cell.textLabel.textAlignment = UITextAlignmentCenter;
	cell.textLabel.text = [array objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - tableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//	[_gAppDelegate hideMenuScreen]; // Hide left menu controller.

	if(indexPath.section == 0)
	{
		switch (indexPath.row)
		{
//			case 0:
//                [self openAllCategoriesWithIndex:indexPath.row];
//				break;
//			case 1:
//            {
//                _gAppData.selectedScreen = kFavoritesTitle;
//                [self openPageWithScreenType:kFavoritesTitle];
//				break;
//            }
//			case 2:
//            {
//                _gAppData.selectedScreen = kRecentltAddedTitle;
//                [self openPageWithScreenType:kRecentltAddedTitle];
//				break;
//            }
			default:
				break;
		}
	}
	else if(indexPath.section ==1)
	{
		switch (indexPath.row)
		{
//			case 3: [self openClientListViewScreen];        break;
//			case 0: [self openBlogPage];        break;
//			case 1: [self openAboutValsList];   break;
//			case 2: [self openSubscribePage];   break;
			default: break;
		}		
	}
	else if (indexPath.section == 2)
	{
//		[self openAppTipsPage];
	}
	else NSLog(@"No selection");
	
	
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma --
#pragma --Menu Item Action
//
//- (void) openAllCategoriesWithIndex:(int)index
//{
//    [[_gAppDelegate getNavigationControllr] popToRootViewControllerAnimated:YES];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"Dismiss" object:nil];
//
//}
//
//- (BOOL) isAlreadyVisible:(NSString*)className
//{
//	[[NSNotificationCenter defaultCenter] postNotificationName:@"Dismiss" object:nil];
//	
//	UINavigationController* navC = [_gAppDelegate getNavigationControllr];
//	UIViewController* visibleControler = navC.visibleViewController;
//	BOOL isExist = [visibleControler isKindOfClass:[NSClassFromString(className) class]];
//	if (isExist == NO)
//		[navC popToRootViewControllerAnimated:NO];
//	return isExist;
//}
//
//- (void) openClientListViewScreen
//{
//	if([self isAlreadyVisible:@"ClientViewController"])
//		return;
//	ClientViewController*  viewC = [[ClientViewController alloc] initWithNibName:@"ClientView" bundle:nil];
//	[[_gAppDelegate getNavigationControllr] pushViewController:viewC animated:YES];
//}

//- (void) openAboutValsList
//{
//	if ([self isAlreadyVisible:@"AboutValViewController"])
//    {
//		return;
//	}
//    AboutValViewController* cltr = [[AboutValViewController alloc] initWithNibName:@"AboutValViewController" bundle:nil];
//    [[_gAppDelegate getNavigationControllr] pushViewController:cltr animated:YES];
//    [cltr release];
//}
//
//- (void) openSubscribePage
//{
//	if ([self isAlreadyVisible:@"VLSubscribeViewController"])
//		return;
//
//	VLSubscribeViewController* cltr = [[VLSubscribeViewController alloc] initWithNibName:@"VLSubscribeViewController" bundle:nil];
//    [[_gAppDelegate getNavigationControllr] pushViewController:cltr animated:YES];
//    [cltr release];	
//}
//
//- (void) openAppTipsPage
//{
//	if ([self isAlreadyVisible:@"VLAppTipsViewController"])
//		return;
//
//	VLAppTipsViewController* cltr = [[VLAppTipsViewController alloc] initWithNibName:@"VLAppTipsViewController" bundle:nil];
//    [[_gAppDelegate getNavigationControllr] pushViewController:cltr animated:YES];
//    [cltr release];
//}
//
//- (void) openBlogPage
//{
//	if ([self isAlreadyVisible:@"ValsBlogViewController"])
//		return;
//    
//	ValsBlogViewController* cltr = [[ValsBlogViewController alloc] initWithNibName:@"ValsBlogViewController" bundle:nil];
//    [[_gAppDelegate getNavigationControllr] pushViewController:cltr animated:YES];
//    [cltr release];
//}
//
//- (void) openPageWithScreenType:(NSString*)screenType // Favourit and Recent Screen
//{
//    if ([self isAlreadyVisible:@"VLFavoritesViewController"])
//    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"FavRec" object:nil];
//		return;
//    }
//	VLFavoritesViewController* cltr = [[VLFavoritesViewController alloc] initWithNibName:@"VLFavoritesViewController" bundle:nil];
//    [[_gAppDelegate getNavigationControllr] pushViewController:cltr animated:YES];
//    
//    [cltr release];
//
//}

@end
