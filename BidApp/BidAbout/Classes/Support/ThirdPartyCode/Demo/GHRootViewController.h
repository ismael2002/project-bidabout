//
//  GHRootViewController.h
//  GHSidebarNav
//
//  Created by Greg Haines on 11/20/11.
//

#import <Foundation/Foundation.h>

typedef void (^RevealBlock)();

@interface GHRootViewController : UIViewController<UITableViewDelegate>
{
@private
	RevealBlock _revealBlock;
	
	__weak IBOutlet UITableView *_tableView;

	NSArray* _homeMenuList;
}
- (IBAction) pushViewController:(id)sender;

- (id)initWithTitle:(NSString *)title withRevealBlock:(RevealBlock)revealBlock;

@end
