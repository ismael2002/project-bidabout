//
//  GHRootViewController.m
//  GHSidebarNav
//
//  Created by Greg Haines on 11/20/11.
//

#import <QuartzCore/QuartzCore.h>

#import "HomeListCell.h"
#import "GHRootViewController.h"
#import "GHPushedViewController.h"


#pragma mark -
#pragma mark Private Interface
@interface GHRootViewController ()
//- (void)pushViewController;
- (void)revealSidebar;
@end

#pragma mark -
#pragma mark Implementation
@implementation GHRootViewController

#pragma mark Memory Management

- (id)initWithTitle:(NSString *)title withRevealBlock:(RevealBlock)revealBlock
{
    if (self = [super initWithNibName:nil bundle:nil])
	{
		self.title = title;
		_revealBlock = [revealBlock copy];
		self.navigationController.navigationBarHidden = YES;
		
//		self.navigationItem.leftBarButtonItem = 
//			[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu.png"] style:UIBarButtonItemStyleDone
//														  target:self
//														  action:@selector(revealSidebar)];
	}
	return self;
}

#pragma mark UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_homeMenuList = @[@"Estimate",@"Invoice",@"Request"];
	
	_tableView.layer.borderWidth = 1;
	_tableView.layer.borderColor = [[UIColor grayColor] CGColor];
	
	self.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
}


- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	self.navigationController.navigationBarHidden = NO;
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];	
}

// New Autorotation support for ios below version 6.0.
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

// New Autorotation support for ios 6.0 and above
- (BOOL) shouldAutorotate
{
	return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void) viewDidUnload
{
	_tableView = nil;
    [super viewDidUnload];
}
#pragma tableView delegate

#pragma mark --
#pragma Action- Button  Action
#pragma mark -- TableViewDelegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [_homeMenuList count];
}

- (HomeListCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString* cellIdentifier = @"TrackListCell";
	
	HomeListCell* cell = (HomeListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil)
	{
		cell = [HomeListCell createCell];
		[cell customizeCell];
	}
	
	[cell setTitleName:[_homeMenuList objectAtIndex:indexPath.row]];
	
	return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[_tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark Private Methods
//- (void) pushViewController
//{
//	NSString *vcTitle = [self.title stringByAppendingString:@" - Pushed"];
//	UIViewController *vc = [[GHPushedViewController alloc] initWithTitle:vcTitle];
//	[self.navigationController pushViewController:vc animated:YES];
//}

- (IBAction) pushViewController:(id)sender
{
	[self revealSidebar];
}

- (void) revealSidebar
{
	_revealBlock();
}

@end
