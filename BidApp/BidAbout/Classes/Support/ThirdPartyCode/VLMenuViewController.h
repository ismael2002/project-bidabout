//
//  VLMenuViewController.h
//  ValsList
//
//  Created by Ratnesh Singh on 14/01/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VLMenuViewController : UIViewController<UITableViewDelegate>
{
	IBOutlet UITableView*   _tableView;
    IBOutlet UILabel*       _versionInfo;

	NSArray*                _menuArrayList;
}

//- (void) openAllCategoriesWithIndex:(int)index;
//- (void) openAboutValsList;
//- (void) openSubscribePage;
//- (void) openPageWithScreenType:(NSString*)screenType; // Favourit and Recent Screen

@end
