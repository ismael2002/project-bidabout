//
//  HomeViewController.m
//  BidAbout
//
//  Created by Waseem on 09/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CreateEstimateViewController.h"
#import "CreateInvoiceViewController.h"
#import "RequestsViewController.h"
#import "ClientViewController.h"
#import "HomeListCell.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"


@implementation HomeViewController
{
    NSInteger requests;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Home";
    
	_homeMenuList = @[@"Clients",@"Requests",@"New Proposal",@"New Invoice"];
    _homeMenuListImages = @[@"btn-clients_main.png",@"btn-requests_main.png",@"btn-estimates_main",@"btn-invoice_main.png"];
	
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    //    [_tableView.layer setBorderWidth: 1.0];
    //    [_tableView.layer setBorderColor:[[UIColor grayColor] CGColor]];
    // get revelButton
    UIButton* barButton = [UIUtils getRevelButtonItem:self];
    
    //Added revel button as nevegation button
    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
	
    if ([UIUtils isiPhone5])
    {
        CGRect rect = _tableView.frame;
        rect.size.height = 389.0;
        _tableView.frame = rect;
    }
    requests = 0;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [_gAppPrefData numberOfPendingRequests:^(NSInteger requestCount) {
        requests = requestCount;
        [UIApplication sharedApplication].applicationIconBadgeNumber += requestCount;
        [_tableView reloadData];
    } clear:NO];
    
    [_gAppPrefData numberOfAcceptedEstimates:^(NSInteger estimateCount) {
        [UIApplication sharedApplication].applicationIconBadgeNumber += estimateCount;
    } clear:NO];
}

- (void)viewDidUnload
{
	_tableView = nil;
    [super viewDidUnload];
}

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [_homeMenuList count];
}

- (void)hasBadge:(NSIndexPath *)path andCell:(HomeListCell *)cell
{
    if (path.row == 1 && path.section == 0) {
        
        if (requests > 0) {
            cell.badgeString = [NSString stringWithFormat:@"%ld", (long)requests];;
            cell.badgeRightOffset = 20.0f;
            cell.badgeColor = [UIColor redColor];
            cell.badgeTextColor = [UIColor whiteColor];
            cell.badge.fontSize = 18;
            cell.badge.radius = 9;
        }    } else {
        return;
    }

}

- (HomeListCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString* cellIdentifier = @"TrackListCell";
	
	HomeListCell* cell = (HomeListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil)
	{
		cell = [HomeListCell createCell];
		[cell customizeCell];
		cell.backgroundColor = [UIColor clearColor];
	}
    
    [self hasBadge:indexPath andCell:cell];
	
	[cell setTitleName:[_homeMenuList objectAtIndex:indexPath.row]];
    [cell setImageAtIndex:[_homeMenuListImages objectAtIndex:indexPath.row]];
	return cell;
}

#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[_tableView deselectRowAtIndexPath:indexPath animated:YES];
    
	switch (indexPath.row)
	{
        case 0:
		{
			ClientViewController* viewC = [[ClientViewController alloc] initWithNibName:@"ClientView" bundle:nil];
			[self.navigationController pushViewController:viewC animated:YES];
		}
			break;
            
        case 1:
		{
			RequestsViewController * viewC = [[RequestsViewController alloc] initWithNibName:@"RequestsView" bundle:nil];
			[self.navigationController pushViewController:viewC animated:YES];
		}
			break;
            
		case 2:
		{
			CreateEstimateViewController* viewC = [[CreateEstimateViewController alloc] initWithNibName:@"CreateEstimateView" bundle:nil];
            viewC.haveClient = NO;
			[self.navigationController pushViewController:viewC animated:YES];
		}
			break;
            
		case 3:
		{
			CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
			[self.navigationController pushViewController:viewC animated:YES];
		}
			break;
			
		default:
			break;
	}
}

@end
