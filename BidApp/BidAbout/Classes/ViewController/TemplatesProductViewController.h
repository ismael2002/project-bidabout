//
//  TemplatesProductViewController.h
//  BidAbout
//
//  Created by Ratnesh Singh on 05/12/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TemplatesProductViewController : UIViewController
{
	IBOutlet UITableView* _expenseTV;
    
    NSMutableArray* _itemDataList;
    id _productObj;
}

@end
