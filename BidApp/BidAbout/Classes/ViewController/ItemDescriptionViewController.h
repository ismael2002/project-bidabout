//
//  ItemDescriptionViewController.h
//  BidAbout
//
//  Created by    Kumar  on 09/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ItemDescriptionViewController;
@protocol ItemDescriptionViewControllerDelegate<NSObject>
@optional
- (void) addComment:(NSString*)commentString;
@end

@interface ItemDescriptionViewController : BaseViewController
{
    IBOutlet UITextView* _itemDescTextV;
}

@property (nonatomic, assign) id<ItemDescriptionViewControllerDelegate> delegate;
@property (nonatomic, retain) NSString* estimateComment;
@property (nonatomic, assign) BOOL isComment;
@property (nonatomic, assign) BOOL isTemplate;


@end
