//
//  UINavigationController+UINavigationiOS7.m
//  BidAbout
//
//  Created by Waseem Ahmad on 01/10/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "UINavigationController+UINavigationiOS7.h"

@implementation UINavigationController (UINavigationiOS7)

- (void) viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.                                                                      //UITextAttributeTextColor
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor colorWithRed:102.0/255.0 green:42.0/255.0 blue:146.0/255.0 alpha:1.0],NSForegroundColorAttributeName,nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
	
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 7
	if (KVersion >= 7)
	{
		self.edgesForExtendedLayout = UIRectEdgeNone;
	}
#endif
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

#endif

@end

