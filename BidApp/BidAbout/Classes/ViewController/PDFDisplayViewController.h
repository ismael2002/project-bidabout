//
//  PDFDisplayViewController.h
//  BidAbout
//
//  Created by waseem  on 04/10/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Customers.h"

@class PDFDisplayViewController;

@protocol PDFDisplayViewControllerDelegate <NSObject>
- (void)addItemViewController:(PDFDisplayViewController *)controller didFinishEnteringDataItem:(NSData *)item;
- (void)adjustPDFOptions:(PDFDisplayViewController*)controller quantity: (BOOL)PDFQuantity rate:(BOOL)PDFRate subtotal:(BOOL) PDFSubtotal totalsOnly: (BOOL)pdfTotalsOnly;
@end

@interface PDFDisplayViewController : BaseViewController <UIActionSheetDelegate>
{
    IBOutlet UIWebView* _webView;
}

@property (nonatomic, weak) id <PDFDisplayViewControllerDelegate> delegate;

@property (nonatomic,retain) NSString* estimateTitleId;

@property(nonatomic,retain) NSData* data;
@property(nonatomic,retain) NSArray* itemArray;
@property(nonatomic,retain) NSString* screenTitle;
@property(nonatomic,retain) Customers* customer;
@property(nonatomic,retain) NSArray* detailList;
@property(nonatomic,retain) NSArray* headerList;
@property(nonatomic,retain) NSString* date;
@property(nonatomic,retain) NSString* estimate;
@property(nonatomic,retain) NSArray* photoList;
@property(nonatomic) BOOL isInvoice;
@property(nonatomic) BOOL showQuantity;
@property(nonatomic) BOOL showRate;
@property(nonatomic) BOOL subtotalsOnly;
@property(nonatomic) BOOL totalsOnly;



//[PDFRenderer  drawPDF:filePath dataArray:_itemArray withTitle:self.title withClient:self.customerObj withDetail:detaillist header:headerList withDate:date type:@"ESTIMATE" photoList:_photoList isinvoice:NO];

//    [PDFRenderer drawPDF:<#(NSString *)#> dataArray:<#(NSArray *)#> withTitle:<#(NSString *)#> withClient:<#(Customers *)#> withDetail:<#(NSArray *)#> header:<#(NSArray *)#> withDate:<#(NSString *)#> type:<#(NSString *)#> photoList:<#(NSArray *)#> isinvoice:<#(BOOL *)#>]
@end
