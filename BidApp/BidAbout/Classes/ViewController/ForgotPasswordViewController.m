//
//  ForgotPasswordViewController.m
//  BidAbout
//
//  Created by    Kumar  on 10/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController (Private)

- (BOOL) isAnyThingEmpty;
- (void) setUpKeyboardControls;
- (void) setUpScrollViewOffSet:(UITextField *)tField;

@end

@implementation ForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Forgot Password";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    UIBarButtonItem *submitBtn = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStyleBordered target:self action:@selector(submitForSignUp:)];
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonAction:)];
    
    self.navigationItem.leftBarButtonItem = backBtn;
    self.navigationItem.rightBarButtonItem = submitBtn;
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signUpDone:) name:kSignUpSucessfullNotification object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void) viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark Button Action

- (IBAction) submitForSignUp:(id)sender
{
    if (![self isAnyThingEmpty]) {
        BOOL isNotValid = [UIUtils validateEmail:_emailTxtField.text];
        if (isNotValid) {
            [_emailTxtField becomeFirstResponder];
            return;
        }
        
        [PFUser requestPasswordResetForEmailInBackground:_emailTxtField.text];
        
        [UIUtils messageAlert:kPasswordReset title:kApplicationName delegate:nil];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction) cancelButtonAction:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Private Methods

- (BOOL) isAnyThingEmpty
{
    if([[ UIUtils checknilAndWhiteSpaceinString:_emailTxtField.text] isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Email %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    return NO;
}

@end
