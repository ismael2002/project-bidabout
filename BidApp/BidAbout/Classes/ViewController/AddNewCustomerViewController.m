//
//  AddNewMemberViewController.m
//  BidAbout
//
//  Created by    on 16/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "CoreDataManger.h"
#import "AddNewCustomerViewController.h"

#define kScrollWidth 320
#define kiPhoneScrollFrameHeightWithoutKeyboard 460
#define kiPhone5ScrollFrameHeightWithoutKeyboard 548

#define kiPhone5ScrollFrameHeightWithKeyboard 246
#define kiPhoneScrollFrameHeightWithKeyboard 220

#define kscrollContentSizeDiffWithoutKeyBoard 210
#define kscrollContentSizeDiffWithKeyBoard 400

#define kiPadScrollWidth 768
#define kiPadScrollFrameHeightWithoutKeyboard 1004
#define kiPadScrollFrameHeightWithKeyboard 650

#define kiPadScrollContentSizeDiffWithoutKeyBoard 120
#define kiPadScrollContentSizeDiffWithKeyBoard 470

#define kTextFieldDiffrence 80

@interface AddNewCustomerViewController ()

@end

@implementation AddNewCustomerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    // add navigations items
    [self addnavigationItems];
    
    // SetUp controls with custom keyboard
    [self setUpKeyboardControls];
    
    // Intital setUp
    [self initialSetUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createCustomerSucess:) name:kCustomerCreateServiceSucess object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void) viewDidUnload
{
    _tapgesture = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark privateMethods

 - (void) addnavigationItems
{
    self.title = @"Customer";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
    UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnAction)];
	self.navigationItem.rightBarButtonItem = saveBtn;
    self.navigationItem.leftBarButtonItem = cancelBtn;
}

- (void) initialSetUp
{
    _tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    _tapgesture.cancelsTouchesInView = YES;

	_notelabel.hidden = NO;
	_customerNotesTextView.hidden = NO;
	
//	[_quotesNoBtn setSelected: YES];
    
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
    }
    else
    {
        _scrollView.frame = CGRectMake(0,0,kiPadScrollWidth,kiPadScrollFrameHeightWithoutKeyboard);
        _scrollView.contentSize = CGSizeMake(kiPadScrollWidth,kiPadScrollFrameHeightWithoutKeyboard+kiPadScrollContentSizeDiffWithoutKeyBoard);
    }
}

#pragma mark -
#pragma mark ButtonAction

- (IBAction) quotesCheckedBtnAction:(id)sender
{
//    UIButton *selectedButton  = (UIButton*)sender;
//    
//    if (selectedButton.tag == 15)
//    {
//        [_quotesYesBtn setSelected:YES];
//        [_quotesNoBtn setSelected:NO];
//        
//        _notelabel.hidden = NO;
//        _customerNotesTextView.hidden = NO;
//    }
//    else
//    {
//        [_quotesNoBtn setSelected:YES];
//        [_quotesYesBtn setSelected:NO];
//        
//        _notelabel.hidden = YES;
//        _customerNotesTextView.hidden = YES;
//    }
}

- (void) saveBtnAction
{
    if ([ UIUtils checknilAndWhiteSpaceinString:_firstNameTxtField.text].length > 0)
    {
    
        if (_customerPhoneTxtField.text.length>0)
        {
            BOOL isPhoneNotValid = [UIUtils isPhoneNumbervalid:_customerPhoneTxtField.text];
            if (isPhoneNotValid)
            {
                [_customerPhoneTxtField becomeFirstResponder];
                return;
            }
        }
        
        if ([ UIUtils checknilAndWhiteSpaceinString:_customerZipTxtField.text].length > 0)
        {
            if (([_customerZipTxtField.text integerValue] == 0))
            {
                [UIUtils messageAlert:@"Please provide a valid zip code." title:kApplicationName delegate:nil];
                [_customerZipTxtField becomeFirstResponder];
                return;
            }
        }
        
        if ([ UIUtils checknilAndWhiteSpaceinString:_customerEmailTxtField.text].length > 0)
        {
            BOOL isNotValid = [UIUtils validateEmail:_customerEmailTxtField.text];
            if (isNotValid)
            {
                [_customerEmailTxtField becomeFirstResponder];
                return;
            }
        }

//        NSString* allowQuotesStr = (_quotesYesBtn.selected) ? @"true":@"false";
		NSString* allowQuotesStr = @"true";
        
        NSArray* objectsArray = [NSArray arrayWithObjects:_gAppPrefData.memberID,[UIUtils checknilAndWhiteSpaceinString:_firstNameTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_LastNameTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerAddress1TxtField.text],[UIUtils checkNil:_customerAddress2TxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerCityTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerStateTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerZipTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerPhoneTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerPhoneTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerPhoneTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerEmailTxtField.text],[UIUtils checknilAndWhiteSpaceinString:_customerNotesTextView.text],allowQuotesStr,nil];
        
        NSArray* keyArray = [NSArray arrayWithObjects:@"CustomerMemberId",@"CustomerFirstName",@"CustomerLastName",@"CustomerAddress1",@"CustomerAddress2",@"CustomerCity",@"CustomerState",@"CustomerZip",@"CustomerHomePhone",@"CustomerOfficePhone",@"CustomerMobilePhone",@"CustomerEmail",@"CustomerNotes",@"CustomerPendingQuotes",nil];

        NSDictionary* dict = [[NSDictionary alloc] initWithObjects:objectsArray forKeys:keyArray];
        
		NSArray* array = [[NSArray alloc] initWithObjects:dict, nil];
		[_gAppData saveCustomerDeail:array];

//        NSData* data = [UIUtils createJSONToPost:dict];
//        [_gAppData sendRequestForCreateCustomer:data];
    }
    else
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"%@ %@",_firstNameTxtField.placeholder,kBlankFieldMessage] title:kMessage delegate:nil];
    }
}

- (IBAction) cancelBtnAction
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([_keyboardControls.textFieldsArray containsObject:textField])
        _keyboardControls.activeTextField = textField;
    
    [self setUpScrollViewOffSet:textField];
}

- (BOOL) textFieldShouldReturn:(UITextField*)textField
{
	[textField resignFirstResponder];
	return YES;
}

#pragma mark -
#pragma mark TextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([_keyboardControls.textFieldsArray containsObject:textView])
        _keyboardControls.activeTextField = textView;
    
    [self setUpScrollViewOffSet:textView];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (![string isEqualToString:@""])
        if ([textField isEqual:_customerPhoneTxtField])
            return [UIUtils setFormatForPhoneNumber:_customerPhoneTxtField OfLength:12];
    
    return YES;
}

#pragma mark -
#pragma mark Private Methods

- (void) setUpScrollViewOffSet:(id)tField
{
    if ([tField isKindOfClass:[UITextField class]])
    {
        UITextField* textField = (UITextField*)tField;
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-kTextFieldDiffrence) animated:YES];
    }
    else if([tField isKindOfClass:[UITextView class]])
    {
        UITextView* textView = (UITextView*)tField;
        
        [_scrollView setContentOffset:CGPointMake(0, textView.frame.origin.y-kTextFieldDiffrence) animated:YES];
    }
}

- (BOOL) isAnyThingEmpty
{
    NSArray* textFieldArray = [NSArray arrayWithObjects:_firstNameTxtField,_LastNameTxtField,_customerPhoneTxtField,_customerAddress1TxtField,_customerCityTxtField,_customerStateTxtField,_customerZipTxtField,_customerEmailTxtField,nil];
    
    BOOL isEmpty = NO;
    
    for (UITextField* textField in textFieldArray)
        {
            if ([[ UIUtils checknilAndWhiteSpaceinString:textField.text] isEqualToString:@""])
            {
                [UIUtils messageAlert:[NSString stringWithFormat:@"%@ %@",textField.placeholder,kBlankFieldMessage] title:kMessage delegate:nil];
                isEmpty = YES;
                break;
            }
        }
    
    return isEmpty;
}

#pragma mark -
#pragma mark Notification

- (void) keyboardWillShow:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard+50);
        }
    }
    else
    {
        _scrollView.frame = CGRectMake(0,0,kiPadScrollWidth,kiPadScrollFrameHeightWithKeyboard);
        _scrollView.contentSize = CGSizeMake(kiPadScrollWidth,kiPadScrollFrameHeightWithKeyboard+kiPadScrollContentSizeDiffWithKeyBoard);
    }
    
    [self.view addGestureRecognizer:_tapgesture];
}

- (void) keyboardWillHide:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
    }
    else
    {
        _scrollView.frame = CGRectMake(0,0,kiPadScrollWidth,kiPadScrollFrameHeightWithoutKeyboard);
        _scrollView.contentSize = CGSizeMake(kiPadScrollWidth,kiPadScrollFrameHeightWithoutKeyboard+kiPadScrollContentSizeDiffWithoutKeyBoard);
    }
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [self.view removeGestureRecognizer:_tapgesture];
}

- (void) createCustomerSucess:(NSNotification *)notification
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) setUpKeyboardControls
{
    _keyboardControls = [[KeyboardControls alloc] init];
    _keyboardControls.delegate = self;
    
    // Add textFields in order
    _keyboardControls.textFieldsArray = [NSArray arrayWithObjects:_firstNameTxtField,_LastNameTxtField,_customerPhoneTxtField,_customerAddress1TxtField,_customerAddress2TxtField,_customerCityTxtField,_customerStateTxtField,_customerZipTxtField,_customerEmailTxtField,_customerNotesTextView,nil];
    
    _keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    _keyboardControls.previousNextTintColor = [UIColor blackColor];
    _keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    _keyboardControls.previousTitle = @"Previous";
    _keyboardControls.nextTitle = @"Next";
    
    for (id textField in _keyboardControls.textFieldsArray)
    {
        if ([textField isKindOfClass:[UITextField class]] || [textField isKindOfClass:[UITextView class]])
        {
            if ([textField isKindOfClass:[UITextField class]])
            {
                ((UITextField *) textField).inputAccessoryView = _keyboardControls;
                ((UITextField *) textField).delegate = self;
            }
            else if ([textField isKindOfClass:[UITextView class]])
            {
                ((UITextView *) textField).inputAccessoryView = _keyboardControls;
                ((UITextView *) textField).delegate = self;
            }
        }
    }
}

- (void) dismissKeyboard
{
    [_firstNameTxtField resignFirstResponder];
    [_LastNameTxtField resignFirstResponder];
    [_customerPhoneTxtField resignFirstResponder];
    [_customerAddress1TxtField resignFirstResponder];
    [_customerAddress2TxtField resignFirstResponder];
    [_customerCityTxtField resignFirstResponder];
    [_customerStateTxtField resignFirstResponder];
    [_customerZipTxtField resignFirstResponder];
    [_customerEmailTxtField resignFirstResponder];
    [_customerNotesTextView resignFirstResponder];
}

#pragma mark -
#pragma mark KeyBoardControlDelegate

- (void)keyboardControlsDonePressed:(KeyboardControls*)controls
{
    [controls.activeTextField resignFirstResponder];
}

- (void)keyboardControlsPreviousNextPressed:(KeyboardControls*)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self setUpScrollViewOffSet:textField];
}

@end
