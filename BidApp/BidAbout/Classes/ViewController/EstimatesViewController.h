//
//  EstimatesViewController.h
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EstimatesViewController : BaseViewController
{
	__weak IBOutlet UITableView* _tableView;
    
}

@property (strong, nonatomic) NSMutableDictionary *estimates;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end
