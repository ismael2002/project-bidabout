//
//  PaymentInfoViewController.h
//  BidAbout
//
//  Created by Waseem  on 14/08/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PaymentInfoViewControllerDelegate<NSObject>



@optional
- (void) savedPaymentItem:(NSString*)paidAmount;
@end

@interface PaymentInfoViewController : BaseViewController<UITextFieldDelegate>
{
    IBOutlet UITableView*  _tableView;
    
    UITextField* _paymentTextField;
    UISwitch* _fullOrLessPaySwitch;
    BOOL _isFullyPaid;
}

@property (nonatomic, retain) NSString* totalAmount;
@property (nonatomic, retain) NSString* paidAmount;
@property (nonatomic, retain) NSString* startingPaidAmount;
//@property (nonatomic, retain) NSString* invoiceTitle;
@property (nonatomic, retain) NSNumber* invoiceID;

@property (nonatomic, assign) id<PaymentInfoViewControllerDelegate> paymentInfoDelegate;

@end
