//
//  LeftViewController.h
//  BidAbout
//
//  Created by     on 01/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSArray* _menuList;
    BOOL _isShowHideSection;
}

@property (nonatomic, retain) IBOutlet UITableView* leftTableView;

@end