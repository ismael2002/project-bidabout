//
//  AddNewMemberViewController.h
//  BidAbout
//
//  Created by Rupesh Kumar  on 16/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardControls.h"

@interface AddNewMemberViewController : UIViewController<UITextFieldDelegate, KeyboardControlsDelegate>
{
    IBOutlet UITextField* _firstNameTxtField;
    IBOutlet UITextField* _LastNameTxtField;
    IBOutlet UITextField* _memberCompanyTxtField;
    IBOutlet UITextField* _memberPhoneTxtField;
    IBOutlet UITextField* _memberAddress1TxtField;
    IBOutlet UITextField* _memberAddress2TxtField;
    IBOutlet UITextField* _memberCityTxtField;
    IBOutlet UITextField* _memberStateTxtField;
    IBOutlet UITextField* _memberZipTxtField;
    IBOutlet UITextField* _memberEmailTxtField;
    IBOutlet UITextField* _passwordTxtField;
    IBOutlet UITextField* _confirmPasswrdTxtField;
    
    IBOutlet UIButton* _adminBtn;
    IBOutlet UIButton* _memberBtn;
    IBOutlet UIButton* _quotesYesBtn;
    IBOutlet UIButton* _quotesNoBtn;
    
    IBOutlet UIScrollView* _scrollView;
    
    KeyboardControls* _keyboardControls;
}

- (IBAction) roleCheckedBtnAction:(id)sender;
- (IBAction) quotesCheckedBtnAction:(id)sender;

@end
