//
//  ForgotPasswordViewController.h
//  BidAbout
//
//  Created by    Kumar  on 10/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ForgotPasswordViewController : BaseViewController <UITextFieldDelegate>
{
    IBOutlet UITextField* _emailTxtField;
}

@end
