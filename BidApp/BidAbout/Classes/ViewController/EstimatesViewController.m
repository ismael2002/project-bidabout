//
//  EstimatesViewController.m
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "EstimatesViewController.h"
#import "CreateEstimateViewController.h"
#import "Estimate.h"
#import "Customers.h"
@interface EstimatesViewController ()

@end

@implementation EstimatesViewController

#pragma mark--ViewLife Cycle.

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Proposals";
    
    if (sizeof(void*) == 4)
    {
        NSLog(@"You're running in 32 bit EstimatesViewController");
        
    } else if (sizeof(void*) == 8)
    {
        NSLog( @"You're running in 64 bit EstimatesViewController");
    }
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // get revelButton
    UIButton* barButton = [UIUtils getRevelButtonItem:self];
    
    //Added revel button as nevegation button
    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    
    UIBarButtonItem* addbarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewEstimateAction)];
	self.navigationItem.rightBarButtonItem = addbarBtn;
    
    self.estimates = [[NSMutableDictionary alloc] initWithDictionary: @{@"Accepted":[[NSMutableArray alloc] init], @"Pending":[[NSMutableArray alloc] init], @"Rejected": [[NSMutableArray alloc] init]}];
    
    [self.segmentedControl addTarget:self
                              action:@selector(segControlClicked) forControlEvents:UIControlEventValueChanged];
    
    CGRect frame= self.segmentedControl.frame;
    [self.segmentedControl setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 28)];
    
    [_gAppData deleteItemsFromCurrentEstimate];
    
    
    NSLog(@"The estimates are :%@", self.estimates);
    
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    // makes sure current estimate items are deleted
    [_gAppData deleteItemsFromCurrentTemplate];
    
    [self getSortedEstimate];
}

- (void) viewDidAppear:(BOOL)animated
{
    [_gAppPrefData numberOfAcceptedEstimates:^(NSInteger requestCount) {} clear:YES];
	[super viewDidAppear:animated];
}

- (void) segControlClicked
{
    [_tableView reloadData];
}

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        return [[self.estimates objectForKey:@"Accepted"] count];
    }
    
    else if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        return [[self.estimates objectForKey:@"Pending"] count];
    }
    
    else if (self.segmentedControl.selectedSegmentIndex == 2)
    {
        return [[self.estimates objectForKey:@"Rejected"] count];
    }
    
    //there are only 3 options on segmented controller
    return 0;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    NSDictionary* dict;
    //if(indexPath.section == 0)
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        dict = [[self.estimates objectForKey:@"Accepted"] objectAtIndex:indexPath.row];
    }
    else if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        dict = [[self.estimates objectForKey:@"Pending"] objectAtIndex:indexPath.row];
    }
    else if (self.segmentedControl.selectedSegmentIndex == 2)
    {
        dict = [[self.estimates objectForKey:@"Rejected"] objectAtIndex:indexPath.row];
    }
    if (dict)
    {
        cell.textLabel.text = [self getCellTittle:dict];
    }
    return cell;
}

#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary* dict;
    
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        dict = [[self.estimates objectForKey:@"Accepted"] objectAtIndex:indexPath.row];
    }
    else if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        dict = [[self.estimates objectForKey:@"Pending"] objectAtIndex:indexPath.row];
    }
    else if (self.segmentedControl.selectedSegmentIndex == 2)
    {
        dict = [[self.estimates objectForKey:@"Rejected"] objectAtIndex:indexPath.row];
    }
    
    
    NSString* str = [NSString stringWithFormat:@"%ld",(long)[[dict objectForKey:@"EstimateID"] integerValue]];
    NSArray* estimateArray  = [_gAppData getEstimatelistWithID:str];
    if (estimateArray.count >0)
    {
        Estimate* estimateObj = [estimateArray objectAtIndex:0];
        CreateEstimateViewController* viewC = [[CreateEstimateViewController alloc] initWithNibName:@"CreateEstimateView" bundle:nil];
        viewC.estimateObj = estimateObj;
        viewC.acceptedInteger = estimateObj.accepted;
        [viewC setIsConvert:YES];
        [self.navigationController pushViewController:viewC animated:YES];
    }
}

#pragma mark --
#pragma mark Methods 

- (NSString*) getCellTittle:(NSDictionary*)dict
{
    NSString* titeStr = @"";
    NSString* fName = [dict objectForKey:@"FirstName"];
    titeStr = (fName.length > 0)? fName:titeStr;
    
    NSString* lName = [dict objectForKey:@"LastName"];
    titeStr = (lName.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,lName]:titeStr;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString* dateStr = [dateFormatter stringFromDate:[dict objectForKey:@"EstimateDate"]];
    
    titeStr = (dateStr.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,dateStr]:titeStr;
    return titeStr;
}

#pragma mark --
#pragma mark Selector Methods

- (void) addNewEstimateAction
{
    CreateEstimateViewController* viewC = [[CreateEstimateViewController alloc] initWithNibName:@"CreateEstimateView" bundle:nil];
    [self.navigationController pushViewController:viewC animated:YES];
}

- (void) getSortedEstimate
{
    NSLog(@"Get sorted Estimate");
    
    PFQuery *query = [PFQuery queryWithClassName:@"Estimate"];
    [query whereKey:@"userId" equalTo:[PFUser currentUser].objectId];
    //[query whereKey:@"accepted" equalTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"accepted" equalTo:[NSNumber numberWithInteger:1]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [self pullCoreData:objects];
    }];
}

- (void)pullCoreData:(NSArray *) parseObjects
{
    NSLog(@"pull core data using parse objects");
    NSLog(@"Parse objects are -------- : %@", parseObjects);
    
    self.estimates = [[NSMutableDictionary alloc] initWithDictionary:@{@"Accepted":[[NSMutableArray alloc] init], @"Pending":[[NSMutableArray alloc] init], @"Rejected": [[NSMutableArray alloc] init]}];
    
    NSArray* estimateArray = [_gAppData getEstimatelistWithID:nil];
    NSMutableArray* estimateDetailArray = [[NSMutableArray alloc] init];
    
    NSLog(@"The estimate array is: %@", estimateArray);
    
    for (Estimate* estimateObj in estimateArray)
    {
        NSLog(@"The estimate object is: %@", estimateObj);
        NSLog(@"The title is: %@", estimateObj.estimate_id);
        NSLog(@"Accepted is: %@", estimateObj.accepted );
        
        NSMutableDictionary* dict  = [[NSMutableDictionary alloc] init];
        [dict setObject:estimateObj.estimate_id forKey:@"EstimateID"];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        NSDate* dateOfEstimate = [dateFormatter dateFromString:estimateObj.estimate_date];
        
        [dict setObject:dateOfEstimate forKey:@"EstimateDate"];
        
        NSArray* arrayOfClient = [_gAppData getCustomerDetails:NO andCustomerId:estimateObj.estimate_ClientId];
        if (arrayOfClient.count > 0)
        {
            Customers* customer = [arrayOfClient objectAtIndex:0];
            [dict setObject:customer.customer_firstname forKey:@"FirstName"];
            [dict setObject:customer.customer_lastname forKey:@"LastName"];
        }
        
        //[dict setObject:[NSNumber numberWithBool: [estimateObj.accepted boolValue]] forKey:@"Accepted"];
//        [dict setObject:estimateObj.accepted forKey:@"Accepted"];
        
        if (!estimateObj.accepted)
        {
            NSLog(@"Set accepted to 2");
            [dict setObject:[NSNumber numberWithInteger:1] forKey:@"Accepted"];
        }
        else {
            NSLog(@"Set accepted to normal, what it should be");
            [dict setObject:estimateObj.accepted forKey:@"Accepted"];
        }
        
        
        for(PFObject *object in parseObjects)
        {
            NSLog(@"PFObject in parse objects, the object is: %@", object);
            
            if([object[@"clientId"] isEqualToString: estimateObj.estimate_ClientId]
               && [object[@"estimateId"] isEqualToString: [estimateObj.estimate_id stringValue]])
            {
                //[dict setObject:[NSNumber numberWithBool: YES] forKey:@"Accepted"];
                [dict setObject:[NSNumber numberWithInteger:1] forKey:@"Accepted"];
            }
        }
        
        [estimateDetailArray addObject:dict];
    }
    
    NSLog(@"The sort descriptor is");
    
    NSSortDescriptor *dSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"EstimateDate" ascending:YES selector:@selector(compare:)];
    
    NSArray* sortDescriptors = [NSArray arrayWithObjects:dSortDescriptor, nil];
    NSArray* sortedArray = [estimateDetailArray sortedArrayUsingDescriptors:sortDescriptors];
    
    for(NSMutableDictionary *dict in sortedArray)
    {
        NSLog(@"The dictionary is: %@", dict);
        
//        if([[dict objectForKey:@"Accepted"] boolValue])
//        {
//            [[self.estimates objectForKey:@"Accepted"] addObject:dict];
//        }
//        else
//        {
//            [[self.estimates objectForKey:@"Pending"] addObject:dict];
//        }
        
        NSNumber* isAccepted = [dict objectForKey:@"Accepted"];
        
        if ([isAccepted integerValue] == 1)
        {
            [[self.estimates objectForKey:@"Accepted"] addObject:dict];
        }
        
        if ([isAccepted integerValue] == 2)
        {
            [[self.estimates objectForKey:@"Pending"] addObject:dict];
        }
        
        if ([isAccepted integerValue] == 3)
        {
            [[self.estimates objectForKey:@"Rejected"] addObject:dict];
        }
    }
    [_tableView reloadData];
    
    NSLog(@"The estimates are :%@", self.estimates);
    
}

@end
