//
//  TemplatesViewController.m
//  BidAbout
//
//  Created by     on 03/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//
#import "TemplatesProductViewController.h"
#import "ScheduleViewController.h"
#import "Templates.h"
#import "TemplatesViewController.h"
#import "CreateInvoiceViewController.h"
#import "Item.h"

@interface TemplatesViewController ()

@end

@implementation TemplatesViewController
{
    BOOL editing;
}
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Templates";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    UIButton* barButton = [UIUtils getRevelButtonItem:self];
    
    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(addPressed)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addPressed)];
    
    
    
    NSLog(@"The template list is: %@", _templateList);
    
    [self.segmentedControl addTarget:self
                              action:@selector(segControlClicked) forControlEvents:UIControlEventValueChanged];
    
    if (_templateChanged != nil)
    {
        //checks to see if there was a change between activity and changes it if there was
        // need to make this change within the save template
//        _templateChanged.active = [NSNumber numberWithBool:![_templateChanged.active boolValue]];
//        [_gAppData updateTemplate:_templateChanged];
//        [_tableView reloadData];
    }
}


-(void) segControlClicked
{
    [_tableView reloadData];
}

- (void) addPressed
{
    CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
    //viewC.template = template;
    viewC.newTemplate = YES;
    viewC.isTemplate = YES;
    
    [self.navigationController pushViewController:viewC animated:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    _templateList = [ NSArray arrayWithArray:[_gAppData getTemplatedetails]];
    
     [_gAppData deleteItemsFromCurrentTemplate];
    
    mutableFilteredTemplates = [[NSMutableArray alloc]init];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(templateFetchSuccess) name:kTemplateListServiceSucess object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(templateFetchFail) name:kTemplateListServiceFail object:nil];
    
    [_tableView reloadData];
}

- (void) filterTemplates
{
    [mutableFilteredTemplates removeAllObjects];
    
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        for (NSInteger x = 0; x < _templateList.count; x++)
        {
            Templates* template = _templateList[x];
            
            if ([template.active integerValue] == 0)
            {
                [mutableFilteredTemplates addObject:_templateList[x]];
            }
        }
    }
    
    if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        for (NSInteger x = 0; x < _templateList.count; x++)
        {
            Templates* template = _templateList[x];
            
            if (![template.active integerValue] == 0)
            {
                [mutableFilteredTemplates addObject:_templateList[x]];
            }
        }
    }
}

#pragma mark --
#pragma mark TableView DataSource

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        NSLog(@"The items that are active are: %@", _templateList);
        
        for (NSInteger x = 0; x < _templateList.count; x++)
            
        {
            Templates* template = _templateList[x];
            
            if ([template.active integerValue] == 0)
            {
                count++;
            }
        }
        return count;
    }
    
    else 
    {
        NSLog(@"The items that are active are: %@", _templateList);
        
        for (NSInteger x = 0; x < _templateList.count; x++)
            
        {
            Templates* template = _templateList[x];
            
            if (![template.active integerValue] == 0)
            {
                count++;
            }
        }
        return count;
    }
}

//- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    if(section == 0){
//        NSPredicate *active = [NSPredicate predicateWithFormat:@"active != 0"];
//        return [_templateList filteredArrayUsingPredicate:active].count;
//    }else{
//        NSPredicate *inactive = [NSPredicate predicateWithFormat:@"active == 0"];
//        return [_templateList filteredArrayUsingPredicate:inactive].count;
//    }
//}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
	//Templates* obj = [_templateList objectAtIndex:indexPath.row];
    
    [mutableFilteredTemplates removeAllObjects];
    
    [self filterTemplates];
    
    Templates* obj = [mutableFilteredTemplates objectAtIndex:indexPath.row];
    
    cell.textLabel.text = obj.template_name;
    
    return cell;
}

#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self filterTemplates];

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(!editing)
    {
        NSLog(@"The template that was selected is %@", _templateList[indexPath.row]);
        
        //Templates* template = [_templateList objectAtIndex:indexPath.row];
        Templates* template = [mutableFilteredTemplates objectAtIndex:indexPath.row];
        
        CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
        viewC.template = template;
        viewC.isTemplate = YES;
        viewC.existingTemplate = YES;
        viewC.templateDictionary = _templateList[indexPath.row];
        
        [self.navigationController pushViewController:viewC animated:YES];
    }
    else
    {
        Templates* template = [_templateList objectAtIndex:indexPath.row];
        template.active = [NSNumber numberWithBool:![template.active boolValue]];
        //[_gAppData updateTemplate:template];
        [_tableView reloadData];
    }
}

#pragma mark --
#pragma mark  Template service Notification

- (void) templateFetchSuccess
{
	_templateList = [_gAppData getTemplatedetails];
    [_tableView reloadData];
}

- (void) templateFetchFail
{
	[UIUtils messageAlert:@"Data is not available OR not in valid format!" title:kApplicationName delegate:nil];
}

@end
