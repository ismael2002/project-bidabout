//
//  AddPhotoViewController.h
//  BidAbout
//
//  Created by waseem  on 30/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGalleryViewController.h"

@protocol AddPhotoViewControllerDelegate<NSObject>
@optional
- (void) addPhotoList:(NSArray*)photoArray;
@end

@interface AddPhotoViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UIActionSheetDelegate,FGalleryViewControllerDelegate>
{
    IBOutlet UITableView* _tableView;
    UIPopoverController* _popOver;
    FGalleryViewController* _localGallery;
}

@property (nonatomic, assign) id<AddPhotoViewControllerDelegate> addPhotoDelegate;
@property (nonatomic,retain) NSMutableArray* photoList;
@property (nonatomic,retain) NSString* estimateTitleId;
@property (nonatomic) NSInteger photoIndex;

@end
