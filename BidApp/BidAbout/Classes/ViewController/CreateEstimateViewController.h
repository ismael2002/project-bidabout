//
//  CreateEstimateViewController.h
//  BidAbout
//
//  Created by Waseem on 04/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableDetailCell.h"
#import <MessageUI/MessageUI.h>
#import "ClientViewController.h"
#import "ItemDescriptionViewController.h"
#import "AddPhotoViewController.h"
#import "CreateInvoiceViewController.h"
#import "PDFDisplayViewController.h"

@class Customers;
@class Estimate;

@interface CreateEstimateViewController : BaseViewController<UIActionSheetDelegate,UITextFieldDelegate,ClientViewControllerDelegate,ItemDescriptionViewControllerDelegate,AddPhotoViewControllerDelegate, MFMailComposeViewControllerDelegate,UIPrintInteractionControllerDelegate, ClientViewControllerDelegate, TemplateAdded, PDFDisplayViewControllerDelegate>
{
	__weak IBOutlet UITableView* _tableView;
    __weak IBOutlet UIDatePicker* _datePicker;
    __weak IBOutlet UIView* _pickerView;
    
    UITapGestureRecognizer* _tapgesture ;
    
    
    NSMutableDictionary* _estimateDict;
    NSMutableArray* _itemArray;
    UIPopoverController* _popOver;
    
    NSMutableArray* _photoList;
}

@property(nonatomic, strong) Customers* customerObj;
@property(nonatomic, strong) NSString* dateString;
@property(nonatomic, assign) BOOL haveClient;
@property(nonatomic, strong) Estimate* estimateObj;
@property(nonatomic, assign)BOOL isConvert;
@property(nonatomic, strong) NSString* comment;
@property(nonatomic, assign) NSNumber* acceptedInteger;
@property(nonatomic, strong) NSData* dataOfPDF;
@property(nonatomic, assign) BOOL fromClientScreen;

@property(nonatomic,assign) BOOL pdfQuantity;
@property(nonatomic,assign) BOOL pdfRate;
@property(nonatomic,assign) BOOL pdfSubtotal;
@property(nonatomic,assign) BOOL pdfTotal;
@property(nonatomic,assign) BOOL pdfOptions;


- (IBAction) selectdateFromPicker:(id)sender;

@end
