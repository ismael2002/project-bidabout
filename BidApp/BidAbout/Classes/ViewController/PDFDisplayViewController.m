//
//  PDFDisplayViewController.m
//  BidAbout
//
//  Created by waseem  on 04/10/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "PDFDisplayViewController.h"
#import "PDFRenderer.h"

@interface PDFDisplayViewController ()

@end

@implementation PDFDisplayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"%@",self.estimateTitleId];
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(hideAction)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    //UIBarButtonItem* actionButton = [[UIBarButtonItem alloc] initWithTitle:@"Options" style:UIBarButtonItemStyleBordered target:self action:@selector(showOptions)];
    //self.navigationItem.rightBarButtonItem = actionButton;
    
    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    
    [PDFRenderer drawPDF:filePath dataArray:self.itemArray withTitle:self.screenTitle withClient:self.customer withDetail:self.detailList header:self.headerList withDate:self.date type:self.estimate photoList:self.photoList isinvoice:NO showQuantity:self.showQuantity showRate:self.showRate showSubtotalsOnly:self.subtotalsOnly showTotalsOnly:self.totalsOnly];
    
    self.data = [NSData dataWithContentsOfFile:filePath];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    NSURL *url = [NSURL fileURLWithPath:filePath];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
}

//- (void)showOptions
//{
//    UIActionSheet* actionsheet = nil;
//    
//    actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Toggle Quantity",@"Show Rate Only", @"Subtotals Only", @"Total Only", nil];
//    
//    [actionsheet showInView:self.view];
//}

//- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    switch (buttonIndex)
//    {
//        case 0:
//            [self toggleQuantity];
//            break;
//        case 1:
//            [self toggleRate];
//            break;
//        case 2:
//            [self showSubtotalsOnly];
//            break;
//        case 3:
//            [self showTotalsOnly];
//            break;
//        default:
//            break;
//    }
//}

//- (void) toggleQuantity
//{
//    if (self.showQuantity)
//    {
//        self.showQuantity = NO;
//    }
//        else {
//            self.showQuantity = YES;
//        }
//    
//    PDFDisplayViewController* pdfDVC = [[PDFDisplayViewController alloc] initWithNibName:@"PDFDisplayView" bundle:nil];
//
//    pdfDVC.estimateTitleId = self.title;
//    pdfDVC.itemArray = _itemArray;
//    pdfDVC.screenTitle = self.screenTitle;
//    pdfDVC.customer = self.customer;
//    pdfDVC.detailList = self.detailList;
//    pdfDVC.headerList = self.headerList;
//    pdfDVC.date = self.date;
//    pdfDVC.photoList = self.photoList;
//    pdfDVC.showQuantity = self.showQuantity;
//    pdfDVC.showRate = self.showRate;
//    pdfDVC.subtotalsOnly = NO;
//    pdfDVC.totalsOnly = NO;
//    
//    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
//    NSString* filePath = [UIUtils pathForDocumentNamed:path];
//    
//    [_webView reload];
//    
//    [PDFRenderer drawPDF:filePath dataArray:self.itemArray withTitle:self.screenTitle withClient:self.customer withDetail:self.detailList header:self.headerList withDate:self.date type:self.estimate photoList:self.photoList isinvoice:NO showQuantity:self.showQuantity showRate:self.showRate showSubtotalsOnly:self.subtotalsOnly showTotalsOnly:self.totalsOnly];
//
//    self.data = [NSData dataWithContentsOfFile:filePath];
//    
//    [_webView reload];
//}

//- (void) toggleRate
//{
//    if (self.showRate)
//    {
//        self.showRate = NO;
//    }
//    else {
//        self.showRate = YES;
//    }
//    
//    PDFDisplayViewController* pdfDVC = [[PDFDisplayViewController alloc] initWithNibName:@"PDFDisplayView" bundle:nil];
//    
//    pdfDVC.estimateTitleId = self.title;
//    pdfDVC.itemArray = _itemArray;
//    pdfDVC.screenTitle = @"que";
//    pdfDVC.customer = self.customer;
//    pdfDVC.detailList = self.detailList;
//    pdfDVC.headerList = self.headerList;
//    pdfDVC.date = self.date;
//    pdfDVC.photoList = self.photoList;
//    pdfDVC.showQuantity = self.showQuantity;
//    pdfDVC.showRate = self.showRate;
//    pdfDVC.subtotalsOnly = NO;
//    pdfDVC.totalsOnly = NO;
//    
//    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
//    NSString* filePath = [UIUtils pathForDocumentNamed:path];
//    
//    [_webView reload];
//    
//    [PDFRenderer drawPDF:filePath dataArray:self.itemArray withTitle:self.screenTitle withClient:self.customer withDetail:self.detailList header:self.headerList withDate:self.date type:self.estimate photoList:self.photoList isinvoice:NO showQuantity:self.showQuantity showRate:self.showRate showSubtotalsOnly:self.subtotalsOnly showTotalsOnly:self.totalsOnly];
//    
//    self.data = [NSData dataWithContentsOfFile:filePath];
//    
//    [_webView reload];
//}

//- (void) showSubtotalsOnly
//{
//    PDFDisplayViewController* pdfDVC = [[PDFDisplayViewController alloc] initWithNibName:@"PDFDisplayView" bundle:nil];
//    
//    pdfDVC.estimateTitleId = self.title;
//    pdfDVC.itemArray = _itemArray;
//    pdfDVC.screenTitle = self.screenTitle;
//    pdfDVC.customer = self.customer;
//    pdfDVC.detailList = self.detailList;
//    pdfDVC.headerList = self.headerList;
//    pdfDVC.date = self.date;
//    pdfDVC.photoList = self.photoList;
//    pdfDVC.showQuantity = self.showQuantity;
//    pdfDVC.showRate = self.showRate;
//    //pdfDVC.subtotalsOnly = YES;
//    pdfDVC.totalsOnly = NO;
//    
//    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
//    NSString* filePath = [UIUtils pathForDocumentNamed:path];
//    
//    [_webView reload];
//    
//    [PDFRenderer drawPDF:filePath dataArray:self.itemArray withTitle:self.screenTitle withClient:self.customer withDetail:self.detailList header:self.headerList withDate:self.date type:self.estimate photoList:self.photoList isinvoice:NO showQuantity:self.showQuantity showRate:self.showRate showSubtotalsOnly:YES showTotalsOnly:self.totalsOnly];
//    
//    self.data = [NSData dataWithContentsOfFile:filePath];
//    
//    [_webView reload];
//}

//- (void) showTotalsOnly
//{
//    PDFDisplayViewController* pdfDVC = [[PDFDisplayViewController alloc] initWithNibName:@"PDFDisplayView" bundle:nil];
//    
//    pdfDVC.estimateTitleId = self.title;
//    pdfDVC.itemArray = _itemArray;
//    pdfDVC.screenTitle = @"que";
//    pdfDVC.customer = self.customer;
//    pdfDVC.detailList = self.detailList;
//    pdfDVC.headerList = self.headerList;
//    pdfDVC.date = self.date;
//    pdfDVC.photoList = self.photoList;
//    pdfDVC.showQuantity = self.showQuantity;
//    pdfDVC.showRate = self.showRate;
//    pdfDVC.subtotalsOnly = self.subtotalsOnly;
//    //pdfDVC.totalsOnly = YES;
//    
//    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
//    NSString* filePath = [UIUtils pathForDocumentNamed:path];
//    
//    [_webView reload];
//    
//    [PDFRenderer drawPDF:filePath dataArray:self.itemArray withTitle:self.screenTitle withClient:self.customer withDetail:self.detailList header:self.headerList withDate:self.date type:self.estimate photoList:self.photoList isinvoice:NO showQuantity:self.showQuantity showRate:self.showRate showSubtotalsOnly:self.subtotalsOnly showTotalsOnly:YES];
//    
//    self.data = [NSData dataWithContentsOfFile:filePath];
//    
//    [_webView reload];
//}

- (void) hideAction
{
    [self.delegate addItemViewController:self didFinishEnteringDataItem:self.data];
    [self.delegate adjustPDFOptions:self quantity:self.showQuantity rate:self.showRate subtotal:self.subtotalsOnly totalsOnly:self.totalsOnly];
    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
}

#pragma mark -- webViewDelegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //[UIUtils messageAlert:@"Error occured in loading PDF file!" title:kApplicationName delegate:nil];
}

@end
