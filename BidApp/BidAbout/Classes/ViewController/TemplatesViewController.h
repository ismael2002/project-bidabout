//
//  TemplatesViewController.h
//  BidAbout
//
//  Created by     on 03/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TemplatesViewController : BaseViewController
{
    __weak IBOutlet UITableView *_tableView;

    NSArray* _templateList;
    NSArray* selectedTemplate;
    NSMutableArray* mutableFilteredTemplates;

}
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property(nonatomic, strong) Templates* templateChanged;

@end
