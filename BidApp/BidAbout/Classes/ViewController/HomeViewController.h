//
//  HomeViewController.h
//  BidAbout
//
//  Created by Waseem on 09/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : BaseViewController<UITableViewDelegate>
{
	__weak IBOutlet UITableView *_tableView;
    
	NSArray* _homeMenuList;
    NSArray* _homeMenuListImages;
}

@end
