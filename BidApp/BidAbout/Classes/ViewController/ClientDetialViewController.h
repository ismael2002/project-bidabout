//
//  ClientDetialViewController.h
//  BidAbout
//
//  Created by     on 03/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class Customers;

@interface ClientDetialViewController : BaseViewController<MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
{
    NSArray* _estimateList;
    NSArray* _invoiceList;

    UITextView* _notetextView;
    IBOutlet UITableView* _tableView;
    
    NSMutableDictionary* _customerDict;
    
    UITapGestureRecognizer* _tapGesture;
    //NSMutableDictionary* _invoiceDict;
    
    BOOL _isEditing;
}

@property (nonatomic, strong) Customers* customerObj;
@end
