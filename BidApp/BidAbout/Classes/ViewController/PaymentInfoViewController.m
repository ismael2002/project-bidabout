//
//  PaymentInfoViewController.m
//  BidAbout
//
//  Created by Waseem  on 14/08/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "PaymentInfoViewController.h"
#import "EditableDetailCell.h"
#import "Invoice.h"
@interface PaymentInfoViewController ()

@end

@implementation PaymentInfoViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Payment Info";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
	UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveBtnAction)];
	self.navigationItem.rightBarButtonItem = saveBtn;
    
    //self.startingPaidAmount = self.paidAmount;
    //NSNumber* startingPaidAmountDouble = [_gAppData getPaidAmount:self.invoiceID invoiceItemTitle:@"testing"];
    //NSLog(@"The staring paid double amount is: %@", startingPaidAmountDouble);

}

- (void)viewDidUnload
{
	_tableView = nil;
    _fullOrLessPaySwitch = nil;
    
	[super viewDidUnload];
}

#pragma mark --
#pragma mark  TableViewData source.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRow = 2;
    return numberOfRow;
}

- (UITableViewCell*) othersCell
{
    NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenifire];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
        _paymentTextField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    return cell;
}

- (EditableDetailCell*) editDetailCell
{
    EditableDetailCell* editCell = [[EditableDetailCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                             reuseIdentifier:nil];
    CGRect  rect = editCell.textField.frame;
    rect.origin.x =  editCell.textField.frame.origin.x + 50;
    rect.size.width = editCell.textField.frame.size.width - 50;
    editCell.textField.frame = rect;
     editCell.textField.keyboardType = UIKeyboardTypeNumberPad;
    
    return editCell;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = nil;
    switch (indexPath.row)
    {
        case 0:
        {
            cell = [self othersCell];
            [self setSwitch:cell];
            [cell.contentView addSubview:_fullOrLessPaySwitch];
            cell.textLabel.text = @"Fully Paid";
        }
            break;
            
        case 1:
        {
            cell = [self editDetailCell];
            UITextField *textField = [(EditableDetailCell*)cell textField];
            UILabel* titlelbl  = [(EditableDetailCell*)cell titleLabel];
            textField.tag = indexPath.row;
            
            _paymentTextField  = textField;
            _paymentTextField.delegate = self;
            if (_isFullyPaid)
            {
                _paymentTextField.text = [NSString stringWithFormat:@"%.02f",[self.totalAmount doubleValue]];
                _paymentTextField.userInteractionEnabled = NO;
            }
            else
            {
                _paymentTextField.text = [NSString stringWithFormat:@"%.02f",[self.paidAmount doubleValue]];
            }
            
            [titlelbl setText:@"Paid"];
        }
            break;
            
        default:
            break;
    }
    return cell;
}

- (void) setSwitch:(UITableViewCell*)cell
{
    if (_fullOrLessPaySwitch == nil)
    {
        float switchorigen = ([UIUtils isiPhone])? 140:200;
        _fullOrLessPaySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(cell.frame.origin.x + switchorigen, cell.frame.origin.y +8, cell.frame.size.width -180,cell.frame.size.height -10)];
        [_fullOrLessPaySwitch addTarget:self action:@selector(switchHasChangedValue:) forControlEvents:UIControlEventValueChanged];
        _fullOrLessPaySwitch.onTintColor = [UIUtils getTintColor];
    }
    if ([self.totalAmount doubleValue] == [self.paidAmount doubleValue])
        [_fullOrLessPaySwitch  setOn:YES animated:YES];
}

#pragma mark --
#pragma mark TextFiled

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger txtlength = [[textField text] length] - range.length + string.length ;
    
    if (txtlength > 8)
    {
        [UIUtils messageAlert:@"You can enter maximum 8 digit value!." title:kApplicationName delegate:nil];
        return NO;
    }
    
    self.paidAmount = textField.text;
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.paidAmount = textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}


#pragma mark --
#pragma mark segmentAction

- (void) switchHasChangedValue:(UISwitch*)switchObj
{
    NSNumber* taxvalue = [NSNumber numberWithBool:switchObj.isOn];
    _isFullyPaid = [taxvalue boolValue];
    
    if (_isFullyPaid)
    {
        _paymentTextField.text = [NSString stringWithFormat:@"%.02f",[self.totalAmount doubleValue]];
        _paymentTextField.userInteractionEnabled = NO;
    }
    else
    {
        _paymentTextField.text = [NSString stringWithFormat:@"%.02f",[self.paidAmount doubleValue]];
        _paymentTextField.userInteractionEnabled = YES;
    }
}

#pragma mark --
#pragma mark  Btn Action


- (void) saveBtnAction
{
    NSArray* invoiceArray = [_gAppData getInvoiceWithID:[self.invoiceID stringValue]];
    Invoice* invoiceObject = [invoiceArray objectAtIndex:0];
    double leftToPay = 0;
    leftToPay = [self.totalAmount doubleValue] - [invoiceObject.invoice_paidAmount doubleValue] ;
    NSString* stringLeftToPay = [NSString stringWithFormat:@"%f", leftToPay];
    
    if (_isFullyPaid)
    {
        if (self.paymentInfoDelegate && [self.paymentInfoDelegate respondsToSelector:@selector(savedPaymentItem:)])
        {
            NSLog(@"the left to pay is: %@", stringLeftToPay);
            
            [self.paymentInfoDelegate savedPaymentItem:stringLeftToPay];
        }
    }
    else
    {
    
        NSLog(@"The total amount is %@, and the paid amount is: %@, the left to pay is: %f", self.totalAmount, invoiceObject.invoice_paidAmount, leftToPay);
    
        [_paymentTextField resignFirstResponder];
        
        if ([self.paidAmount doubleValue] > [self.totalAmount doubleValue])
        {
            [UIUtils messageAlert:@"Please enter less or equal amount of total amount!." title:kApplicationName delegate:nil];
            return;
        }
    
        if ([invoiceObject.invoice_paidAmount doubleValue] + [self.paidAmount doubleValue] > [self.totalAmount doubleValue])
        {
            double amount = [invoiceObject.invoice_paidAmount doubleValue] + [self.paidAmount doubleValue] - [self.totalAmount doubleValue];
            NSLog(@"the amount that comes out, that is overflowed is: %f", amount);
        
            NSString* message = [NSString stringWithFormat:@"You have entered an amount higher than what is left to pay.\n Your remaining balance is: %.2f", leftToPay];
        
            self.paidAmount = 0;
            
            if (self.paymentInfoDelegate && [self.paymentInfoDelegate respondsToSelector:@selector(savedPaymentItem:)])
                [self.paymentInfoDelegate savedPaymentItem:self.paidAmount];
            
            [UIUtils messageAlert: message title:kApplicationName delegate:nil];
            return;
        }

    
    
        if (self.paymentInfoDelegate && [self.paymentInfoDelegate respondsToSelector:@selector(savedPaymentItem:)])
            [self.paymentInfoDelegate savedPaymentItem:self.paidAmount];
    }
    
    
    NSLog(@"The payment amount is: %@", self.paidAmount);
    NSLog(@"The total amount is: %@", self.totalAmount);
    //NSLog(@"The initial paid amount is: %@", invoiceObject.invoice_paidAmount);
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
