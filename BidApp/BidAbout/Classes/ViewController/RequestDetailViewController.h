//
//  RequestDetailViewController.h
//  BidAbout
//
//  Created by     on 18/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customers;

@interface RequestDetailViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView* _tableView;
}

@property (nonatomic, strong) Customers* customerObj;
@end
