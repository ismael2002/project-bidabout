//
//  ItemDescriptionViewController.m
//  BidAbout
//
//  Created by    Kumar  on 09/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "ItemDescriptionViewController.h"
#import "UIUtils.h"

@interface ItemDescriptionViewController ()

@end

@implementation ItemDescriptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"The nib name is:%@", nibBundleOrNil);
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"view did load on item description view controller");
    
    if(self.isComment)
        self.title = @"Comment";
    else
        self.title = @"Item Description";
    
    //if its a template, it changes to the correc title
    if (self.isTemplate)
    {
        self.title = @"Terms and Conditions";
    }

    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    [_itemDescTextV becomeFirstResponder];
    
    _itemDescTextV.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    if (self.estimateComment.length > 0)
        _itemDescTextV.text = self.estimateComment;

    // Cancel bar button item.
	UIBarButtonItem* cancelBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
	self.navigationItem.leftBarButtonItem = cancelBarBtn;
    
	UIBarButtonItem* nextBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnAction)];
	self.navigationItem.rightBarButtonItem = nextBarBtn;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
}

- (void) viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

#pragma mark --
#pragma mark Btn Action

- (void) cancelBtnAction
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) saveBtnAction
{
    NSLog(@"save button tapped 0");
    
//    if (self.itemDescDelegate && [self.itemDescDelegate respondsToSelector:@selector(addComment:)])
//    {
//        [self.itemDescDelegate addComment:_itemDescTextV.text];
//    }

    [self.delegate addComment:_itemDescTextV.text];    
    
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark --
#pragma mark NSNotification 

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    NSLog(@"textfield did begin editing");
//    [self animateTextField: textField up: YES];
//}
//
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    NSLog(@"textfield did end editing");
//    [self animateTextField: textField up: NO];
//}
//
//- (void) animateTextField: (UITextField*) textField up: (BOOL) up
//{
//    const int movementDistance = 80; // tweak as needed
//    const float movementDuration = 0.3f; // tweak as needed
//    
//    int movement = (up ? -movementDistance : movementDistance);
//    
//    [UIView beginAnimations: @"anim" context: nil];
//    [UIView setAnimationBeginsFromCurrentState: YES];
//    [UIView setAnimationDuration: movementDuration];
//    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
//    [UIView commitAnimations];
//}

- (void) keyBoardWillShow:(NSNotification *)notification
{
    NSLog(@"The keyboard will show");
    CGRect rect = _itemDescTextV.frame;
    if ([UIUtils isiPhone])
        rect.size.height = rect.size.height - 260;
    else
        rect.size.height = rect.size.height - 70;

    _itemDescTextV.frame = rect;
}

- (void) keyboardWillHide:(NSNotification *)notification
{
    NSLog(@"The keyboard will hide");
//    CGRect rect = _itemDescTextV.frame;
//    if ([UIUtils isiPhone])
//        rect.size.height = rect.size.height + 216;
//    else
//        rect.size.height = rect.size.height + 70;
//    
//    _itemDescTextV.frame = rect;
    
     _itemDescTextV.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}



@end
