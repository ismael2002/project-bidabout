//
//  LoginViewController.h
//  BidAbout
//
//  Created by Waseem on 09/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : BaseViewController
{
	__weak IBOutlet UITextField* _userNameTextField;
	__weak IBOutlet UITextField* _passwordTextField;
    __weak IBOutlet UILabel* _versionLbl;
    __weak IBOutlet UIButton* _rememberMeBtn;

    
    UITapGestureRecognizer* _tapgesture;
}

- (IBAction) loginButtonAction:(id)sender;
- (IBAction) radioButtonAction:(UIButton*)sender;
- (IBAction) signUpAction:(id)sender;
- (IBAction) forgotPasswordAction:(id)sender;

@end
