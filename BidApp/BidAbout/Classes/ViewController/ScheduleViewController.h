//
//  ScheduleViewController.h
//  BidAbout
//
//  Created by    Kumar  on 09/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol ScheduleViewControllerDelegate<NSObject>
//@optional
//- (void) savedItem:(NSString*)itemIDStr;
//@end


@protocol TemplateAdded <NSObject>

- (void) templateAdded:(Templates *)template completion:(void (^)(void))block;

@end

@interface ScheduleViewController : BaseViewController<UITextFieldDelegate, UITableViewDelegate, UIActionSheetDelegate>
{
    IBOutlet UITableView*  _tableView;
    UITextView* _notetextView;
    UITextView* _titletextView;
    
    NSMutableDictionary* _itemDict;
    
    UITextField* _amountTextField;
    UITextField* _rateTextField;
    UITextField* _quantTextField;
    UISwitch* _taxApplicableSwitch;
}

@property (weak, nonatomic) IBOutlet UITableView *mainTableview;
@property (nonatomic, strong) Templates* templet;
@property (nonatomic, strong) id product;
@property (nonatomic, assign) BOOL isUpdateEstimate;
@property (nonatomic, assign) BOOL isAddEstimate;
@property (nonatomic, assign) BOOL isUpdateInvoice;
@property (nonatomic, assign) BOOL isAddInvoice;
@property (nonatomic, assign) BOOL isProductItem;
@property (nonatomic, assign) BOOL addInvoiceProduct;
@property (nonatomic, assign) BOOL fromProductsScreen;
@property (nonatomic, assign) BOOL isTemplate;
@property (nonatomic, assign) BOOL existingTemplateItem;
@property (nonatomic, assign) BOOL addToEstimate;
@property (nonatomic, assign) BOOL creatingTemplateFromEstimate;
@property (nonatomic, assign) BOOL updateInvoiceOrAddToProductList;
@property (nonatomic, assign) BOOL fromClientScreen;

@property (nonatomic, assign) BOOL isEstimate;
@property (nonatomic, strong) id<TemplateAdded> templateAddedDelegate;

@end
