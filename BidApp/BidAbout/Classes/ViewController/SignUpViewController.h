//
//  SignUpViewController.h
//  BidAbout
//
//  Created by    Kumar  on 10/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardControls.h"


@interface SignUpViewController : BaseViewController <UITextFieldDelegate,KeyboardControlsDelegate>
{
    IBOutlet UITextField* _firstNameTxtField;
    IBOutlet UITextField* _LastNameTxtField;
    IBOutlet UITextField* _phoneNumberTxtField;
    IBOutlet UITextField* _emailTxtField;
    IBOutlet UITextField* _passwordTxtField;
    IBOutlet UITextField* _confirmPasswrdTxtField;
    
    IBOutlet UIScrollView* _scrollView;
    
    KeyboardControls* _keyboardControls;
}

@end
