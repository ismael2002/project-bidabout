//
//  EditCustomerViewController.m
//  BidAbout
//
//  Created by    Kumar  on 08/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "EditCustomerViewController.h"
#import "Customers.h"
#import "EditableDetailCell.h"

@interface EditCustomerViewController ()

@end

@implementation EditCustomerViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Customer";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // Cancel bar button item.
//	UIBarButtonItem* cancelBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
//	self.navigationItem.leftBarButtonItem = cancelBarBtn;
//	UIBarButtonItem* nextBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnAction)];
//	self.navigationItem.rightBarButtonItem = nextBarBtn;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma mark --
#pragma mark TableViewDataSource 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            if (self.customerObj)
                cell.textLabel.text = [NSString stringWithFormat:@"Name: %@ %@",_customerObj.customer_firstname,_customerObj.customer_lastname];
        }
            break;
            
        case 1:
        {
            if (self.customerObj)
                cell.textLabel.text = [NSString stringWithFormat:@"Email: %@",self.customerObj.customer_email];
            
        }
            break;
            
        case 2:
        {
            if (self.customerObj)
                cell.textLabel.text = [NSString stringWithFormat:@"Address: %@",self.customerObj.customer_addres1];
            
        }
            break;
            
        case 3:
        {
            if (self.customerObj)
                cell.textLabel.text = [NSString stringWithFormat:@"City: %@",self.customerObj.customer_city];
            
        }
            break;
        case 4:
        {
            if (self.customerObj)
                cell.textLabel.text = [NSString stringWithFormat:@"State: %@",self.customerObj.customer_state];
        }
            break;
            
        case 5:
        {
            if (self.customerObj)
                cell.textLabel.text = [NSString stringWithFormat:@"Zip: %@",self.customerObj.customer_zip];
        }
            break;
            
        case 6:
        {
            if (self.customerObj)
                cell.textLabel.text = [NSString stringWithFormat:@"Phone: %@",self.customerObj.customer_mobilePhone];
        }
            break;
    }
    return cell;
}

//- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
//{
//    static NSString* cellIdentifier = @"CellIdentifier";
//
//	EditableDetailCell* cell = (EditableDetailCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//	if (cell == nil)
//	{
//		cell = [[EditableDetailCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                         reuseIdentifier:nil];
//        UITextField *textField = [cell textField];
//        textField.tag = indexPath.row;
//        switch (textField.tag)
//        {
//            case 0:
//            {
//                if (self.customerObj)
//                {
//                    textField.text = [NSString stringWithFormat:@"%@ %@",_customerObj.customer_firstname,_customerObj.customer_lastname];
//                }
//                else
//                {
//                    [textField setPlaceholder:@"Customer Name"];
//                }
//                [textField becomeFirstResponder];
//            }
//                break;
//                
//            case 1:
//            {
//                if (self.customerObj)
//                {
//                    textField.text = [NSString stringWithFormat:@"%@",self.customerObj.customer_email];
//                }
//                else
//                {
//                    [textField setPlaceholder:@"Email:"];
//                }
//            }
//                break;
//            case 2:
//            {
//                if (self.customerObj)
//                {
//                    textField.text = [NSString stringWithFormat:@"%@",self.customerObj.customer_addres1];
//                }
//                else
//                {
//                    [textField setPlaceholder:@"Address:"];
//                }                
//            }
//                break;
//            case 3:
//            {
//                if (self.customerObj)
//                {
//                    textField.text = [NSString stringWithFormat:@"%@",self.customerObj.customer_city];
//                }
//                else
//                {
//                    [textField setPlaceholder:@"City:"];
//                }
//            }
//                break;
//            case 4:
//            {
//                if (self.customerObj)
//                {
//                    textField.text = [NSString stringWithFormat:@"%@",self.customerObj.customer_state];
//                }
//                else
//                {
//                    [textField setPlaceholder:@"State:"];
//                }
//            }
//                  break;
//            case 5:
//            {
//                if (self.customerObj)
//                {
//                    textField.text = [NSString stringWithFormat:@"%@",self.customerObj.customer_zip];
//                }
//                else
//                {
//                    [textField setPlaceholder:@"Zip:"];
//                }
//            }
//                  break;
//                
//            case 6:
//            {
//                if (self.customerObj)
//                {
//                    textField.text = [NSString stringWithFormat:@"%@",self.customerObj.customer_mobilePhone];
//                }
//                else
//                {
//                    [textField setPlaceholder:@"Phone:"];
//                }
//            }
//                break;
//        }
//        [textField setDelegate:self];
//	}
//
//    UITextField *textField = [(EditableDetailCell*)cell textField];
//    [textField setText:textField.text];
//    return cell;
//}
//
//#pragma mark --
//#pragma mark TextField Delegate
//
//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    switch (textField.tag)
//    {
//        case 0:
//        {
//            NSLog(@"filed %d",textField.tag);
//        }
//            break;
//            
//        case 1:
//        {
//            NSLog(@"filed %d",textField.tag);
//
//        }
//            break;
//        case 2:
//        {
//            NSLog(@"filed %d",textField.tag);
//
//        }
//            break;
//        case 3:
//        {
//            NSLog(@"filed %d",textField.tag);
//
//        }
//            break;
//        case 4:
//        {
//            NSLog(@"filed %d",textField.tag);
//
//        }
//            break;
//    }
//}

//#pragma mark --
//#pragma mark Button Action 
//
//- (void) cancelBtnAction
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//- (void) saveBtnAction
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}

@end
