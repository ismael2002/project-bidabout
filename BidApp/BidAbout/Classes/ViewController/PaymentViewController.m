//
//  PaymentViewController.m
//  BidAbout
//
//  Created by Marc Cain on 5/5/15.
//  Copyright (c) 2015 BolderImage. All rights reserved.
//

#import "PaymentViewController.h"
#import <StoreKit/StoreKit.h>

@interface PaymentViewController () <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@end

@implementation PaymentViewController

#define kRemoveAdsProductIdentifier @"com.BidAbout.TestPaypal"

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Pay Pal";
    self.textView.delegate = self;
    self.userName.delegate = self;
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    UIButton* barButton = [UIUtils getRevelButtonItem:self];
    
    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    
    UIBarButtonItem* saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveUserInformation)];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    NSDictionary* paypalSettings = [_gAppData getPaypalInformation];
    NSString* userName = [paypalSettings valueForKey:@"userName"];
    
    if (userName.length > 0)
    {
        NSString* domain = [paypalSettings valueForKey:@"domain"];
        NSString* emailService = [paypalSettings valueForKey:@"emailService"];
        
        NSMutableString* emailAddress = [[NSMutableString alloc]init];
        
        [emailAddress appendString:userName];
        [emailAddress appendString:@"@"];
        [emailAddress appendString:emailService];
        [emailAddress appendString:@"."];
        [emailAddress appendString:domain];
        
        self.userName.text = emailAddress;
    }
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    // - comparing the date
    NSDate *purchaseDate = [currentInstallation objectForKey:@"PaypalPurchase"];
    NSDate *today = [NSDate date];
    
    NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger units = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
    NSDateComponents *components = [gregorian components:units fromDate:purchaseDate toDate:today options:0];
    
    NSInteger years = [components year];
    NSInteger months = [components month];
    NSInteger days = [components day];
    
    // tests if the one year subscription is over
    if (years >= 1)
    {
        NSLog(@"years is: %ld", (long)years);
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isPaypalAvailable"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }  else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isPaypalAvailable"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSLog(@"Years: %ld, Months: %ld, Days: %ld", (long)years, (long)months, (long)days);
    NSLog(@"The purchase date is: %@", purchaseDate);
}

-(void)dismissKeyboard
{
    [self.textView resignFirstResponder];
    [self.userName resignFirstResponder];
}

//paypal information
-(void) saveUserInformation
{
    NSString* email = self.userName.text;
    NSInteger indexOfAt = [email rangeOfString:@"@"].location;
    NSInteger dotLocation = [email rangeOfString:@"."].location;
    
    if ( ![self validateEmailWithString:email] )
    {
        [UIUtils messageAlert:@"Please enter a valid email address." title:@"Warning" delegate:nil];
        // please enter a valid email address
        
    } else
    {
        NSString* userName = [email substringWithRange:NSMakeRange(0, indexOfAt)];
        //NSLog(@"The user name is: %@", userName);
        
        NSString* emailService = [email substringWithRange:NSMakeRange(indexOfAt + 1, (dotLocation - indexOfAt)- 1 )];
        //NSLog(@"The email service is: %@", emailService);
        
        NSString* endFormat = [email substringWithRange:NSMakeRange(dotLocation + 1 , (email.length - (dotLocation + 1)) )];
        //NSLog(@"The dot format is: %@", endFormat);
        
        //updating the information
        [_gAppData updatePaypalUserName:userName emailProvider:emailService domain:endFormat];
        [UIUtils messageAlert:@"Email address successfully saved." title:@"" delegate:nil];
    }
    
    NSLog(@"the information from the settings is: %@", [_gAppData getPaypalInformation]);
    
    [self dismissKeyboard];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.userName resignFirstResponder];
    
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (IBAction)addPaypal
{
    if([SKPaymentQueue canMakePayments]){
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:@"com.BidAbout.paypal"]];
        
        productsRequest.delegate = self;
        [productsRequest start];
    }
    else{
        NSLog(@"User cannot make payments due to parental controls");
        //this is called the user cannot make payments, most likely due to parental controls
    }
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct)
    {
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (IBAction)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (IBAction) restore{
    //this is called when the user restores purchases, you should hook this up to a button
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %lu", (unsigned long)queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");
            
            [self doRemoveAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for(SKPaymentTransaction *transaction in transactions)
        {
        switch(transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
                
            case SKPaymentTransactionStateDeferred: NSLog(@"SK Payment Transaction State Deferred");
                break;
                
            case SKPaymentTransactionStatePurchased:
                //this is called when the user has successfully purchased the package (Cha-Ching!)
                [self doRemoveAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                break;
                
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                //called when the transaction does not finish
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}

- (void)doRemoveAds
{
    //need to keep track when it was purchased
    
    NSDate* startSubscription = [NSDate date];
    NSLog(@"start subscription is: %@", startSubscription);
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setObject:startSubscription forKey:@"PaypalPurchase"];
    [currentInstallation saveInBackground];
    
    NSLog(@"The purchase went through");
    
    
    
    paypalAdded = YES;

    
    [[NSUserDefaults standardUserDefaults] setBool:paypalAdded forKey:@"isPaypalAvailable"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
