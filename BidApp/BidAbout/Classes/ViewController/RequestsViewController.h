//
//  RequestsViewController.h
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestsViewController : BaseViewController
{
	__weak IBOutlet UITableView* _tableView;
    
    NSArray* _requestCustomerList;
}

@end