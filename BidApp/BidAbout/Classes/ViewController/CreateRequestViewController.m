//
//  CreateRequestViewController.m
//  BidAbout
//
//  Created by Waseem on 04/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "CreateRequestViewController.h"

@interface CreateRequestViewController ()

@end

@implementation CreateRequestViewController


#pragma mark--ViewLife Cycle.

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Request #";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // Cancel bar button item.
	UIBarButtonItem* cancelBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
	self.navigationItem.leftBarButtonItem = cancelBarBtn;
	
	UIBarButtonItem* nextBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(nextBtnAction)];
	self.navigationItem.rightBarButtonItem = nextBarBtn;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma mark Action

- (void) cancelBtnAction
{
	UIActionSheet* actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Don't Save",@"Cancel", nil];
	actionsheet.tag = 1001;
	actionsheet.destructiveButtonIndex = 2;
	[actionsheet showInView:self.view];
}

- (void) nextBtnAction
{
	UIActionSheet* actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Preview",@"Email",@"Print", nil];
	actionsheet.tag = 1002;
	[actionsheet showInView:self.view];
}

#pragma mark -- Action Sheet Delegate

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(actionSheet.tag == 1001)
	{
		[self cancelActionSheatAction:buttonIndex];
	}
	else if(actionSheet.tag == 1002)
	{
		[self nextActionSheatAction:buttonIndex];
	}
}

- (void) cancelActionSheatAction:(NSInteger) btnIndex
{
	switch (btnIndex)
	{
		case 0:
			NSLog(@"Save clicked");
			break;
		case 1:
            [self.navigationController popViewControllerAnimated:YES];
            
			break;
		case 2:
            NSLog(@"Don't Save clicked");
			break;
			
		default:
			break;
	}
}

- (void) nextActionSheatAction:(NSInteger) btnIndex
{
	switch (btnIndex)
	{
		case 0:
			NSLog(@"Save clicked");
			break;
		case 1:
			NSLog(@"Preview clicked");
			break;
		case 2:
			NSLog(@"Email clicked");
			break;
		case 3:
			NSLog(@"Print clicked");
			break;
			
		default:
			break;
	}
}

@end
