//
//  InvoiceViewController.m
//  BidAbout
//
//  Created by     on 11/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "InvoiceViewController.h"
#import "CreateInvoiceViewController.h"
#import "Customers.h"
#import "Invoice.h"
#import "ScheduleViewController.h"
#import "InvoiceItem.h"

@interface InvoiceViewController ()

@end

@implementation InvoiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Invoices";
    NSLog(@"This is the invoice view controller");
    
    // segmented control - modifiers
    CGRect frame= self.segmentedControl.frame;
    [self.segmentedControl setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 32)];
    
    [self.segmentedControl addTarget:self
                                action:@selector(segControlClicked:)
                      forControlEvents:UIControlEventValueChanged];
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // get revelButton
    UIButton* barButton = [UIUtils getRevelButtonItem:self];
    
    //Added revel button as nevegation button
    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    
    UIBarButtonItem* addBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewInvoiceAction)];
	self.navigationItem.rightBarButtonItem = addBarBtn;
    
    //_invoiceList = [[NSMutableArray alloc]init];
    
    //[self segControlClicked:_invoiceList];
    //[_tableView reloadData];
    
    if (sizeof(void*) == 4) {
        NSLog(@"You're running in 32 bit InvoiceViewController");
        
    } else if (sizeof(void*) == 8) {
        NSLog( @"You're running in 64 bit InvoiceViewController");
    }
    
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    _invoiceList = [[NSMutableArray alloc]init];
    
    [_gAppData deleteItemsFromCurrentInvoice];
    [self segControlClicked:_invoiceList];
    
    
    
    [_tableView reloadData];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _invoiceList.count;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    NSLog(@"=== cell for row at index path");
    
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    // removing subviews before adding updated ones, so that they wont be on top of each other
    for (UIView *view in [cell.contentView subviews])
    {
        [view removeFromSuperview];
    }
    
    NSDictionary* dict = [_invoiceList objectAtIndex:indexPath.row];
    
    if (dict)
    {
        NSLog(@"If there is a dictionary");

        //gets invoice
        NSArray* invoiceClicked = [_invoiceList objectAtIndex:indexPath.row];
        NSString* invoiceID = [invoiceClicked valueForKey:@"InvoiceId"];
        NSArray* invoiceArray = [_gAppData getInvoiceWithID:invoiceID];
        Invoice* invoice = invoiceArray[0];
        
        NSLog(@"THe invoice item for cell row %ld, and the invoice itself is: %@", (long)indexPath.row, invoice);
        
        
        
        cell.textLabel.text = [self getCellTittle:dict];
        cell.detailTextLabel.text = [self getInvoiceID:dict];
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(190.0, 2.0, 90.0, 19.0)];
        [dateLabel setTag:indexPath.row];
        [dateLabel setBackgroundColor:[UIColor clearColor]]; // transparent label background
        [dateLabel setFont:[UIFont boldSystemFontOfSize:13.0]];
        dateLabel.text = [self getCellDate:dict];
        dateLabel.textAlignment = NSTextAlignmentRight;
        [cell.contentView addSubview:dateLabel];
        
        UILabel *amountOpen = [[UILabel alloc] initWithFrame:CGRectMake(190.0, 23.0, 90.0, 19.0)];
        [amountOpen setTag:indexPath.row];
        [amountOpen setBackgroundColor:[UIColor clearColor]];
        [amountOpen setFont:[UIFont boldSystemFontOfSize:13.0]];
        amountOpen.text = [NSString stringWithFormat: @"%.2f", [self getRemainingAmmount:dict]];
        amountOpen.textAlignment = NSTextAlignmentRight;
        [cell.contentView addSubview:amountOpen];
    }
    return cell;
}


#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
 
    NSArray* invoiceClicked = [_invoiceList objectAtIndex:indexPath.row];
    NSString* invoiceID = [invoiceClicked valueForKey:@"InvoiceId"];
    NSArray* invoiceArray = [_gAppData getInvoiceWithID:invoiceID];
    
    Invoice* invoice = invoiceArray[0];
    
    
    NSLog(@"invoicearray is: %@", invoice);
    

    CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
    viewC.invoiceObj = invoice;
    viewC.existingInvoice = YES;
    
    [self.navigationController pushViewController:viewC animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

// need to delete the invoice appropriately
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //remove the deleted object from your data source.
//        //If your data source is an NSMutableArray, do this
//        
//        NSDictionary* dict = [_invoiceList objectAtIndex:indexPath.row];
//        NSString* invoiceID = [dict objectForKey:@"item_ID"];
//        
//        NSLog(@"ITEM ID EXTRACTED");
//        
//        //[_gAppData deleteInvoiceItem:<#(NSString *)#>]
//        
//        [tableView reloadData]; // tell table to refresh now
//    }
//}


#pragma mark --
#pragma mark Selector Methods

- (void) addNewInvoiceAction
{
    CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
    [self.navigationController pushViewController:viewC animated:YES];
}

#pragma mark --
#pragma mark Methods

- (NSString*) getCellTittle:(NSDictionary*)dict
{
    NSString* titeStr = @"";
    NSString* fName = [dict objectForKey:@"FirstName"];
    titeStr = (fName.length > 0)? fName:titeStr;
    
    NSString* lName = [dict objectForKey:@"LastName"];
    titeStr = (lName.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,lName]:titeStr;
    
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
//    NSString* dateStr = [dateFormatter stringFromDate:[dict objectForKey:@"InvoiceDate"]];
//    
//    titeStr = (dateStr.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,dateStr]:titeStr;
    return titeStr;
}

- (NSString*) getCellDate: (NSDictionary*) dict
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    [dateFormatter setDateFormat:@"MM-dd-yy"];
    NSString* dateStr = [dateFormatter stringFromDate:[dict objectForKey:@"InvoiceDate"]];
    
    return dateStr;
}

-(NSString*) getInvoiceID: (NSDictionary*) dict
{
    //setting up the invoice number
    NSString* invoiceNumber = [NSString stringWithFormat:@"%ld",(long)[[dict objectForKey:@"InvoiceNumber"] integerValue]];
    
    NSLog(@"The invoice ID is: %@", invoiceNumber);
    NSLog(@"The dictionary is: %@", dict);
    
    return invoiceNumber;
}

//getting the amount that remains in the invoice
- (double) getRemainingAmmount: (NSDictionary*) dict
{
    NSLog(@"get remaining ammount, the dictionary is: %@", dict);
    
    double tax = [_gAppData getTaxWithInvoiceID:[dict objectForKey:@"InvoiceId"]];
    NSLog(@"The results of the taxes are: %f", tax);
    
    double amount = 0;
    amount = [[dict objectForKey:@"PaidAmount"] doubleValue];
    //NSInteger invoiceID = [[dict objectForKey:@"InvoiceId"] integerValue];
    NSArray*  itemArray = [NSMutableArray arrayWithArray:[_gAppData getInvoiceItemsWithInvoiceID:[dict objectForKey:@"InvoiceId"]]];
    
    NSNumber* invoiceID = [dict objectForKey:@"InvoiceId"];
  
    double totalAmountForInvoice = 0;
    double tempDoubleValue = 0;
    NSString* tempItemAmount;
    
    NSLog(@"about to start total amount calculation");
    
    for (NSInteger x = 0; x < itemArray.count; x++)
    {
        InvoiceItem* item = itemArray[x];
        
        tempItemAmount = item.item_amount;
        tempDoubleValue = [tempItemAmount doubleValue];
    
        totalAmountForInvoice = totalAmountForInvoice + tempDoubleValue;
        
        NSLog(@"The total amount for invoice before anything is: %f", totalAmountForInvoice);
    }
    
    //add the tax
    double taxPercent = 0;
    double taxAmount = 0;
    double totalAmountForInvoiceAfterTax = 0;
    
    taxPercent = .01 * tax;
    taxAmount = taxPercent * totalAmountForInvoice;
    totalAmountForInvoiceAfterTax = totalAmountForInvoice + taxAmount;

    NSLog(@"The tax is: %f, the tax percent is: %f, the tax amount is: %f, the total amount after tax is: %f", tax, taxPercent,taxAmount, totalAmountForInvoiceAfterTax);
    
    
    double finalAmount =  0;
    finalAmount = totalAmountForInvoiceAfterTax - amount;
    
    NSLog(@"== The final amount is: %f, for invoiceid: %@", finalAmount, invoiceID);
    
    
    return finalAmount;
}

- (NSArray*) getSortedInvoice
{
    NSArray* invoiceArray = [_gAppData getInvoicelistWithId:nil];
    NSMutableArray* invoiceDetailArray = [[NSMutableArray alloc] init];
    
    NSLog(@"The invoice array is at sorted invoice is: %@", invoiceArray);

    for (Invoice* invoiceObj in invoiceArray)
    {
        NSMutableDictionary* dict  = [[NSMutableDictionary alloc] init];
        [dict setObject:invoiceObj.invoice_id forKey:@"InvoiceId"];

        //getting invoice number ---
        [dict setObject:invoiceObj.invoice_number forKey:@"InvoiceNumber"];
        
        [dict setObject:invoiceObj.invoice_paidAmount forKey:@"PaidAmount"];
        [dict setObject:invoiceObj.invoice_item forKey:@"InvoiceItem"]; // getting the item to calculate what was the total amount
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        NSDate* dateOfInvoice = [dateFormatter dateFromString:invoiceObj.invoice_date];
        dateOfInvoice = dateOfInvoice == nil ? [NSDate date] : dateOfInvoice;
        [dict setObject:dateOfInvoice forKey:@"InvoiceDate"];
        
        NSArray* arrayOfClient = [_gAppData getCustomerDetails:NO andCustomerId:invoiceObj.invoice_ClientId];
        if (arrayOfClient.count > 0)
        {
            Customers* customer = [arrayOfClient objectAtIndex:0];
            [dict setObject:customer.customer_firstname forKey:@"FirstName"];
            [dict setObject:customer.customer_lastname forKey:@"LastName"];
        }
        [invoiceDetailArray addObject:dict];
    }
    
//    NSSortDescriptor *fSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
//    NSSortDescriptor *lSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"FirstName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor *dSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"InvoiceDate" ascending:YES selector:@selector(compare:)];
    
    NSArray* sortDescriptors = [NSArray arrayWithObjects:/*fSortDescriptor,lSortDescriptor,*/dSortDescriptor, nil];
    NSArray* sortedArray = [invoiceDetailArray sortedArrayUsingDescriptors:sortDescriptors];
    return sortedArray;
}

- (IBAction)segControlClicked:(id)sender
{
    NSInteger amountRemaining;
    NSMutableArray* finalSortedArray = [[NSMutableArray alloc] init] ;
    _invoiceList = [self getSortedInvoice];
    
    NSLog(@"after getting the sorted array, the aray is: %@", _invoiceList);
    
    if (self.segmentedControl.selectedSegmentIndex == 0)
    {
        for (NSInteger x = 0; x < _invoiceList.count; x++)
        {
            NSDictionary* dict = [_invoiceList objectAtIndex:x];
            double doubleAmountRemaining = 0;
            doubleAmountRemaining = [self getRemainingAmmount:dict];
            
            double withoutDecimal = 0;
            withoutDecimal = doubleAmountRemaining * 100;
            amountRemaining = (NSInteger)withoutDecimal;
            
            if (amountRemaining > 0)
            {
                [finalSortedArray addObject:dict];
            }
        }
    }
    
    if (self.segmentedControl.selectedSegmentIndex == 1)
    {
        
        for (NSInteger y = 0; y < _invoiceList.count; y++)
        {
            NSDictionary* dict = [_invoiceList objectAtIndex:y];
            double doubleAmountRemaining = 0;
            doubleAmountRemaining = [self getRemainingAmmount:dict];
            
            double withoutDecimal = 0;
            withoutDecimal = doubleAmountRemaining * 100;
            amountRemaining = (NSInteger)withoutDecimal;
            
            NSLog(@"amount remaining is: %ld", (long)amountRemaining);
            
            if (amountRemaining < 1)
            {
                [finalSortedArray addObject:dict];
            }
        }
    }
    _invoiceList = [[NSArray alloc] initWithArray:finalSortedArray];
    [_tableView reloadData];
}
@end
