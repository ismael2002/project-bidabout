//
//  ClientDetialViewController.m
//  BidAbout
//
//  Created by     on 03/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "Customers.h"
#import "Invoice.h"
#import "Estimate.h"
#import "EditableDetailCell.h"
#import "CreateInvoiceViewController.h"
#import "CreateEstimateViewController.h"
#import "ClientDetialViewController.h"

typedef enum textFiledType
{
    EComopanyNameTextFiled = 500,
    ECustomerFirstNameTxtFiled,
    ECustomerLastNameTxtField,
    ECustomerNoteTextField,
    ECustomerPhoneTextField
} ECustomerTextFieldType;

#define kScrollWidth 320
#define kiPhoneScrollFrameHeightWithoutKeyboard 460
#define kiPhone5ScrollFrameHeightWithoutKeyboard 548

#define kiPhone5ScrollFrameHeightWithKeyboard 246
#define kiPhoneScrollFrameHeightWithKeyboard 220

@interface ClientDetialViewController () <UITextFieldDelegate,UITextViewDelegate>

@end

@implementation ClientDetialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _estimateList = [[NSArray alloc]init];
    _invoiceList = [[NSArray alloc] init];
    
    _tableView.backgroundColor = [UIColor clearColor];
    
    UIBarButtonItem* addBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBarBtnAction)];
    
    UIBarButtonItem* editBtn = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editBtnAction:)];
	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:addBarBtn,editBtn,nil];
    
    
    
    UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelBtnAction)];
    
	//UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
    
    
    
	self.navigationItem.leftBarButtonItem = cancelBtn;
	
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    NSLog(@"Client detail view controller");
    [_gAppData deleteItemsFromCurrentEstimate];
    [_gAppData deleteItemsFromCurrentInvoice];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    _estimateList = [self getSortedEstimate];
    _invoiceList = [self getSortedInvoice];
    [self initialSetUp];
}

- (void) initialSetUp
{
    if (_customerDict)
        _customerDict = nil;
    
    _customerDict = [[NSMutableDictionary alloc] initWithCapacity:15];
    
    NSArray* arrayofCust = [_gAppData getCustomerDetails:NO andCustomerId:[NSString stringWithFormat:@"%ld",(long)[self.customerObj.customer_id integerValue]]];
    
    if (arrayofCust.count > 0)
        self.customerObj = [arrayofCust objectAtIndex:0];
    
    if (self.customerObj)
    {
        [_customerDict setObject:[NSString stringWithFormat:@"%ld",(long)[self.customerObj.customer_id integerValue]] forKey:@"CustomerID"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_company] forKey:@"CustomerMemberCompany"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_firstname] forKey:@"CustomerFirstName"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_lastname] forKey:@"CustomerLastName"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_addres1] forKey:@"CustomerAddress1"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_addres2] forKey:@"CustomerAddress2"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_city] forKey:@"CustomerCity"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_state] forKey:@"CustomerState"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_zip] forKey:@"CustomerZip"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_mobilePhone] forKey:@"CustomerMobilePhone"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_email] forKey:@"CustomerEmail"];
        [_customerDict setObject:[ self validateForBlank:self.customerObj.customer_notes] forKey:@"CustomerNotes"];
		[_customerDict setObject:[NSString stringWithFormat:@"%ld",(long)[self.customerObj.customer_quotes integerValue]] forKey:@"CustomerQuotes"];
    }
    [_tableView reloadData];
}

- (NSString*) validateForBlank:(NSString*)str
{
    return ((str.length > 0) && (str != nil))?str: @"";
}

- (void)sendMail:(NSString *)emailID
{
	if([MFMailComposeViewController canSendMail])
	{
        
		MFMailComposeViewController* mailVC = [[MFMailComposeViewController alloc] init];
		mailVC.mailComposeDelegate = self;
		[mailVC setToRecipients:[NSArray arrayWithObjects:emailID, nil]];
		[self presentViewController:mailVC animated:YES completion:nil];
	}
	else
	{
		[UIUtils messageAlert:nil title:@"Please configure the mail in your device" delegate:nil];
	}
}

- (void)urlLinkActoin:(NSIndexPath *)indexPath
{
    if (indexPath.row == 5)
    {
        NSString* mobileStr = [self getTextStr:indexPath.row];
        if (mobileStr.length > 0)
        {
            NSString* stringURL = [NSString stringWithFormat:@"tel:%@",[self getTextStr:indexPath.row]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringURL]];
        }
        
    }
    else if (indexPath.row == 6)
    {
        NSString* emailStr = [self getTextStr:indexPath.row];
        if (emailStr.length > 0)
            [self sendMail:emailStr];
        
    }
    else
    {
        if (indexPath.row == 0 || indexPath.row == 1)
        {
            NSString* add1 = [self getTextStr:0];
            NSString* add2 = [self getTextStr:1];
            NSString* city = [self getTextStr:2];
            NSString* state = [self getTextStr:3];
            
            NSMutableString* address = [NSMutableString string];
            if (add1.length > 0)
                [address appendString:[NSString stringWithFormat:@"%@,", add1]];
            if (add2.length > 0)
                [address appendString:[NSString stringWithFormat:@"%@,", add2]];
            if (city.length > 0)
                [address appendString:[NSString stringWithFormat:@"%@,", city]];
            if (state.length > 0)
                [address appendString:[NSString stringWithFormat:@"%@,", state]];
            if (address.length > 0)
            {
                NSString* newAddress = [address substringToIndex:[address length]-1];

                newAddress = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@", newAddress];
                newAddress = [newAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:newAddress]];

            }
        }
        
    }
}

- (void) cancelActionSheatAction:(NSInteger) btnIndex
{
	switch (btnIndex)
	{
		case 0:
		{
			NSString* notes = (_notetextView.text != nil) ? _notetextView.text : @"";
			[_customerDict setObject:notes forKey:@"CustomerNotes"];
			[_gAppData updateCustomerDetail:_customerDict];
			[self.navigationController popViewControllerAnimated:YES];
			break;
		}
		case 1:
            [self.navigationController popViewControllerAnimated:YES];
			break;
		case 2:
			break;
			
		default:
			break;
	}
}

#pragma mark --
#pragma mark TableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //    return (_estimateList.count > 0)? 3:2;
    NSInteger i = 3;
    
    if(_estimateList.count > 0)
        i++;
    if(_invoiceList.count > 0)
        i++;
    
    return i;
}

- (EditableDetailCell *) editDetailCell
{
    EditableDetailCell* editCell = [[EditableDetailCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                             reuseIdentifier:nil];
    
    return editCell;
}

- (UITableViewCell*) othersCell
{
    NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenifire];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    return cell;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1)
        return 7;
	else if(section == 2)
        return 1;
    else if(section == 3)
	{
        if(_estimateList.count > 0)
            return _estimateList.count;
        else if (_invoiceList.count > 0)
            return _invoiceList.count;
	}
	else if(section == 4)
	{
		if(_invoiceList.count > 0)
            return _invoiceList.count;
	}
    
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return  20.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat expextedCize = 44.0;
    if (indexPath.section == 2)
    {
        expextedCize = 200.0;
    }
    
    return expextedCize;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    NSString* titleStr = @"";
    
    if (section == 2)
    {
        titleStr = @"Note";
    }
    else if(section == 3)
    {
        if(_estimateList.count > 0)
            titleStr = @"Proposals";
        else if (_invoiceList.count > 0)
            titleStr = @"Invoices";
	}
	else if(section == 4)
	{
        if (_invoiceList.count > 0)
            titleStr = @"Invoices";
	}
    
    return titleStr;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = nil;
    switch (indexPath.section)
    {
        case 0:
        {
            if (cell == nil)
            {
                cell = [self editDetailCell];
                UITextField *textField = [(EditableDetailCell*)cell textField];
                textField.delegate = self;
                UILabel* titlelbl  = [(EditableDetailCell*)cell titleLabel];
                switch (indexPath.row)
                {
                    case 0:
                    {
                        titlelbl.text = @"Company:";
                        textField.text = [_customerDict objectForKey:@"CustomerMemberCompany"];
                        textField.tag = EComopanyNameTextFiled;
						textField.keyboardType = UIKeyboardTypeDefault;
						textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                    }
                        break;
                        
                    case 1:
                    {
                        titlelbl.text = @"First Name:";
                        textField.text = [_customerDict objectForKey:@"CustomerFirstName"];
                        textField.tag = ECustomerFirstNameTxtFiled;
						textField.keyboardType = UIKeyboardTypeDefault;
						textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    }
                        break;
                        
                    case 2:
                    {
                        titlelbl.text = @"Last Name:";
                        textField.text = [_customerDict objectForKey:@"CustomerLastName"];
                        textField.tag = ECustomerLastNameTxtField;
						textField.keyboardType = UIKeyboardTypeDefault;
						textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                    }
                        break;
                }
                textField.userInteractionEnabled = _isEditing;
                textField.clearButtonMode = _isEditing;
            }
        }
		break;
            
        case 1:
        {
            cell = [self editDetailCell];
            UITextField *textField = [(EditableDetailCell*)cell textField];
            UILabel* titlelbl  = [(EditableDetailCell*)cell titleLabel];
            textField.delegate = self;
            titlelbl.text = [self getTitle:indexPath.row];
            textField.text = [self getTextStr:indexPath.row];
            textField.tag = indexPath.row;
            textField.userInteractionEnabled = _isEditing;
            textField.clearButtonMode = _isEditing;
            
            switch (indexPath.row)
            {
				case 0:
				case 1:
					textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
					textField.keyboardType = UIKeyboardTypeDefault;
					break;
				case 2:
					textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
					textField.keyboardType = UIKeyboardTypeDefault;
					break;
				case 3:
					textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
					textField.keyboardType = UIKeyboardTypeDefault;
					break;
				case 4:
					textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
					textField.keyboardType = UIKeyboardTypeNumberPad;
					break;
                case 5:
                    textField.keyboardType = UIKeyboardTypePhonePad;
					textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    textField.tag = ECustomerPhoneTextField;
                    break;
                    
                case 6:
                    textField.keyboardType = UIKeyboardTypeEmailAddress;
                    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                    break;
                    
                default:
                    textField.keyboardType = UIKeyboardTypeDefault;
                    break;
            }
        }
		break;
            
        case 2:
        {
            cell = [self othersCell];
            [self settextView:cell];
            [cell.contentView addSubview:_notetextView];
        }
            break;
            
        case 3:
        {
            cell = [self othersCell];
            if(_estimateList.count > 0)
            {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                NSDictionary* dict = [_estimateList objectAtIndex:indexPath.row];
                if (dict)
                    cell.textLabel.text = [self getCellTittle:dict];
            }
            else if(_invoiceList.count > 0)
            {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                NSDictionary* dict = [_invoiceList objectAtIndex:indexPath.row];
                if (dict)
                    cell.textLabel.text = [self getCellInvoiceTittle:dict];
            }
        }
			break;
            
		case 4:
		{
            cell = [self othersCell];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            NSDictionary* dict = [_invoiceList objectAtIndex:indexPath.row];
            if (dict)
                cell.textLabel.text = [self getCellInvoiceTittle:dict];
		}
            break;
            
        default:
            break;
    }
    return cell;
}

- (NSString*) getTitle:(NSInteger) rowIndex
{
    NSString* str = @"";
    
    switch (rowIndex)
    {
        case 0:
            str = @"Address 1:";
            
            break;
            
        case 1:
            str = @"Address 2";
            break;
            
        case 2:
            str = @"City:";
            break;
            
        case 3:
            str = @"State:";
            break;
            
        case 4:
            str = @"Zip:";
            break;
            
        case 5:
            str = @"Mobile:";
            break;
            
        case 6:
            str = @"Email:";
            break;
            
        default:
            str = @"";
            break;
    }
    
    return str;
}

- (NSString*) getTextStr:(NSInteger) rowIndex
{
    NSString* textstr = @"";
    switch (rowIndex)
    {
        case 0:
            textstr = [_customerDict objectForKey:@"CustomerAddress1"];
            break;
            
        case 1:
            textstr = [_customerDict objectForKey:@"CustomerAddress2"];
            break;
            
        case 2:
            textstr = [_customerDict objectForKey:@"CustomerCity"];
            break;
            
        case 3:
            textstr = [_customerDict objectForKey:@"CustomerState"];
            break;
            
        case 4:
            textstr = [_customerDict objectForKey:@"CustomerZip"];
            break;
            
        case 5:
            textstr = [_customerDict objectForKey:@"CustomerMobilePhone"];
            break;
            
        case 6:
            textstr = [_customerDict objectForKey:@"CustomerEmail"];
            break;
            
        default:
            textstr = @"";
            break;
    }
    return textstr;
}

- (void) settextView:(UITableViewCell*)cell
{
    if (_notetextView == nil)
    {
        _notetextView = [[UITextView alloc] initWithFrame:CGRectMake(cell.frame.origin.x +5, cell.frame.origin.y + 5, cell.bounds.size.width -30,190.0)];
        _notetextView.backgroundColor = [UIColor clearColor];
        _notetextView.delegate = self;
        [_notetextView setFont:[UIFont boldSystemFontOfSize:14.0]];
        _notetextView.text = [_customerDict objectForKey:@"CustomerNotes"];
        _notetextView.tag = ECustomerNoteTextField;
    }
    _notetextView.editable = _isEditing;
}


#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.section == 1 && !_isEditing)
    {
        [self urlLinkActoin:indexPath];
    }
    else if (indexPath.section == 3)
	{
        if(_estimateList.count > 0)
        {
            NSDictionary* dict = [_estimateList objectAtIndex:indexPath.row];
            NSString* str = [NSString stringWithFormat:@"%ld",(long)[[dict objectForKey:@"EstimateID"] integerValue]];
            NSArray* estimateArray  = [_gAppData getEstimatelistWithID:str];
            if (estimateArray.count >0)
            {
                Estimate* estimateObj = [estimateArray objectAtIndex:0];
                CreateEstimateViewController* viewC = [[CreateEstimateViewController alloc] initWithNibName:@"CreateEstimateView" bundle:nil];
                viewC.estimateObj = estimateObj;
                viewC.haveClient = YES;
                viewC.fromClientScreen = YES;
                [viewC setIsConvert:YES];
                
                [self.navigationController pushViewController:viewC animated:YES];
            }
        }
        else
        {
            NSDictionary* dict = [_invoiceList objectAtIndex:indexPath.row];
            NSString* str = [NSString stringWithFormat:@"%ld",(long)[[dict objectForKey:@"InvoiceId"] integerValue]];
            NSArray* invoiceArray  = [_gAppData getInvoicelistWithId:str];
            if (invoiceArray.count >0)
            {
                Invoice* invoiceObj = [invoiceArray objectAtIndex:0];
                CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
                viewC.invoiceObj = invoiceObj;
                [self.navigationController pushViewController:viewC animated:YES];
            }
        }
	}
	else if((_invoiceList.count > 0) && (indexPath.section == 4))
	{
		NSDictionary* dict = [_invoiceList objectAtIndex:indexPath.row];
        NSString* str = [NSString stringWithFormat:@"%ld",(long)[[dict objectForKey:@"InvoiceId"] integerValue]];
        NSArray* invoiceArray  = [_gAppData getInvoicelistWithId:str];
        if (invoiceArray.count >0)
        {
            Invoice* invoiceObj = [invoiceArray objectAtIndex:0];
            CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
            viewC.invoiceObj = invoiceObj;
            [self.navigationController pushViewController:viewC animated:YES];
        }
	}
}

- (void) addBarBtnAction
{
    
    UIActionSheet* actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Add Proposal",@"Add Invoice", nil];
    
	[actionsheet showInView:self.view];
  
}

- (void) addInvoice
{
    CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
    viewC.customerObj = self.customerObj;
    viewC.haveClient = YES;
    [self.navigationController pushViewController:viewC animated:YES];
}

- (void) addEstimate
{
    CreateEstimateViewController* viewC = [[CreateEstimateViewController alloc] initWithNibName:@"CreateEstimateView" bundle:nil];
    viewC.customerObj = self.customerObj;
    viewC.haveClient = YES;
    viewC.fromClientScreen = YES;
    [self.navigationController pushViewController:viewC animated:YES];
}


- (NSString*) getCellTittle:(NSDictionary*)dict
{
    NSString* titeStr = @"";
    NSString* fName = [dict objectForKey:@"FirstName"];
    titeStr = (fName.length > 0)? fName:titeStr;
    
    NSString* lName = [dict objectForKey:@"LastName"];
    titeStr = (lName.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,lName]:titeStr;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString* dateStr = [dateFormatter stringFromDate:[dict objectForKey:@"EstimateDate"]];
    
    titeStr = (dateStr.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,dateStr]:titeStr;
    return titeStr;
}

- (NSString*) getCellInvoiceTittle:(NSDictionary*)dict
{
    NSString* titeStr = @"";
    NSString* fName = [dict objectForKey:@"FirstName"];
    titeStr = (fName.length > 0)? fName:titeStr;
    
    NSString* lName = [dict objectForKey:@"LastName"];
    titeStr = (lName.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,lName]:titeStr;
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString* dateStr = [dateFormatter stringFromDate:[dict objectForKey:@"InvoiceDate"]];
    
    titeStr = (dateStr.length > 0)? [NSString stringWithFormat:@"%@ %@",titeStr,dateStr]:titeStr;
    return titeStr;
}

#pragma mark --
#pragma mark Selector Methods

- (NSArray*) getSortedEstimate
{
    NSArray* estimateArray = [_gAppData getEstimatelistWithClientID:[NSString stringWithFormat:@"%@",self.customerObj.customer_id]];
    NSMutableArray* estimateDetailArray = [[NSMutableArray alloc] init];
    
    for (Estimate* estimateObj in estimateArray)
    {
        NSMutableDictionary* dict  = [[NSMutableDictionary alloc] init];
        [dict setObject:estimateObj.estimate_id forKey:@"EstimateID"];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        NSDate* dateOfEstimate = [dateFormatter dateFromString:estimateObj.estimate_date];
        
        [dict setObject:dateOfEstimate forKey:@"EstimateDate"];
        
        NSArray* arrayOfClient = [_gAppData getCustomerDetails:NO andCustomerId:estimateObj.estimate_ClientId];
        if (arrayOfClient.count > 0)
        {
            Customers* customer = [arrayOfClient objectAtIndex:0];
            [dict setObject:customer.customer_firstname forKey:@"FirstName"];
            [dict setObject:customer.customer_lastname forKey:@"LastName"];
        }
        [estimateDetailArray addObject:dict];
    }
    
    //    NSSortDescriptor *fSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    //    NSSortDescriptor *lSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"FirstName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor *dSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"EstimateDate" ascending:YES selector:@selector(compare:)];
    
    NSArray* sortDescriptors = [NSArray arrayWithObjects:/*fSortDescriptor,lSortDescriptor,*/dSortDescriptor, nil];
    NSArray* sortedArray = [estimateDetailArray sortedArrayUsingDescriptors:sortDescriptors];
    return sortedArray;
}


- (NSArray*) getSortedInvoice
{
    NSArray* invoiceArray = [_gAppData getInvoielistWithClientID:[NSString stringWithFormat:@"%@",self.customerObj.customer_id]];
    NSMutableArray* invoiceDetailArray = [[NSMutableArray alloc] init];
    
    for (Invoice* invoiceObj in invoiceArray)
    {
        NSMutableDictionary* dict  = [[NSMutableDictionary alloc] init];
        [dict setObject:invoiceObj.invoice_id forKey:@"InvoiceId"];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        NSDate* dateOfInvoice = [dateFormatter dateFromString:invoiceObj.invoice_date];
        
        // It crashes when you select the next line, when the client that you have, has an invoice that did not enter a date
        //*** Terminating app due to uncaught exception 'NSInvalidArgumentException', reason: '*** setObjectForKey: object cannot be nil (key: InvoiceDate)'
        [dict setObject:dateOfInvoice forKey:@"InvoiceDate"];
        
        NSArray* arrayOfClient = [_gAppData getCustomerDetails:NO andCustomerId:invoiceObj.invoice_ClientId];
        if (arrayOfClient.count > 0)
        {
            Customers* customer = [arrayOfClient objectAtIndex:0];
            [dict setObject:customer.customer_firstname forKey:@"FirstName"];
            [dict setObject:customer.customer_lastname forKey:@"LastName"];
        }
        [invoiceDetailArray addObject:dict];
    }
    
    //    NSSortDescriptor *fSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"LastName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    //    NSSortDescriptor *lSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"FirstName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor *dSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"InvoiceDate" ascending:YES selector:@selector(compare:)];
    
    NSArray* sortDescriptors = [NSArray arrayWithObjects:/*fSortDescriptor,lSortDescriptor,*/dSortDescriptor, nil];
    NSArray* sortedArray = [invoiceDetailArray sortedArrayUsingDescriptors:sortDescriptors];
    return sortedArray;
}


#pragma mark --
#pragma mark TextFiled and textView Delgate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    textView.keyboardType = UIKeyboardTypeDefault;
	textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    [_tableView setContentOffset:CGPointMake(0, textView.frame.origin.y+8) animated:YES];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (![string isEqualToString:@""])
        if ((textField.tag == ECustomerPhoneTextField))
            return [UIUtils setFormatForPhoneNumber:textField OfLength:12];
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

//- (void) setUpScrollViewOffSet:(UITextField*)tField
//{
//    [_tableView setContentOffset:CGPointMake(0, tField.frame.origin.y + 280) animated:YES];
//}
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    [self setUpScrollViewOffSet:textField];
//}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag)
    {
        case EComopanyNameTextFiled:
            [_customerDict setObject:textField.text forKey:@"CustomerMemberCompany"];
            break;
            
        case ECustomerFirstNameTxtFiled:
            [_customerDict setObject:textField.text forKey:@"CustomerFirstName"];
            break;
            
        case ECustomerLastNameTxtField:
            [_customerDict setObject:textField.text forKey:@"CustomerLastName"];
            break;
            
        case 0:
            [_customerDict setObject:textField.text forKey:@"CustomerAddress1"];
            break;
            
        case 1:
            [_customerDict setObject:textField.text forKey:@"CustomerAddress2"];
            break;
            
        case 2:
            [_customerDict setObject:textField.text forKey:@"CustomerCity"];
            break;
            
        case 3:
            [_customerDict setObject:textField.text forKey:@"CustomerState"];
            break;
            
        case 4:
            [_customerDict setObject:textField.text forKey:@"CustomerZip"];
            break;
            
        case 5:
            [_customerDict setObject:textField.text forKey:@"CustomerMobilePhone"];
            break;
            
        case 6:
            [_customerDict setObject:textField.text forKey:@"CustomerEmail"];
            break;
            
        case 7:
            [_customerDict setObject:textField.text forKey:@"CustomerNotes"];
            break;
            
        default:
            break;
    }
}

- (BOOL) textFieldShouldClear:(UITextField*)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark- UIActionSheet delegate.

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (actionSheet.tag == 1001)
	{
		[self cancelActionSheatAction:buttonIndex];
	}
	else
	{
		switch (buttonIndex)
		{
			case 0:
				[self addEstimate];
				break;
				
			case 1:
				[self addInvoice];
				break;
				
			default:
				break;
		}
    }
}

#pragma mark -
#pragma mark- mailcomposer delegate.
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark -
#pragma mark Notification

- (void) keyboardWillShow:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _tableView.frame = CGRectMake(0, 0, 320, kiPhone5ScrollFrameHeightWithKeyboard+50);
        }
        else
        {
            _tableView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard);
        }
    }
    
    [self.view addGestureRecognizer:_tapGesture];
    
    if (self.navigationItem.rightBarButtonItems.count == 2)
    {
        UIBarButtonItem* editBtn = (UIBarButtonItem*)[self.navigationItem.rightBarButtonItems objectAtIndex:1];
        [editBtn setEnabled:NO];
        
        UIBarButtonItem* cancelBtn = (UIBarButtonItem*)[self.navigationItem.leftBarButtonItems objectAtIndex:0];
        [cancelBtn setEnabled:NO];
    }
    
}

- (void) keyboardWillHide:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _tableView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithoutKeyboard);
        }
        else
        {
            _tableView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithoutKeyboard);
        }
    }
    [_tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [self.view removeGestureRecognizer:_tapGesture];
    if (self.navigationItem.rightBarButtonItems.count == 2)
    {
        UIBarButtonItem* editBtn = (UIBarButtonItem*)[self.navigationItem.rightBarButtonItems objectAtIndex:1];
        [editBtn setEnabled:YES];
        
        UIBarButtonItem* cancelBtn = (UIBarButtonItem*)[self.navigationItem.leftBarButtonItems objectAtIndex:0];
        [cancelBtn setEnabled:YES];
    }
}

- (void) editBtnAction:(UIBarButtonItem *)editBtn
{
    //UIBarButtonItem* editBtn = (UIBarButtonItem*)[self.navigationItem.rightBarButtonItems objectAtIndex:1];
    NSString* title = editBtn.title;
    
    if ([title caseInsensitiveCompare:@"Edit"] == NSOrderedSame)
    {
        _isEditing = YES;
        [_tableView reloadData];
        [editBtn setTitle:@"Done"];
//        self.navigationItem.rightBarButtonItem.title = @"Done";
    }
    else if ([title caseInsensitiveCompare:@"Done"] == NSOrderedSame)
    {
        NSString* notes = (_notetextView.text != nil) ? _notetextView.text : @"";
        [_customerDict setObject:notes forKey:@"CustomerNotes"];
        [_gAppData updateCustomerDetail:_customerDict];
        _isEditing = NO;
        [editBtn setTitle:@"Edit"];
//        self.navigationItem.rightBarButtonItem.title = @"Edit";
        [self viewWillAppear:NO];
    }
}

- (void) cancelBtnAction
{
    UIActionSheet* actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Don't Save",@"Cancel", nil];
	actionsheet.tag = 1001;
	actionsheet.destructiveButtonIndex = 2;
	[actionsheet showInView:self.view];
}

- (void) dismissKeyboard
{
    [self.view endEditing:YES];
}

@end
