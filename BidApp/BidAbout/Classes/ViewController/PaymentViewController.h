//
//  PaymentViewController.h
//  BidAbout
//
//  Created by Marc Cain on 5/5/15.
//  Copyright (c) 2015 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PaymentViewController : UIViewController <UITextViewDelegate, UITextFieldDelegate>
{
    BOOL paypalAdded;
}


- (IBAction)restore;
- (IBAction)addPaypal;

@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextView *textView;



@end
