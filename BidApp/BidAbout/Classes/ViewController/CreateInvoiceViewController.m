//
//  CreateInvoiceViewController.m
//  BidAbout
//
//  Created by Waseem on 04/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "CreateInvoiceViewController.h"
#import "EditCustomerViewController.h"
#import "Item.h"
#import "ClientDetialViewController.h"
#import "PDFRenderer.h"
#import "ScheduleViewController.h"
#import "ExpensceListViewController.h"
#import "PDFDisplayViewController.h"
#import "Customers.h"
#import "Invoice.h"
#import "InvoiceItem.h"
#import "ItemDescriptionViewController.h"
#import "PaymentInfoViewController.h"
#import "TemplatesViewController.h"
#import "CompanySetting.h"

#define kButtonPoint ([UIUtils isiPhone] ? 30: 80)
#define kButtonGap ([UIUtils isiPhone] ? 70: 140)
#define kNumberOfFixedSection 6
#define kCellLabelheader 399

@interface CreateInvoiceViewController ()<TemplateAdded>

@end

@implementation CreateInvoiceViewController
{
    UITextField *textField;
    UITableViewCell *cellWithTextField;
    
    // The original product list, to save in case the user does not want to save the object.
    // Yes, this 'list' is a string
    NSString *originalString;
    NSString *currentDate;
}

#pragma mark--ViewLife Cycle.

- (void) viewDidLoad
{
    [super viewDidLoad];
    _mainTableView.delegate = self;
    
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"LLL-dd-yyyy"];
    currentDate = [DateFormatter stringFromDate:[NSDate date]];
    
    textField =[[UITextField alloc] init];
    
    //checking if there was a product added to a template, once it returns here, checking to see if there is an item pending for
    // a template that is currently being created
    
    
    if (_isTemplate || _newTemplate)
    {
        self.title = @"CreateTemplate";
    }else{
        self.title = @"Create Invoice";
        if (!self.invoiceObj)
        {
            [_datePicker setDate:[NSDate date]];
            [self selectdateFromPicker:_datePicker];
        }

        [self initialSetUp];
    }
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    _tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideDatePicker)];
    _tapgesture.cancelsTouchesInView = YES;
    
    if (self.isTemplate)
    {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(dismissKeyboard)];
        tap.cancelsTouchesInView = NO;
        [self.view addGestureRecognizer:tap];
    }
    
    _tableView.editing = YES;
    _tableView.allowsSelectionDuringEditing = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savedItemData:) name:kSavedInvoiceItemNotification object:nil];
    
    UIBarButtonItem* cancelBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
    self.navigationItem.leftBarButtonItem = cancelBarBtn;
    
    UIBarButtonItem* nextBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(nextBtnAction)];
    self.navigationItem.rightBarButtonItem = nextBarBtn;
    
    viewC = [[TemplatesViewController alloc] initWithNibName:@"TemplatesView" bundle:nil];
    _changeActivity = NO;
    
    //[_gAppData changeTermsAndConditions:@"Test"];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View will appear");
    
    possibleTemplateItems = [_gAppData getItemsFromCurrentTemplate];
    if ( possibleTemplateItems.count > 0)
    {
        self.newTemplate = YES;
        self.isTemplate = YES;
    }
    
    if (_isTemplate || _newTemplate)
    {
        //self.title = @"Create Template";

        [self setupTemplate];
    } else {
        [self initialSetUp];
    }
//    if (self.invoiceObj) {
//        NSLog(@"Reloading Data");
//        [_tableView reloadData];
//    }
}

- (void)setupTemplate
{
    NSLog(@"About to set up the template");
    //self.comment = commentString; = comment
    
    possibleTemplateItems = [_gAppData getItemsFromCurrentTemplate];
    
    if (self.template.template_name.length > 1)
    {
        self.title = self.template.template_name;
    }
    
    
    if (possibleTemplateItems.count > 0) {
       // _itemArray = possibleTemplateItems;
        _itemArray = [NSMutableArray arrayWithArray:possibleTemplateItems];
    }
    

    if (self.existingTemplate)
    {
        NSNumber* templateID = [[NSNumber alloc] init];
        templateID = self.template.template_id;
        NSArray* currentTemplateItems = [_gAppData getItemsFromCurrentTemplate];
        
        _itemArray = [NSMutableArray arrayWithArray:[_gAppData getItemsFromExistingTemplate:templateID]];
        
        NSLog(@"The item array on the existing template is: %@", _itemArray);
        
        if (currentTemplateItems.count > 0)
        {
            [_itemArray addObjectsFromArray:[_gAppData getItemsFromCurrentTemplate]];
        }
    }
    [_tableView reloadData];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_isTemplate || self.newTemplate) originalString = self.template.tempProductList;
}

- (void)viewDidUnload
{
    _tableView = nil;
    _invoiceDict = nil;
    _itemArray = nil;
    _invoicePhotoList = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

- (void) initialSetUp
{
    
    _invoiceDict = [[NSMutableDictionary alloc] initWithCapacity:12];
    _itemArray = [[NSMutableArray alloc] init];
    _invoicePhotoList = [[NSMutableArray alloc] init];
    NSArray* extrainvoiceItems = [[NSArray alloc] init];
    
    NSLog(@"self invoice object is: %@", self.invoiceObj);
    
    if (self.invoiceObj)
    {
        NSLog(@"The invoice object is: %@", self.invoiceObj.invoice_pdf_quantity);
        
        [_invoiceDict setObject:self.invoiceObj.invoice_number forKey:@"InvoiceNumber"];
        [_invoiceDict setObject:self.invoiceObj.invoice_id forKey:@"InvoiceId"];
        [_invoiceDict setObject:self.invoiceObj.invoice_comment forKey:@"Comment"];
        [_invoiceDict setObject:self.invoiceObj.invoice_date forKey:@"InvoiceDate"];
        [_invoiceDict setObject:self.invoiceObj.invoice_item forKey:@"Items"];
        [_invoiceDict setObject:self.invoiceObj.invoice_ClientId forKey:@"Client"];
        [_invoiceDict setObject:self.invoiceObj.invoice_paidAmount forKey:@"PaidAmount"];
        
        [_invoiceDict setObject:self.invoiceObj.invoice_pdf_quantity forKey:@"Quantity"];
        [_invoiceDict setObject:self.invoiceObj.invoice_pdf_rate forKey:@"Rate"];
        [_invoiceDict setObject:self.invoiceObj.invoice_pdf_subtotal forKey:@"Subtotal"];
        [_invoiceDict setObject:self.invoiceObj.invoice_pdf_total forKey:@"Total"];
        
        //this rewrites
        if (!self.pdfOptions) {
            
            self.pdfQuantity = [self.invoiceObj.invoice_pdf_quantity boolValue];
            self.pdfRate = [self.invoiceObj.invoice_pdf_rate boolValue];
            self.pdfSubtotal = [self.invoiceObj.invoice_pdf_subtotal boolValue];
            self.pdfTotal = [self.invoiceObj.invoice_pdf_total boolValue];
        }
        
        if (amountPaid > 0)
        {
            double totalAmountPaid = [self.invoiceObj.invoice_paidAmount doubleValue] + amountPaid;
            
            [_invoiceDict setObject:[NSNumber numberWithDouble:totalAmountPaid]  forKey:@"PaidAmount"];
        }
        
        
        [self setTaxdata];
        
        NSInteger invoiceID = [self.invoiceObj.invoice_id integerValue];
        invoice_ID = invoiceID;
        
        
        
        _itemArray = [NSMutableArray arrayWithArray:[_gAppData getInvoiceItemsFromCurrentInvoice]];
        extrainvoiceItems = [NSMutableArray arrayWithArray:[_gAppData getInvoiceItemsWithInvoiceID:self.invoiceObj.invoice_id]];
        
        if (extrainvoiceItems.count > 0)
        {
            [_itemArray addObjectsFromArray:extrainvoiceItems];
        }
        
        _invoicePhotoList = [NSMutableArray arrayWithArray:[self.invoiceObj.invoice_photo componentsSeparatedByString:@","]];
        
        if (self.invoiceObj.invoice_ClientId.length > 0)
        {
            NSArray* customeArray = [_gAppData getCustomerDetails:NO andCustomerId:self.invoiceObj.invoice_ClientId];
            if (customeArray.count > 0)
            {
                self.customerObj = [customeArray objectAtIndex:0];
                self.haveClient = YES;
            }
        }
    }else{
         extrainvoiceItems = [NSMutableArray arrayWithArray:[_gAppData getInvoiceItemsFromCurrentInvoice]];
        
        if (extrainvoiceItems.count > 0)
        {
            [_itemArray addObjectsFromArray:extrainvoiceItems];
        }
    }
    
    if (self.customerObj)
    {
        [_invoiceDict setObject:[self.customerObj.customer_id stringValue] forKey:@"Client"];
    }
    
    if (self.comment.length > 0)
    {
        [_invoiceDict setObject:self.comment forKey:@"Comment"];
    }
    
    [_tableView reloadData];
}

- (void) setTaxdata
{
    //NSLog(@"set tax data start");
    //    if (self.invoiceObj)
    //        [_invoiceDict setObject:self.invoiceObj.invoice_tax forKey:@"Tax"];
    //    else
    //        [_invoiceDict setObject:_gAppPrefData.currentTax forKey:@"Tax"];
    
    if(_gAppPrefData.isDefualttaxOn)
    {
        [_invoiceDict setObject:_gAppPrefData.currentTax forKey:@"Tax"];
    }
    else
        [_invoiceDict setObject:[NSNumber numberWithDouble:kCurrentTax] forKey:@"Tax"];

}

#pragma mark --
#pragma mark Button Action

- (void) cancelBtnAction
{
    //cancelingg
    possibleTemplateItems = [_gAppData getItemsFromCurrentTemplate];
    if ( possibleTemplateItems.count > 0)
    {
        [_gAppData deleteItemsFromCurrentTemplate];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    if(_isTemplate){
        self.template.tempProductList = originalString;
    }
}

- (void) nextBtnAction
{
    UIActionSheet* actionsheet = nil;
    
    if(_isTemplate || self.newTemplate)
    {
        actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Save Template", @"Active / Inactive", @"Default Terms and Conditions", nil];
    }else
    {
        actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Receive Payment",@"Preview",@"Email",@"Print", nil];
    }
    
    actionsheet.tag = 1002;
    [actionsheet showInView:self.view];
}

#pragma mark -- Action Sheet Delegate

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(actionSheet.tag == 1001)
    {
        [self cancelActionSheatAction:buttonIndex];
    }
    else if(actionSheet.tag == 1002)
    {
        [self nextActionSheatAction:buttonIndex];
    }
}

- (void) cancelActionSheatAction:(NSInteger) btnIndex
{
    switch (btnIndex)
    {
        case 0:
            [self saveInvoice:_invoiceDict];
            break;
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
            break;
            
        default:
            break;
    }
}

- (void) nextActionSheatAction:(NSInteger) btnIndex
{
    if(_isTemplate || self.newTemplate){
        switch (btnIndex)
        {
            case 0:
                [self saveTemplate];
                break;
                
            case 1:
                [self activeInactiveToggle];
            
            case 2:
                
                if (self.comment)
                {
                    [_gAppData changeTermsAndConditions:self.comment];
                }
                else
                    {
                        [_gAppData changeTermsAndConditions:self.template.tempProductList];
                    }
                
            default:
                break;
        }
    }
    else{
        switch (btnIndex)
        {
            case 0:
                [self saveInvoice:_invoiceDict];
                break;
            case 1:
                [self addPayment];
                break;
            case 2:
                [self createPDF:YES isprinitng:NO];
                break;
            case 3:
                [self sendMailButoonAction];
                break;
            case 4:
                [self createPDF:NO isprinitng:YES];
                break;
                
            default:
                break;
        }
    }
}

// for some weird reason, a pdf display view controller is brought up before going to the mail view controller, and its
// causing for the old pdf to be sent instead of the updated, altered pdf
- (void) sendMailButoonAction
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* mailVC = [[MFMailComposeViewController alloc] init];
        mailVC.mailComposeDelegate = self;
        //		[mailVC setSubject:[[KAppDelegate dynamicPullVersion] lowercaseString]];
        //		NSArray *toRecipients = [NSArray arrayWithObjects:@"test@test.com", nil];
        //		[mailVC setToRecipients:toRecipients];
        
        // paypal URL
        
        
        NSDictionary* paypalInfo = [_gAppData getPaypalInformation];
        
        NSLog(@"The paypal info is: %@", paypalInfo);
        
        NSString* start = @"https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=";
        NSString* email = [paypalInfo valueForKey:@"userName"];
        NSString* at = @"%40";
        NSString* emailprovider = [paypalInfo valueForKey:@"emailService"];
        NSString* dot = @"%2e";
        NSString* com = [paypalInfo valueForKey:@"domain"];
        NSString* currencyType = @"&lc=";
        NSString* currency = @"US";
        NSString* itemType = @"&item_name=";
        
        NSString* itemName = @"Proposal";
        NSString* itemNumberType = @"&item_number=";
        NSString* item_number = [_invoiceDict objectForKey:@"InvoiceId"];
        NSString* amountType = @"&amount=";
        
        double dollarValue =[[NSString stringWithFormat:@" %.0f", [self getSubtotal]] doubleValue];
        NSString* amount = @"%2e";
        double cents = dollarValue - [self getSubtotal];
        
        NSString* centsAfterDeceimal = [NSString stringWithFormat:@"%.02f", cents];
        double withoutdecimal = 100 * [centsAfterDeceimal doubleValue];
        
        
        NSString* currencyCodeType = @"&curency_code=";
        NSString* currency_code = @"USD";
        NSString* taxRateType = @"&button_subtype=services&no_note=0&tax_rate=";
        NSString* taxPercentage = [self getTaxCellData];
        NSString* tax_rate = @"%2e000";
        NSString* shippingType = @"&shipping=";
        NSString* shipping = @"0%2e00";
        NSString* theEnd = @"&bn=PP%2dBuyNowBF%3abtn_buynowCC_LG%2egif%3aNonHostedGuest";
        
        NSLog(@"The dollar amount is:%f", dollarValue);
        NSLog(@"The cents amount is: %f", cents);
        NSLog(@"the tax percentage is: %@", taxPercentage);
        
        
        NSString* halfOfPaymentURL = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@", start, email, at, emailprovider, dot, com, currencyType, currency, itemType, itemName, itemNumberType, item_number, amountType, [NSString stringWithFormat:@"%.0f", dollarValue], amount, [NSString stringWithFormat:@"%.f", withoutdecimal]];
        
        NSString* secondHalfPaymentURL = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",currency_code, taxRateType, taxPercentage, tax_rate, shippingType, shipping, theEnd];
        
        NSMutableString* paymentURL = [[NSMutableString alloc]init];
        
        //currency code was
        [paymentURL appendString:halfOfPaymentURL];
        [paymentURL appendString:currencyCodeType];
        [paymentURL appendString:secondHalfPaymentURL];
        
        NSLog(@"The payment URL is: %@", paymentURL);   
        //paypal URL
        
        NSString* explainPaymentURL = @"Click the link below to be taken to a secure paypal website to pay for your invoice.";
        
        NSString *body = [NSString stringWithFormat:@"%@ invoice.\n\nTo Accept this invoice click the link below\n%@ \n\n%@\n\n\n%@",kMailMessage, @"invoice", explainPaymentURL , paymentURL];
        
        NSArray* companSettingData = [_gAppData getCompanyData];
        NSString* companyName;
        if (companSettingData.count > 0)
        {
            CompanySetting* obj = [companSettingData objectAtIndex:0];
            companyName = obj.company_name;
        }
        
        if (companyName.length > 0)
        {
            body = [NSString stringWithFormat:@"%@ invoice from %@.\n\nTo Accept this invoice click the link below\n \n\n%@\n\n\n%@", kMailMessage ,companyName, explainPaymentURL , paymentURL];
        }
        
        
//        [mailVC setMessageBody:[NSString stringWithFormat:body,@"estimate"] isHTML:YES];
//        [mailVC setMessageBody:body isHTML:NO];
        
        
        [mailVC setToRecipients:(self.customerObj.customer_email.length > 0) ? [NSArray arrayWithObjects:self.customerObj.customer_email, nil]: [NSArray arrayWithObjects:nil]];
        
        //changed html to no
        [mailVC setMessageBody:[NSString stringWithFormat:body,@"Invoice"] isHTML:NO];
        
        //testing out commenting out the create pdf
        [self createPDF:NO isprinitng:NO];
        NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
        NSString* filePath = [UIUtils pathForDocumentNamed:path];
        NSData* data = [NSData dataWithContentsOfFile:filePath];
        
        NSLog(@"The location of the pdf is: %@ and the name is: %@", filePath, self.title);
        
        if (self.dataOfPDF) {
            NSLog(@"The altered data of the altered PDF was used");
            [mailVC addAttachmentData:self.dataOfPDF mimeType:@"application/pdf" fileName:@"invoice"];
        }else
            {
                NSLog(@"The default data was used from the pdf");
                [mailVC addAttachmentData:data mimeType:@"application/pdf" fileName:@"invoice"];
            }
        
        
        [self presentViewController:mailVC animated:YES completion:nil];
    }
    else
    {
        [UIUtils messageAlert:nil title:@"Please configure the mail in your device" delegate:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_isTemplate || self.newTemplate) {
        return _itemArray.count + 4;
    }
    NSLog(@"inside item Array is: %lu", (unsigned long)_itemArray.count);
    return (7 + _itemArray.count);
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger totalsection = kNumberOfFixedSection + _itemArray.count;
    
    if (_isTemplate || self.newTemplate) {
        totalsection = totalsection-2;
        if(section == 0 || section == 1 || section == totalsection-1) return 1;
        else if(section == totalsection-2) return 5;
        else return 2;
    }
    
    if(section == (totalsection -3))
        return 5;
    else if ((section == 0) || (section == 1) || (section == 2) || (section == (totalsection -2)))
        return 1;
    else if (section == (totalsection -1))
        return _invoicePhotoList.count+1;
    else if (section == kNumberOfFixedSection + _itemArray.count)
        return 5;
    else
        return 2;
}

//change to add item
- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    NSLog(@"The section is: %ld, and the indexpath is: %ld", (long)indexPath.section, (long)indexPath.row);
    UITableViewCell* cell = nil;
    NSInteger sectionNumber = indexPath.section;
    
    NSLog(@"the cell is: %@", cell);
    
    //need to make it be so that it starts in the same format as when an already completed template is selected
    if(_isTemplate || _newTemplate)
    {
        NSLog(@"it is a template");
        if (!_newTemplate)
        {
            NSLog(@"Not a new template: section number is: %ld and the row is: %ld", (long)indexPath.section, (long)indexPath.row);
            NSInteger sections = 4 + _itemArray.count;
            if (sectionNumber == 0 )
            {
                cell = [self normalCell];
                cell.textLabel.text = @"";
                //cell.textLabel.text = self.template.template_name;
                
                if ((self.template) && (sectionNumber == 0) && (indexPath.row == 0))
                {
                    NSLog(@"The title of the template was used.");
                    
                    //self.templateTitleBox = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
                    
                    UITextView* newTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
                    
                    cell.userInteractionEnabled=YES;
                    [newTextView setEditable:YES];
                    [newTextView setUserInteractionEnabled:YES];
                    newTextView.text = self.template.template_name;
                    //newTextView.text = @"test";
                    //savingg - template name
                    NSLog(@"the name if the current template is: %@", self.template.template_name);
                    //newTextView.text = @"Testing";
                    newTextView.editable=YES;
                    [newTextView setFont:[UIFont systemFontOfSize:20]];
                    
                    [cell addSubview: newTextView];
                    
                    
                }
                else {
                    cell = nil;
                }
                
            }
            else if(sectionNumber == 1)
            {
                cell = nil;
               cell = [self getAddProductCell];
                NSLog(@"get add product cell existing template");
            }
            else if(sectionNumber == sections - 1)
            {
                NSLog(@"get comment cell existing template");
                cell = nil;
                cell = [self getCommentCell];

            }
            else if(sectionNumber == sections - 2){
                cell = nil;
                cell = [self getTotalSectionCell:indexPath.row];
                if(KVersion < 7.0)
                {
                    cell.accessoryType = (indexPath.row == 3)? UITableViewCellAccessoryDisclosureIndicator:UITableViewCellAccessoryNone;
                }
            }else{
                if (_itemArray.count > 0 && sectionNumber != kNumberOfFixedSection + _itemArray.count)
                {
                    NSLog(@"get item a cell");
                    NSInteger objectIndex = sectionNumber -2;
                    cell = nil;
                    cell = [self getItemACell:objectIndex WithRow:indexPath.row];
                }
            }
//            if([cell isEqual:cellWithTextField] && sectionNumber != 0){
//                NSLog(@"remove from superview ");
//                [textField removeFromSuperview];
//            }
        }else{
            NSLog(@"Second if statement ===");
            NSInteger sections = 4 + _itemArray.count;
            
            if (sectionNumber == 0){
                NSLog(@"The name of the template was used again");
                cell = nil;
                cell = [self normalCell];
                cell.textLabel.text = @"";
                textField.frame = CGRectMake(15, 0, cell.bounds.size.width-30, cell.bounds.size.height);
                [textField removeFromSuperview];
                textField.text = self.template.template_name;
                
                [cell addSubview:textField];
                cellWithTextField = cell;
            }
            
            else if(sectionNumber == 1)
            {
                NSLog(@"get add product cell");
                cell = nil;
                cell = [self getAddProductCell];
            }
            else if(sectionNumber == sections - 1)
            {
                NSLog(@"get comment cell new");
                cell = nil;
                cell = [self getCommentCell];
            }
            else if(sectionNumber == sections - 2)
            {
                NSLog(@"getting the total section cell");
                cell = nil;
                cell = [self getTotalSectionCell:indexPath.row];
                if(KVersion < 7.0)
                {
                    cell.accessoryType = (indexPath.row == 3)? UITableViewCellAccessoryDisclosureIndicator:UITableViewCellAccessoryNone;
                }
            }else{
                    //-- this is the cell where the product goes
                if (_itemArray.count > 0 && sectionNumber != kNumberOfFixedSection + _itemArray.count)
                {
                    NSLog(@"second to last else");
                    NSInteger objectIndex = sectionNumber -2;
                    cell = nil;
                    cell = [self getItemACell:objectIndex WithRow:indexPath.row];
                }
            }
            
//            if([cell isEqual:cellWithTextField] && sectionNumber != 0)
//            {
//                NSLog(@"remove from superview");
//                [textField removeFromSuperview];
//            }
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            return cell;
        }
    }else{
        NSLog(@"Second else statement ");
        if (sectionNumber == 0)
        {
            cell = [self getDateCell];
        }
        else if(sectionNumber == 1)
        {
            cell = [self getCustomerdetailCell];
        }
        else if(sectionNumber == 2)
        {
            cell = [self getAddProductCell];
        }
        else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count-3))
        {
            cell = [self getTotalSectionCell:indexPath.row];
            if(KVersion < 7.0)
            {
                cell.accessoryType = (indexPath.row == 3)? UITableViewCellAccessoryDisclosureIndicator:UITableViewCellAccessoryNone;
            }
            
        }
        else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count -2))
        {
            cell = [self getCommentCell];
        }
        else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count -1))
        {
            cell = [self getAddPhotoCellData:indexPath.row];
            NSLog(@"Add photo cell data");
        } // remove next else if, if causing problems
        else if(sectionNumber == kNumberOfFixedSection + _itemArray.count)
        {
            NSLog(@"adding pdf options");
            cell = [self getPDFOptions:indexPath.row];
            return cell;
        }
        //need to add following for possible photos
        else
        {
            if (_itemArray.count > 0 && sectionNumber != kNumberOfFixedSection + _itemArray.count)
            {
                NSLog(@"Item array count greater than zero");
                NSInteger objectIndex = sectionNumber -3;
                cell = [self getItemACell:objectIndex WithRow:indexPath.row];
            }
        }
        
        NSLog(@"The section number is: %ld, and the _item array count is: %lu", (long)sectionNumber, (unsigned long)_itemArray.count);
    }
    
    if ((self.template) && (sectionNumber != 0) && (indexPath.row != 0))
            {
                for (UIView *view in cell.contentView.subviews) {
                    if([view isKindOfClass:[UITextView class]])
                    {
                        NSLog(@"removing from superview");
                        
                        [view removeFromSuperview];
                    }
                }
            }

    cell.accessoryType = UITableViewCellAccessoryNone;
    
    //NSLog(@"The cell and the subviews are: %@", cell, cellWithTextField);
    
    for (UIView *view in cell.subviews)
    {
        NSLog(@"view is: %@",view);
        // code here
    }
    NSLog(@"the title is: %@", cell.textLabel);
    
    return cell;
}

#pragma mark
#pragma mark CellConfiguration Method

- (UITableViewCell*) normalCell
{
    NSLog(@"Normal cell");
    
    NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    cell = nil;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenifire];
    }
    
    UILabel* label = (UILabel*)[cell viewWithTag:kCellLabelheader];
    if (label)
        [label removeFromSuperview];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.imageView.image = nil;
    
    return cell;
}

- (UITableViewCell*) detiledCellWithData:(NSString*)headertext
{
    NSLog(@"deliled cell with data");
    UITableViewCell* cell  = [self normalCell];
    [[cell viewWithTag:kCellLabelheader] removeFromSuperview];
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    UILabel* headerlabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 150, 44)];
    headerlabel.textColor = [UIColor grayColor];
    headerlabel.tag = kCellLabelheader;
    headerlabel.text = headertext;
    headerlabel.font = [UIFont systemFontOfSize:12.0];
    [headerlabel setBackgroundColor:[UIColor clearColor]];
    [cell addSubview:headerlabel];
    return cell;
}

- (UITableViewCell*) getDateCell
{
    NSLog(@"select date from picker");
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString* selectedDateStr = [dateFormatter stringFromDate:_datePicker.date];
    
    
    
    UITableViewCell* cell = [self normalCell];
    NSString* invoicedateStr = [_invoiceDict objectForKey:@"InvoiceDate"];
    //cell.textLabel.text = (invoicedateStr.length > 0) ? invoicedateStr:@"Select Date";
    cell.textLabel.text = (invoicedateStr.length > 0) ? invoicedateStr:selectedDateStr;
    cell.accessoryType = /*(self.invoiceObj)? UITableViewCellAccessoryNone:*/UITableViewCellAccessoryDetailDisclosureButton;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.imageView.image = [UIImage imageNamed:@"calendar-icon.png"];
    
    return cell;
}

- (UITableViewCell*) getCustomerdetailCell
{
    NSLog(@"get customer detail cell");
    
    UITableViewCell* cell = [self normalCell];
    cell.textLabel.text = [self getCustomerCellData];
    
    if (self.customerObj)
    {
        cell.accessoryType = (self.haveClient)? UITableViewCellAccessoryDisclosureIndicator:(UITableViewCellAccessoryNone);
        cell.accessoryType = (self.invoiceObj)? UITableViewCellAccessoryDisclosureIndicator:cell.accessoryType;
        
        cell.textLabel.textColor = [UIColor blackColor];
    }
    else
        cell.textLabel.textColor = [UIColor grayColor];
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    
    return cell;
}

- (UITableViewCell*) getAddProductCell
{
    NSLog(@"get add product cell");
    
    UITableViewCell* cell = [self normalCell];
    
    if (self.template || self.newTemplate) {
        cell.textLabel.text = @"Add Product";
    }else{
        cell.textLabel.text = @"Add Product or Template";
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.imageView.image = [UIImage imageNamed:@"additems.png"];
    return cell;
}

- (UITableViewCell*) getTotalSectionCell:(NSInteger)rownumber
{
    NSLog(@"get total section cell");
    
    //NSLog(@"The row number is: %ld", (long)rownumber);
    NSArray* dataArray = [[self getTotalSectionData:rownumber] componentsSeparatedByString:@","];
    UITableViewCell* cell = [self getSubtotalTypecell:dataArray];
    
    if (rownumber == 1)
        //NSLog(@"if row number is 1 inside total section");
        if(KVersion < 7.0)
        {
            cell.accessoryType = (self.invoiceObj)? UITableViewCellAccessoryNone:UITableViewCellAccessoryDisclosureIndicator;
        }
    
    if ((rownumber == 2) || (rownumber == 3) || (rownumber == 4))
    {
        //NSLog(@"if row number 2 or 3 or 4 inside the total section");
//        [(UILabel*)[cell viewWithTag:kCellLabelheader] setTextColor:[UIColor blackColor]];
//        [(UILabel*)[cell viewWithTag:kCellLabelheader] setFont:[UIFont boldSystemFontOfSize:12.0]];
//        cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    return cell;
}

- (UITableViewCell*) getCommentCell
{
    NSLog(@"get comment cell");
    
    UITableViewCell* cell = [self normalCell];
    
    NSString* commentstr = [_invoiceDict objectForKey:@"Comment"];
    
//    if (self.isTemplate)
//    {
//        commentstr = [_invoiceDict objectForKey:@"Comment"];
//    } else {
//        commentstr = [_invoiceDict objectForKey:@"Terms and Conditions"];
//    }
    
    
    //cell.textLabel.text = (commentstr.length > 0) ? commentstr:@"Comment";
    if (commentstr.length > 0)
    {
        cell.textLabel.text = commentstr;
    } else {
        
            if (self.isTemplate)
            {
                NSLog(@"The template its: %@", self.template);
                
                cell.textLabel.text = @"Terms and Conditions";
                
                if (self.template.tempProductList.length > 0)
                {
                    cell.textLabel.text = self.template.tempProductList;
                }
                
                if (self.comment.length > 0)
                {
                    cell.textLabel.text = self.comment;
                }
                
            } else {
                cell.textLabel.text = @"Comment";
            }
    }
    
    cell.textLabel.textColor = (commentstr.length > 0)?[UIColor blackColor]:[UIColor grayColor];
    cell.textLabel.font = (commentstr.length > 0)?[UIFont systemFontOfSize:14.0]:[UIFont boldSystemFontOfSize:14.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (UITableViewCell*) getAddPhotoCellData :(NSInteger) rowNumber
{
    UITableViewCell* cell = [self normalCell];
    NSString* textStr = @"";
    if (rowNumber == 0)
    {
        textStr = @"Add Photo";
        cell.textLabel.textColor = [UIColor blackColor];
    }
    else
    {
        if (_invoicePhotoList.count >= rowNumber)
            textStr = [NSString stringWithFormat:@"Photo %ld",(long)rowNumber];
        
        cell.textLabel.textColor = [UIColor grayColor];
    }
    cell.textLabel.text = textStr;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (UITableViewCell*) getPDFOptions: (NSInteger) rowNumber
{
    UITableViewCell* cell = [self normalCell];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.imageView.image = [UIImage imageNamed:@"checkbox.png"];
    
    NSString* textStr = @"";
    
    if (rowNumber == 0)
    {
        textStr = @"PDF Display Options";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.imageView.image = nil;
    }
    else if (rowNumber == 1)
    {
        textStr = @"Quantity";
        if (self.pdfQuantity) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    else if (rowNumber == 2)
    {
        textStr = @"Price";
        if (self.pdfRate) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    else if (rowNumber == 3)
    {
        textStr = @"Item Total";
        if (self.pdfSubtotal) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    else if (rowNumber == 4)
    {
        textStr = @"Totals";
        if (self.pdfTotal) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    cell.textLabel.text = textStr;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    
    return cell;
}

- (UITableViewCell*) getSubtotalTypecell:(NSArray*) dataArray
{
    UITableViewCell* cell = [self detiledCellWithData:[dataArray objectAtIndex:0]];
    NSString* currencyValue = [UIUtils getCurrencyValue:[dataArray objectAtIndex:1]];
    cell.textLabel.text = currencyValue;
    return cell;
}

- (UITableViewCell*) getItemTotalcell:(NSArray*) dataArray
{
    NSLog(@"get item total cell");
    
    NSString* currencyStr = @"0.00";
    UITableViewCell* cell = [self detiledCellWithData:[dataArray objectAtIndex:0]];
    
    if (dataArray.count > 2)
        currencyStr = [NSString stringWithFormat:@"%@ X %@",[dataArray objectAtIndex:1],[UIUtils getCurrencyValue:[dataArray objectAtIndex:2]]];
    else if (dataArray.count == 2)
        currencyStr  = [UIUtils getCurrencyValue:[dataArray objectAtIndex:1]];
    
    cell.textLabel.text = currencyStr;
    return cell;
}

- (UITableViewCell*) getItemACell:(NSInteger)objectIndex WithRow:(NSInteger)rowNumber
{
    NSLog(@"get item a cell");
    
    //NSLog(@"Inside of the get item a cell");
    UITableViewCell* cell = nil;
    NSString* dataString = [self getItemsectionData:objectIndex WithRow:rowNumber];
    if (rowNumber == 0)
    {
        cell = [self normalCell];
        cell.textLabel.text = dataString;
        cell.accessoryType = (self.invoiceObj)? UITableViewCellAccessoryNone:UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (rowNumber == 1)
    {
        NSArray* dataArray = [dataString componentsSeparatedByString:@","];
        cell = [self getItemTotalcell:dataArray];
        if(KVersion < 7.0)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    return cell;
}

#pragma mark --
#pragma mar get Cell data private methods

- (NSString*) getCustomerCellData
{
    //NSLog(@"get customer cell data");
    NSString* name = @"Customer Name";
    if (self.customerObj)
    {
        name = [NSString stringWithFormat:@"%@ %@", self.customerObj.customer_firstname, self.customerObj.customer_lastname];
    }
    
    return name;
}

- (NSString*) getItemsectionData:(NSInteger)itemIndex WithRow:(NSInteger)rowNumber
{
    NSLog(@"the item index is: %ld, and the row number is: %ld", (long)itemIndex, (long)rowNumber);
    NSLog(@"the item array is === : %@", _itemArray);
    
    InvoiceItem* item = [_itemArray objectAtIndex:itemIndex];
    NSString* cellStr = @"";
    
    switch (rowNumber)
    {
        case 0:
        {
            NSString* itemDesc = item.item_title;
            if (itemDesc.length > 0 )
                cellStr = [NSString stringWithFormat:@"%@",item.item_title];
            else
                cellStr = @"Product";
            
        }
            break;
            
        case 1:
        {
            NSString* itemamount = item.item_amount;
            //NSLog(@"The item amount is: %@", item.item_amount);
            
            if ([itemamount floatValue] > 0 )
                cellStr = [NSString stringWithFormat:@"Amount,%.02f,%@",[item.item_quantity floatValue],item.item_rate];
            else
                cellStr = @"Amount,0.00";
        }
            break;
            
        default:
            break;
    }
    return cellStr;
}

- (NSString*) getTotalSectionData:(NSInteger)rowNumber
{
    //NSLog(@"get total section");
    NSString* cellStr = @"";
    
    switch (rowNumber)
    {
        case 0:
        {
            cellStr = [self getSubTotalCellData];
        }
            break;
            
        case 1:
        {
            cellStr = [self getTaxCellData];
        }
            break;
            
        case 2:
        {
            cellStr = [self getTotalCellData];
        }
            break;
            
        case 3:
        {
            cellStr = [self getPaidCellData];
        }
            break;
            
        case 4:
        {
            cellStr = [self getRemainCellData];
        }
            break;
            
        default:
            break;
    }
    return cellStr;
}

- (NSString*) getSubTotalCellData
{
    //NSLog(@"get subtotal");
    return [NSString stringWithFormat:@"SubTotal,%.02f",[self getSubtotal]];
}

- (NSString*) getTaxCellData
{
    NSLog(@"get tax cell");
    double currentTax = [[_invoiceDict objectForKey:@"Tax"] doubleValue];
    NSString* stringTaxCellData = [NSString stringWithFormat:@"TAX(%.02f%%),%.02f",currentTax,[self getAppliedtax]];
    
    NSLog(@"the cell data is: %@", stringTaxCellData);
    
    return stringTaxCellData;
}

- (NSString*) getTotalCellData
{
    //NSLog(@"get total cell");
    return [NSString stringWithFormat:@"TOTAL,%.02f",[self getTotalData]];
}

- (NSString*) getPaidCellData
{
    //NSLog(@"get paid cell");
    return [NSString stringWithFormat:@"Paid,%.02f",[self getPaidData]];
}

- (NSString*) getRemainCellData
{
    //NSLog(@"get remaining cell");
    return [NSString stringWithFormat:@"Remain,%.02f",[self getRemaindata]];
}

- (double) getSubtotal
{
    double subTotalSum = 0.00;
    if (_itemArray.count > 0)
    {
        for (InvoiceItem* item in _itemArray)
        {
            double amount = [item.item_amount doubleValue];
            subTotalSum = subTotalSum + amount;
        }
    }
    return subTotalSum;
}

- (double) getAppliedtax
{
    double taxDeduction = 0.00;
    
    for (InvoiceItem* item in _itemArray)
    {
        double amount = [item.item_amount doubleValue];
        double currentTax = [[_invoiceDict objectForKey:@"Tax"] doubleValue];
        //double taxableamount = ([item.item_taxAllow boolValue])? ((amount*currentTax)/100.00):0.0;
        double taxableamount = (item.item_taxAllow  > 0)? ((amount*currentTax)/100.00):0.0;
        //double taxableamount = (amount*currentTax)/100.00;
        
        taxDeduction = taxDeduction + taxableamount;
    }
    
    return taxDeduction;
}

- (double) getTotalData
{
    double totalsum = 0.00;
    double subtotal = [self getSubtotal];
    double taxPaid = [self getAppliedtax];
    
    totalsum = subtotal + taxPaid;
    return totalsum;
}

- (double) getPaidData
{
    NSNumber* paid = [_invoiceDict objectForKey:@"PaidAmount"];
    amountPaid = [paid doubleValue];
    
    return amountPaid;
}

- (double) getRemaindata
{
    double remainData = 0.0;
    double totalData = 0;
    double paidData = 0;
    
    totalData = [self getTotalData];
    paidData = [self getPaidData];
    remainData = totalData - paidData;
    
    return remainData;
}

#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger fixed = kNumberOfFixedSection;
    if (_isTemplate || self.newTemplate) {
        fixed = 4 + [_itemArray count];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger sectionNumber = indexPath.section;
    
    if(_isTemplate || self.newTemplate){
        if(sectionNumber == 0) ;//TODO EDIT NAME
        else if(sectionNumber == 1) [self addExpenseAction];
        else if(sectionNumber == fixed - 1) [self addItemsAction];
        else if(sectionNumber == fixed - 2) ; // Nothing
        else{
            if (indexPath.row == 1)
            {
                NSInteger objectIndex = indexPath.section -2;
                [self productDetail:objectIndex];
            }
        }
        return;
    }
    
    
    if (sectionNumber == 0)
    {
        //        if (!self.invoiceObj)
        //        {
        _pickerView.hidden = NO;
        [_pickerView addGestureRecognizer:_tapgesture];
        
        [self selectdateFromPicker:nil];
        
        //        }
    }
    else if(sectionNumber == 1)
    {
        
        [self clientDetialAction];
    }
    else if(sectionNumber == 2)
    {
        //        if (!self.invoiceObj)
        [self addExpenseAction];
    }
    else if(sectionNumber == (fixed + _itemArray.count - 3))
    {
        if (indexPath.row == 3)
        {
            //[self addPayment];
        }
    }
    else if(sectionNumber == (fixed + _itemArray.count -2))
    {
        //        if (!self.invoiceObj)
        [self addItemsAction];
    }
    else if(sectionNumber == (fixed + _itemArray.count -1))
    {
        //        if (!self.invoiceObj)
        [self addPhotoSection:indexPath.row];
    }
    else if (sectionNumber == kNumberOfFixedSection + _itemArray.count)
    {
        
        if (indexPath.row == 0)
        {
            NSLog(@"nothing happens with the PDF options");
        } else {
            [self editPDFOptions:indexPath];
        }
        
    }
    else
    {
        if (indexPath.row == 1)
        {
            NSInteger objectIndex = indexPath.section -3;
            [self productDetail:objectIndex];
        }
    }
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger totalSections = _itemArray.count;
    
    if (_isTemplate)
    {
        if (indexPath.section > 1 && indexPath.section < totalSections + 2 && indexPath.row == 1)
        {
            return UITableViewCellEditingStyleDelete;
        }
        
        return UITableViewCellEditingStyleNone;
    }
    
        else
        {
            if (indexPath.section > 2 && indexPath.section < totalSections + 3 && indexPath.row == 1)
            {
                return UITableViewCellEditingStyleDelete;
            }
            
            return UITableViewCellEditingStyleNone;
        }
}

- (void) tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if(_isTemplate)
        {
            Item* item = [_itemArray objectAtIndex:indexPath.section - 2];
            
            [_gAppData deleteItemFromTemplate:item.item_teplateID ItemTitle:item.item_title];
            
            [_itemArray removeObjectAtIndex:(indexPath.section - 2)];
            
            
//            NSMutableArray *itemList = [[NSMutableArray alloc] initWithArray:[self.template.tempProductList componentsSeparatedByString:@","]];
//            
//            [itemList removeObjectAtIndex:(indexPath.section - 2)];
//            
//            self.template.tempProductList = [itemList componentsJoinedByString:@","];
            
            //[_itemArray removeObjectAtIndex:(indexPath.section - 2)];
            

            [_tableView reloadData];
            
        }else
        {
            NSInteger objectIndex = indexPath.section -2;
            
            [self deleteItemAtIndex:objectIndex];
            
            [_tableView reloadData];
        }
    }
}

//- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    //NSLog(@"should indent");
//    return NO;
//}

- (void) deleteItemAtIndex:(NSInteger)objectIndex
{
    [_gAppData deleteInvoiceItem:[_itemArray objectAtIndex:objectIndex - 1]];
    
    [_itemArray removeObjectAtIndex:objectIndex - 1];
    
    NSString* itemString = @"";
    for (InvoiceItem* item in _itemArray)
    {
        itemString = [itemString stringByAppendingString:[NSString stringWithFormat:@"%@,",item.item_id]];
    }
    [_invoiceDict setObject:itemString forKey:@"Items"];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark Delegate

- (void) addCustomerDetial:(Customers *)customers
{
    self.customerObj = customers;
    [_invoiceDict setObject:[self.customerObj.customer_id stringValue] forKey:@"Client"];
    
    [_tableView reloadData];
}

- (void) addComment:(NSString*)commentString
{
    [_invoiceDict setObject:commentString forKey:@"Comment"];
    
    NSLog(@"the comment that is being passed back is: %@", commentString);
    self.comment = commentString;
    
//    if (self.isTemplate && self.template)
//    {
//        self.comment = self.template.tempProductList;
//    }
    
    [_tableView reloadData];
}

- (void) addPhotoList:(NSArray*)photoArray
{
    //NSLog(@"invoice photo list");
    _invoicePhotoList = [NSMutableArray arrayWithArray:photoArray];
}

- (void) savedPaymentItem:(NSString*)paidAmount
{
    [_invoiceDict setObject:[NSNumber numberWithDouble:[paidAmount doubleValue]] forKey:@"PaidAmount"];
    amountPaid = [paidAmount doubleValue];
    
    //causing weird errors?
    [self initialSetUp];
    
    [_tableView reloadData];
}


#pragma mark --
#pragma mark Notification

- (void) savedItemData:(NSNotification *)notificationObject
{
    NSString* itemIdStr = [notificationObject object];
    NSArray* items = [_gAppData getInvoiceItemWithID:itemIdStr];
    
    if (items.count)
    {
        BOOL isExists = NO;
        InvoiceItem* item = items[0];
        for (NSInteger i = 0; i < _itemArray.count; i++)
        {
            InvoiceItem* itemObj = [_itemArray objectAtIndex:i];
            if ([itemObj.item_id isEqualToString:item.item_id])
            {
                [_itemArray replaceObjectAtIndex:i withObject:items];
                isExists = YES;
                break;
            }
        }
        
        if (!isExists)
        {
            [_itemArray addObject:items[0]];
        }
    }
    
    // adding an item to the invoice and checking to see if the id matches
    
    NSString* itemString = @"";
    for (InvoiceItem* item in _itemArray)
    {
        itemString = [itemString stringByAppendingString:[NSString stringWithFormat:@"%@,", item.item_id]];
        
    }
    [_invoiceDict setObject:itemString forKey:@"Items"];
    [_tableView reloadData];
}

- (void) saveItems:(NSString *)itemsSeparatedByCommas
{
    //NSLog(@"SAVE ITEMS METHOD.......");
    
    NSString* itemIdStr = itemsSeparatedByCommas;
    NSArray *itemIds = [itemIdStr componentsSeparatedByString:@","];
    NSMutableArray *itemList = [[NSMutableArray alloc] init];
    
    for (NSString *string in itemIds) {
        [itemList addObject: [[_gAppData getItemWithID:string] firstObject]];
    }
    if (itemList.count > 0)
    {
        for(NSInteger i = 0; i < itemList.count; i++)
        {
            BOOL isExists = NO;
            InvoiceItem* item = [itemList objectAtIndex:i];
            for (NSInteger i = 0; i < _itemArray.count; i++)
            {
                InvoiceItem* itemObj = [_itemArray objectAtIndex:i];
                if ([itemObj.item_id isEqualToString:item.item_id])
                {
                    [_itemArray replaceObjectAtIndex:i withObject:item];
                    isExists = YES;
                    break;
                }
            }
            
            if (!isExists)
                [_itemArray addObject:item];
        }
    }
    
    NSString* itemString = @"";
    for (InvoiceItem* item in _itemArray)
    {
        itemString = [itemString stringByAppendingString:[NSString stringWithFormat:@"%@,",item.item_id]];
    }
    [_invoiceDict setObject:itemString forKey:@"Items"];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark Selector Methods

- (void) clientDetialAction
{
    if (self.haveClient )
    {
        EditCustomerViewController* editCustomerVC  = [[EditCustomerViewController alloc] initWithNibName:@"EditCustomerView" bundle:nil];
        editCustomerVC.customerObj = self.customerObj;
        [self.navigationController pushViewController:editCustomerVC animated:YES];
    }
    else if( !self.invoiceObj)
    {
        ClientViewController* clientVC = [[ClientViewController alloc] initWithNibName:@"ClientView" bundle:nil];
        clientVC.isListMode = YES;
        clientVC.clientDelegate = self;
        clientVC.delegate = self;

        [self.navigationController pushViewController:clientVC animated:YES];
    }
}

- (void)addItemViewController:(ClientViewController *)controller didFinishEnteringItem:(Customers *)item
{
    self.customerObj = item;
    self.haveClient = YES;
    [self initialSetUp];
}

- (void) addItemsAction
{
    ItemDescriptionViewController* itemDescVC  = [[ItemDescriptionViewController alloc] initWithNibName:@"ItemDescriptionView" bundle:nil];
    itemDescVC.delegate = self;
    
    if (self.isTemplate)
    {
        itemDescVC.isTemplate = YES;
    }
    
    NSString* commentstr = [_invoiceDict objectForKey:@"Comment"];
    
    if (self.isTemplate)
    {
        commentstr = self.template.tempProductList;
    }
    
    if (commentstr.length > 0)
        itemDescVC.estimateComment = commentstr;
    
    UINavigationController* navc  = [[UINavigationController alloc] initWithRootViewController:itemDescVC];
    
    if (![UIUtils isiPhone])
        navc.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:navc animated:YES completion:nil];
}

- (void) addPhotoSection:(NSInteger)rowNumber
{
    //NSLog(@"add photo");
    AddPhotoViewController* addPhotoVC  = [[AddPhotoViewController alloc] initWithNibName:@"AddPhotoView" bundle:nil];
    addPhotoVC.photoList = _invoicePhotoList;
    addPhotoVC.estimateTitleId = self.title;
    addPhotoVC.addPhotoDelegate = self;
    addPhotoVC.photoIndex = rowNumber;
    
    UINavigationController* navc  = [[UINavigationController alloc] initWithRootViewController:addPhotoVC];
    
    //    if (![UIUtils isiPhone])
    //        navc.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:navc animated:YES completion:nil];
}

- (void) editPDFOptions: (NSIndexPath*) indexPath
{
    if (indexPath.row == 1)
    {
        if (self.pdfQuantity)
        {
            self.pdfQuantity = NO;
            [self.mainTableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfQuantity = YES;
            [self.mainTableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    if (indexPath.row == 2)
    {
        if (self.pdfRate)
        {
            self.pdfRate = NO;
            [self.mainTableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfRate = YES;
            [self.mainTableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    if (indexPath.row == 3)
    {
        if (self.pdfSubtotal)
        {
            self.pdfSubtotal = NO;
            [self.mainTableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfSubtotal = YES;
            [self.mainTableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    if (indexPath.row == 4)
    {
        if (self.pdfTotal)
        {
            self.pdfTotal = NO;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfTotal = YES;
            [self.mainTableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    NSLog(@"The name of the cell is: %@", [self.mainTableView cellForRowAtIndexPath:indexPath].textLabel.text);
}

- (void) addExpenseAction
{
    NSLog(@"add expense");
    ExpensceListViewController* expenseVC  = [[ExpensceListViewController alloc] initWithNibName:@"ExpensceView" bundle:nil];

    if (_isTemplate)
    {
        expenseVC.isTemplate = YES;
        expenseVC.isEstimate = NO;
        expenseVC.templateAddedDelegate = self;
        expenseVC.isInvoice = NO;
        expenseVC.fromTemplate = YES;
        expenseVC.creatingTemplateFromEstimate = self.creatingTemplateFromEstimate;
    }
    else{
        expenseVC.isTemplate = NO;
        expenseVC.isInvoice = YES;
        expenseVC.isEstimate = NO;
    }

    [self.navigationController pushViewController:expenseVC animated:YES];
}

- (void) addPayment
{
    PaymentInfoViewController* paymnetInfo = [[PaymentInfoViewController alloc] initWithNibName:@"PaymentInfoView" bundle:nil];
    paymnetInfo.paymentInfoDelegate = self;
    paymnetInfo.totalAmount = [NSString stringWithFormat:@"%.02f",[self getTotalData]];
    paymnetInfo.paidAmount = [NSString stringWithFormat:@"%@",[_invoiceDict objectForKey:@"PaidAmount"]];
    paymnetInfo.invoiceID = [_invoiceDict objectForKey:@"InvoiceId"];
    [self.navigationController pushViewController:paymnetInfo animated:YES];
}

- (void) productDetail:(NSInteger)objectIndex
{
    InvoiceItem* item = [_itemArray objectAtIndex:objectIndex];
    
    ScheduleViewController* scheduleVC  = [[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
    
    scheduleVC.product = item;
    scheduleVC.isUpdateInvoice = YES;
    scheduleVC.isProductItem = NO;
    scheduleVC.isAddInvoice = NO;
    //scheduleVC.isTemplate = YES;
    scheduleVC.isTemplate = NO;
    scheduleVC.existingTemplateItem = YES;
    
    if (!self.isTemplate)
    {
        NSLog(@"Not a template");
        
        scheduleVC.updateInvoiceOrAddToProductList = YES;
        scheduleVC.isTemplate = NO;
        scheduleVC.existingTemplateItem = NO;
        scheduleVC.isProductItem = YES;
        
    }
    
    [self.navigationController pushViewController:scheduleVC animated:YES];
}

- (void) hideDatePicker
{
    _pickerView.hidden = YES;
    [_pickerView removeGestureRecognizer:_tapgesture];
    [_tableView reloadData];
}

- (IBAction) selectdateFromPicker:(id)sender
{
    NSLog(@"select date from picker");
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    
    NSString* selectedDateStr = [dateFormatter stringFromDate:_datePicker.date];
    
    [_invoiceDict setObject:selectedDateStr forKey:@"InvoiceDate"];
    [_tableView reloadData];

}

-(void) activeInactiveToggle
{
    //sends over the template that was toggled between inactive and active
    // the if statement is so that when a user presses twice on active/inactive, it gets rid of the modification
    if (_changeActivity)
    {
        _changeActivity = NO;
    } else
        {
            _changeActivity = YES;
        }
    
    //self.activatingOrDeactivatingTemplate = YES;
    
    [self saveTemplate];
    
    //self.activatingOrDeactivatingTemplate = NO;

    NSLog(@"will change activity when template gets saved: %@", self.template.active);
}

- (void) saveInvoice:(NSDictionary*)invoiceDict
{
    if(_isTemplate)
    {
        [self updateTemplate: self.template];
        // the change between active and inactive gets sent to be changed in the other view controller
        [self.navigationController pushViewController:viewC animated:YES];
        
        return;
    }

    NSString* cliendId = [_invoiceDict objectForKey:@"Client"];
    if (cliendId.length < 1 || !cliendId)
    {
        [UIUtils messageAlert:@"You can not save an invoice without client. Please add a client!" title:kApplicationName delegate:nil];
        return;
    }
    
    //Making sure that each invoice has a date, even if the user doesn't select one.
    NSString *dateInInvoice = [_invoiceDict objectForKey:@"InvoiceDate"];
    if (dateInInvoice.length == 0)
    {
        [_invoiceDict setObject: currentDate forKey:@"InvoiceDate"];
    }
    
    [_invoiceDict setObject:[self createInvoicePhotoList] forKey:@"PhotoList"];
    
    //making sure its not nil
    if (!self.pdfQuantity) {
        self.pdfQuantity = NO;
    }
    if (!self.pdfRate) {
        self.pdfRate = NO;
    }
    if (!self.pdfSubtotal) {
        self.pdfSubtotal = NO;
    }
    if (!self.pdfTotal) {
        self.pdfTotal = NO;
    }
    
    //set up PDF Options
    [_invoiceDict setValue:[NSNumber numberWithBool:self.pdfQuantity] forKey:@"PDFQuantity"];
    [_invoiceDict setValue:[NSNumber numberWithBool:self.pdfRate] forKey:@"PDFRate"];
    [_invoiceDict setValue:[NSNumber numberWithBool:self.pdfSubtotal] forKey:@"PDFSubtotal"];
    [_invoiceDict setValue:[NSNumber numberWithBool:self.pdfTotal] forKey:@"PDFTotal"];
    
    NSLog(@"The invoice being saved is === %@", _invoiceDict);
    
    if (self.invoiceObj)
    {
        NSLog(@"There was a previous invoice, and it was updated with the new one");
        [_gAppData addNewInvoice:_invoiceDict replaceTemplateID:YES];
    }
    else
    {
        NSLog(@"There was no previous invoice so it was added");
        
        [_gAppData addNewInvoice:_invoiceDict replaceTemplateID:NO];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateTemplate:(Templates *)template
{
    //NSLog(@"update template");
    //[_gAppData updateTemplate:template];
}

- (void) saveTemplate
{
    NSLog(@"save template ==");
    NSDictionary* templateDict = [[NSMutableDictionary alloc] init];
    
    UITableViewCell *cellView = (UITableViewCell*) [self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    NSString* templateName;
    
    if (possibleTemplateItems.count > 0)
    {
        NSArray* views = [cellView subviews];

            UITextField* cellText = views[1];
            templateName = cellText.text;

        }
    else{
        NSArray* views = [cellView subviews];

        if (self.existingTemplate) {
            UITextField* cellText = views[1];
            templateName = cellText.text;
        }
        else{
            UITextField* cellText = views[views.count - 1];
            templateName = cellText.text;
        }
        
        }
    
    if (!self.newTemplate)
    {
        templateName = self.templateTitleBox.text;
    }
    
    if (_changeActivity) {
        if ([self.template.active integerValue] == 0)
        {
            self.template.active = [NSNumber numberWithInteger:1];
        }
        else if ([self.template.active integerValue] == 1)
        {
            self.template.active = [NSNumber numberWithInteger:0];
        }
    }
    
    if ((templateName.length < 1) && self.template)
    {
        templateName = self.template.template_name;
    }
    
    [templateDict setValue:templateName forKey:@"template_name"];
    [templateDict setValue:self.template.active forKey:@"active"];
    [templateDict setValue:self.comment forKey:@"comment"];
    
    if (possibleTemplateItems.count > 0 && !self.existingTemplate)
    {
        [_gAppData addNewTemplate:templateDict replaceTemplateID:NO newTemplateID:0];
    } else{
        if (self.existingTemplate)
        {
            [_gAppData addNewTemplate:templateDict replaceTemplateID:YES newTemplateID:self.template.template_id];
        }
        else{
            [UIUtils messageAlert:@"Please add products before saving the Template." title:@"Message" delegate:nil];
            return;
        }
        
    }
    
    if (self.changeActivity) {
        [_gAppData changeActiveInactiveTempalte:self.template.template_id];
    }
    
    [self afterSavingTemplate];
}

- (void) afterSavingTemplate
{
    if (self.creatingTemplateFromEstimate)
    {
        // needs to go back to create estimate if you're done creating a template while creating an estimate
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }
    else {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
        
    }
}

- (NSString*) createInvoicePhotoList
{
    NSString* str = @"";
    for (NSString* name in _invoicePhotoList)
    {
        str = (str.length > 0)?[NSString stringWithFormat:@"%@,%@",str,name]:[NSString stringWithFormat:@"%@",name];
    }
    return str;
}


#pragma mark --
#pragma mark PDF

- (void) printAction
{
    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    if (data == nil)
    {
        [UIUtils messageAlert:@"Data is not available for prinitng." title:kApplicationName delegate:nil];
        return;
    }
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    if(printController && [UIPrintInteractionController canPrintData:data])
    {
        printController.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [filePath lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = data;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error)
            {
                //NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, (NSInteger)error.code);
            }
        };
        
        [printController presentAnimated:YES completionHandler:completionHandler];
    }
}

- (void) createPDF:(BOOL) showMessage isprinitng:(BOOL)isprinting
{
    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    
    NSString* date = [_invoiceDict objectForKey:@"InvoiceDate"];
    
    double currentTax = [[_invoiceDict objectForKey:@"Tax"] doubleValue];
    
    NSArray* detaillist = @[[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getSubtotal]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getAppliedtax]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getTotalData]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getPaidData]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getRemaindata]]]];
    
    NSArray* headerList = @[@"Sub Total",[NSString stringWithFormat:@"TAX(%.02f%%)",currentTax],@"Total",@"Paid",@"Unpaid"];
    
    if (_itemArray.count < 1)
    {
        if (showMessage)
            [UIUtils messageAlert:@"No preview available, please add products." title:kApplicationName delegate:nil];
        if (isprinting)
            [UIUtils messageAlert:@"Please add products to print estimate pdf." title:kApplicationName delegate:nil];
    
        return;
    }
    
    NSLog(@"The title is: %@", self.title);
    //removed the INVOICE text within the type, no title on top of document
    [PDFRenderer  drawPDF:filePath dataArray:_itemArray withTitle:self.title withClient:self.customerObj withDetail:detaillist header:headerList withDate:date type:@"" photoList:_invoicePhotoList isinvoice:YES showQuantity:self.pdfQuantity showRate:self.pdfRate showSubtotalsOnly:self.pdfSubtotal showTotalsOnly:self.pdfTotal];
    
    if (showMessage)
        [self performSelector:@selector(showPreview)];
    if (isprinting)
        [self performSelector:@selector(printAction) withObject:nil afterDelay:1.0];
}

- (void) showPreview
{
    NSString* date = [_invoiceDict objectForKey:@"InvoiceDate"];
    
    double currentTax = [[_invoiceDict objectForKey:@"Tax"] doubleValue];
    
    NSArray* detaillist = @[[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getSubtotal]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getAppliedtax]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getTotalData]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getPaidData]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getRemaindata]]]];
    
    NSArray* headerList = @[@"Sub Total",[NSString stringWithFormat:@"TAX(%.02f%%)",currentTax],@"Total",@"Paid",@"Unpaid"];
    
    PDFDisplayViewController* pdfDVC = [[PDFDisplayViewController alloc] initWithNibName:@"PDFDisplayView" bundle:nil];
    pdfDVC.estimateTitleId = self.title;
    pdfDVC.itemArray = _itemArray;
    pdfDVC.screenTitle = @"Invoice";
    pdfDVC.customer = self.customerObj;
    pdfDVC.detailList = detaillist;
    pdfDVC.headerList = headerList;
    pdfDVC.date = date;
    pdfDVC.photoList = _invoicePhotoList;
    pdfDVC.showQuantity = self.pdfQuantity;
    pdfDVC.showRate = self.pdfRate;
    pdfDVC.subtotalsOnly = self.pdfSubtotal;
    pdfDVC.totalsOnly = self.pdfTotal;
    pdfDVC.delegate = self;
    
    [self.navigationController pushViewController:pdfDVC animated:YES];
}

- (void)adjustPDFOptions:(PDFDisplayViewController*)controller quantity: (BOOL)PDFQuantity rate:(BOOL)PDFRate subtotal:(BOOL) PDFSubtotal totalsOnly: (BOOL)pdfTotalsOnly
{
    self.pdfQuantity = PDFQuantity;
    self.pdfRate = PDFRate;
    self.pdfSubtotal = PDFSubtotal;
    self.pdfTotal = pdfTotalsOnly;
    self.pdfOptions = YES;
    
    NSLog(@"after the change, self quantity is: %@ and rate is: %@ and subtotal is: %@ and totals are: %@", [NSNumber numberWithBool:self.pdfQuantity], [NSNumber numberWithBool:self.pdfRate], [NSNumber numberWithBool:self.pdfSubtotal], [NSNumber numberWithBool:self.pdfTotal]);
}

- (void)addItemViewController:(PDFDisplayViewController *)controller didFinishEnteringDataItem:(NSData *)item
{
    self.dataOfPDF = item;
}

#pragma mark - TemplateAdded 

- (void)templateAdded:(Templates *)template
{
    [self saveItems:template.tempProductList];
}

@end
