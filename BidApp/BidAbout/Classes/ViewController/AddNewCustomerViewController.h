//
//  AddNewMemberViewController.h
//  BidAbout
//
//  Created by    on 16/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardControls.h"
@class CoreDataManger;

@interface AddNewCustomerViewController : BaseViewController<UITextFieldDelegate,UITextViewDelegate,KeyboardControlsDelegate>
{
    IBOutlet UITextField* _firstNameTxtField;
    IBOutlet UITextField* _LastNameTxtField;
    IBOutlet UITextField* _customerPhoneTxtField;
    IBOutlet UITextField* _customerAddress1TxtField;
    IBOutlet UITextField* _customerAddress2TxtField;
    IBOutlet UITextField* _customerCityTxtField;
    IBOutlet UITextField* _customerStateTxtField;
    IBOutlet UITextField* _customerZipTxtField;
    IBOutlet UITextField* _customerEmailTxtField;
    
    IBOutlet UILabel* _notelabel;
    IBOutlet UITextView*  _customerNotesTextView;

    IBOutlet UIButton* _quotesYesBtn;
    IBOutlet UIButton* _quotesNoBtn;
    
    IBOutlet UIScrollView* _scrollView;
    
    KeyboardControls* _keyboardControls;
    UITapGestureRecognizer* _tapgesture ;
	
	CoreDataManger* _coreDataManager;
}

- (IBAction) quotesCheckedBtnAction:(id)sender;

@end
