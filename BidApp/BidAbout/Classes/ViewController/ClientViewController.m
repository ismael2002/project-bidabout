//
//  ClientViewController.m
//  BidAbout
//
//  Created by Waseem on 15/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "HomeListCell.h"
#import "SWRevealViewController.h"
#import "ClientDetialViewController.h"
#import "Customers.h"
#import "AddNewCustomerViewController.h"
#import "ClientViewController.h"
#import "Invoice.h"
#import "Estimate.h"
#import "CreateEstimateViewController.h"

@implementation ClientViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Clients";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;

    [self initialUI];
    
//    if (_customersList.count < 1 )
//    {
//        [_gAppData sendRequestForCustomerWithID:[_gAppPrefData.memberID integerValue]];
//    }
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(customerFetchFail) name:kCustomerListServiceFail object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(customerFetchSuccess) name:kCustomerListServiceSucess object:nil];
    
    _customersList = [_gAppData getCustomerDetails:NO andCustomerId:nil];
    [_tableView reloadData];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidUnload
{
    _customersList = nil;
	_tableView = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

#pragma mark --
#pragma mark PrivateMethods

- (void) initialUI
{
    if (self.isListMode)
    {
        UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
        self.navigationItem.leftBarButtonItem = cancelBtn;
    }
    else
    {
        // get revelButton
        UIButton* barButton = [UIUtils getRevelButtonItem:self];
        //Added revel button as nevegation button
        UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
        self.navigationItem.leftBarButtonItem = revealbuttonItem;
    }
    
    UIBarButtonItem* addbarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewCustomerAction)];
    
    UIBarButtonItem* editBtn = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editBtnAction)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:addbarBtn,editBtn,nil];
    
    _customersList = [ NSArray arrayWithArray:[_gAppData getCustomerDetails:NO andCustomerId:nil]];
}

- (void)removeAllInovice:(NSString *)customerID
{
	NSArray* invoiceArray = [_gAppData getInvoielistWithClientID:customerID];
    
    for (Invoice* invoiceObj in invoiceArray)
    {
		[_gAppData deleteInvoice:invoiceObj];
	}
}

// need to change delete estimate item using the id, because some estimate items do not have an id.
//change to delete all items using the estiate id tied to that 
- (void)removeAllEstimate:(NSString *)customerID
{
	NSArray* invoiceArray = [_gAppData getEstimatelistWithClientID:customerID];
	for (Estimate* estimateObj in invoiceArray)
    {
		[_gAppData deleteEstimate:estimateObj];
	}
}

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _customersList.count;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = (self.isListMode) ? UITableViewCellAccessoryNone:UITableViewCellAccessoryDisclosureIndicator;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
        cell.backgroundColor = [UIColor clearColor];
        //[cell setTintColor:[UIColor blackColor]];  trying to change the color of the accessory of the cell, its too light
    }
    
	Customers* obj = [_customersList objectAtIndex:indexPath.row];
    if (obj)
    {
        NSString* nameStr = [NSString stringWithFormat:@"%@ %@",obj.customer_firstname, obj.customer_lastname];
        cell.textLabel.text = nameStr;
    }
    
    return cell;
}

#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.isListMode)
    {
        Customers* obj = [_customersList objectAtIndex:indexPath.row];
        
        [self.delegate addItemViewController: self didFinishEnteringItem:obj];
        [self.navigationController popViewControllerAnimated:NO];

    }
    else
    {
        ClientDetialViewController* clientDVC = [[ClientDetialViewController alloc] initWithNibName:@"ClientDetailView" bundle:nil];
        clientDVC.customerObj = [_customersList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:clientDVC animated:YES];
    }

}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    
    BOOL found = NO;
    NSInteger b = 0;
    for (Customers *obj in _customersList)
    {
        NSString* nameStr = [obj.customer_firstname substringToIndex:1];
        if ([nameStr caseInsensitiveCompare:title] == NSOrderedSame)
            if (!found)
            {
                [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:b inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                found = YES;
            }
        b++;
    }
    
    //[tableView setBackgroundColor:[UIColor clearColor]];
    
    
    return b;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section];
//}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// The editing style for a row is the kind of button displayed to the left of the cell when in editing mode
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellEditingStyleDelete;
}

- (void) tableView:(UITableView*)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
	if(editingStyle == UITableViewCellEditingStyleDelete)
	{
		NSString* localizedOkay = NSLocalizedString(@"Confirm", nil);
		NSString* localizedCancel = NSLocalizedString(@"Cancel", nil);
		
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"CONFIRM" message:@"If you delete this customer all estimates/invoices related will also be deleted" delegate:self cancelButtonTitle:localizedOkay otherButtonTitles:localizedCancel, nil];
		[alert setTag:indexPath.row];
		[alert show];
	}
}


#pragma mark --
#pragma mark  Customer Service Notification

- (void) customerFetchSuccess
{
	_customersList = [_gAppData getCustomerDetails:NO andCustomerId:nil];
    [_tableView reloadData];
}

- (void) customerFetchFail
{
	[UIUtils messageAlert:@"Data is not available OR not in valid format!" title:kApplicationName delegate:nil];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark  Selector Action

- (void) addNewCustomerAction
{
    AddNewCustomerViewController* addMemebrVC = [[AddNewCustomerViewController alloc] initWithNibName:@"AddNewCustomerView" bundle:nil];
    UINavigationController* navc  = [[UINavigationController alloc] initWithRootViewController:addMemebrVC];
    [self presentViewController:navc animated:YES completion:nil];
}

- (void) editBtnAction
{
    static BOOL isEditing = NO;
    isEditing = !isEditing;
    [_tableView setEditing:isEditing animated:YES];
}

- (void) cancelBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --
#pragma mark  UiAlertView Deleget

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		Customers* cust = [_customersList objectAtIndex:alertView.tag];
		[self removeAllInovice:[NSString stringWithFormat:@"%ld",(long)[cust.customer_id integerValue]]];
		[self removeAllEstimate:[NSString stringWithFormat:@"%ld",(long)[cust.customer_id integerValue]]];
		[_gAppData deleteCustomerFromeClientList:cust];
	}
	_customersList = [_gAppData getCustomerDetails:NO andCustomerId:nil];
	[_tableView reloadData];
}

@end
