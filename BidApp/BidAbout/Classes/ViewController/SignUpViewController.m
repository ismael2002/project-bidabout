//
//  SignUpViewController.m
//  BidAbout
//
//  Created by    Kumar  on 10/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "SignUpViewController.h"

#define kScrollWidth 320
#define kiPhoneScrollFrameHeightWithoutKeyboard 460
#define kiPhone5ScrollFrameHeightWithoutKeyboard 548

#define kiPhone5ScrollFrameHeightWithKeyboard 246
#define kiPhoneScrollFrameHeightWithKeyboard 220

#define kscrollContentSizeDiffWithoutKeyBoard 20
#define kscrollContentSizeDiffWithKeyBoard 100

#define kTextFieldDiffrence 80

typedef enum textFiledType
{
    EComopanyNameTextFiled = 500,
    ECustomerFirstNameTxtFiled,
    ECustomerLastNameTxtField,
    ECustomerNoteTextField,
    ECustomerPhoneTextField
} ECustomerTextFieldType;


@interface SignUpViewController (Private)

- (BOOL) isAnyThingEmpty;
- (void) setUpKeyboardControls;
- (void) setUpScrollViewOffSet:(UITextField *)tField;

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"SignUp";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    [self setUpKeyboardControls];
    
    UIBarButtonItem* submitBtn = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStyleBordered target:self action:@selector(submitForSignUp:)];
    UIBarButtonItem* backBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonAction:)];
    
    self.navigationItem.leftBarButtonItem = backBtn;
    self.navigationItem.rightBarButtonItem = submitBtn;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signUpDone:) name:kSignUpSucessfullNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    _phoneNumberTxtField.tag = ECustomerPhoneTextField;
    
    if ([UIUtils isiPhone]) {
        if ([UIUtils isiPhone5]) {
            _scrollView.contentSize = CGSizeMake(kScrollWidth,
                                                 kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        } else {
            _scrollView.contentSize = CGSizeMake(kScrollWidth,
                                                 kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
    } else {
        _scrollView.contentSize = CGSizeMake(768,1024);
    }
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void) viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([_keyboardControls.textFieldsArray containsObject:textField]) {
        _keyboardControls.activeTextField = textField;
    }
    
    [self setUpScrollViewOffSet:textField];
}


- (BOOL) textFieldShouldReturn:(UITextField*)textField
{
	[textField resignFirstResponder];
    
    [self handleKeyboardHideShow];
	return YES;
}


#pragma mark -
#pragma mark Button Action

- (IBAction) submitForSignUp:(id)sender
{
    if (![self isAnyThingEmpty]) {
        BOOL isNotValid = [UIUtils validateEmail:_emailTxtField.text];
        if (isNotValid) {
            [_emailTxtField becomeFirstResponder];
            return;
        }
        
        BOOL isPhoneNotValid = [UIUtils isPhoneNumbervalid:_phoneNumberTxtField.text];
        if (isPhoneNotValid) {
            [_phoneNumberTxtField becomeFirstResponder];
            return;
        }
        
        AppPrefData *appData = _gAppPrefData;
        appData.password = _passwordTxtField.text;
        appData.emailId = _emailTxtField.text;
        
        PFUser *user = [PFUser user];
        user.username = _emailTxtField.text;
        user.password = _passwordTxtField.text;
        user.email = _emailTxtField.text;
        
        user[@"firstName"] = _firstNameTxtField.text;
        user[@"lastName"] = _LastNameTxtField.text;
        user[@"phone"] = _phoneNumberTxtField.text;
        
        
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSString *errorString = [error userInfo][@"error"];
                [self signUpDone:NO withError:errorString];
            }
            if (succeeded) {
                [self signUpDone:YES withError:nil];
            }
        }];
    }
}

- (IBAction) cancelButtonAction:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Private Methods

- (void) setUpScrollViewOffSet:(UITextField *)tField
{
    NSLog(@"%f",tField.frame.origin.y);
    [_scrollView setContentOffset:CGPointMake(0, tField.frame.origin.y-kTextFieldDiffrence) animated:YES];
    NSLog(@"%f",tField.frame.origin.y - kTextFieldDiffrence);
}

- (BOOL) isAnyThingEmpty
{
    if ([[ UIUtils checknilAndWhiteSpaceinString:_firstNameTxtField.text]  isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"First Name %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([[ UIUtils checknilAndWhiteSpaceinString:_LastNameTxtField.text] isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Last Name %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([[ UIUtils checknilAndWhiteSpaceinString:_phoneNumberTxtField.text] isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Mobile Number %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([[ UIUtils checknilAndWhiteSpaceinString:_emailTxtField.text] isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Email %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_passwordTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Password %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_confirmPasswrdTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Confirm password %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark Notification

- (void)signUpDone:(BOOL)isSuccessfull withError:(NSString *)error
{
	if (isSuccessfull) {
        AppPrefData *appData = _gAppPrefData;
		appData.password = _passwordTxtField.text;
		appData.emailId = _emailTxtField.text;
        appData.isFirstTimeLogedIn = YES;
        appData.isDefualttaxOn = _gAppPrefData.isDefualttaxOn;
        appData.currentTax = _gAppPrefData.currentTax;
        _gAppPrefData.isRememberMe = YES;
		[appData saveAllData];
        
        [PFUser logInWithUsernameInBackground:_emailTxtField.text
                                     password:_passwordTxtField.text
                                        block:^(PFUser *user, NSError *lError) {
                                            if (user) {
                                                appData.memberID = user.objectId;
                                                id appDelg = [[UIApplication sharedApplication] delegate];
                                                [appDelg performSelector:@selector(loginSuccessful)
                                                              withObject:nil
                                                              afterDelay:0.01];
                                            } else {
                                                [UIUtils messageAlert:@"An error occurred"
                                                                title:kApplicationName
                                                             delegate:nil];
                                            }
                                        }];
	} else {
		[UIUtils messageAlert:error title:kApplicationName delegate:nil];
	}
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard+50);
        }
    }
    else
        _scrollView.frame = CGRectMake(0,0,768,1004);
}

- (void) setUpKeyboardControls
{
    _keyboardControls = [[KeyboardControls alloc] init];
    _keyboardControls.delegate = self;
    
    // Add textFields in order
    _keyboardControls.textFieldsArray = [NSArray arrayWithObjects:_firstNameTxtField,_LastNameTxtField,_phoneNumberTxtField
                                         ,_emailTxtField,_passwordTxtField,_confirmPasswrdTxtField,nil];
    
    _keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    _keyboardControls.previousNextTintColor = [UIColor blackColor];
    _keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    _keyboardControls.previousTitle = @"Previous";
    _keyboardControls.nextTitle = @"Next";
    
    for (id textField in _keyboardControls.textFieldsArray)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = _keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
    }
}

- (void) dismissKeyboard
{
    [_firstNameTxtField resignFirstResponder];
    [_LastNameTxtField resignFirstResponder];
    [_phoneNumberTxtField resignFirstResponder];
    [_emailTxtField resignFirstResponder];
    [_passwordTxtField resignFirstResponder];
    [_confirmPasswrdTxtField resignFirstResponder];
    
    [self handleKeyboardHideShow];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (![string isEqualToString:@""])
        if ((textField.tag == ECustomerPhoneTextField))
            return [UIUtils setFormatForPhoneNumber:textField OfLength:12];
    
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

#pragma mark -
#pragma mark KeyBoardControlDelegate

- (void)keyboardControlsDonePressed:(KeyboardControls*)controls
{
    [controls.activeTextField resignFirstResponder];
    [self handleKeyboardHideShow];
}

- (void)keyboardControlsPreviousNextPressed:(KeyboardControls*)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self setUpScrollViewOffSet:textField];
}

- (void) handleKeyboardHideShow
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
    }
    else
        _scrollView.frame = CGRectMake(0,0,768,1004);
}

@end
