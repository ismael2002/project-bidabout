//
//  EditCustomerViewController.h
//  BidAbout
//
//  Created by    Kumar  on 08/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customers;

@interface EditCustomerViewController : BaseViewController <UITextFieldDelegate>
{
    IBOutlet UITableView* _customerInfoTable;
}

@property (nonatomic, retain) Customers* customerObj;
@end
