//
//  ExpensceListViewController.m
//  BidAbout
//
//  Created by    Kumar  on 09/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "ExpensceListViewController.h"
#import "Templates.h"
#import "Item.h"
#import "CreateInvoiceViewController.h"

@interface ExpensceListViewController ()<TemplateAdded>

@end

@implementation ExpensceListViewController
{
    BOOL isTemplate;
    NSMutableArray *allItems;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    isTemplate = NO;
    self.title = @"Add Product";
    
    NSLog(@"WE ARE NOW IN THE EXPENSE SCREEN, on expence list view controller");
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // Cancel bar button item.
	UIBarButtonItem* cancelBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
	self.navigationItem.leftBarButtonItem = cancelBarBtn;
    
	UIBarButtonItem* addBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBarBtnAction)];
	self.navigationItem.rightBarButtonItem = addBarBtn;
    
    _itemDataList = [[NSMutableArray alloc] init];
    
    //UIAlertView * alert =[[UIAlertView alloc ]
//    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Note:"
//                                                    message:@"This will add an item to this proposal/invoice only. Permanent items should be added in 'Products' in the main menu."
//                                                   delegate:self
//                                          cancelButtonTitle:@"Don't show me this again"
//                                          otherButtonTitles:@"Ok", nil];
//    
//    NSInteger showAlert = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ShowProductWarning"] integerValue];
//    
//    NSLog(@"The value of the standard user defaults is: %ld", (long)showAlert);
//    
//    if (showAlert == 1) {
//        
//    } else{
//        [alert show];
//    }
    
    //[alert show];

    

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
	if (KVersion >= 7)
	{
		self.edgesForExtendedLayout = UIRectEdgeNone;
	}
#endif
    
    [self showProduct];
    // addition to try and show the product first
}

//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    NSLog(@"Button Index =%ld",(long)buttonIndex);
//    
//    if (buttonIndex == 0)
//    {
//        NSLog(@"You have clicked dont show this again");
//        // dont show this message again
//        
//        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ShowProductWarning"];
//        
//    }
//    else if(buttonIndex == 1)
//    {
//        NSLog(@"You have clicked okay");
//    }
//}


#pragma mark --
#pragma mark SegmentControlBar mtehod

- (IBAction) segmentedControlHasChangedValue:(id)sender
{
    switch (_segmentControlBar.selectedSegmentIndex)
    {
        case 0:
            [self showProduct];
            break;
            
        case 1:
            [self showTemplate];
            break;
            
        default:
            break;
    }
}

- (void) showTemplateItems: (NSNumber*) templateID
{
    NSArray* templateItemArray = [NSMutableArray arrayWithArray:[_gAppData getItemsFromExistingTemplate:templateID]];
    
    [_itemDataList removeAllObjects];
    [_itemDataList addObjectsFromArray:templateItemArray];
    self.isTemplateItems = YES;
    
    [_expenseTV reloadData];
}

- (void) showTemplate
{
    //change so that when you press the add sign,
    self.title = @"Add Template";
    
    UIBarButtonItem* addTemplateBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTemplateAction)];
    self.navigationItem.rightBarButtonItem = addTemplateBtn;
    
    NSArray* templateList = [ NSArray arrayWithArray:[_gAppData getTemplatedetails]];
    
    if (_itemDataList.count>0)
    {
        [_itemDataList removeAllObjects];
    }
    
    [_itemDataList addObjectsFromArray:templateList];
    [_expenseTV reloadData];
}

-(void) addTemplateAction
{
    CreateInvoiceViewController* viewC = [[CreateInvoiceViewController alloc] initWithNibName:@"CreateInvoiceView" bundle:nil];
    viewC.newTemplate = YES;
    viewC.isTemplate = YES;
    viewC.creatingTemplateFromEstimate = YES;
    
    [self.navigationController pushViewController:viewC animated:YES];
}

- (void) showProduct
{
    self.title = @"Add Product";
    self.isTemplateItems = NO;
    
    UIBarButtonItem* addBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBarBtnAction)];
    
    self.navigationItem.rightBarButtonItem = addBarBtn;
    
    if (!self.isTemplate)
    {
        isTemplate = NO;
    }
    
    if (_itemDataList.count>0)
    {
        [_itemDataList removeAllObjects];
    }
    
    [_itemDataList addObjectsFromArray:[_gAppData getFilteredItemResults]];
    
    if (_itemDataList.count < 1)
        [UIUtils messageAlert:@"No data found. Please activate a template or add products on the products screen." title:@"Message" delegate:nil];
    
    [_expenseTV reloadData];
}

- (void) addAllTemplateItems
{
    //_itemDataList
    
    for (NSInteger x = 0; x < _itemDataList.count; x ++)
    {
        Item* item = _itemDataList[x];
        
        NSDictionary *itemDictionary = @{
                                        @"Note" : item.item_desc,
                                        @"TaxAllow" : item.item_taxAllow,
                                        @"Title" : item.item_title,
                                        @"Quantity" : item.item_quantity,
                                        @"ItemID" : item.item_id,
                                        @"Rate" : item.item_rate,
                                        @"Amount" : item.item_amount,
                                        };
        
        [_gAppData addNewInvoiceItem:itemDictionary];
    }
    
    [self.navigationController popViewControllerAnimated:NO];
    
}

#pragma mark -- 
#pragma mark  TableViewData source.

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _itemDataList.count;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    if (_segmentControlBar.selectedSegmentIndex == 0)
    {
        Item *item = _itemDataList[indexPath.row];
        
        cell.textLabel.text = item.item_title;
    }
    
    if (_segmentControlBar.selectedSegmentIndex == 1 && !self.isTemplateItems)
    {
        Templates *template = _itemDataList[indexPath.row];
        
        cell.textLabel.text = template.template_name;
    }
    
    if (_segmentControlBar.selectedSegmentIndex == 1 && self.isTemplateItems)
    {
        Item *item = _itemDataList[indexPath.row];
        
        cell.textLabel.text = item.item_title;
    }
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _productObj = [_itemDataList objectAtIndex:indexPath.row];
    
    if (self.isTemplate)
    {
        id obj = [allItems objectAtIndex:indexPath.row];
        if ([obj isKindOfClass:[Templates class]])
        {
            ScheduleViewController* scheduLeVC = [[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
            scheduLeVC.templet = obj;
            scheduLeVC.templateAddedDelegate = self;
            scheduLeVC.isProductItem = NO;
            scheduLeVC.isAddEstimate = NO;
            scheduLeVC.isAddInvoice = NO;
            scheduLeVC.isUpdateEstimate = NO;
            scheduLeVC.isEstimate = self.isEstimate;
            scheduLeVC.isTemplate = YES;

            [self.navigationController pushViewController:scheduLeVC animated:YES];
        }
    }

    if ([_productObj isKindOfClass:[Item class]])
    {
        ScheduleViewController* scheduLeVC = [[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
        scheduLeVC.product = _productObj;
        scheduLeVC.isUpdateEstimate = NO;
        scheduLeVC.isProductItem = YES;
        scheduLeVC.isAddEstimate = self.isEstimate;
        scheduLeVC.isAddInvoice = !self.isEstimate;
        scheduLeVC.isEstimate = self.isEstimate;
        scheduLeVC.fromClientScreen = self.fromClientScreen;
        
        if (self.isInvoice)
        {
            scheduLeVC.addInvoiceProduct = YES;
        }
    
        if (self.isTemplate)
        {
            id obj = [allItems objectAtIndex:indexPath.row];
            scheduLeVC.templet = obj;
            scheduLeVC.templateAddedDelegate = self;
            scheduLeVC.isProductItem = NO;
            scheduLeVC.isAddEstimate = NO;
            scheduLeVC.isAddInvoice = NO;
            scheduLeVC.isUpdateEstimate = NO;
            scheduLeVC.isEstimate = self.isEstimate;
            scheduLeVC.isTemplate = YES;
        }
        [self.navigationController pushViewController:scheduLeVC animated:YES];
    }
    
    if ([_productObj isKindOfClass:[Templates class]] && !self.isTemplate)
    {
        NSLog(@"template class type");
        
        Templates* template = _productObj;
        
        if ([template.active integerValue] == 0)
        {
            NSArray* templateItems = [_gAppData getItemsFromExistingTemplate:template.template_id];
            
            NSMutableDictionary* templateItemDictionary = [NSMutableDictionary new];
            
            for (NSInteger x = 0; x < templateItems.count; x++)
            {
                Item* templateItem = templateItems[x];
                
                [templateItemDictionary setObject:templateItem.item_title forKey:@"Title"];
                [templateItemDictionary setObject:templateItem.item_desc forKey:@"Note"];
                [templateItemDictionary setObject:templateItem.item_taxAllow forKey:@"TaxAllow"];
                [templateItemDictionary setObject:templateItem.item_title forKey:@"Title"];
                [templateItemDictionary setObject:templateItem.item_quantity forKey:@"Quantity"];
                [templateItemDictionary setObject:templateItem.item_rate forKey:@"Rate"];
                [templateItemDictionary setObject:templateItem.item_amount forKey:@"Amount"];
                
                if (self.isEstimate)
                {
                    [_gAppData addNewEstimateItem:templateItemDictionary];
                }
                
                if (self.isInvoice)
                {
                    [templateItemDictionary setObject:templateItem.item_id forKey:@"ItemID"];
                    [_gAppData addNewInvoiceItem:templateItemDictionary];
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        if ([template.active integerValue] == 1)
        {
            NSLog(@"Inactive");
            
            [UIUtils messageAlert:@"Please activate this template on template screen." title:kApplicationName delegate:nil];
            
        }


//        UIBarButtonItem* templateBtn = [[UIBarButtonItem alloc] initWithTitle:@"Add all" style:UIBarButtonItemStylePlain target:self action:@selector(addAllTemplateItems)];
//        self.navigationItem.rightBarButtonItem = templateBtn;
//        self.title = @"Add Products";
//        
//        [self showTemplateItems:template.template_id];
        
        //============
    }
    
//	else
//	{
//		id obj = [allItems objectAtIndex:indexPath.row];
//		if ([obj isKindOfClass:[Templates class]])
//		{
//            NSLog(@"This is a template");
//			ScheduleViewController* scheduLeVC = [[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
//			scheduLeVC.templet = obj;
//            scheduLeVC.templateAddedDelegate = self;
//			scheduLeVC.isProductItem = YES;
//			scheduLeVC.isAddEstimate = NO;
//			scheduLeVC.isUpdateEstimate = NO;
//            scheduLeVC.isEstimate = self.isEstimate;
//            if (self.isTemplate) {
//                NSLog(@"It is a template to start with");
//                scheduLeVC.isTemplate = YES;
//                scheduLeVC.isProductItem = NO;
//                scheduLeVC.isAddInvoice = NO;
//                scheduLeVC.isProductItem = NO;
//                
//            }
//            
//           
//            
//			[self.navigationController pushViewController:scheduLeVC animated:YES];
//		}
//	}
    
//    if ([_productObj isMemberOfClass:[Item class]])
//        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveditemNotification object:[_productObj item_id]];
//    
//    [self dismissModalViewControllerAnimated:YES];
}



#pragma mark --
#pragma mark Btn Action

- (void) cancelBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
	//[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) addBarBtnAction
{
    NSLog(@"About to go to schedule view controller");
    
    ScheduleViewController* scheduleVC =[[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
    
    if (self.isTemplate == NO)
    {
        NSLog(@"addbarBtn Action");
        scheduleVC.isUpdateEstimate = NO;
        scheduleVC.isProductItem = YES;
        scheduleVC.isAddEstimate = self.isEstimate;
        scheduleVC.isAddInvoice = !self.isEstimate;
        scheduleVC.isEstimate = NO;
        scheduleVC.addInvoiceProduct = YES;
    }
    if(self.isEstimate)
    {
        NSLog(@"Estimate");
        scheduleVC.isEstimate = YES;
        //scheduleVC.addToEstimate = self.addToEstimate;
        scheduleVC.addToEstimate = YES;
        scheduleVC.isAddEstimate = self.addToEstimate;
    }
    
    if (self.isTemplate) {
        NSLog(@"it is a template at the end");
        
        scheduleVC.templateAddedDelegate = self;
        scheduleVC.isProductItem = NO;
        scheduleVC.isAddEstimate = NO;
        scheduleVC.isAddInvoice = NO;
        scheduleVC.isUpdateEstimate = NO;
        scheduleVC.isEstimate = self.isEstimate;
        scheduleVC.isTemplate = YES;
        scheduleVC.creatingTemplateFromEstimate = self.creatingTemplateFromEstimate;
    }
    
    //issue with going back to client screen after adding product
    scheduleVC.fromClientScreen = self.fromClientScreen;
    
    [self.navigationController pushViewController:scheduleVC animated:YES];
}

- (void)templateAdded:(Templates *)template completion:(void (^)(void))block
{
    NSLog(@"ExpenseList");
    [self.templateAddedDelegate templateAdded:template completion:block];
}

@end
