//
//  ExpensceListViewController.h
//  BidAbout
//
//  Created by    Kumar  on 09/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScheduleViewController.h"

@interface ExpensceListViewController : BaseViewController <UIAlertViewDelegate>
{
    IBOutlet UITableView* _expenseTV;
    IBOutlet UISegmentedControl* _segmentControlBar;
    
    NSMutableArray* _itemDataList;
    //NSMutableArray* _itemList;
    id _productObj;
}

@property (nonatomic) BOOL isEstimate;
@property (nonatomic) BOOL isInvoice;
@property (nonatomic) BOOL isTemplate;
@property (nonatomic, assign) BOOL addToEstimate;
@property (nonatomic) BOOL isTemplateItems;
@property (nonatomic) BOOL fromTemplate;
@property (nonatomic) BOOL creatingTemplateFromEstimate;
@property (nonatomic, assign) BOOL fromClientScreen;

@property (nonatomic, strong) id<TemplateAdded> templateAddedDelegate;

- (IBAction) segmentedControlHasChangedValue:(id)sender;

@end