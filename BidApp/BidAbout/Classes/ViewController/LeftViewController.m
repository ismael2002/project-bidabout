//
//  LeftViewController.m
//  BidAbout
//
//  Created by     on 01/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "LeftViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HomeViewController.h"
#import "EstimatesViewController.h"
#import "ClientViewController.h"

#import "GHMenuCell.h"

#define kCompanySection 1
#define kSettingSection 2
#define kDefaultHeaderHeight 22.0

@interface LeftViewController ()

@end

@implementation LeftViewController
{
    NSInteger requests;
    NSInteger estimates;
}

@synthesize leftTableView = _leftTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    UIColor *bgColor = [UIColor colorWithRed:(50.0f/255.0f) green:(57.0f/255.0f) blue:(74.0f/255.0f) alpha:1.0f];
    self.leftTableView.backgroundColor = bgColor;
    _isShowHideSection = NO;
    
    _menuList = [[NSArray alloc] initWithContentsOfFile:ResourcePath(@"MenuItem.plist")];
    requests = 0;
    
    
    CGRect rect = self.leftTableView.frame;
    rect.size.width = 260;
    self.leftTableView.frame = rect;
    
}

//it only works if you open it, need to change it to push notification
- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSLog(@"View will appear --- for the left view controller");
    
    
    [_gAppPrefData numberOfPendingRequests:^(NSInteger requestCount) {
        requests = requestCount;
        [UIApplication sharedApplication].applicationIconBadgeNumber += requestCount;
        [_leftTableView reloadData];
    } clear:NO];
    
    [_gAppPrefData numberOfAcceptedEstimates:^(NSInteger estimateCount) {
        estimates = estimateCount;
        [UIApplication sharedApplication].applicationIconBadgeNumber += estimateCount;
        [_leftTableView reloadData];
    } clear:NO];
    
    NSLog(@"The new requests are: %ld, the estimtes are: %ld", (long)requests, (long)estimates);
    
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
- (UIStatusBarStyle)preferredStatusBarStyle
{
    NSLog(@"preferred status bay style");
    
	return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden
{
    NSLog(@"prefers status bar hidden");
    
	return YES;
}

#endif

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"number of rows in section");
    
    NSArray* sectionDetailList = [_menuList objectAtIndex:section];
    
    if (sectionDetailList != nil)
    {
        if ((section == kSettingSection) && _isShowHideSection)
            return 0;
        
        return sectionDetailList.count;
    }
    return 0;
}

#pragma mark --
#pragma mark TableView Delegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_menuList != nil)
        return _menuList.count;
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSLog(@"height for header");
    
    //    if (section == 0 )
    //        return 0.0;
    
    return kDefaultHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"view for header insection");
    
	UIView *headerView = nil;
	NSArray* headerTextArray = [NSArray arrayWithObjects:@"Company",@"Settings",nil];
    
    //    NSArray* headerTextArray = [NSArray arrayWithObjects:@"",@"Company",@"Settings",nil];
    NSString* headertext = [headerTextArray objectAtIndex:section];
    
    if (headertext.length > 1)
    {
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.height, 26.0f)];
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = headerView.bounds;
        gradient.colors = @[(id)[UIColor colorWithRed:(67.0f/255.0f) green:(74.0f/255.0f) blue:(94.0f/255.0f) alpha:1.0f].CGColor,(id)[UIColor colorWithRed:(57.0f/255.0f) green:(64.0f/255.0f) blue:(82.0f/255.0f) alpha:1.0f].CGColor,];
        [headerView.layer insertSublayer:gradient atIndex:0];
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectInset(headerView.bounds, 12.0f, 0.0f)];
        textLabel.text = headertext;
        textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:[UIFont systemFontSize]];
        textLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        textLabel.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.25f];
        textLabel.textColor = [UIColor colorWithRed:(196.0f/255.0f) green:(204.0f/255.0f) blue:(218.0f/255.0f) alpha:1.0f];
        textLabel.backgroundColor = [UIColor clearColor];
        [headerView addSubview:textLabel];
        UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [UIScreen mainScreen].bounds.size.height, 1.0f)];
        topLine.backgroundColor = [UIColor colorWithRed:(78.0f/255.0f) green:(86.0f/255.0f) blue:(103.0f/255.0f) alpha:1.0f];
        [headerView addSubview:topLine];
		
        UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 26.0f, [UIScreen mainScreen].bounds.size.height, 1.0f)];
        bottomLine.backgroundColor = [UIColor colorWithRed:(36.0f/255.0f) green:(42.0f/255.0f) blue:(5.0f/255.0f) alpha:1.0f];
        [headerView addSubview:bottomLine];
    }
    
    if (section == kSettingSection)
    {
        UIButton* showHideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [showHideBtn setImage:[UIImage imageNamed:@"btn-arrow.png"] forState:UIControlStateNormal];
        showHideBtn.frame = CGRectMake(250.0 - 30, 1, 25, 24);
        [showHideBtn addTarget:self action:@selector(showHideSetting) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:showHideBtn];
    }
    
	return headerView;
}

- (void) showHideSetting
{
    NSLog(@"show hide setting");
    
    _isShowHideSection = !_isShowHideSection;
    [_leftTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
}

- (GHMenuCell*)hasBadge:(NSIndexPath *)path andCell:(GHMenuCell *)cell
{
    NSLog(@"Has Badge the requests are: %ld, the estimtes are: %ld", (long)requests, (long)estimates);
    
    if (path.row == 1 && path.section == 0) {
        
        if (requests > 0) {
            [cell.badge setHidden:NO];
            cell.badgeString = [NSString stringWithFormat:@"%ld", (long)requests];
            cell.badgeRightOffset = 75.0f;
            cell.badgeColor = [UIColor colorWithRed:255.0f green:0.0f blue:0.0f alpha:1.0f];
            cell.badgeTextColor = [UIColor whiteColor];
            return cell;
        } else {
            [cell.badge setHidden:YES];
            cell.badgeString = nil;
            return cell;
        }
    } else if(path.row == 3 && path.section == 0){
        if (estimates > 0) {
            NSLog(@"Greater than 0 estimates: %ld", (long)estimates);
            [cell.badge setHidden:NO];
            cell.badgeString = [NSString stringWithFormat:@"%ld", (long)estimates];
            cell.badgeRightOffset = 75.0f;
            cell.badgeColor = [UIColor colorWithRed:255.0f green:0.0f blue:0.0f alpha:1.0f];
            cell.badgeTextColor = [UIColor whiteColor];
            return cell;
        } else {
            NSLog(@"no new proposals");
            [cell.badge setHidden:YES];
            cell.badgeString = nil;
            return cell;
        }
    } else {
        return cell;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"tableview");
    
	static NSString *cellIdentifier = @"Cell";
	GHMenuCell *cell = (GHMenuCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[GHMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		cell.backgroundColor = [UIColor clearColor];
    }
    
    cell = [self hasBadge:indexPath andCell:cell];
    
    NSArray* array = [ _menuList objectAtIndex:indexPath.section];
    if (array.count > indexPath.row)
    {
        NSDictionary* dict = [array objectAtIndex:indexPath.row];
        if (dict)
        {
            cell.imageView.image = [UIImage imageNamed:[dict objectForKey:@"ImageName"]];
            cell.textLabel.text = [dict objectForKey:@"Name"];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"tableview did select");
    
    //    if (indexPath.section == 0)
    //    {
    //        [_gAppData sendRequestForSynsc];
    //        return;
    //    }
    
    NSArray* array = [ _menuList objectAtIndex:indexPath.section];
    if (array.count > indexPath.row)
    {
        NSDictionary* dict = [array objectAtIndex:indexPath.row];
        if (dict)
        {
            NSString* viewControllerName = [dict objectForKey:@"ViewControllerName"];
            NSLog(@"The view controller name is: %@", viewControllerName);
            [self displayFrontViewController:viewControllerName];
        }
    }
}

#pragma mark --
#pragma mark Nevegation Logic

// This method is used to navigate the user to respective controller from menu list. Please add viewContrller name in menulist to navigate rather than useing using if-else or switch condition.

- (void) displayFrontViewController:(NSString*)viewControllerName
{
    NSLog(@"display front view controller");
    
    //[self.leftTableView reloadData];
    
    NSString *classNameStr = viewControllerName;
    Class theClass = NSClassFromString(classNameStr);
    if (theClass)
    {
        id viewControllerObj = [[theClass alloc] init];
        
        SWRevealViewController *revealController = self.revealViewController;
        
        // we know it is a NavigationController
        UINavigationController *frontNavigationController = (id)revealController.frontViewController;
        
        if ( ![frontNavigationController.topViewController isKindOfClass:[viewControllerObj class]] )
        {
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewControllerObj];
            
            [revealController setFrontViewController:navigationController animated:YES];
        }
        // Seems the user attempts to 'switch' to exactly the same controller he came from!
        else
        {
            [revealController revealToggle:self];
        }
    }
}

@end
