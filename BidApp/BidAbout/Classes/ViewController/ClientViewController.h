//
//  ClientViewController.h
//  BidAbout
//
//  Created by Waseem on 15/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
//@class Customers;
@class ClientViewController;

@protocol ClientViewControllerDelegate<NSObject>

- (void)addItemViewController:(ClientViewController *)controller didFinishEnteringItem:(Customers *)item;

@optional
- (void) addCustomerDetial:(Customers*)customers;
@end

@interface ClientViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource,UIActionSheetDelegate>
{
	__weak IBOutlet UITableView *_tableView;
    
	NSArray* _customersList;
}
@property (nonatomic, weak) id <ClientViewControllerDelegate> delegate;

@property(nonatomic, assign) BOOL isListMode;
@property (nonatomic, assign) id<ClientViewControllerDelegate> clientDelegate;


@end
