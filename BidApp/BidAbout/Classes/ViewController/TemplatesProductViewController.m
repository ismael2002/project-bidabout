//
//  TemplatesProductViewController.m
//  BidAbout
//
//  Created by Ratnesh Singh on 05/12/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "TemplatesProductViewController.h"

@interface TemplatesProductViewController ()

@end

@implementation TemplatesProductViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Template Product";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // Cancel bar button item.
	UIBarButtonItem* cancelBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
	self.navigationItem.leftBarButtonItem = cancelBarBtn;
//	UIBarButtonItem* addBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBarBtnAction)];
//	self.navigationItem.rightBarButtonItem = addBarBtn;
    
    _itemDataList = [[NSMutableArray alloc] initWithArray:[_gAppData getItemWithID:nil]];
	
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
	if (KVersion >= 7)
	{
		self.edgesForExtendedLayout = UIRectEdgeNone;
	}
#endif
}

- (void) cancelBtnAction
{
	[self.navigationController popViewControllerAnimated:YES];
}
@end
