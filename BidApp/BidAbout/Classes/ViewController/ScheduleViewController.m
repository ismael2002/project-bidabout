//
//  ScheduleViewController.m
//  BidAbout
//
//  Created by    Kumar  on 09/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "ScheduleViewController.h"
#import "EditableDetailCell.h"
#import "Templates.h"
#import "Item.h"
#import "TemplatesViewController.h"
#import "CreateInvoiceViewController.h"

#define kScrollWidth 320
#define kiPhoneScrollFrameHeightWithoutKeyboard 460
#define kiPhone5ScrollFrameHeightWithoutKeyboard 548

#define kiPhone5ScrollFrameHeightWithKeyboard 246
#define kiPhoneScrollFrameHeightWithKeyboard 220

#define kTextViewTag 399
#define kTaxSwitchtag 400
#define kTitletextViewtag 401
#define kQuantTaxField 0
#define kRatetextField 1


@interface ScheduleViewController () <UITextViewDelegate>

@end

@implementation ScheduleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Item";
    self.mainTableview.delegate = self;
    
    NSLog(@"WE ARE NOW IN THE EXPENSE SCREEN, on the schedule view controller");
    //NSLog(@"The view controllers are: %@", self.navigationController.viewControllers);
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    
    if (self.addInvoiceProduct || self.isProductItem)
    {
        UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveBtnAction)];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    
    if (self.isEstimate)
    {
        NSLog(@"IT IS AN ESTIMATE");
        
        UIBarButtonItem* addToEstimateBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveEstimateProductBtnAction)];
        self.navigationItem.rightBarButtonItem = addToEstimateBtn;
    }
    
    if(_product && !self.isTemplate && !self.addInvoiceProduct && !self.isEstimate)
    {
        NSLog(@"IF PRODUCT comes from product screen....");
        
        UIBarButtonItem* updateBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(updateProductBtnAction)];
        
        self.navigationItem.rightBarButtonItem = updateBtn;
    }
    if(self.isTemplate)
	{
        UIBarButtonItem* updateBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(templateAdded)];
        self.navigationItem.rightBarButtonItem = updateBtn;
	}
    if (self.existingTemplateItem)
    {
        UIBarButtonItem* updateTemplateItemBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(updateTemplateItemBtn)];
        self.navigationItem.rightBarButtonItem = updateTemplateItemBtn;
    }
    
    [self initialSetUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (self.addInvoiceProduct && !self.isTemplate && !self.isEstimate)
    {
        NSLog(@"IF PRODUCT comes from an invoice....");
        //before there's another of the same if statement
        
        UIBarButtonItem* saveInvoiceItemBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveInvoiceItemAction)];
        self.navigationItem.rightBarButtonItem = saveInvoiceItemBtn;
    }

    if (self.fromClientScreen) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Note:"
                                                        message:@"This will add an item to this proposal/invoice only. Permanent items should be added in 'Products' in the main menu."
                                                       delegate:self
                                              cancelButtonTitle:@"Don't show me this again"
                                              otherButtonTitles:@"Ok", nil];
        
        NSInteger showAlert = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ShowProductWarning"] integerValue];
        
        NSLog(@"The value of the standard user defaults is: %ld", (long)showAlert);
        
        if (showAlert == 1) {
            
        } else{
            [alert show];
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ShowProductWarning"];
    }
    else if(buttonIndex == 1)
    {
        NSLog(@"You have clicked okay");
    }
}

- (void) nextBtnAction
{
    // just add an option to save the item to the estimate. ------
    
    UIActionSheet* actionsheet = nil;
    
    actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Update",@"Add to Product List", nil];
    
    //actionsheet.tag = 1005;
    [actionsheet showInView:self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self nextActionSheatAction:buttonIndex];
}

- (void) nextActionSheatAction:(NSInteger) btnIndex
{
    switch (btnIndex)
    {
        case 0:
            [self updateProductBtnAction];
            break;
        case 1:
            [self addInvoiceItemToProductList];
            break;
            
        default:
            break;
    }
}

-(void) addInvoiceItemToProductList
{
    [_gAppData addInvoiceItemToProductList:_itemDict];
    
    if (self.presentingViewController)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
	_tableView = nil;
    _itemDict = nil;
	[super viewDidUnload];
}

- (void) setproductObj:(id)obj
{
    _product = nil;
    _product = obj;
}

#pragma mark --
#pragma mark InitialsetUp

- (void) initialSetUp
{
    _itemDict = [[NSMutableDictionary alloc] initWithCapacity:6];
    
    if (_product)
    {

        [_itemDict setObject:[_product item_quantity] forKey:@"Quantity"];
        [_itemDict setObject:[_product item_rate] forKey:@"Rate"];
        [_itemDict setObject:[_product item_amount] forKey:@"Amount"];
        [_itemDict setObject:[_product item_desc] forKey:@"Note"];
        [_itemDict setObject:[_product item_teplateID] forKey:@"templateId"];
        
        if (self.isProductItem) {
            if (![_product item_id])
            {
                 [_itemDict setObject:@"" forKey:@"ItemID"];
            }
            else {
                [_itemDict setObject:[_product item_id] forKey:@"ItemID"];
            }
        }
        
        //[_itemDict setObject:[_product item_id] forKey:@"ItemID"];
        //error here because its nil after inserting item to a new template, but there is no need to transfer over the item id right?
        [_itemDict setObject:[_product item_taxAllow] forKey:@"TaxAllow"];
        [_itemDict setObject:[_product item_title] forKey:@"Title"];
    }
	else if(_templet)
	{
		
	}
    else
    {
        [_itemDict setObject:[NSNumber numberWithBool:YES] forKey:@"TaxAllow"];
    }
    
    if ((!self.addToEstimate || self.updateInvoiceOrAddToProductList) && self.isTemplate)
    {
        UIBarButtonItem* nextBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(nextBtnAction)];
        self.navigationItem.rightBarButtonItem = nextBarBtn;
    }
    
    if ((self.addInvoiceProduct || self.isProductItem) && !self.updateInvoiceOrAddToProductList)
    {
        UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveBtnAction)];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    
    if (self.isTemplate)
    {
        UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(templateAdded)];
        self.navigationItem.rightBarButtonItem = saveBtn;
    }
    
    [_tableView reloadData];
}

#pragma mark --
#pragma mark  TableViewData source.

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 4;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRow = (section == 2)? 3:1;
    return numberOfRow;
}

- (EditableDetailCell *) editDetailCell
{
    EditableDetailCell* editCell = [[EditableDetailCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                             reuseIdentifier:nil];
    return editCell;
}

- (UITableViewCell*) othersCell
{
    NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenifire];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
        _notetextView.keyboardType = UIKeyboardTypeDefault;
    }

    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* titlestr = @"";
    switch (section)
    {
        case 0:
            titlestr = @"Title";
            break;
            
        case 1:
            titlestr = @"Item Description";
            break;
            
        default:
            break;
    }
    
    return titlestr;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = nil;
    switch (indexPath.section)
    {
        case 0:
        {
            cell = [self othersCell];
            [self setTitletextView:cell];
            [cell.contentView addSubview:_titletextView];
        }
            break;
            
        case 1:
        {
            cell = [self othersCell];
            [self settextView:cell];
            [cell.contentView addSubview:_notetextView];
        }
            break;
            
        case 2:
        {
            if (cell == nil)
            {
                cell = [self editDetailCell];
                UITextField *textField = [(EditableDetailCell*)cell textField];
                UILabel* titlelbl  = [(EditableDetailCell*)cell titleLabel];
                textField.tag = indexPath.row;
                switch (textField.tag)
                {
                    case 0:
                    {
                        [titlelbl setText:@"Quantity"];
                        NSNumber* quantityStr = [NSNumber numberWithFloat:[[_itemDict objectForKey:@"Quantity"] floatValue]];
                        CGFloat q = [quantityStr floatValue];
                        
                        _quantTextField = textField;
                        _quantTextField.tag = kQuantTaxField;
                        textField.text = (q > 0)? [NSString stringWithFormat:@"%.02f",q ] :@"";
                        
                        textField.placeholder = [NSString stringWithFormat:@"%d",0];
                        
                        textField.keyboardType = UIKeyboardTypeDecimalPad;
                    }
                        break;
                        
                    case 1:
                    {
                        [titlelbl setText:@"Price"];
                        NSNumber* rateStr = [_itemDict objectForKey:@"Rate"];
                        double rate = [rateStr doubleValue];
                        
                        _rateTextField = textField;
                        _rateTextField.tag = kRatetextField;
                        textField.text = (rate > 0)? [NSString stringWithFormat:@"%.02f",[rateStr doubleValue]]:@"";
                        
                        textField.placeholder = [NSString stringWithFormat:@"%.02f",0.0];
                        textField.keyboardType = UIKeyboardTypeDecimalPad;
                    }
                        break;
                        
                    case 2:
                    {
                        _amountTextField  = textField;
                        [titlelbl setText:@"Amount"];
                        textField.text = [NSString stringWithFormat:@"$%.02f",[self getCalculatedAmount]];
                        textField.userInteractionEnabled = NO;
                    }
                        break;
                        
                    default:
                        break;
                }
                [textField setDelegate:self];
            }
            
            UITextField *textField = [(EditableDetailCell*)cell textField];
            [textField setText:textField.text];
        }
            break;
            
        case 3:
        {
            cell = [self othersCell];
            [self setSwitch:cell];
            [cell.contentView addSubview:_taxApplicableSwitch];
            [_taxApplicableSwitch setOn:[[_itemDict objectForKey:@"TaxAllow"] boolValue] animated:YES];
            cell.textLabel.text = @"Tax";
        }
            break;
        
        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat expextedCize = 44.0;
    if (indexPath.section == 1)
    {
        expextedCize = 200.0;
    }
    
    return expextedCize;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_titletextView resignFirstResponder];
    [_amountTextField resignFirstResponder];
    [_notetextView resignFirstResponder];
    [_rateTextField resignFirstResponder];
    //[_quantTextField resignFirstResponder];
}

- (void) setSwitch:(UITableViewCell*)cell
{
    if (_taxApplicableSwitch == nil)
    {
        _taxApplicableSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 160, cell.frame.origin.y +8, cell.frame.size.width -180,cell.frame.size.height -10)];
        [_taxApplicableSwitch addTarget:self action:@selector(switchHasChangedValue:) forControlEvents:UIControlEventValueChanged];
        _taxApplicableSwitch.onTintColor = [UIUtils getTintColor];
        _taxApplicableSwitch.tag = kTaxSwitchtag;
    }
}

- (void) setTitletextView:(UITableViewCell*)cell
{
    if (_titletextView == nil)
    {
        _titletextView = [[UITextView alloc] initWithFrame:CGRectMake(cell.frame.origin.x +5, cell.frame.origin.y + 5, cell.bounds.size.width -30,38.0)];
        _titletextView.backgroundColor = [UIColor clearColor];
        _titletextView.delegate = self;
        _titletextView.scrollEnabled = NO;
        [_titletextView setFont:[UIFont boldSystemFontOfSize:14.0]];
        _titletextView.text = [_itemDict objectForKey:@"Title"];
        _titletextView.tag = kTitletextViewtag;
    }
}
- (void) settextView:(UITableViewCell*)cell
{
    if (_notetextView == nil)
    {
        _notetextView = [[UITextView alloc] initWithFrame:CGRectMake(cell.frame.origin.x +5, cell.frame.origin.y + 10, cell.bounds.size.width -30,180.0)];
        _notetextView.backgroundColor = [UIColor clearColor];
        _notetextView.delegate = self;
        [_notetextView setFont:[UIFont boldSystemFontOfSize:14.0]];
        _notetextView.text = [_itemDict objectForKey:@"Note"];
        _notetextView.tag = kTextViewTag;
    }
}

#pragma mark --
#pragma mark TextFiled and textView Delgate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSInteger textlength = [[textView text] length] - range.length + text.length ;

    if (!_product)
    {
        BOOL isEmpty =  (( _quantTextField.text.length == 0 ) || (_rateTextField.text.length == 0));
        
        if(textView.tag == kTitletextViewtag)
        {
            self.navigationItem.rightBarButtonItem.title =  (isEmpty || (_notetextView.text.length < 1) || (textlength < 1)) ? @"Save" : @"Add";
        }
        else if(textView.tag == kTextViewTag)
        {
            self.navigationItem.rightBarButtonItem.title =  (isEmpty || (_titletextView.text.length < 1) || (textlength < 1)) ? @"Save" : @"Add";
        }
    }
    
    if ((textlength > 30) && textView.tag == kTitletextViewtag)
    {
        [UIUtils messageAlert:@"Please enter title within 30 characters." title:kApplicationName delegate:nil];
        return NO;
    }
    
    if([text isEqualToString:@"\n"])
        return NO;
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    textView.keyboardType = UIKeyboardTypeDefault;
	textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
	
    [_tableView setContentOffset:CGPointMake(0, textView.frame.origin.y+8) animated:YES];
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
    
}

- (void) setUpScrollViewOffSet:(UITextField*)tField
{
    [_tableView setContentOffset:CGPointMake(0, tField.frame.origin.y + 280) animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setUpScrollViewOffSet:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 0:
        {
            //NSNumber* integerValue = [NSNumber numberWithInteger:[textField.text integerValue]];
            [_itemDict setObject:textField.text forKey:@"Quantity"];
        }
            break;
            
        case 1:
        {
            NSNumber* integerValue = [NSNumber numberWithDouble:[textField.text doubleValue]];
            
            [_itemDict setObject:integerValue forKey:@"Rate"];
        }
            break;
    }
    
    double amount = [self getCalculatedAmount];
    
    _amountTextField.text = [NSString stringWithFormat:@"$%.02f",amount];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger txtlength = [[textField text] length] - range.length + string.length ;

    if (!_product)
    {
        BOOL isEmpty =  (( _notetextView.text.length == 0 ) || (_titletextView.text.length == 0));
        
        if(textField.tag == kQuantTaxField)
        {
            self.navigationItem.rightBarButtonItem.title =  (isEmpty || (_rateTextField.text.length < 1) || (txtlength < 1)) ? @"Save" : @"Add";
        }
        else if(textField.tag == kRatetextField)
        {
            self.navigationItem.rightBarButtonItem.title =  (isEmpty || (_quantTextField.text.length < 1) || (txtlength < 1)) ? @"Save" : @"Add";
        }
    }
    
    if ((txtlength > 7) && ((textField.tag == 0)|| (textField.tag == 1)))
    {
        [UIUtils messageAlert:@"You can not enter more than 7 digits." title:kApplicationName delegate:nil];
        return NO;
    }

    [textField addTarget:self action:@selector(yourTextFieldChanged:) forControlEvents:UIControlEventEditingChanged];
	
	if(textField.tag == kRatetextField)
	{
		NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
		NSString *decimalSymbol = [formatter decimalSeparator];
		
		NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
		
		NSString *expression = [NSString stringWithFormat:@"^([0-9]+)?(\\%@([0-9]{1,2})?)?$", decimalSymbol];
		
		NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
																			   options:NSRegularExpressionCaseInsensitive
																				 error:nil];
		NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
															options:0
															  range:NSMakeRange(0, [newString length])];
		if (numberOfMatches == 0) {
			return NO;
		}
	}

    return YES;
}



- (void)yourTextFieldChanged:(UITextField *)textField
{
    switch (textField.tag)
    {
        case 0:
        {
            //NSNumber* integerValue = [NSNumber numberWithInteger:[textField.text integerValue]];
            [_itemDict setObject:textField.text forKey:@"Quantity"];
        }
            break;
            
        case 1:
        {
            NSNumber* integerValue = [NSNumber numberWithDouble:[textField.text doubleValue]];
            
            [_itemDict setObject:integerValue forKey:@"Rate"];
        }
            break;
    }
    
    double amount = [self getCalculatedAmount];
    
    _amountTextField.text = [NSString stringWithFormat:@"$%.02f",amount];
    
}


- (BOOL) textFieldShouldClear:(UITextField*)textField
{
    self.navigationItem.rightBarButtonItem.title = @"Save";
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"RETURN BUTTON PRESSED in the product description");
    
    //    [textField resignFirstResponder];
    return YES;
}

- (double) getCalculatedAmount
{
    NSNumber* quntitystr = [NSNumber numberWithFloat:[[_itemDict objectForKey:@"Quantity"] floatValue]];
    NSNumber* rateStr = [_itemDict objectForKey:@"Rate"];
    
    CGFloat quant =  [quntitystr floatValue];
    double rate = [rateStr doubleValue];
    
    double amount = quant*rate;
    [_itemDict setObject:[NSString stringWithFormat:@"%.02f",amount] forKey:@"Amount"];
    
    return amount;
}

#pragma mark -
#pragma mark Notification

- (void) keyboardWillShow:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _tableView.frame = CGRectMake(0, 0, 320, kiPhone5ScrollFrameHeightWithKeyboard+50);
        }
        else
        {
            _tableView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard);
        }
    }
}

- (void) keyboardWillHide:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _tableView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithoutKeyboard);
        }
        else
        {
            _tableView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithoutKeyboard);
        }
    }
    [_tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

#pragma mark --
#pragma mark segmentAction

- (void) switchHasChangedValue:(UISwitch*)switchObj
{
    NSNumber* taxvalue = [NSNumber numberWithBool:switchObj.isOn];
    [_itemDict setObject:taxvalue forKey:@"TaxAllow"];
}

#pragma mark --
#pragma mark  Btn Action

- (void) cancelBtnAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) updateBtnAction
{
    NSLog(@"Update button tapped......");
    for (NSInteger row = 0; row < 3; ++row)
    {
        NSIndexPath* path = [NSIndexPath indexPathForItem:row inSection:2];
        EditableDetailCell* cell = (EditableDetailCell*)[_tableView cellForRowAtIndexPath:path];
        [cell.textField resignFirstResponder];
    }
    
    [_itemDict setObject:_notetextView.text forKey:@"Note"];
    
    NSString* title = _titletextView.text;
    if (title.length > 0)
    {
        [_itemDict setObject:_titletextView.text forKey:@"Title"];
    }
    else
    {
        [UIUtils messageAlert:@"Please enter title!" title:kApplicationName delegate:nil];
        return;
    }
    

    //needs to update, not save
    if(self.isProductItem)
    {
        NSLog(@"about to update invoice item");

        [_gAppData updateInvoiceItem:_itemDict];
    }
    
    if (self.isUpdateEstimate) {
        NSLog(@"Update the estimate invoice.....");
        [_gAppData updateEstimateItem:_itemDict];
        //[self addUpdateEstimateInvoice];
    }
    
    if (self.presentingViewController)
		[self dismissViewControllerAnimated:YES completion:nil];
	else
		[self.navigationController popViewControllerAnimated:YES];
}

- (void) updateProductBtnAction
{
    NSLog(@"Update product btn action ......");
    
    for (NSInteger row = 0; row < 3; ++row)
    {
        NSIndexPath* path = [NSIndexPath indexPathForItem:row inSection:2];
        EditableDetailCell* cell = (EditableDetailCell*)[_tableView cellForRowAtIndexPath:path];
        [cell.textField resignFirstResponder];
    }
    
    [_itemDict setObject:_notetextView.text forKey:@"Note"];
    
    NSString* title = _titletextView.text;
    if (title.length > 0)
    {
        [_itemDict setObject:_titletextView.text forKey:@"Title"];
    }
    else
    {
        [UIUtils messageAlert:@"Please enter title!" title:kApplicationName delegate:nil];
        return;
    }
    
    //needs to update, not save
    if(self.isProductItem && !self.isTemplate && !self.isEstimate && !self.isUpdateInvoice)
    {
        NSLog(@"The object that is being passed through is a product that will be updated.......");
        
        [_gAppData updateProductItem:_itemDict];
    }
    
    if (self.isUpdateInvoice)
    {
        NSLog(@"Updating the invoice Item");
        
        [_gAppData updateInvoiceItem:_itemDict];
        
    }
    
    if (self.isUpdateEstimate)
    {
        [_gAppData updateEstimateItem:_itemDict];
        
        NSLog(@"the item dictionary used to update is: %@", _itemDict);
    }
    
    
    if (self.isTemplate)
    {
        // this is when you are selecting a product that already exists within the product screen from an already existing template
        // this does not run if you go to here from a new template / self.isTemplate = NO for this for some reason
        
        NSLog(@"The template item was saved!!! or it will save eventually");
        //TemplatesViewController *viewC = [[TemplatesViewController alloc] initWithNibName:@"TemplatesView" bundle:nil];
        
        if (self.presentingViewController)
            [self dismissViewControllerAnimated:YES completion:nil];
        else
            [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (self.presentingViewController)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

// need to update this so that new products from estimate created products
- (void) saveEstimateProductBtnAction
{
    [_itemDict setObject:_titletextView.text forKey:@"Title"];
    [_itemDict setObject:_notetextView.text forKey:@"Note"];
    

    NSLog(@"The item dictionary is for the estimate: %@", _itemDict);
    
    [_gAppData addNewEstimateItem:_itemDict];
    
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) updateTemplateItemBtn
{
    NSLog(@"Going to update an already existing Template Item");
    // need to look for existing template item and updating it with the updated information

    for (NSInteger row = 0; row < 3; ++row)
    {
        NSIndexPath* path = [NSIndexPath indexPathForItem:row inSection:2];
        EditableDetailCell* cell = (EditableDetailCell*)[_tableView cellForRowAtIndexPath:path];
        [cell.textField resignFirstResponder];
    }
    
    NSLog(@"for key object");
    [_itemDict setObject:_notetextView.text forKey:@"Note"];
    
    NSString* title = _titletextView.text;
    if (title.length > 0)
    {
        NSLog(@"setting the title");
        [_itemDict setObject:_titletextView.text forKey:@"Title"];
    }
    else
    {
        [UIUtils messageAlert:@"Please enter title!" title:kApplicationName delegate:nil];
        return;
    }
    
    NSLog(@"before going to core data, the item dict is: %@", _itemDict);
    
    NSDictionary *productDictionary = @{
                                       @"item_title" : [_itemDict objectForKey:@"Title"],
                                       @"item_desc" : [_itemDict objectForKey:@"Note"],
                                       @"item_quantity" : [_itemDict objectForKey:@"Quantity"],
                                       @"item_rate" : [_itemDict objectForKey:@"Rate"],
                                       @"item_amount" : [_itemDict objectForKey:@"Amount"],
                                       @"item_teplateID" : [_itemDict objectForKey:@"templateId"],
                                       @"item_taxAllow" : [_itemDict objectForKey:@"TaxAllow"],
                                       };
    
    NSMutableDictionary* mutatedProductDictionary = [NSMutableDictionary dictionaryWithDictionary:productDictionary];
    
    [_gAppData updateTemplateItem:_product withUpdateTemplateItem:mutatedProductDictionary];
    //[_gAppData insertNewItemFromTempalte:<#(NSDictionary *)#> oldItem:<#(Item *)#> newTemplateItem:<#(BOOL)#>]
    
    NSLog(@"finished updating the template item");
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void) addUpdateEstimateInvoice
{
    NSLog(@"Add update estimate invoice");
    
    if (self.isUpdateEstimate)
    {
        NSString* itemIdStr = [_itemDict objectForKey:@"ItemID"];
        NSArray* estimateitemArray = [_gAppData getEstimateItemWithID:itemIdStr];
        
        if (estimateitemArray.count > 0)
            [_gAppData updateEstimateItem:_itemDict];
        else
            [_gAppData addNewEstimateItem:_itemDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveditemNotification object:itemIdStr];
    }
    
    if (self.isAddEstimate)
    {
        NSLog(@"about to add to the estimate");
        
        NSString* newEstimate = [self getCurrentDateTimeAsNSString];
        [_itemDict setObject:newEstimate forKey:@"ItemID"];
        
        [_gAppData addNewEstimateItem:_itemDict];
        
        [self.navigationController popViewControllerAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (self.isUpdateInvoice)
    {
        NSLog(@"Update Invoice if statement");
        NSString* itemIdStr = [_itemDict objectForKey:@"ItemID"];
        NSArray* estimateitemArray = [_gAppData getInvoiceItemWithID:itemIdStr];
        
        if (estimateitemArray.count > 0)
            [_gAppData updateInvoiceItem:_itemDict];
        else
            [_gAppData addNewInvoiceItem:_itemDict];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveditemNotification object:itemIdStr];
    }

    if (self.isAddInvoice)
    {
        NSLog(@"add Invoice if statement");
        NSString* newEstimate = @"999999999";
        
        [_itemDict setObject:newEstimate forKey:@"ItemID"];
        
        [_gAppData addNewInvoiceItem:_itemDict];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSavedInvoiceItemNotification object:newEstimate];
    }
}



- (void) saveBtnAction
{
    NSLog(@"SAVE BUTTON TAPPED");
    
    for (NSInteger row = 0; row < 3; ++row)
    {
        NSIndexPath* path = [NSIndexPath indexPathForItem:row inSection:2];
        EditableDetailCell* cell = (EditableDetailCell*)[_tableView cellForRowAtIndexPath:path];
        [cell.textField resignFirstResponder];
    }
    
    NSString* itemIdStr = [self getCurrentDateTimeAsNSString];
    [_itemDict setObject:itemIdStr forKey:@"ItemID"];
    [_itemDict setObject:_notetextView.text forKey:@"Note"];
    
    NSString* title = _titletextView.text;
    if (title.length > 0)
    {
        [_itemDict setObject:_titletextView.text forKey:@"Title"];
    }
    else
    {
        [UIUtils messageAlert:@"Please enter title!" title:kApplicationName delegate:nil];
        return;
    }
    
    if ([self.product isMemberOfClass:[Templates class]])
    {
        [_itemDict setObject:[self.product template_id] forKey:@"templateId"];
    }
    
    if (self.isAddEstimate)
    {
        [_gAppData addNewEstimateItem:_itemDict];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSaveditemNotification object:itemIdStr];
    }
    
    if (self.isAddInvoice)
    {
        [_gAppData addNewInvoiceItem:_itemDict];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSavedInvoiceItemNotification object:itemIdStr];
    }
    
    if(self.isProductItem && !self.isEstimate)
    {
        NSLog(@"The template dictionary is: %@", _itemDict);
        NSLog(@"the product is: %@", _product);
        
        //[_itemDict setObject:nil forKey:@"templateId"];
        
        //[_gAppData addNewItem:_itemDict];
        [_gAppData addNewItem:_itemDict oldItem:_product];
        NSLog(@"A NEW ITEM HAS BEEN INSERTED----------- ");
    }
    
    NSLog(@"THe view controllers are: %@", self.navigationController.viewControllers);
    
    
    
    if (self.isProductItem && !self.isEstimate)
    {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
    } else
    {
        if (self.fromClientScreen)
        {
            NSLog(@"Sending you to object at index 2");
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
        }
        else{
            NSLog(@"Sending you to object at index 1");
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
        }
    }
}

-(void) saveInvoiceItemAction
{
    NSLog(@"SAVE BUTTON TAPPED for the invoice item");
    
    for (NSInteger row = 0; row < 3; ++row)
    {
        NSIndexPath* path = [NSIndexPath indexPathForItem:row inSection:2];
        EditableDetailCell* cell = (EditableDetailCell*)[_tableView cellForRowAtIndexPath:path];
        [cell.textField resignFirstResponder];
    }
    
    NSString* itemIdStr = [self getCurrentDateTimeAsNSString];
    [_itemDict setObject:itemIdStr forKey:@"ItemID"];
    [_itemDict setObject:_notetextView.text forKey:@"Note"];
    
    NSString* title = _titletextView.text;
    if (title.length > 0)
    {
        [_itemDict setObject:_titletextView.text forKey:@"Title"];
    }
    else
    {
        [UIUtils messageAlert:@"Please enter title!" title:kApplicationName delegate:nil];
        return;
    }
    
    [_gAppData addNewInvoiceItem:_itemDict]; // doesn't crash if all info is filled out?
    
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)templateAdded
{
    NSString* title = _titletextView.text;
    if (title.length > 0)
    {
        NSLog(@"Title is: %@", _titletextView.text);
    }
    else
    {
        [UIUtils messageAlert:@"Please enter title!" title:kApplicationName delegate:nil];
        return;
    }
    
    NSNumber* tax = [NSNumber numberWithBool:_taxApplicableSwitch.on];

    NSString* templateItemID = [self getCurrentDateTimeAsNSString];
    [_itemDict setObject:templateItemID forKey:@"ItemID"];
    
    double rate = [_rateTextField.text doubleValue];
    double quantity = [_quantTextField.text doubleValue];
    double amount = rate * quantity;
    
    NSString* amountString = [NSString stringWithFormat:@"%f", amount];
    
    NSDictionary *templateItemDict = @{
                                @"Title" : _titletextView.text,
                                @"Note" : _notetextView.text,
                                @"Quantity" : _quantTextField.text,
                                @"Rate" : _rateTextField.text,
                                @"Amount" : amountString,
                                @"templateId" : @"888888888",
                                @"TaxAllow" : tax,
                                @"isTemplateItem" : [NSNumber numberWithBool:NO],
                                };
    
    [_gAppData insertNewItemFromTempalte:templateItemDict oldItem:_product newTemplateItem:YES];
    
     NSLog(@"The view controllers are: %@", self.navigationController.viewControllers);
    
    if (self.creatingTemplateFromEstimate)
    {
        //since it comes from the estimate, it needs to go back to just the create template, not to the start
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:3] animated:YES];
    }
    else
    {
        NSLog(@"saved the new template item");
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }
}

#pragma mark --
#pragma mark unique Id method

// Method to get unique name for images

- (NSString*) getCurrentDateTimeAsNSString
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyyMMddHHmmss"];
    
    NSDate *now = [NSDate date];
    NSString *retStr = [format stringFromDate:now];
    
    return retStr;
}

@end