//
//  AddNewMemberViewController.m
//  BidAbout
//
//  Created by Rupesh Kumar  on 16/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "AddNewMemberViewController.h"

#define kScrollWidth 320
#define kiPhoneScrollFrameHeightWithoutKeyboard 660
#define kiPhone5ScrollFrameHeightWithoutKeyboard 548

#define kiPhone5ScrollFrameHeightWithKeyboard 246
#define kiPhoneScrollFrameHeightWithKeyboard 220

#define kscrollContentSizeDiffWithoutKeyBoard 240
#define kscrollContentSizeDiffWithKeyBoard 500

#define kTextFieldDiffrence 80

@interface AddNewMemberViewController ()

@end

@implementation AddNewMemberViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    // add navigations items
    [self addnavigationItems];
    
    // SetUp controls with custom keyboard
    [self setUpKeyboardControls];
    
    // Intital setUp
    [self initialSetUp];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createMemberSucess:) name:kMemberCreateServiceSucess object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void) viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark privateMethods

 - (void) addnavigationItems
{
    self.title = @"Member";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
    UIBarButtonItem* saveBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnAction)];
	self.navigationItem.rightBarButtonItem = saveBtn;
    self.navigationItem.leftBarButtonItem = cancelBtn;
}

- (void) initialSetUp
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [_adminBtn setSelected:YES];
    [_quotesYesBtn setSelected: YES];
    
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        else
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
    }
    else
        _scrollView.contentSize = CGSizeMake(768,1024);
}

#pragma mark -
#pragma mark ButtonAction

- (IBAction) roleCheckedBtnAction:(id)sender
{
    UIButton *selectedButton  = (UIButton*)sender;
    
    if (selectedButton.tag == 13)
    {
        [_adminBtn setSelected:YES];
        [_memberBtn setSelected:NO];
    }
    else
    {
        [_memberBtn setSelected:YES];
        [_adminBtn setSelected:NO];
    }
}

- (IBAction) quotesCheckedBtnAction:(id)sender
{
    UIButton *selectedButton  = (UIButton*)sender;
    
    if (selectedButton.tag == 15)
    {
        [_quotesYesBtn setSelected:YES];
        [_quotesNoBtn setSelected:NO];
    }
    else
    {
        [_quotesNoBtn setSelected:YES];
        [_quotesYesBtn setSelected:NO];
    }
}

- (void) saveBtnAction
{
    if (![self isAnyThingEmpty])
    {
        BOOL isNotValid = [UIUtils validateEmail:_memberEmailTxtField.text];
        if (isNotValid)
        {
            [_memberEmailTxtField becomeFirstResponder];
            return;
        }
        
        NSString* roleStr = (_adminBtn.selected) ? @"Administrator":@"Member";
        NSString* allowQuotesStr = (_quotesYesBtn.selected) ? @"true":@"false";
        
        NSArray* objectsArray = [NSArray arrayWithObjects:_firstNameTxtField.text,_LastNameTxtField.text,_memberCompanyTxtField.text,_memberPhoneTxtField.text,_memberAddress1TxtField.text,_memberAddress2TxtField.text,_memberCityTxtField.text,_memberStateTxtField.text,_memberZipTxtField.text,_memberEmailTxtField.text,roleStr,allowQuotesStr,_passwordTxtField.text,_confirmPasswrdTxtField.text,nil];
        
        NSArray* keyArray = [NSArray arrayWithObjects:@"MemberFirstName",@"MemberLastName",@"MemberCompany",@"MemberPhone",@"MemberAddress1",@"MemberAddress2",@"MemberCity",@"MemberState",@"MemberZip",@"MemberEmail",@"MemberRole",@"MemberAllowQuotes",@"Password",@"ConfirmPassword",nil];
        
        NSDictionary* dict = [[NSDictionary alloc] initWithObjects:objectsArray forKeys:keyArray];
        
        NSData* data = [UIUtils createJSONToPost:dict];
        [_gAppData sendRequestForCreateMember:data];
    }
}

- (IBAction) cancelBtnAction
{
    [self dismissModalViewControllerAnimated:YES];
    [_gAppData sendRequestForMemberList];
}

#pragma mark -
#pragma mark TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([_keyboardControls.textFieldsArray containsObject:textField])
        _keyboardControls.activeTextField = textField;
    
    [self setUpScrollViewOffSet:textField];
}

- (BOOL) textFieldShouldReturn:(UITextField*)textField
{
	[textField resignFirstResponder];
    
    [self handleKeyboardHideShow];
	return YES;
}

#pragma mark -
#pragma mark Private Methods

- (void) setUpScrollViewOffSet:(UITextField *)tField
{
    NSLog(@"%f",tField.frame.origin.y);
    [_scrollView setContentOffset:CGPointMake(0, tField.frame.origin.y-kTextFieldDiffrence) animated:YES];
    NSLog(@"%f",tField.frame.origin.y - kTextFieldDiffrence);
}

- (BOOL) isAnyThingEmpty
{
    NSArray* textFieldArray = [NSArray arrayWithObjects:_firstNameTxtField,_LastNameTxtField,_memberCompanyTxtField,_memberPhoneTxtField,_memberAddress1TxtField,_memberAddress2TxtField,_memberCityTxtField,_memberStateTxtField,_memberZipTxtField,_memberEmailTxtField,_passwordTxtField,_confirmPasswrdTxtField,nil];
    
    BOOL isEmpty = NO;
    
    for (UITextField* textField in textFieldArray)
        {
            if ([textField.text isEqualToString:@""])
            {
                [UIUtils messageAlert:[NSString stringWithFormat:@"%@ %@",textField.placeholder,kBlankFieldMessage] title:kMessage delegate:nil];
                isEmpty = YES;
                break;
            }
        }
    
    return isEmpty;
}

#pragma mark -
#pragma mark Notification

- (void)keyboardWillShow:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard+50);
        }
    }
    else
        _scrollView.frame = CGRectMake(0,0,768,1004);
}

- (void) createMemberSucess:(NSNotification *)notification
{
    [UIUtils messageAlert:@"Member successfully created!" title:@"Successful" delegate:nil];
    [self dismissModalViewControllerAnimated:YES];
}

- (void) setUpKeyboardControls
{
    _keyboardControls = [[KeyboardControls alloc] init];
    _keyboardControls.delegate = self;
    
    // Add textFields in order
    _keyboardControls.textFieldsArray = [NSArray arrayWithObjects:_firstNameTxtField,_LastNameTxtField,_memberCompanyTxtField,_memberPhoneTxtField,_memberAddress1TxtField,_memberAddress2TxtField,_memberCityTxtField,_memberStateTxtField,_memberZipTxtField,_memberEmailTxtField,_passwordTxtField,_confirmPasswrdTxtField,nil];
    
    _keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    _keyboardControls.previousNextTintColor = [UIColor blackColor];
    _keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    _keyboardControls.previousTitle = @"Previous";
    _keyboardControls.nextTitle = @"Next";
    
    for (id textField in _keyboardControls.textFieldsArray)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = _keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
    }
}

- (void) dismissKeyboard
{
    [_firstNameTxtField resignFirstResponder];
    [_LastNameTxtField resignFirstResponder];
    [_memberCompanyTxtField resignFirstResponder];
    [_memberPhoneTxtField resignFirstResponder];
    [_memberAddress1TxtField resignFirstResponder];
    [_memberAddress2TxtField resignFirstResponder];
    [_memberCityTxtField resignFirstResponder];
    [_memberStateTxtField resignFirstResponder];
    [_memberZipTxtField resignFirstResponder];
    [_memberEmailTxtField resignFirstResponder];
    [_passwordTxtField resignFirstResponder];
    [_confirmPasswrdTxtField resignFirstResponder];
    
    [self handleKeyboardHideShow];
}

#pragma mark -
#pragma mark KeyBoardControlDelegate

- (void)keyboardControlsDonePressed:(KeyboardControls*)controls
{
    [controls.activeTextField resignFirstResponder];
    [self handleKeyboardHideShow];
}

- (void)keyboardControlsPreviousNextPressed:(KeyboardControls*)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self setUpScrollViewOffSet:textField];
}

- (void) handleKeyboardHideShow
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
    }
    else
        _scrollView.frame = CGRectMake(0,0,768,1004);
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}


@end
