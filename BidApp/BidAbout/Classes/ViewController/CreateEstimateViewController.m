//
//  CreateEstimateViewController.m
//  BidAbout
//
//  Created by Waseem on 04/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "CreateEstimateViewController.h"
#import "AddPhotoViewController.h"
#import "EditCustomerViewController.h"
#import "EstimateItem.h"
#import "Item.h"
#import "Estimate.h"
#import "ScheduleViewController.h"
#import "PDFDisplayViewController.h"
#import "PDFRenderer.h"
#import "ExpensceListViewController.h"
#import "Customers.h"
#import "ItemDescriptionViewController.h"
#import "CompanySetting.h"


#define kButtonPoint ([UIUtils isiPhone] ? 30: 80)
#define kButtonGap ([UIUtils isiPhone] ? 70: 140)
#define kNumberOfFixedSection 6
#define kCellLabelheader 399

@interface CreateEstimateViewController ()

@end

@implementation CreateEstimateViewController
{
    NSString *currentDate;
}


#pragma mark--ViewLife Cycle.

- (void) viewDidLoad
{
    [super viewDidLoad];

    NSLog(@"Create Estimate View Controller");
    
    self.title = (self.estimateObj)? [NSString stringWithFormat:@"Proposal #%ld",(long)[self.estimateObj.estimate_number integerValue]]: [NSString stringWithFormat:@"Proposal #%@",_gAppPrefData.estimatelastID];
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;

    _tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideDatePicker)];
    _tapgesture.cancelsTouchesInView = YES;

    _tableView.editing = /*(self.estimateObj) ? NO:*/YES;
    _tableView.allowsSelectionDuringEditing = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savedItemData:) name:kSaveditemNotification object:nil];
    
    [self initialSetUp];

    // Cancel bar button item.
//    if (!self.estimateObj)
//    {
        UIBarButtonItem* cancelBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelBtnAction)];
        self.navigationItem.leftBarButtonItem = cancelBarBtn;
        
        UIBarButtonItem* nextBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(nextBtnAction)];
        self.navigationItem.rightBarButtonItem = nextBarBtn;
    
    if (!self.estimateObj)
    {
        [_datePicker setDate:[NSDate date]];
        [self selectdateFromPicker:_datePicker];
    }
    
        if (self.customerObj)
        {
            [_estimateDict setObject:self.customerObj.customer_id forKey:@"Client"];
        }
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savedItemData:) name:kSaveditemNotification object:nil];
    [self initialSetUp];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"LLL-dd-yyyy"];
    currentDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSLog(@"The comment that was added is on view will appear is: %@", self.comment);
    
    [_tableView reloadData];
    [self setTaxdata];
}

#pragma mark - TemplateAdded

//- (void)templateAdded:(Templates *)template completion:(void (^)(void))block
//{
//    block();
//    [self saveItems:template.tempProductList];
//}

//- (void)saveItems:(NSString *)itemsSeparatedByCommas
//{
//    NSString* itemIdStr = itemsSeparatedByCommas;
//    NSArray *itemIds = [itemIdStr componentsSeparatedByString:@","];
//    NSMutableArray *itemList = [[NSMutableArray alloc] init];
//    
//    for (NSString *string in itemIds) {
//        [itemList addObject: [[_gAppData getItemWithID:string] firstObject]];
//    }
//    
//    if ([itemList count] > 0)
//    {
//        for(NSInteger i = 0; i < itemList.count; i++)
//        {
//            BOOL isExists = NO;
//            EstimateItem* item = [itemList objectAtIndex:i];
//            for (NSInteger i = 0; i < [_itemArray count]; i++)
//            {
//                EstimateItem* itemObj = [_itemArray objectAtIndex:i];
//                if ([itemObj.item_id isEqualToString:item.item_id])
//                {
//                    [_itemArray replaceObjectAtIndex:i withObject:item];
//                    isExists = YES;
//                    break;
//                }
//            }
//            
//            if (!isExists)
//                [_itemArray addObject:item];
//        }
//    }
//    
//    NSString* itemString = @"";
//    for (EstimateItem* item in _itemArray)
//    {
//        itemString = [itemString stringByAppendingString:[NSString stringWithFormat:@"%@,",item.item_id]];
//    }
//    
//    //[_estimateDict setObject:itemString forKey:@"Items"];
//    [_tableView reloadData];
//}

- (void)addItemViewController:(ClientViewController *)controller didFinishEnteringItem:(Customers *)item
{
    NSLog(@"This was returned from ViewControllerB %@",item);
    
    self.customerObj = item;
    self.haveClient = YES;

    [self initialSetUp];
    
}

- (void) addComment:(NSString*)commentString
{
    NSLog(@"the comment that is being passed back is: %@", commentString);
    self.comment = commentString;
}

- (void) initialSetUp
{
    NSLog(@"The initial set up");
    NSLog(@"The client id is: %@", self.estimateObj.estimate_ClientId);

    _estimateDict = [[NSMutableDictionary alloc] initWithCapacity:12];
    _itemArray = [[NSMutableArray alloc] init];
    _photoList = [[NSMutableArray alloc] init];
    
    if (self.estimateObj)
    {
        NSLog(@"There is an estimate object present %@", self.estimateObj);
        
        //invoice number.
        [_estimateDict setObject:self.estimateObj.estimate_id forKey:@"EstimateNumber"];
        [_estimateDict setObject:self.estimateObj.estimate_id forKey:@"EstimateId"];
        [_estimateDict setObject:self.estimateObj.estimate_comment forKey:@"Comment"];
        [_estimateDict setObject:self.estimateObj.estimate_date forKey:@"EstimateDate"];
        [_estimateDict setObject:self.estimateObj.estimate_item forKey:@"Items"];
        [_estimateDict setObject:self.estimateObj.estimate_ClientId forKey:@"Client"];
        [_estimateDict setObject:self.estimateObj.accepted forKey:@"Accepted"];
        
        [_estimateDict setObject:self.estimateObj.estimate_pdf_quantity forKey:@"Quantity"];
        [_estimateDict setObject:self.estimateObj.estimate_pdf_rate forKey:@"Rate"];
        [_estimateDict setObject:self.estimateObj.estimate_pdf_subtotal forKey:@"Subtotal"];
        [_estimateDict setObject:self.estimateObj.estimate_pdf_total forKey:@"Total"];
        
        if (!self.pdfOptions) {
            NSLog(@"running PDF Options");
            
            self.pdfQuantity = [self.estimateObj.estimate_pdf_quantity boolValue];
            self.pdfRate = [self.estimateObj.estimate_pdf_rate boolValue];
            self.pdfSubtotal = [self.estimateObj.estimate_pdf_subtotal boolValue];
            self.pdfTotal = [self.estimateObj.estimate_pdf_total boolValue];
        }
        
        [self setTaxdata];
        
//        if (self.estimateObj.estimate_item.length > 0)
//        {
//            //_itemArray = [NSMutableArray arrayWithArray:[_gAppData getEstimateItemWithID:self.estimateObj.estimate_item]];
//            _itemArray = [NSMutableArray arrayWithArray:[_gAppData getEstimateItemWithID:[self.estimateObj.estimate_id stringValue]]];
//            NSLog(@"the item array is: %@", _itemArray);
//        }
        
        //_itemArray = [NSMutableArray arrayWithArray:[_gAppData getEstimateItemWithID:[self.estimateObj.estimate_id stringValue]]];
        
        NSArray* estimateItems = [_gAppData getEstimateItemWithID:self.estimateObj.estimate_id.stringValue];
        if (estimateItems.count > 0)
        {
            [_itemArray addObjectsFromArray:estimateItems];
        }
        
        NSString* str = self.estimateObj.estimate_photo;
        if (str.length > 0)
            _photoList = [NSMutableArray arrayWithArray:[str componentsSeparatedByString:@","]];
        
        if (self.estimateObj.estimate_ClientId.length > 0)
        {
            NSArray* customeArray = [_gAppData getCustomerDetails:NO andCustomerId:self.estimateObj.estimate_ClientId];
            if (customeArray.count > 0)
            {
                self.customerObj = [customeArray objectAtIndex:0];
                self.haveClient = YES;
            }
        }
        
        NSLog(@"The item array retrieved is: %@", _itemArray);
    }
    
    NSArray* currentEstimateItems = [NSArray arrayWithArray:[_gAppData getEstimateItemsFromCurrentEstimate]];
    
    NSLog(@"The items that are current are: %@", currentEstimateItems);
    
    if (currentEstimateItems.count > 0)
    {
        [_itemArray addObjectsFromArray:currentEstimateItems];
    }
    
    if (self.comment.length > 0)
    {
        NSLog(@"changing the comment: %@", self.comment);
        [_estimateDict setObject:self.comment forKey:@"Comment"];
    }
    
    [_tableView reloadData];
}


- (void) setTaxdata
{
//    if (self.estimateObj)
//        [_estimateDict setObject:self.estimateObj.estimate_tax forKey:@"Tax"];
//    else
//        [_estimateDict setObject:_gAppPrefData.currentTax forKey:@"Tax"];
	if(_gAppPrefData.isDefualttaxOn)
        [_estimateDict setObject:_gAppPrefData.currentTax forKey:@"Tax"];
	else
		[_estimateDict setObject:[NSNumber numberWithDouble:kCurrentTax] forKey:@"Tax"];

}

#pragma mark --
#pragma mark Button Action

- (void) cancelBtnAction
{
    NSLog(@"Cancel button action"); // when there is a current estimate that is being worked on
    
	UIActionSheet* actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Don't Save",@"Cancel", nil];
	actionsheet.tag = 1001;
	actionsheet.destructiveButtonIndex = 2;
	[actionsheet showInView:self.view];
}

//need to change it so that the title of the last two buttons depends on which section you are on the estimates screen
- (void) nextBtnAction
{
	UIActionSheet* actionsheet = nil;
    
    NSString* moveEstimate1;
    NSString* moveEstimate2;
    
    if ([self.acceptedInteger integerValue] == 1)
    {
        moveEstimate1 = @"Pending Proposal";
        moveEstimate2 = @"Reject Proposal";
    }
    
    if ([self.acceptedInteger integerValue] == 2)
    {
        moveEstimate1 = @"Accept Proposal";
        moveEstimate2 = @"Reject Proposal";
    }
    
    if ([self.acceptedInteger integerValue] == 3)
    {
        moveEstimate1 = @"Pending Proposal";
        moveEstimate2 = @"Accept Proposal";
    }
    
//    if (self.estimateObj)
//       actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Preview", nil];
//    else
    
    
    if (_isConvert)
    {
        actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Preview",@"Email",@"Print",@"Convert to Invoice", moveEstimate1, moveEstimate2, nil];
    }
    else
        actionsheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Hide Actions" destructiveButtonTitle:nil otherButtonTitles:@"Save",@"Preview",@"Email",@"Print", nil];
    
	actionsheet.tag = 1002;
	[actionsheet showInView:self.view];
}

#pragma mark -- Action Sheet Delegate

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(actionSheet.tag == 1001)
		[self cancelActionSheatAction:buttonIndex];
	else if(actionSheet.tag == 1002)
    	[self nextActionSheatAction:buttonIndex title:[actionSheet buttonTitleAtIndex:buttonIndex]];
}

- (void) cancelActionSheatAction:(NSInteger) btnIndex
{
	switch (btnIndex)
	{
		case 0:
            [self saveEstimate:_estimateDict popViewController:YES];
			break;
		case 1:
            [self.navigationController popViewControllerAnimated:YES];
			break;
		case 2:
            NSLog(@"Don't Save clicked");
			break;
			
		default:
			break;
	}
}

- (void) nextActionSheatAction:(NSInteger) btnIndex title:(NSString *)title
{
    NSLog(@"the estimate dict is: %@", _estimateDict);
    NSLog(@"the estimate %f", [self getSubtotal]);
    
	switch (btnIndex)
	{
		case 0:
			[self saveEstimate:_estimateDict popViewController:YES];
			break;
		case 1:
			[self createPDF:YES isprinitng:NO];
			break;
		case 2:
			[self sendMailButoonAction];
			break;
		case 3:
			[self createPDF:NO isprinitng:YES];
            break;
        case 4:
        {
            if ([title isEqualToString:@"Convert to Invoice"])
            {                
                NSMutableDictionary* dict = [NSMutableDictionary dictionary];
                if ([_estimateDict valueForKey:@"Comment"])
                {
                    [dict setValue:[_estimateDict valueForKey:@"Comment"] forKey:@"Comment"];
                }
                
                if ([_estimateDict valueForKey:@"EstimateDate"])
                {
                    [dict setValue:[_estimateDict valueForKey:@"EstimateDate"] forKey:@"InvoiceDate"];
                }
                
                if ([_estimateDict valueForKey:@"Direction"])
                {
                    [dict setValue:[_estimateDict valueForKey:@"Direction"] forKey:@"Direction"];
                }
                
                if ([_estimateDict valueForKey:@"Items"])
                {
                    [dict setValue:[_estimateDict valueForKey:@"Items"] forKey:@"Items"];
                }
                
                if ([_estimateDict valueForKey:@"Client"])
                {
                    [dict setValue:[_estimateDict valueForKey:@"Client"] forKey:@"Client"];
                }
                
                if ([_estimateDict valueForKey:@"Tax"])
                {
                    [dict setValue:[_estimateDict valueForKey:@"Tax"] forKey:@"Tax"];
                }
                
                [dict setValue:[self estimatePhotoList] forKey:@"PhotoList"];
                
                NSMutableArray* itemsArray = [[NSMutableArray alloc] init];
                for (EstimateItem* item in _itemArray)
                {
                    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
                    [dict setObject:item.item_amount forKey:@"Amount"];
                    [dict setObject:item.item_desc forKey:@"Note"];
                    [dict setObject:item.item_quantity forKey:@"Quantity"];
                    [dict setObject:item.item_rate forKey:@"Rate"];
                    [dict setObject:item.item_taxAllow forKey:@"TaxAllow"];
                    [dict setObject:item.item_teplateID forKey:@"templateId"];
                    [dict setObject:item.item_title forKey:@"Title"];
                    
                    if (item.item_id) {
                        [dict setObject:item.item_id forKey:@"ItemID"];
                    } else {
                        [dict setObject:@"" forKey:@"ItemID"];
                    }
                    
                    [itemsArray addObject:item];
                }
                
                //making sure pdf options do not contain null values
                if (!self.pdfQuantity) {
                    self.pdfQuantity = NO;
                }
                if (!self.pdfRate) {
                    self.pdfRate = NO;
                }
                if (!self.pdfSubtotal) {
                    self.pdfSubtotal = NO;
                }
                if (!self.pdfTotal) {
                    self.pdfTotal = NO;
                }
                
                //set up PDF Options
                [_estimateDict setValue:[NSNumber numberWithBool:self.pdfQuantity] forKey:@"PDFQuantity"];
                [_estimateDict setValue:[NSNumber numberWithBool:self.pdfRate] forKey:@"PDFRate"];
                [_estimateDict setValue:[NSNumber numberWithBool:self.pdfSubtotal] forKey:@"PDFSubtotal"];
                [_estimateDict setValue:[NSNumber numberWithBool:self.pdfTotal] forKey:@"PDFTotal"];
                
                
                [_gAppData convertEstimateToInvoice:_estimateDict withItems:itemsArray];
                
                NSLog(@"changing it to accepted on estimate");
                [self changeAcceptStatusOfEstimate:[NSNumber numberWithInteger:1]];
                [self saveEstimate:_estimateDict popViewController:YES];
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            break;
        }
        case 5:
        {
            [self acceptActionOne];
            break;
        }
        case 6:
        {
            [self acceptActionTwo];
            break;
        }
		default:
			break;
	}
}

//determines what action it performes depending on which category the estimate is already in
// 1 is accepted - 2 is pending - 3 is rejected
-(void) acceptActionOne
{
    if ([self.acceptedInteger integerValue] == 1)
    {
        [self changeAcceptStatusOfEstimate:[NSNumber numberWithInteger:2]];
    }
    
    if ([self.acceptedInteger integerValue] == 2)
    {
        [self changeAcceptStatusOfEstimate:[NSNumber numberWithInteger:1]];
    }
    
    if ([self.acceptedInteger integerValue] == 3)
    {
        [self changeAcceptStatusOfEstimate:[NSNumber numberWithInteger:2]];
    }
    NSLog(@"accept action one: %@", _estimateDict);
}

-(void) acceptActionTwo
{
    if ([self.acceptedInteger integerValue] == 1)
    {
        [self changeAcceptStatusOfEstimate:[NSNumber numberWithInteger:3]];
    }
    
    if ([self.acceptedInteger integerValue] == 2)
    {
        [self changeAcceptStatusOfEstimate:[NSNumber numberWithInteger:3]];
    }
    
    if ([self.acceptedInteger integerValue] == 3)
    {
        [self changeAcceptStatusOfEstimate:[NSNumber numberWithInteger:1]];
    }
}

- (void) changeAcceptStatusOfEstimate: (NSNumber*) accepted
{
    [_estimateDict setObject:accepted forKey:@"Accepted"];
    self.estimateObj.accepted = accepted;
}

- (void) sendMailButoonAction
{
	if([MFMailComposeViewController canSendMail])
	{
		MFMailComposeViewController* mailVC = [[MFMailComposeViewController alloc] init];
		mailVC.mailComposeDelegate = self;
        [mailVC setToRecipients:(self.customerObj.customer_email.length > 0) ? [NSArray arrayWithObjects:self.customerObj.customer_email, nil]: [NSArray arrayWithObjects:nil]];
        
        //NSString *link = [NSString stringWithFormat:@"http://bidabout.parseapp.com/accept?clientid=%@&estimateid=%@&userid=%@", _estimateObj.estimate_ClientId, _estimateObj.estimate_id, [PFUser currentUser].objectId];
        //http://www.bidabout.com/estimateapproval/?clientid=15&estimateid=23&userid=Uu9Cv0EqCR
        NSString *link = [NSString stringWithFormat:@"http://www.bidabout.com/estimateapproval/?clientid=%@&estimateid=%@&userid=%@", _estimateObj.estimate_ClientId, _estimateObj.estimate_id, [PFUser currentUser].objectId];
        
        NSLog(@"The estimate id is: %@", _estimateObj.estimate_id);
        
        NSArray* companSettingData = [_gAppData getCompanyData];
        NSString* companyName;
        if (companSettingData.count > 0)
        {
            CompanySetting* obj = [companSettingData objectAtIndex:0];
            companyName = obj.company_name;
        }
        
        NSString *body = [NSString stringWithFormat:@"%@ proposal.\n\nTo Accept this proposal click the link below\n%@" ,kMailMessage, link];
        
        
        if (companyName.length > 0)
        {
             body = [NSString stringWithFormat:@"%@ proposal sent from %@.\n\nTo Accept this proposal click the link below\n%@", companyName ,kMailMessage, link];
        }
        
		[mailVC setMessageBody:[NSString stringWithFormat:body,@"proposal"] isHTML:YES];
        [mailVC setMessageBody:body isHTML:NO];
		
		[self createPDF:NO isprinitng:NO];
		NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
		NSString* filePath = [UIUtils pathForDocumentNamed:path];

		if (self.dataOfPDF)
		{
            NSLog(@"attached pdf data of the altered version of the PDF: %@", self.dataOfPDF);
			[mailVC addAttachmentData:self.dataOfPDF mimeType:@"application/pdf" fileName:@"proposal"];
        } else {
            NSData* data = [NSData dataWithContentsOfFile:filePath];
            [mailVC addAttachmentData:data mimeType:@"application/pdf" fileName:@"proposal"];
        }
		
		[self presentViewController:mailVC animated:YES completion:nil];
	}
	else
	{
		[UIUtils messageAlert:nil title:@"Please configure the mail in your device" delegate:nil];
	}
}

- (void)addItemViewController:(PDFDisplayViewController *)controller didFinishEnteringDataItem:(NSData *)item
{
    self.dataOfPDF = item;
}

#pragma mark-- mailcomposer delegate.

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return (7 + _itemArray.count);
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger totalsection = kNumberOfFixedSection + _itemArray.count;
    
	if(section == (totalsection -3))
		return 3;
	else if ((section == 0) || (section == 1) || (section == 2) || (section == (totalsection -2)))
		return 1;
    else if (section == (totalsection -1))
        return _photoList.count+1;
    else if (section == kNumberOfFixedSection + _itemArray.count)
        return 5;
    else
        return 2;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = nil;
    NSInteger sectionNumber = indexPath.section;
    
    if (sectionNumber == 0)
    {
        cell = [self getDateCell];
    }
    else if(sectionNumber == 1)
    {
        cell = [self getCustomerdetailCell];
    }
    else if(sectionNumber == 2)
    {
        cell = [self getAddProductCell];
    }
    else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count-3))
    {
        cell = [self getTotalSectionCell:indexPath.row];
    }
    else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count -2))
    {
        cell = [self getCommentCell];
    }
    else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count -1))
    {
        cell = [self getAddPhotoCellData:indexPath.row];
    }
    else if(sectionNumber == kNumberOfFixedSection + _itemArray.count)
    {
        cell = [self getPDFOptions:indexPath.row];
        return cell;
    }
    else
    {
        if ((_itemArray.count > 0) && (sectionNumber != kNumberOfFixedSection + _itemArray.count))
        {
            NSInteger objectIndex = sectionNumber -3;
            cell = [self getItemACell:objectIndex WithRow:indexPath.row];
        }
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark 
#pragma mark CellConfiguration Method

- (UITableViewCell*) normalCell
{
    NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenifire];
    }
    
    UILabel* label = (UILabel*)[cell viewWithTag:kCellLabelheader];
    if (label)
        [label removeFromSuperview];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.imageView.image = nil;
    
    return cell;
}

- (UITableViewCell*) detiledCellWithData:(NSString*)headertext
{
    UITableViewCell* cell  = [self normalCell];
    [[cell viewWithTag:kCellLabelheader] removeFromSuperview];
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    cell.accessoryType = UITableViewCellAccessoryNone;

    UILabel* headerlabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 150, 44)];
    headerlabel.textColor = [UIColor grayColor];
    headerlabel.tag = kCellLabelheader;
    headerlabel.text = headertext;
    headerlabel.font = [UIFont systemFontOfSize:12.0];
    [headerlabel setBackgroundColor:[UIColor clearColor]];
    [cell addSubview:headerlabel];
    return cell;
}

- (UITableViewCell*) getDateCell
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
    NSString* selectedDateStr = [dateFormatter stringFromDate:_datePicker.date];
    
    UITableViewCell* cell = [self normalCell];
    NSString* estimatedateStr = [_estimateDict objectForKey:@"EstimateDate"];
    cell.textLabel.text = (estimatedateStr.length > 0) ? estimatedateStr:selectedDateStr;
    cell.accessoryType = /*(self.estimateObj)? UITableViewCellAccessoryNone:*/UITableViewCellAccessoryDetailDisclosureButton;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.imageView.image = [UIImage imageNamed:@"calendar-icon.png"];

    return cell;
}

- (UITableViewCell*) getCustomerdetailCell
{
    UITableViewCell* cell = [self normalCell];
    cell.textLabel.text = [self getCustomerCellData];
    
    if (self.customerObj)
    {
        cell.accessoryType = (self.haveClient)? UITableViewCellAccessoryDisclosureIndicator:(UITableViewCellAccessoryNone);
        cell.accessoryType = (self.estimateObj)? UITableViewCellAccessoryDisclosureIndicator:cell.accessoryType;

        cell.textLabel.textColor = [UIColor blackColor];
    }
    else
        cell.textLabel.textColor = [UIColor grayColor];
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];

    return cell;
}

- (UITableViewCell*) getAddProductCell
{
    UITableViewCell* cell = [self normalCell];
    cell.textLabel.text = @"Add Product or Template";
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.imageView.image = [UIImage imageNamed:@"additems.png"];
    
    return cell;
}

- (UITableViewCell*) getTotalSectionCell:(NSInteger)rownumber
{
    NSArray* dataArray = [[self getTotalSectionData:rownumber] componentsSeparatedByString:@","];
    UITableViewCell* cell = [self getSubtotalTypecell:dataArray];

    if(KVersion < 7.0)
	{
		if (rownumber == 1)
			cell.accessoryType = (self.estimateObj)? UITableViewCellAccessoryNone:UITableViewCellAccessoryDisclosureIndicator;
	}
    
    if (rownumber == 2)
    {
        [(UILabel*)[cell viewWithTag:kCellLabelheader] setTextColor:[UIColor blackColor]];
        [(UILabel*)[cell viewWithTag:kCellLabelheader] setFont:[UIFont boldSystemFontOfSize:12.0]];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    return cell;
}

- (UITableViewCell*) getCommentCell
{
    UITableViewCell* cell = [self normalCell];
    NSString* commentstr = [_estimateDict objectForKey:@"Comment"];
    
    cell.textLabel.text = (commentstr.length > 0) ? commentstr:@"Comment";
    cell.textLabel.textColor = (commentstr.length > 0)?[UIColor blackColor]:[UIColor grayColor];
    cell.textLabel.font = (commentstr.length > 0)?[UIFont systemFontOfSize:14.0]:[UIFont boldSystemFontOfSize:14.0];
    cell.accessoryType = /*(self.estimateObj)? UITableViewCellAccessoryNone:*/UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (UITableViewCell*) getAddPhotoCellData:(NSInteger)rowNumber
{
    UITableViewCell* cell = [self normalCell];
    NSString* textStr = @"";
    if (rowNumber == 0)
    {
        textStr = @"Add Photo";
        cell.textLabel.textColor = [UIColor blackColor];
    }

    else
    {
        if (_photoList.count >= rowNumber)
            textStr = [NSString stringWithFormat:@"Photo %ld",(long)rowNumber];
        
        cell.textLabel.textColor = [UIColor grayColor];
    }
    cell.textLabel.text = textStr;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (UITableViewCell*) getPDFOptions: (NSInteger) rowNumber
{
    UITableViewCell* cell = [self normalCell];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.imageView.image = [UIImage imageNamed:@"checkbox.png"];
    
    NSString* textStr = @"";
    
    if (rowNumber == 0)
    {
        textStr = @"PDF Display Options";
        cell.textLabel.textColor = [UIColor blackColor];
        cell.imageView.image = nil;
    }
    else if (rowNumber == 1)
    {
        textStr = @"Quantity";
        if (self.pdfQuantity) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    else if (rowNumber == 2)
    {
        textStr = @"Price";
        if (self.pdfRate) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    else if (rowNumber == 3)
    {
        textStr = @"Item Total";
        if (self.pdfSubtotal) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    else if (rowNumber == 4)
    {
        //change it so that you could toggle the totals
        textStr = @"Totals";
        if (self.pdfTotal) {
            cell.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    cell.textLabel.text = textStr;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    
    return cell;
}




- (UITableViewCell*) getSubtotalTypecell:(NSArray*) dataArray
{
    UITableViewCell* cell = [self detiledCellWithData:[dataArray objectAtIndex:0]];
    NSString* currencyValue = [UIUtils getCurrencyValue:[dataArray objectAtIndex:1]];
    cell.textLabel.text = currencyValue;
    return cell;
}

- (UITableViewCell*) getItemTotalcell:(NSArray*) dataArray
{
    NSString* currencyStr = @"0.00";
    UITableViewCell* cell = [self detiledCellWithData:[dataArray objectAtIndex:0]];
    
    if (dataArray.count > 2)
        currencyStr = [NSString stringWithFormat:@"%@ X %@",[dataArray objectAtIndex:1],[UIUtils getCurrencyValue:[dataArray objectAtIndex:2]]];
    else if (dataArray.count == 2)
        currencyStr  = [UIUtils getCurrencyValue:[dataArray objectAtIndex:1]];
    
    cell.textLabel.text = currencyStr;
    return cell;
}


- (UITableViewCell*) getItemACell:(NSInteger)objectIndex WithRow:(NSInteger)rowNumber
{
    UITableViewCell* cell = nil;
    NSString* dataString = [self getItemsectionData:objectIndex WithRow:rowNumber];
    if (rowNumber == 0)
    {
        cell = [self normalCell];
        cell.textLabel.text = dataString;
        cell.accessoryType = (self.estimateObj)? UITableViewCellAccessoryNone:UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (rowNumber == 1)
    {
        NSArray* dataArray = [dataString componentsSeparatedByString:@","];
        cell = [self getItemTotalcell:dataArray];
		if(KVersion < 7.0)
		{
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    return cell;
}

#pragma mark --
#pragma mar get Cell data private methods

- (NSString*) getCustomerCellData
{
    NSString* name = @"Customer Name";
    if (self.customerObj)
    {
        name = [NSString stringWithFormat:@"%@ %@", self.customerObj.customer_firstname, self.customerObj.customer_lastname];
    }
    
    return name;
}

- (NSString*) getItemsectionData:(NSInteger)itemIndex WithRow:(NSInteger)rowNumber
{
    EstimateItem* item = [_itemArray objectAtIndex:itemIndex];
    
    EstimateItem* estimateItem = _itemArray[0];
    NSLog(@"the item array is: %@", estimateItem.item_title);
    
    NSString* cellStr = @"";
    
    switch (rowNumber)
    {
        case 0:
        {
            NSString* itemDesc = item.item_title;
            if (itemDesc.length > 0 )
                cellStr = [NSString stringWithFormat:@"%@",item.item_title];
            else
                cellStr = @"Product";
            
        }
            break;
            
        case 1:
        {
            NSString* itemamount = item.item_amount;
            if ([itemamount floatValue] > 0 )
                cellStr = [NSString stringWithFormat:@"Amount,%.02f,%@",[item.item_quantity floatValue],item.item_rate];
            else
                cellStr = @"Amount,0.00";
        }
            break;

        default:
            break;
    }
    return cellStr;
}

- (NSString*) getTotalSectionData:(NSInteger)rowNumber
{
    NSString* cellStr = @"";
    
    switch (rowNumber)
    {
        case 0:
        {
            cellStr = [self getSubTotalCellData];
        }
            break;
            
        case 1:
        {
            cellStr = [self getTaxCellData];
        }
            break;
            
        case 2:
        {
            cellStr = [self getTotalCellData];
        }
            break;
            
        default:
            break;
    }
    return cellStr;
}

- (NSString*) getSubTotalCellData
{
    return [NSString stringWithFormat:@"SubTotal,%.02f",[self getSubtotal]];
}

- (NSString*) getTaxCellData
{
    double currentTax = [[_estimateDict objectForKey:@"Tax"] doubleValue];
    return [NSString stringWithFormat:@"TAX(%.02f%%),%.02f",currentTax,[self getAppliedtax]];
}

- (NSString*) getTotalCellData
{
    return [NSString stringWithFormat:@"TOTAL,%.02f",[self getTotalData]];
}

- (double) getSubtotal
{
    double subTotalSum = 0.00;
    if (_itemArray.count > 0)
    {
        for (EstimateItem* item in _itemArray)
        {
            double amount = [item.item_amount doubleValue];
            subTotalSum = subTotalSum + amount;
        }
    }
    return subTotalSum;
}

- (double) getAppliedtax
{
    double taxDeduction = 0.00;
    
    for (EstimateItem* item in _itemArray)
    {
        double amount = [item.item_amount doubleValue];
        double currentTax = [[_estimateDict objectForKey:@"Tax"] doubleValue];
        double taxableamount = ([item.item_taxAllow boolValue])? ((amount*currentTax)/100.00):0.0;
        taxDeduction = taxDeduction + taxableamount;
    }     
    
    return taxDeduction;
}

- (double) getTotalData
{
    double totalsum = 0.00;
    double subtotal = [self getSubtotal];
    double taxPaid = [self getAppliedtax];
    
    totalsum = subtotal + taxPaid;
    
    return totalsum;
}

#pragma mark --
#pragma mark TableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger sectionNumber = indexPath.section;
    
    if (sectionNumber == 0)
    {
//        if (!self.estimateObj)
//        {
            _pickerView.hidden = NO;
            [_pickerView addGestureRecognizer:_tapgesture];
        _isConvert = NO;
        
        [self selectdateFromPicker:nil];

        
//        }
    }
    else if(sectionNumber == 1)
    {
        [self clientDetialAction];
        _isConvert = NO;
    }
    else if(sectionNumber == 2)
    {
//        if (!self.estimateObj)
            [self addExpenseAction];
        _isConvert = NO;
    }
    else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count - 3))
    {
    }
    else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count - 2))
    {
        [self addItemsAction];
        _isConvert = NO;
    }
    else if(sectionNumber == (kNumberOfFixedSection + _itemArray.count -1))
    {
//        if (!self.estimateObj)
        [self addPhotoSection:indexPath.row];
        _isConvert = NO;
    }
    else if (sectionNumber == kNumberOfFixedSection + _itemArray.count)
    {
        if (indexPath.row == 0)
        {
            NSLog(@"nothing happens with the PDF options");
        } else {
            [self editPDFOptions:indexPath];
        }
        
    }
    else
    {
        if (indexPath.row == 1)
        {
            NSInteger objectIndex = indexPath.section -3;
            [self productDetail:objectIndex];
            _isConvert = NO;
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger totalsection = kNumberOfFixedSection + _itemArray.count;
    NSInteger section  = indexPath.section;
    BOOL isEdit = ((section == 0) || (section == 1) || (section == 2) || (section == (totalsection -3)) ||(section == (totalsection -2)) || (section == (totalsection -1) || (section == totalsection)))? NO: YES;
    if (isEdit)
        return ((indexPath.row == 1) && totalsection > 6)? UITableViewCellEditingStyleDelete:(UITableViewCellEditingStyleNone);
    
    return UITableViewCellEditingStyleNone;
}

- (void) tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
	{
        NSInteger objectIndex = indexPath.section -3;
        [self deleteItemAtIndex:objectIndex];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

//checking
- (void) deleteItemAtIndex:(NSInteger)objectIndex
{
    EstimateItem* item = [_itemArray objectAtIndex:objectIndex];
    NSString* templateid = [item.item_teplateID stringValue];
    
    [_gAppData deleteEstimateItemWithID:templateid andTitle:item.item_title];
    
    [_itemArray removeObjectAtIndex:objectIndex];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark Delegate

- (void) addCustomerDetial:(Customers *)customers
{
    self.customerObj = customers;
    [_estimateDict setObject:self.customerObj.customer_id forKey:@"Client"];

    [_tableView reloadData];
}

- (void) addPhotoList:(NSArray*)photoArray
{
    _photoList = [NSMutableArray arrayWithArray:photoArray];
}

#pragma mark --
#pragma mark Notification

- (void) savedItemData:(NSNotification *)notificationObject
{
    NSString* itemIdStr = [notificationObject object];
    NSArray* items = [_gAppData getEstimateItemWithID:itemIdStr];
    if (items.count > 0)
    {
        BOOL isExists = NO;
        EstimateItem* item = [items objectAtIndex:0];
        for (NSInteger i = 0; i < _itemArray.count; i++)
        {
            EstimateItem* itemObj = [_itemArray objectAtIndex:i];
            if ([itemObj.item_id isEqualToString:item.item_id])
            {
                [_itemArray replaceObjectAtIndex:i withObject:item];
                isExists = YES;
                break;
            }
        }
        
        if (!isExists)
            [_itemArray addObject:item];
    }
    
    NSString* itemString = @"";
    for (EstimateItem* item in _itemArray)
    {
        itemString = [itemString stringByAppendingString:[NSString stringWithFormat:@"%@,",item.item_id]];
    }
    [_estimateDict setObject:itemString forKey:@"Items"];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark Selector Methods

- (void) clientDetialAction
{
    if (self.haveClient )
    {
        EditCustomerViewController* editCustomerVC  = [[EditCustomerViewController alloc] initWithNibName:@"EditCustomerView" bundle:nil];
        editCustomerVC.customerObj = self.customerObj;
        [self.navigationController pushViewController:editCustomerVC animated:YES];
    }
    else if( !self.estimateObj)
    {
        ClientViewController* clientVC = [[ClientViewController alloc] initWithNibName:@"ClientView" bundle:nil];
        clientVC.isListMode = YES;
        clientVC.clientDelegate = self;
        clientVC.delegate = self;
        [[self navigationController] pushViewController:clientVC animated:YES];
    }
}

- (void) addItemsAction
{
    ItemDescriptionViewController* itemDescVC  = [[ItemDescriptionViewController alloc] initWithNibName:@"ItemDescriptionView" bundle:nil];
    itemDescVC.delegate = self;
    itemDescVC.isComment = YES;
    
    NSString* commentstr = [_estimateDict objectForKey:@"Comment"];
    if (commentstr.length > 0)
        itemDescVC.estimateComment = commentstr;
    
    UINavigationController* navc  = [[UINavigationController alloc] initWithRootViewController:itemDescVC];

    if (![UIUtils isiPhone])
        navc.modalPresentationStyle = UIModalPresentationFormSheet;
    
	[self presentViewController:navc animated:YES completion:nil];
}

- (void) addPhotoSection:(NSInteger)rownumber
{
    AddPhotoViewController* addPhotoVC  = [[AddPhotoViewController alloc] initWithNibName:@"AddPhotoView" bundle:nil];
    addPhotoVC.photoList = _photoList;
    addPhotoVC.estimateTitleId = self.title;
    addPhotoVC.addPhotoDelegate = self;
    addPhotoVC.photoIndex = rownumber;
    
    UINavigationController* navc  = [[UINavigationController alloc] initWithRootViewController:addPhotoVC];
//    
//    if (![UIUtils isiPhone])
//        navc.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:navc animated:YES completion:nil];
}

- (void) editPDFOptions: (NSIndexPath*) indexPath
{
    if (indexPath.row == 1)
    {
        if (self.pdfQuantity)
        {
            self.pdfQuantity = NO;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfQuantity = YES;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    if (indexPath.row == 2)
    {
        if (self.pdfRate)
        {
            self.pdfRate = NO;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfRate = YES;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    if (indexPath.row == 3)
    {
        if (self.pdfSubtotal)
        {
            self.pdfSubtotal = NO;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfSubtotal = YES;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
    
    if (indexPath.row == 4)
    {
        if (self.pdfTotal)
        {
            self.pdfTotal = NO;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox.png"];
        } else {
            self.pdfTotal = YES;
            [_tableView cellForRowAtIndexPath:indexPath].imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        }
    }
}

//-(void) pdfUncheckTotalsOnly
//{
//    self.pdfTotal = NO;
//    [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:kNumberOfFixedSection + _itemArray.count] ].imageView.image = [UIImage imageNamed:@"checkbox.png"];
//}
//
//-(void) pdfUncheckAllButTotals
//{
//    self.pdfQuantity = NO;
//    self.pdfRate = NO;
//    self.pdfSubtotal = NO;
//    
//    for (NSInteger x = 0; x < 4; x++)
//    {
//        [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:x inSection:kNumberOfFixedSection + _itemArray.count] ].imageView.image = [UIImage imageNamed:@"checkbox.png"];
//    }
//}


- (void) addExpenseAction
{
    ExpensceListViewController* expensceVC  = [[ExpensceListViewController alloc] initWithNibName:@"ExpensceView" bundle:nil];
    expensceVC.templateAddedDelegate = self;
    expensceVC.isEstimate = YES;
    expensceVC.addToEstimate = YES;
    expensceVC.fromClientScreen = self.fromClientScreen;

    [self.navigationController pushViewController:expensceVC animated:YES];
}

- (void) productDetail:(NSInteger)objectIndex
{
    EstimateItem* item = [_itemArray objectAtIndex:objectIndex];
    
    ScheduleViewController* scheduleVC  = [[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
    scheduleVC.product = item;
    scheduleVC.isUpdateEstimate = YES;
    scheduleVC.isProductItem = NO;
    scheduleVC.isAddEstimate = NO;
    
    
    [self.navigationController pushViewController:scheduleVC animated:YES];
}

- (void) hideDatePicker
{
    _pickerView.hidden = YES;
    [_pickerView removeGestureRecognizer:_tapgesture];
    [_tableView reloadData];
}

- (IBAction) selectdateFromPicker:(id)sender
{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM-dd-yyyy"];

    NSString* selectedDateStr = [dateFormatter stringFromDate:_datePicker.date];

//    NSDate* enddate = _datePicker.date;
//    NSDate* currentdate = [NSDate date];
//    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
//    double secondsInMinute = 60;
//    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
//    
//    if (secondsBetweenDates < 0)
//    {
//        [UIUtils messageAlert:@"Please select a date valid date!" title:@"Message" delegate:nil];
//    }
//    else
//    {
        [_estimateDict setObject:selectedDateStr forKey:@"EstimateDate"];
        [_tableView reloadData];
//    }
}

- (void) saveEstimate:(NSMutableDictionary*)estimateDict popViewController:(BOOL) popView
{
    [_estimateDict setObject:[self estimatePhotoList] forKey:@"PhotoList"];
    
    NSString *dateInInvoice = [_estimateDict objectForKey:@"EstimateDate"];
    if (dateInInvoice.length == 0)
    {
        [_estimateDict setObject: currentDate forKey:@"EstimateDate"];
    }
    
    NSInteger customerLength = self.customerObj.customer_firstname.length;
    
    if (!customerLength > 0)
    {
        [UIUtils messageAlert:@"You can not save an estimate without client. Please add a client!" title:kApplicationName delegate:nil];
        return;
    }
    
    //making sure its not nil
    if (!self.pdfQuantity) {
        self.pdfQuantity = NO;
    }
    if (!self.pdfRate) {
        self.pdfRate = NO;
    }
    if (!self.pdfSubtotal) {
        self.pdfSubtotal = NO;
    }
    if (!self.pdfTotal) {
        self.pdfTotal = NO;
    }
    
    //set up PDF Options
    [_estimateDict setValue:[NSNumber numberWithBool:self.pdfQuantity] forKey:@"PDFQuantity"];
    [_estimateDict setValue:[NSNumber numberWithBool:self.pdfRate] forKey:@"PDFRate"];
    [_estimateDict setValue:[NSNumber numberWithBool:self.pdfSubtotal] forKey:@"PDFSubtotal"];
    [_estimateDict setValue:[NSNumber numberWithBool:self.pdfTotal] forKey:@"PDFTotal"];
    
    if (self.estimateObj.accepted == nil) {
        NSLog(@"accepted == nil and setting it to 1 before saving");
        [estimateDict setObject:[NSNumber numberWithInteger:1] forKey:@"Accepted"];
    } else{
        [estimateDict setObject:self.estimateObj.accepted forKey:@"Accepted"];
    }
    
    NSLog(@"after accepted %@", estimateDict);
    
    [estimateDict setObject:self.customerObj.customer_id.stringValue forKey:@"Client"];
    // [estimateDict setObject:self.estimateObj.accepted forKey:@"Accepted"];
    
    if (self.estimateObj)
    {
        NSLog(@"The estimate will replace the estimate ID");
    
        [_gAppData insertNewEstimate:estimateDict replaceEstimateID:YES];
    }
    else
    {
        NSLog(@"Add new estimate");
        [_gAppData insertNewEstimate:estimateDict replaceEstimateID:NO];
    }
    
    if (popView) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (NSString*) estimatePhotoList
{
    NSString* str = @"";
    for (NSString* name in _photoList)
    {
        str = (str.length > 0)?[NSString stringWithFormat:@"%@,%@",str,name]:[NSString stringWithFormat:@"%@",name];
    }
    return str;
}

#pragma mark--
#pragma mark PDF generation

- (void) printAction
{
    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    if (data == nil)
    {
        [UIUtils messageAlert:@"Data is not available for prinitng." title:kApplicationName delegate:nil];
        return;
    }
    UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
    if(printController && [UIPrintInteractionController canPrintData:data])
    {
        printController.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [filePath lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printController.printInfo = printInfo;
        printController.showsPageRange = YES;
        printController.printingItem = data;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error)
            {
                NSLog(@"FAILED! due to error in domain %@ with error code %ld", error.domain, (long)error.code);
            }
        };
        
        [printController presentAnimated:YES completionHandler:completionHandler];
    }
}

- (void) createPDF:(BOOL) showMessage isprinitng:(BOOL)isprinting
{
    NSString* path = [NSString stringWithFormat:@"%@.pdf",self.title];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    
    //NSString* str = @"";
    //if (self.customerObj)
      //  str = [NSString stringWithFormat:@"%@ %@",self.customerObj.customer_firstname,self.customerObj.customer_lastname];
		
	double currentTax = [[_estimateDict objectForKey:@"Tax"] doubleValue];
	
//	NSString* subtotaldetail = [NSString stringWithFormat:@"Sub total\t\t\t\t  : %@",[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getSubtotal]]]];
//	NSString* taxdata = [NSString stringWithFormat:@"TAX(%.02f%%)\t\t\t\t  : %@",currentTax,[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getAppliedtax]]]];
//	NSString* total = [NSString stringWithFormat:@"Total\t\t\t\t  : %@",[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getTotalData]]]];
//	
//	NSString* finalstr = [NSString stringWithFormat:@" \n%@ \n%@ \n%@",subtotaldetail,taxdata,total];
	
	NSArray* detaillist = @[[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getSubtotal]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getAppliedtax]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getTotalData]]]];
	
	NSArray* headerList = @[@"Sub Total",[NSString stringWithFormat:@"TAX(%.02f%%)",currentTax],@"Total"];
	
    NSString* date = [_estimateDict objectForKey:@"EstimateDate"];
    if (_itemArray.count < 1)
    {
        if (showMessage)
            [UIUtils messageAlert:@"No preview available, please add products." title:kApplicationName delegate:nil];
        if (isprinting)
            [UIUtils messageAlert:@"Please add products to print estimate pdf." title:kApplicationName delegate:nil];

        return;
    }
	
    NSLog(@"PDF Renderer: draw PDF: %@, data array: %@, with title: %@, with client: %@, with detail: %@, with header: %@, with date: %@, with photo list: %@", filePath, _itemArray, self.title, self.customerObj, detaillist, headerList, date, _photoList);
    
    //ESTIMATE removed from within type
    [PDFRenderer  drawPDF:filePath dataArray:_itemArray withTitle:self.title withClient:self.customerObj withDetail:detaillist header:headerList withDate:date type:@"" photoList:_photoList isinvoice:NO showQuantity:self.pdfQuantity showRate:self.pdfRate showSubtotalsOnly:self.pdfSubtotal showTotalsOnly:self.pdfTotal];
    
    if (showMessage)
        [self performSelector:@selector(showPreview) withObject:nil afterDelay:1.0];
    if (isprinting)
        [self performSelector:@selector(printAction) withObject:nil afterDelay:1.0];
}

- (void)adjustPDFOptions:(PDFDisplayViewController*)controller quantity: (BOOL)PDFQuantity rate:(BOOL)PDFRate subtotal:(BOOL) PDFSubtotal totalsOnly: (BOOL)pdfTotalsOnly
{
    self.pdfQuantity = PDFQuantity;
    self.pdfRate = PDFRate;
    self.pdfSubtotal = PDFSubtotal;
    self.pdfTotal = pdfTotalsOnly;
    self.pdfOptions = YES;
}

- (void) showPreview
{
    double currentTax = [[_estimateDict objectForKey:@"Tax"] doubleValue];
    
    NSArray* detaillist = @[[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getSubtotal]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getAppliedtax]]],[UIUtils getCurrencyValue:[NSString stringWithFormat:@"%.02f",[self getTotalData]]]];
    
    NSArray* headerList = @[@"Sub Total",[NSString stringWithFormat:@"TAX(%.02f%%)",currentTax],@"Total"];
    
    NSString* date = [_estimateDict objectForKey:@"EstimateDate"];
    
    PDFDisplayViewController* pdfDVC = [[PDFDisplayViewController alloc] initWithNibName:@"PDFDisplayView" bundle:nil];
    pdfDVC.estimateTitleId = self.title;
    pdfDVC.itemArray = _itemArray;
    pdfDVC.screenTitle = self.title;
    pdfDVC.customer = self.customerObj;
    pdfDVC.detailList = detaillist;
    pdfDVC.headerList = headerList;
    pdfDVC.date = date;
    pdfDVC.photoList = _photoList;
    pdfDVC.showQuantity = self.pdfQuantity;
    pdfDVC.showRate = self.pdfRate;
    pdfDVC.subtotalsOnly = self.pdfSubtotal;
    pdfDVC.totalsOnly = self.pdfTotal;
    pdfDVC.delegate = self;

    [self.navigationController pushViewController:pdfDVC animated:YES];
}


@end
