//
//  ProductsViewController.m
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "ProductsViewController.h"
#import "Item.h"
#import "ScheduleViewController.h"

@interface ProductsViewController ()

@end

@implementation ProductsViewController
#pragma mark--ViewLife Cycle.

- (void) viewDidLoad
{
    [super viewDidLoad];

    NSLog(@"THE VIEW FOR THE PRODUCTS HAS LOADED");
	self.title = @"Products";
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // get revelButton
    UIButton* barButton = [UIUtils getRevelButtonItem:self];
    
    //Added revel button as nevegation button
    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    
	UIBarButtonItem* addBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBarBtnAction)];
	self.navigationItem.rightBarButtonItem = addBarBtn;

    _results = [_gAppData getFilteredItemResults];
    //_tableView.allowsMultipleSelectionDuringEditing = NO; // slide to delete
    [_tableView reloadData];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    _results = [_gAppData getFilteredItemResults];
    
    [_tableView reloadData];
}

- (void)viewDidUnload
{
	_tableView = nil;
	[super viewDidUnload];
}

- (void) addBarBtnAction
{
    ScheduleViewController* scheduleVC =[[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
    scheduleVC.isProductItem = YES;
    scheduleVC.isAddEstimate = NO;
    scheduleVC.isUpdateEstimate = NO;
    scheduleVC.isTemplate = NO;
    scheduleVC.addInvoiceProduct = NO;
    scheduleVC.isEstimate = NO;
    
    [self.navigationController pushViewController:scheduleVC animated:YES];
}

#pragma mark --
#pragma mark  TableViewData source.

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _results.count;
    
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    
    
    Item *productInformation = _results[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[productInformation valueForKey:@"item_title"]];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[productInformation item_title]];

    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    Item* obj = _results [indexPath.row];

    ScheduleViewController* scheduLeVC = [[ScheduleViewController alloc] initWithNibName:@"ScheduleView" bundle:nil];
    scheduLeVC.product = obj;
    scheduLeVC.isProductItem = YES;
    scheduLeVC.isAddEstimate = NO;
    scheduLeVC.isUpdateEstimate = NO;
    scheduLeVC.fromProductsScreen = YES;
    [self.navigationController pushViewController:scheduLeVC animated:YES];
}

@end
