//
//  HelpViewController.m
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

#pragma mark--ViewLife Cycle.

- (void) viewDidLoad
{
    [super viewDidLoad];
		
    self.title = @"Help";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // get revelButton
    UIButton *barButton = [UIUtils getRevelButtonItem:self];
    
    //Added revel button as nevegation button
    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    
    NSString* sVersionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString* sBuildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString* sBuildInfo = [NSString stringWithFormat:@"Ver %@ Build %@", sVersionNumber, sBuildNumber];
    _version.text = sBuildInfo;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.feedback setReturnKeyType:UIReturnKeyDone];
    
    
    [self.view addGestureRecognizer:tap];
    
    
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.feedback];
    [self.scrollView addSubview:self.feedbackType];
    [self.scrollView addSubview:self.helpText];
    [self.scrollView addSubview:self.submitBTN];
    [self.scrollView addSubview:self.version];
    [self.scrollView addSubview:self.feedbackLabel];
    self.scrollView.contentSize=CGSizeMake(315,6100);
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
    self.scrollView.scrollEnabled = YES;
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    
    //[self.feedback setFrame:CGRectMake(self.feedback.frame.size.width,self.feedback.frame.size.height,20,20)];
    
//    double width = self.version.frame.size.width;
//    double screenWidth = self.view.frame.size.width;
//    
//    double placement = screenWidth - width;
//    double middle = placement / 2;
//    
//    self.version.frame = CGRectMake(middle, self.version.frame.origin.y, self.version.frame.size.width, self.version.frame.size.height);
    
}



-(void)dismissKeyboard {
    [self.feedback resignFirstResponder];
}

//return key pressed?
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    NSLog(@" TEXT FIELD SHOULD RETURN");
    //[theTextField resignFirstResponder];
   
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 80.0

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:self.feedback])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    double width = self.version.frame.size.width;
    double screenWidth = self.view.frame.size.width;
    
    double placement = screenWidth - width;
    double middle = placement / 2;
    
    self.version.frame = CGRectMake(middle, self.version.frame.origin.y, self.version.frame.size.width + 10, self.version.frame.size.height);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)submitFeedback:(id)sender
{
    PFObject *feedback = [PFObject objectWithClassName:@"Feedback"];
    feedback[@"type"] = [self feedbackType:_feedbackType.selectedSegmentIndex];
    feedback[@"feedback"] = _feedback.text;
    [feedback saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            $alert(@"Thank you!", @"Your feedback has been submitted");
            _feedback.text = @"";
            [_feedbackType setSelectedSegmentIndex:0];
        }
    }];
}

- (NSString *)feedbackType:(NSInteger)index
{
    if (index == 0) {
        return @"Bug";
    } else if (index == 1) {
        return @"Suggestion";
    } else {
        return @"Comment";
    }
}

@end
