//
//  CompanySettingsViewController.h
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardControls.h"
#import "CompanySetting.h"

@interface CompanySettingsViewController : BaseViewController <
    UITextFieldDelegate,
    UITextViewDelegate,
    UIPopoverControllerDelegate,
    UIImagePickerControllerDelegate,
    UIActionSheetDelegate,
    UINavigationControllerDelegate,
    KeyboardControlsDelegate
>
{
    IBOutlet UITextField* _companyNameTxtField;
    IBOutlet UITextField* _addressTxtField;
    IBOutlet UITextField* _cityTxtField;
    IBOutlet UITextField* _stateTxtField;
    IBOutlet UITextField* _zipTxtField;
    IBOutlet UITextField* _countryTxtField;
    
    IBOutlet UITextField* _mobielTxtField;
    IBOutlet UITextField* _emailTxtField;
    
    IBOutlet UITextField* _taxvaluetxtField;
    
    IBOutlet UITextView* _termsTextView;
    
    IBOutlet UIScrollView* _scrollView;
    IBOutlet UIButton* _defaultTaxButton;
    IBOutlet UIButton* _includeTermsButton;
    IBOutlet UIButton* _logoBtn;
	
    NSString* _urlgenText;
	IBOutlet UITextView* _urlgenratertextview;
    IBOutlet UIButton* _urlgeneratorButton;
    
    UITapGestureRecognizer* _tapGesture;
    
    KeyboardControls* _keyboardControls;
    UIPopoverController* _popOver;
    NSMutableDictionary* _companySettingDict;
    UIImage* _logoImage;
    BOOL _isFromPicker;
    
    CompanySetting* _companyObj;
    UIWebView *webView;
}

- (IBAction) defaultTaxBtnAction:(UIButton*)sender;
- (IBAction) logobtnAction:(id)sender;

@end
