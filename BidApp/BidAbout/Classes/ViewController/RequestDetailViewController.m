//
//  RequestDetailViewController.m
//  BidAbout
//
//  Created by     on 18/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "Customers.h"
#import "RequestDetailViewController.h"
#import "CreateEstimateViewController.h"

#define kCellHeightiPhone 46.0
#define kCellHeightiPad   60.0

@interface RequestDetailViewController ()<MFMailComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@end

@implementation RequestDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Request Detail";
    NSLog(@"INSIDE THE REQUEST DETAIL VIEW CONTROLLER");
    
    UIBarButtonItem* createEstimateBtn = [[UIBarButtonItem alloc] initWithTitle:@"Estimate" style:UIBarButtonItemStyleBordered target:self action:@selector(createEstimateBtnaction)];
	self.navigationItem.rightBarButtonItem = createEstimateBtn;
    
//    _tableView.delegate = self;
//    _tableView.dataSource = self;
    
    
}

#pragma mark --
#pragma mark TableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1)
        return 5;
    else if (section == 2)
        return 1;
    
    return 2;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    
    
    //NSLog(@"in cell for row %i %i",indexPath.section,indexPath.row);
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryNone;
       // cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    NSInteger section = indexPath.section;
    if (section == 0)
    {
        if (indexPath.row == 0)
        {
            NSString* cellText = [NSString stringWithFormat:@"Name:  %@ %@",self.customerObj.customer_firstname,self.customerObj.customer_lastname];
            cell.textLabel.text = cellText;
        }
        else if (indexPath.row ==1)
        {
            
            NSString* cellText = [NSString stringWithFormat:@"Company:  %@",self.customerObj.customer_company];
            cell.textLabel.text = cellText;
        }
        
    }
    else if (section == 1)
    {
        if (indexPath.row == 0)
        {
            NSString* cellText = [NSString stringWithFormat:@"Addres1:  %@",self.customerObj.customer_addres1];
            cell.textLabel.text = cellText;
        }
        else if (indexPath.row == 1)
        {
            NSString* cellText = [NSString stringWithFormat:@"State:  %@",self.customerObj.customer_state];
            cell.textLabel.text = cellText;
        }
        else if (indexPath.row == 2)
        {
            NSString* cellText = [NSString stringWithFormat:@"Zip:  %@",self.customerObj.customer_zip];
            cell.textLabel.text = cellText;
        }
        else if (indexPath.row == 3)
        {
            NSString* cellText = [NSString stringWithFormat:@"Email:  %@",self.customerObj.customer_email];
            cell.textLabel.text = cellText;
        }
        else if (indexPath.row == 4)
        {
            NSString* cellText = [NSString stringWithFormat:@"Mobile Phone:  %@",self.customerObj.customer_mobilePhone];
            cell.textLabel.text = cellText;
        }
    }
    else if (section == 2)
    {
        cell.detailTextLabel.numberOfLines = 0;
        cell.textLabel.text = @"Note:";
        cell.detailTextLabel.text = self.customerObj.customer_notes;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize expectedLabelSize;
    if (indexPath.section == 2)
    {
        if ([UIUtils isiPhone])
        {
            //expectedLabelSize = [self.customerObj.customer_notes sizeWithFont:[UIFont systemFontOfSize:16.0] constrainedToSize:CGSizeMake(tableView.frame.size.width,4000) lineBreakMode:NSLineBreakByWordWrapping];
            
            
            UIFont* font = [UIFont systemFontOfSize:16.0];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
            paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
            NSDictionary *attributes = @{ NSFontAttributeName: font,
                                          NSParagraphStyleAttributeName: paragraphStyle };
            
            CGSize maximumLabelSize = CGSizeMake(tableView.frame.size.width,4000);
            [self.customerObj.customer_notes boundingRectWithSize:maximumLabelSize
                                      options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                   attributes:attributes
                                      context:nil];
            
            expectedLabelSize = [self.customerObj.customer_notes sizeWithAttributes:attributes];
            
        }
        else
        {
            //expectedLabelSize = [self.customerObj.customer_notes sizeWithFont:[UIFont systemFontOfSize:20.0] constrainedToSize:CGSizeMake(tableView.frame.size.width,2000) lineBreakMode:NSLineBreakByWordWrapping];
            
            
            UIFont* font = [UIFont systemFontOfSize:20.0];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
            paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
            NSDictionary *attributes = @{ NSFontAttributeName: font,
                                          NSParagraphStyleAttributeName: paragraphStyle };
            
            CGSize maximumLabelSize = CGSizeMake(tableView.frame.size.width,2000);
            [self.customerObj.customer_notes boundingRectWithSize:maximumLabelSize
                                                          options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                       attributes:attributes
                                                          context:nil];
            
            expectedLabelSize = [self.customerObj.customer_notes sizeWithAttributes:attributes];
        }
        return MAX(expectedLabelSize.height+30.0, kCellHeightiPhone);
    }
    
    if ([UIUtils isiPhone])
        return kCellHeightiPhone;
    
    return kCellHeightiPad;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1 && indexPath.row == 4)
    {

        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *cellText = cell.textLabel.text;
        
        //getting the actual phone number from the cell text
        NSRange range = [cellText rangeOfString:@" " options:NSBackwardsSearch];
        NSString *phoneNumber = [cellText substringFromIndex:range.location+1];
        
        //adding the format to the phone number
        NSMutableString *phoneFormatForCall = [NSMutableString stringWithString:@"tel://"];
        [phoneFormatForCall appendString:phoneNumber];

        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneFormatForCall]];
    }
    
    if (indexPath.section == 1 && indexPath.row == 3)
    {
        
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        NSString *cellText = cell.textLabel.text;
        
        // getting the actual email from the cell text
        NSRange range = [cellText rangeOfString:@" " options:NSBackwardsSearch];
        NSString *email = [cellText substringFromIndex:range.location+1];

        [self sendMail:email];
        
    }
}

#pragma mark --
#pragma mark TableView Delegate

/*-------------- selecting table row
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 2 && indexPath.row == 3)
    {
        
    }
    else if(indexPath.section == 2 && indexPath.row == 4)
    {
        
    }
    
    NSLog(@"Cell selected");
}
*/
- (void)sendEmail
{
    NSString* emailStr = self.customerObj.customer_email;
    if (emailStr.length > 0)
        [self sendMail:emailStr];
}

- (void)sendMail:(NSString *)emailID
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController* mailVC = [[MFMailComposeViewController alloc] init];
        mailVC.mailComposeDelegate = self;
        [mailVC setToRecipients:[NSArray arrayWithObjects:emailID, nil]];
        [self presentViewController:mailVC animated:YES completion:nil];
    }
    else
    {
        [UIUtils messageAlert:nil title:@"Please configure the mail in your device" delegate:nil];
    }
}

//- (void)callPhone
//{
//    NSString* mobileStr = self.customerObj.customer_mobilePhone;
//    if (mobileStr.length > 0)
//    {
//        NSString* stringURL = [NSString stringWithFormat:@"tel:%@",mobileStr];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringURL]];
//    }
//}

- (void) createEstimateBtnaction
{
    CreateEstimateViewController* viewC = [[CreateEstimateViewController alloc] initWithNibName:@"CreateEstimateView" bundle:nil];
    viewC.customerObj = self.customerObj;
    viewC.haveClient = YES;
    [self.navigationController pushViewController:viewC animated:YES];
}

#pragma mark -
#pragma mark- mailcomposer delegate.
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
