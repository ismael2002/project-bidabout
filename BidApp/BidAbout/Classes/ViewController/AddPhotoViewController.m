//
//  AddPhotoViewController.m
//  BidAbout
//
//  Created by waseem  on 30/09/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "AddPhotoViewController.h"

@interface AddPhotoViewController ()

@end

@implementation AddPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"%@ Photo",self.estimateTitleId];
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    UIBarButtonItem* nextBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(addPhotoAction)];
    self.navigationItem.rightBarButtonItem = nextBarBtn;
    
    _tableView.editing = /*(self.estimateObj) ? NO:*/YES;
    _tableView.allowsSelectionDuringEditing = YES;
    
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(hidePhotoAction)];
    self.navigationItem.leftBarButtonItem = backButton;
	// Do any additional setup after loading the view.
    
    // to navigate from other index
    if (self.photoIndex >= 1)
        [self openGalleryPic:self.photoIndex-1];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)viewDidUnload
{
	_tableView = nil;
    _photoList = nil;
    
	[super viewDidUnload];
}


#pragma mark --
#pragma mark tableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.photoList.count;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellEditingStyleNone;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Photo %ld",(NSInteger)indexPath.row +1];

    return cell;
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self openGalleryPic:indexPath.row];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void) tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
	{
        NSInteger objectIndex = indexPath.row;
        [self deletImage:objectIndex];
    }
}

- (void) addPhotoAction
{
    if (self.photoList.count == 3)
    {
        [UIUtils messageAlert:@"You can add maximum 3 photo!" title:kApplicationName delegate:nil];
        return;
    }
    
    UIActionSheet* actionSheet = nil;
    if (_photoList.count > 0)
    {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add photo from Camera", @"Add photo from Library", @"View photo gallery", nil];
        actionSheet.tag = 1004;
    }
    else
    {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add photo from Camera", @"Add photo from Library", nil];
        actionSheet.tag = 1003;
    }
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	[self photoSheetAction:buttonIndex];
}

- (void) hidePhotoAction
{
    NSArray* arrayP = self.photoList;
    if (self.addPhotoDelegate && [self.addPhotoDelegate respondsToSelector:@selector(addPhotoList:)])
        [self.addPhotoDelegate addPhotoList:arrayP];
    
	[self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) photoSheetAction:(NSInteger)selectedIndex
{
    switch (selectedIndex)
    {
        case 0:
        {
#if TARGET_IPHONE_SIMULATOR
            [UIUtils messageAlert:@"Camera not available!" title:@"Add Images" delegate:self];
            
#else
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
			picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
//            [picker release];
            
#endif
        }
            break;
            
        case 1:
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
			picker.allowsEditing = YES;
            
            if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
            {
                _popOver = [[UIPopoverController alloc] initWithContentViewController:picker];
                _popOver.delegate = self;
                [_popOver presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                self.navigationController.navigationBar.userInteractionEnabled = NO;
            }
            else
            {
                picker.navigationBar.barStyle = UIBarStyleBlackOpaque;
                [self presentViewController:picker animated:YES completion:nil];
            }
        }
            break;
            
        case 2:
        {
            [self openGalleryPic:0];
        }
            
        default:
			break;
    }
}


- (void) openGalleryPic:(NSInteger)index
{
    _localGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
    _localGallery.useThumbnailView = NO;
    _localGallery.startingIndex = index;
    [self.navigationController pushViewController:_localGallery animated:YES];
}

#pragma mark --
#pragma mark ImagePicker Delegate

- (void) imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
	NSData* dataImage = UIImagePNGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage"]);
    
    UIImage *image = [[UIImage alloc] initWithData:dataImage]; // imageView is image from camera
    
    NSString* imageName = [NSString stringWithFormat:@"%@.png",[self getCurrentDateTimeAsNSString]];
    
    NSString* path = [NSString stringWithFormat:@"%@",imageName];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    
    BOOL isSaveImage = [UIUtils saveData:UIImagePNGRepresentation(image) path:filePath];
    if (isSaveImage)
    {
        [_photoList addObject:imageName];
        
        [UIUtils messageAlert:@"Image successfully saved with product" title:kApplicationName delegate:nil];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            [_popOver dismissPopoverAnimated:YES];
            self.navigationController.navigationBar.userInteractionEnabled = YES;
        }
    }
    else
        [UIUtils messageAlert:@"Image not saved with product. Try Again!" title:kApplicationName delegate:nil];
    
    [_tableView reloadData];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	[self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [_tableView reloadData];
}

#pragma mark --
#pragma mark ImageName method

// Method to get unique name for images

- (NSString*) getCurrentDateTimeAsNSString
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyyMMddHHmmss"];
    
    NSDate *now = [NSDate date];
    NSString *retStr = [format stringFromDate:now];
    
    return retStr;
}

- (void) deletImage:(NSInteger)index
{
//    NSString* imageName = [NSString stringWithFormat:@"%@",[_photoList objectAtIndex:index]];
//    NSString* path = [NSString stringWithFormat:@"%@/%@", self.estimateTitleId,imageName];
//    NSString* filePath = [UIUtils pathForDocumentNamed:path];
//    [UIUtils deleteItemAtPath:filePath];
    [_photoList removeObjectAtIndex:index];
    [_tableView reloadData];
}

#pragma mark -
#pragma mark - FGalleryViewControllerDelegate Methods

- (NSInteger) numberOfPhotosForPhotoGallery:(FGalleryViewController *)gallery
{
    return (NSInteger)_photoList.count;
}

- (FGalleryPhotoSourceType)photoGallery:(FGalleryViewController *)gallery sourceTypeForPhotoAtIndex:(NSUInteger)index
{
    return FGalleryPhotoSourceTypeLocal;
}

- (NSString*) photoGallery:(FGalleryViewController*)gallery filePathForPhotoSize:(FGalleryPhotoSize)size atIndex:(NSUInteger)index
{
    NSString* imageName = [NSString stringWithFormat:@"%@",[_photoList objectAtIndex:index]];

    NSString* path = [NSString stringWithFormat:@"%@",imageName];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    
    return filePath;
}


@end
