//
//  CompanySettingsViewController.m
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "CompanySettingsViewController.h"
#import "HomeViewController.h"
#import "CompanySetting.h"
#import <QuartzCore/QuartzCore.h>

#define kScrollWidth 320
#define kiPhoneScrollFrameHeightWithoutKeyboard 460
#define kiPhone5ScrollFrameHeightWithoutKeyboard 548

#define kiPhone5ScrollFrameHeightWithKeyboard 346
#define kiPhoneScrollFrameHeightWithKeyboard 320

#define kscrollContentSizeDiffWithoutKeyBoard 220 //HERE
#define kscrollContentSizeDiffWithKeyBoard 490 // ADDED 250 to both

#define kTextFieldDiffrence 80

@interface CompanySettingsViewController (Private)

- (BOOL) isAnyThingEmpty;
- (void) setUpKeyboardControls;
- (void) setUpScrollViewOffSet:(UITextField *)tField;

@end

@implementation CompanySettingsViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _termsTextView.layer.cornerRadius = 5;
    _termsTextView.layer.masksToBounds = YES;
    
    self.title = @"Company Settings";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    //webView = [[UIWebView alloc] init];
    
    
    [self setNavigationData];
    [self initialSetUp];
    [self setUpKeyboardControls];
    [self setOtherInitalData];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    if (_gAppPrefData.currentTax.floatValue > 0)
    {
        _taxvaluetxtField.text = [NSString stringWithFormat:@"%.02f",[_gAppPrefData.currentTax doubleValue]];
    }

    [self setCompanyInfo];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _companySettingDict = nil;
    
    [super viewDidUnload];
}


#pragma mark --
#pragma mark Intitial method

- (void) setNavigationData
{
    UIButton* barButton = [UIUtils getRevelButtonItem:self];

    UIBarButtonItem* revealbuttonItem =[[UIBarButtonItem alloc] initWithCustomView:barButton];
    UIBarButtonItem* updateBtn = [[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStylePlain target:self action:@selector(updateAction:)];
    
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    self.navigationItem.rightBarButtonItem = updateBtn;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void) tapHappened:(UITapGestureRecognizer *)gesture
{
    CGPoint point = [gesture locationInView:gesture.view];

    if (gesture.state == UIGestureRecognizerStateEnded) {
        if (CGRectContainsPoint(_urlgenratertextview.frame, point))
        {
            NSString *copyStringverse = _urlgenratertextview.text;
            UIPasteboard *pb = [UIPasteboard generalPasteboard];
            [pb setString:copyStringverse];
            
            
//            UIBarButtonItem* updateBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];//change update action
            
//            self.navigationItem.rightBarButtonItem = updateBtn;
//            self.navigationItem.rightBarButtonItem.enabled = YES;
            
//            [webView setFrame:CGRectMake(0, 0, 320, 510)];
//            [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlgenText]]];
//            [[self view] addSubview:webView];
//            webView.scalesPageToFit = TRUE;
//            webView.scrollView.scrollEnabled = TRUE;
            
            [UIUtils messageAlert:@"URL Copied" title:kApplicationName delegate:nil];
            
        }
    }
}

- (void) initialSetUp
{
    _urlgenText = [NSString stringWithFormat:kURLGenrater,_gAppPrefData.memberID];
	_urlgenratertextview.text = _urlgenText;
    
	
    _companySettingDict = [[NSMutableDictionary alloc] initWithCapacity:10];
    NSArray* companSettingData = [_gAppData getCompanyData];
    if (companSettingData.count > 0)
    {
        CompanySetting* obj = [companSettingData objectAtIndex:0];
     
        // there was a crash here but couldn't repeat test case
        
        [_companySettingDict setObject:obj.company_name forKey:@"CompanyName"];
        [_companySettingDict setObject:obj.company_address forKey:@"CompanyAddress"];
        [_companySettingDict setObject:obj.company_city forKey:@"CompanyCity"];
        [_companySettingDict setObject:obj.company_state forKey:@"CompanyState"];
        [_companySettingDict setObject:obj.company_zip forKey:@"CompanyZip"];
        [_companySettingDict setObject:obj.company_country forKey:@"CompanyCountry"];
        [_companySettingDict setObject:obj.company_tax forKey:@"CompanyTax"];
        [_companySettingDict setObject:obj.company_logo forKey:@"CompanyLogo"];
        [_companySettingDict setObject:obj.company_mobilePhone forKey:@"CompanyMobile"];
        [_companySettingDict setObject:obj.company_email forKey:@"CompanyEmail"];
        [_companySettingDict setObject:obj.company_terms forKey:@"CompanyTerms"];
        [_companySettingDict setObject:[NSNumber numberWithBool: obj.include_terms] forKey:@"includeTerms"];
    }
}

- (void) setOtherInitalData
{
    _defaultTaxButton.selected = _gAppPrefData.isDefualttaxOn;
    _includeTermsButton.selected = _gAppPrefData.isIncludeTermsOn;
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHappened:)];
    [_urlgenratertextview.superview addGestureRecognizer:_tapGesture];
    
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        else
        {
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard+88);
        }
        
    }
    else
        _scrollView.contentSize = CGSizeMake(768,1200);
}

 - (void) setCompanyInfo
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDone:) name:kUpdateCompanyinfoSettingNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    NSString* companyLogoStr = [_companySettingDict objectForKey:@"CompanyLogo"];
    
    UIImage* image = [UIUtils getimageFromDocumentDirectoryWithName:companyLogoStr];
    if (!_isFromPicker)
    {
        [self setDataFromCompanyDict];
        if (image != nil)
            [_logoBtn setImage:image forState:UIControlStateNormal];
    }
    else
    {
        if (_logoImage)
            [_logoBtn setImage:_logoImage forState:UIControlStateNormal];
    }
}

 -(void) setDataFromCompanyDict
{
    NSString* companyNameStr = [_companySettingDict objectForKey:@"CompanyName"];
    _companyNameTxtField.text = (companyNameStr.length > 0)?companyNameStr:@"";
    
    NSString* companyAddStr = [_companySettingDict objectForKey:@"CompanyAddress"];
    _addressTxtField.text = (companyAddStr.length > 0)?companyAddStr:@"";
    
    NSString* companyCityStr = [_companySettingDict objectForKey:@"CompanyCity"];
    _cityTxtField.text = (companyCityStr.length > 0)?companyCityStr:@"";
    
    NSString* companyStateStr = [_companySettingDict objectForKey:@"CompanyState"];
    _stateTxtField.text = (companyStateStr.length > 0)?companyStateStr:@"";
    
    NSString* companyZipstr = [_companySettingDict objectForKey:@"CompanyZip"];
    _zipTxtField.text = (companyZipstr.length > 0)?companyZipstr:@"";
    
    NSString* companyCountryStr = [_companySettingDict objectForKey:@"CompanyCountry"];
    _countryTxtField.text = (companyCountryStr.length > 0)?companyCountryStr:@"";
    
    NSString* companyMobileStr = [_companySettingDict objectForKey:@"CompanyMobile"];
    _mobielTxtField.text = (companyMobileStr.length > 0)?companyMobileStr:@"";
    
    NSString* companyEmailStr = [_companySettingDict objectForKey:@"CompanyEmail"];
    _emailTxtField.text = (companyEmailStr.length > 0)?companyEmailStr:@"";
    
    NSString* companyTermsStr = [_companySettingDict objectForKey:@"CompanyTerms"];
    _termsTextView.text = (companyTermsStr.length > 0)?companyTermsStr:kDefaultTerms;
    if(companyTermsStr.length == 0){
        _termsTextView.textColor = [UIColor lightGrayColor];
    }else
        _termsTextView.textColor = [UIColor blackColor];
}

#pragma mark TextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString: @"Enter terms and conditions here."])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    if ([_keyboardControls.textFieldsArray containsObject:textView])
        _keyboardControls.activeTextField = textView;
    //EHREHASFDASFA
    [_scrollView setContentOffset:CGPointMake(0, textView.frame.origin.y-0) animated:YES];
}

- (void)textViewDidEndEditing:(UITextField *)textView
{
    if([textView.text isEqualToString: @""])
    {
        textView.text = @"Enter terms and conditions here.";
        textView.textColor = [UIColor lightGrayColor];
    }
    
    if (self.navigationItem.rightBarButtonItem.enabled == NO)
	{
        self.navigationItem.rightBarButtonItem.enabled = YES;
		[self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
	}
}



#pragma mark TextFieldDelegate

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if ([_keyboardControls.textFieldsArray containsObject:textField])
        _keyboardControls.activeTextField = textField;
    
    [self setUpScrollViewOffSet:textField];
}

- (BOOL) textFieldShouldReturn:(UITextField*)textField
{
	[textField resignFirstResponder];
    
    [self handleKeyboardHideShow];
	return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    //textField.text = [textField.text capitalizedString];
    
    if (self.navigationItem.rightBarButtonItem.enabled == NO)
	{
        self.navigationItem.rightBarButtonItem.enabled = YES;
		[self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
	}
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if(textField.tag == 1000)
	{
		NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
		NSString *decimalSymbol = [formatter decimalSeparator];
		
		NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
		
		NSString *expression = [NSString stringWithFormat:@"^([0-9]+)?(\\%@([0-9]{1,2})?)?$", decimalSymbol];
		
		NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
																			   options:NSRegularExpressionCaseInsensitive
																				 error:nil];
		NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
															options:0
															  range:NSMakeRange(0, [newString length])];
		if (numberOfMatches == 0)
		{
			return NO;
		}
	}
	
	if (![string isEqualToString:@""])
		if ([textField isEqual:_mobielTxtField])
			return [UIUtils setFormatForPhoneNumber:_mobielTxtField OfLength:12];
	
	return YES;
}

#pragma mark -
#pragma mark Button Action

- (IBAction) updateAction:(id)sender
{
    [self dismissKeyboard];
    [_companySettingDict setObject:_companyNameTxtField.text forKey:@"CompanyName"];
    [_companySettingDict setObject:_addressTxtField.text forKey:@"CompanyAddress"];
    [_companySettingDict setObject:_cityTxtField.text forKey:@"CompanyCity"];
    [_companySettingDict setObject:_stateTxtField.text forKey:@"CompanyState"];
    [_companySettingDict setObject:_zipTxtField.text forKey:@"CompanyZip"];
    [_companySettingDict setObject:_countryTxtField.text forKey:@"CompanyCountry"];
    [_companySettingDict setObject:_mobielTxtField.text forKey:@"CompanyMobile"];
    [_companySettingDict setObject:_emailTxtField.text forKey:@"CompanyEmail"];
    NSString *terms = [_termsTextView.text isEqualToString:@"Enter terms and conditions here."] ? @"" : _termsTextView.text;
    [_companySettingDict setObject:terms forKey:@"CompanyTerms"];
    [_companySettingDict setObject:[NSNumber numberWithBool:_gAppPrefData.isIncludeTermsOn] forKey:@"IncludeTerms"];
    
    // company logo imagename obtain from document directory.
    [_companySettingDict setObject:@"company_logo" forKey:@"CompanyLogo"];
    
    if (_taxvaluetxtField.text.doubleValue > 99.99)
    {
        [UIUtils messageAlert:@"Please check, entered tax percentage can not exceed more than 100." title:kApplicationName delegate:nil];
        return;
    }
   
    [_gAppData addUpdateCompanyData:_companySettingDict];
}

//- (IBAction) doneAction:(id)sender
//{
//    UIBarButtonItem* updateBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(updateAction:)];
//    
//    self.navigationItem.rightBarButtonItem = updateBtn;
//    self.navigationItem.rightBarButtonItem.enabled = NO;
//    
//    //[webView removeFromSuperview];
//    
//    
//}

- (IBAction) defaultTaxBtnAction:(UIButton*)sender
{
    if (sender.selected)
    {
        _defaultTaxButton.selected = NO;
        sender.imageView.image = [UIImage imageNamed:@"checkbox.png"];
//        _taxvaluetxtField.enabled = YES;
    }
    else
    {
        _defaultTaxButton.selected = YES;
        sender.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
//		_taxvaluetxtField.text = [NSString stringWithFormat:@"%.02f",[_gAppPrefData.currentTax doubleValue]];
//        _taxvaluetxtField.text  = [NSString stringWithFormat:@"%.02f",kCurrentTax];
//        _taxvaluetxtField.enabled = NO;
    }
    self.navigationItem.rightBarButtonItem.enabled = YES;
	[self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
}

- (IBAction) logobtnAction:(id)sender
{
    [self addPhotoAction];
}

- (IBAction)inludeTermsButtonPressed:(UIButton *)sender
{
    if (sender.selected)
    {
        _includeTermsButton.selected = NO;
        sender.imageView.image = [UIImage imageNamed:@"checkbox.png"];
        //        _taxvaluetxtField.enabled = YES;
    }
    else
    {
        _includeTermsButton.selected = YES;
        sender.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
        //		_taxvaluetxtField.text = [NSString stringWithFormat:@"%.02f",[_gAppPrefData.currentTax doubleValue]];
        //        _taxvaluetxtField.text  = [NSString stringWithFormat:@"%.02f",kCurrentTax];
        //        _taxvaluetxtField.enabled = NO;
    }
    self.navigationItem.rightBarButtonItem.enabled = YES;
	[self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark Private Methods

- (void) setUpScrollViewOffSet:(UITextField *)tField
{
    [_scrollView setContentOffset:CGPointMake(0, tField.frame.origin.y-kTextFieldDiffrence) animated:YES];
}

- (BOOL) isAnyThingEmpty
{
    if ([_companyNameTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Company Name %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_addressTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Address %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_cityTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"City %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_stateTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"State %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_zipTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Zip %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_countryTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Country %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_mobielTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Mobile %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    else if([_emailTxtField.text isEqualToString:@""])
    {
        [UIUtils messageAlert:[NSString stringWithFormat:@"Email %@", kBlankFieldMessage] title:kMessage delegate:nil];
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark Notification

- (void) updateDone:(NSNotification*)notification
{
    //NSString* path = [NSString stringWithFormat:@"company_logo"];
    //NSString* filePath = [UIUtils pathForDocumentNamed:path];
    
    
    //BOOL isSaveImage = [UIUtils saveData:UIImagePNGRepresentation(_logoImage) path:filePath];
    _isFromPicker = NO;
    
    _gAppPrefData.currentTax = [NSNumber numberWithDouble:[_taxvaluetxtField.text doubleValue]];
    _gAppPrefData.isDefualttaxOn = _defaultTaxButton.selected;
    _gAppPrefData.isIncludeTermsOn = _includeTermsButton.selected;
    [_gAppPrefData saveAllData];
    HomeViewController* homeVC = [[HomeViewController alloc] init];
    
    [_gAppDelegate initializeApplication:homeVC];
    
    [UIUtils messageAlert:@"Successfully updated!" title:kApplicationName delegate:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhone5ScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth, kiPhoneScrollFrameHeightWithKeyboard+ kscrollContentSizeDiffWithKeyBoard+100);
        }
    }
    else
	{
		_scrollView.contentSize = CGSizeMake(768, 1200);
        _scrollView.frame = CGRectMake(0,0,768,660);
	}
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHappened:)];
    [self.view addGestureRecognizer:_tapGesture];
}

- (void) setUpKeyboardControls
{
    _keyboardControls = [[KeyboardControls alloc] init];
    _keyboardControls.delegate = self;
    
    // Add textFields in order
    _keyboardControls.textFieldsArray = [NSArray arrayWithObjects:_companyNameTxtField,_addressTxtField,_cityTxtField,_stateTxtField,_zipTxtField,_countryTxtField,_mobielTxtField,_emailTxtField,_taxvaluetxtField, _termsTextView, nil];
    
    _keyboardControls.barStyle = UIBarStyleBlackTranslucent;
    _keyboardControls.previousNextTintColor = [UIColor blackColor];
    _keyboardControls.doneTintColor = [UIColor colorWithRed:34.0/255.0 green:164.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    _keyboardControls.previousTitle = @"Previous";
    _keyboardControls.nextTitle = @"Next";
    
    for (id textField in _keyboardControls.textFieldsArray)
    {
        if ([textField isKindOfClass:[UITextField class]])
        {
            ((UITextField *) textField).inputAccessoryView = _keyboardControls;
            ((UITextField *) textField).delegate = self;
        }
        else if([textField isKindOfClass:[UITextView class]])
        {
            ((UITextView *) textField).inputAccessoryView = _keyboardControls;
            ((UITextView *) textField).delegate = self;
        }
    }
}

- (void) dismissKeyboard
{
    [_companyNameTxtField resignFirstResponder];
    [_addressTxtField resignFirstResponder];
    [_cityTxtField resignFirstResponder];
    [_stateTxtField resignFirstResponder];
    [_zipTxtField resignFirstResponder];
    [_countryTxtField resignFirstResponder];
    [_taxvaluetxtField resignFirstResponder];
    [_mobielTxtField resignFirstResponder];
    [_emailTxtField resignFirstResponder];
	[_urlgenratertextview resignFirstResponder];
    [_termsTextView resignFirstResponder];
    
    [self handleKeyboardHideShow];
}

#pragma mark -
#pragma mark KeyBoardControlDelegate

- (void)keyboardControlsDonePressed:(KeyboardControls*)controls
{
    [controls.activeTextField resignFirstResponder];
    [self handleKeyboardHideShow];
}

- (void)keyboardControlsPreviousNextPressed:(KeyboardControls*)controls withDirection:(KeyboardControlsDirection)direction andActiveTextField:(id)textField
{
    [textField becomeFirstResponder];
    [self setUpScrollViewOffSet:textField];
}

- (void) handleKeyboardHideShow
{
    if ([UIUtils isiPhone])
    {
        if ([UIUtils isiPhone5])
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhone5ScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhone5ScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard);
        }
        else
        {
            _scrollView.frame = CGRectMake(0, 0, kScrollWidth, kiPhoneScrollFrameHeightWithoutKeyboard);
            _scrollView.contentSize = CGSizeMake(kScrollWidth,kiPhoneScrollFrameHeightWithoutKeyboard+kscrollContentSizeDiffWithoutKeyBoard+100);
        }
    }
    else
	{
		_scrollView.contentSize = CGSizeMake(768, 1200);
		_scrollView.frame = CGRectMake(0,0,768,970);
	}
    [self.view removeGestureRecognizer:_tapGesture];
}


#pragma  mark - Add Logo Implementation

- (void) addPhotoAction
{
    UIActionSheet* actionSheet =  actionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add photo from Camera", @"Add photo from Library", nil];
        actionSheet.tag = 1003;
    
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	[self photoSheetAction:buttonIndex];
}



- (void) photoSheetAction:(NSInteger)selectedIndex
{
    switch (selectedIndex)
    {
        case 0:
        {
            
#if TARGET_IPHONE_SIMULATOR
            [UIUtils messageAlert:@"Camera not available!" title:@"Add Images" delegate:self];
            
#else
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
			picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.delegate = self;
            
            //[self.navigationController pushViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
#endif
        }
            break;
            
        case 1:
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
			picker.allowsEditing = YES;
            
            if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
            {
                _popOver = [[UIPopoverController alloc] initWithContentViewController:picker];
                _popOver.delegate = self;
                [_popOver presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                self.navigationController.navigationBar.userInteractionEnabled = NO;
            }
            else
            {
                
                picker.navigationBar.barStyle = UIBarStyleBlackOpaque;
				[self presentViewController:picker animated:YES completion:nil];
            }
        }
            break;
            
        case 2:
        {
            //            FGalleryViewController* localGallery = [[FGalleryViewController alloc] initWithPhotoSource:self];
            //            localGallery.useThumbnailView = NO;
            //            [self.navigationController pushViewController:localGallery animated:YES];
        }
            
        default:
			break;
    }
}

#pragma mark --
#pragma mark ImagePicker Delegate

- (void) imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
	NSData* dataImage = UIImagePNGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage"]);
    
    UIImage *image = [[UIImage alloc] initWithData:dataImage]; // imageView is image from camera
    _logoImage = image;
    
    NSString* path = [NSString stringWithFormat:@"logo"];
    NSString* filePath = [UIUtils pathForDocumentNamed:path];
    
    
    BOOL isSaveImage = [UIUtils saveData:UIImagePNGRepresentation(image) path:filePath];
    if (isSaveImage)
    {        
//        [UIUtils messageAlert:@"Image successfully saved with product" title:kApplicationName delegate:nil];
        _isFromPicker = YES;
        [_logoBtn setImage:image forState:UIControlStateNormal];
        if (self.navigationItem.rightBarButtonItem.enabled == NO)
		{
            self.navigationItem.rightBarButtonItem.enabled = YES;
			[self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor blackColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
		}
        
		[picker dismissViewControllerAnimated:YES completion:nil];
        
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            [_popOver dismissPopoverAnimated:YES];
            self.navigationController.navigationBar.userInteractionEnabled = YES;
        }
    }
    else
        [UIUtils messageAlert:@"Image not saved with product. Try Again!" title:kApplicationName delegate:nil];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end

