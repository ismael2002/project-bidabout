//
//  LoginViewController.m
//  BidAbout
//
//  Created by Waseem on 09/04/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "BidAboutAppDelegate.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
#import "AppPrefData.h"
#import "SignUpViewController.h"
#import "ForgotPasswordViewController.h"
#import "DataManager.h"

#define kSignInAlerttag 150

@interface LoginViewController ()

@end

@implementation LoginViewController

#pragma mark - ViewLifeCycle

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    NSString* sVersionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString* sBuildNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString* sBuildInfo = [NSString stringWithFormat:@"Ver %@ Build %@", sVersionNumber, sBuildNumber];
    _versionLbl.text = sBuildInfo;
    
    // To hide keyboard on touch outside textfield
    _tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    _tapgesture.cancelsTouchesInView = YES;
    
    _rememberMeBtn.selected = _gAppPrefData.isRememberMe;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logInDone:) name:kLogInSucessfullNotification object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.navigationController.navigationBarHidden = YES;
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	self.navigationController.navigationBarHidden = NO;
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void) viewDidUnload
{
    _userNameTextField = nil;
    _passwordTextField = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark --
#pragma mark Button  Action

- (IBAction) loginButtonAction:(id)sender
{
    BOOL isEmpty = [UIUtils validateForEmptyString:_userNameTextField.text
                                      andFieldName:@"Email"];
    if (isEmpty) {
        [_userNameTextField becomeFirstResponder];
        return;
    }
    
    isEmpty = [UIUtils validateForEmptyString:_passwordTextField.text
                                 andFieldName:@"Password"];
    if (isEmpty) {
        [_passwordTextField becomeFirstResponder];
        return;
    }
    
    //    BOOL isNotValid = [UIUtils validateEmail:_userNameTextField.text];
    //    if (isNotValid)
    //    {
    //        [_userNameTextField becomeFirstResponder];
    //        return;
    //    }
    if (!_gAppPrefData.isRememberMe) {
        if ([_gAppPrefData.emailId isEqualToString:_userNameTextField.text] && [_gAppPrefData.password isEqualToString:_passwordTextField.text]) {
            [self logInLogic];
        } else {
            [UIUtils messageAlertWithOkCancel:@"You have entered updated log-in info, It will erase your previous data." title:@"Sign-In Alert" delegate:self withTag:kSignInAlerttag];
        }
    } else {
        [self logInLogic];
    }
}

- (IBAction) radioButtonAction:(UIButton*)sender
{
    if (sender.selected)
    {
        _rememberMeBtn.selected = NO;
        sender.imageView.image = [UIImage imageNamed:@"checkbox.png"];
    }
    else
    {
        _rememberMeBtn.selected = YES;
        sender.imageView.image = [UIImage imageNamed:@"checkbox-checked.png"];
    }
}

- (IBAction) signUpAction:(id)sender
{
    SignUpViewController* signUpVC = [[SignUpViewController alloc] initWithNibName:@"SignUpView" bundle:nil];
    UINavigationController* navc  = [[UINavigationController alloc] initWithRootViewController:signUpVC];
	[self presentViewController:navc animated:YES completion:nil];
}

- (IBAction) forgotPasswordAction:(id)sender
{
    ForgotPasswordViewController *signUpVC = [[ForgotPasswordViewController alloc] initWithNibName:@"ForgotPasswordView" bundle:nil];
    UINavigationController* navc  = [[UINavigationController alloc] initWithRootViewController:signUpVC];
	[self presentViewController:navc animated:YES completion:nil];
}

#pragma mark --
#pragma mark Textfield delegate.

- (void) setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    float movedPixel = 100.0;
    
    if ([UIUtils isiPhone5])
        movedPixel = 100;
    else
        movedPixel = 190;
    
    // Make changes to the view's frame inside the animation block. They will be animated instead
    // of taking place immediately.
    CGRect rect = self.view.frame;
    if (movedUp)
    {
		if(rect.origin.y >= 0)
			rect.origin.y = self.view.frame.origin.y - movedPixel;
        [self.view addGestureRecognizer:_tapgesture];
    }
	else
    {
		if(rect.origin.y < 0)
			rect.origin.y = self.view.frame.origin.y + movedPixel;
        [self.view removeGestureRecognizer:_tapgesture];
    }
	self.view.frame = rect;
    [UIView commitAnimations];
}

- (void) textFieldDidBeginEditing:(UITextField*)textField
{
    [self setViewMovedUp:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField*)textField
{
	[textField resignFirstResponder];
	[self setViewMovedUp:NO];
	return NO;
}

#pragma mark --
#pragma mark NSNotification selector

- (void) logInDone:(BOOL)isSuccessfull withUser:(PFUser *)user
{
	if (isSuccessfull) {
        AppPrefData *appData = _gAppPrefData;
		appData.password = _passwordTextField.text;
		appData.emailId = _userNameTextField.text;
        appData.isLogin = appData.isRememberMe;
        appData.isFirstTimeLogedIn = YES;
        appData.isDefualttaxOn = _gAppPrefData.isDefualttaxOn;
        appData.currentTax = _gAppPrefData.currentTax;
        appData.memberID = user.objectId;
        _gAppPrefData.memberID = user.objectId;

        [_gAppPrefData saveAllData];
		[appData saveAllData];
        
        id appDelg = [[UIApplication sharedApplication] delegate];
        [appDelg performSelector:@selector(loginSuccessful) withObject:nil afterDelay:0.01];
	} else {
		[UIUtils messageAlert:kLoginError title:kApplicationName delegate:nil];
	}
}

- (void)keyboardWillHide:(NSNotification *)notification
{
	[self setViewMovedUp:NO];
}

#pragma mark --
#pragma mark Private methods

-(void)dismissKeyboard
{
    [_userNameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    [self setViewMovedUp:NO];
}

- (void) logInLogic
{
    AppPrefData *appData = _gAppPrefData;
    appData.password = _passwordTextField.text;
    appData.emailId = _userNameTextField.text;
    appData.isRememberMe = _rememberMeBtn.selected;
    
    [PFUser logInWithUsernameInBackground:_userNameTextField.text password:_passwordTextField.text
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            appData.memberID = user.objectId;
                                            [self logInDone:YES withUser:user];
                                        } else {
                                            [self logInDone:NO withUser:nil];
                                        }
                                    }];
    
    [self setViewMovedUp:NO];
    [_userNameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
}

#pragma mark --
#pragma mark AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kSignInAlerttag)
    {
        switch (buttonIndex)
        {
            case 1:
            {
                _gAppPrefData.currentTax = [NSNumber numberWithDouble:kCurrentTax];
                _gAppPrefData.isDefualttaxOn = YES;
                [_gAppData deleteUserData];
                [self logInLogic];
            }
                break;
                
            default:
                break;
        }
    }
    
}
@end
