//
//  CreateInvoiceViewController.h
//  BidAbout
//
//  Created by Waseem on 04/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClientViewController.h"
#import "ItemDescriptionViewController.h"
#import "AddPhotoViewController.h"
#import "PaymentInfoViewController.h"
#import <MessageUI/MessageUI.h>
#import "Templates.h"
#import "ScheduleViewController.h"
#import "TemplatesViewController.h"
#import "PDFDisplayViewController.h"

@class Customers;
@class Invoice;

@interface CreateInvoiceViewController : BaseViewController<UIActionSheetDelegate,UITextFieldDelegate,UITableViewDelegate, ClientViewControllerDelegate,ItemDescriptionViewControllerDelegate, PaymentInfoViewControllerDelegate ,AddPhotoViewControllerDelegate, MFMailComposeViewControllerDelegate,UIPrintInteractionControllerDelegate, PDFDisplayViewControllerDelegate>
{
	__weak IBOutlet UITableView* _tableView;
    __weak IBOutlet UIDatePicker* _datePicker;
    __weak IBOutlet UIView* _pickerView;
    
    UITapGestureRecognizer* _tapgesture ;
    
    TemplatesViewController* viewC;
    NSMutableDictionary* _invoiceDict;
    NSMutableArray* _itemArray;
    NSMutableArray* _invoicePhotoList;
    NSArray* possibleTemplateItems;
    NSIndexPath* indexpathForTitle;
    NSInteger invoice_ID;
    double amountPaid;
}

@property(nonatomic, retain) Customers* customerObj;
@property(nonatomic, retain) Templates* templateDictionary;
@property(nonatomic, retain) NSString* dateString;
@property(nonatomic, assign) BOOL haveClient;
@property(nonatomic, retain) Invoice* invoiceObj;
@property(nonatomic, retain) NSString* comment;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@property(nonatomic, strong) Templates* template;
@property(nonatomic, assign) BOOL changeActivity;
@property(nonatomic, assign) BOOL newTemplate;
@property(nonatomic, assign) BOOL isTemplate;
@property(nonatomic,assign) BOOL existingTemplate;
@property(nonatomic,assign) BOOL existingInvoice;
@property(nonatomic,assign) BOOL creatingTemplateFromEstimate;
@property(nonatomic,assign) BOOL activatingOrDeactivatingTemplate;

@property(nonatomic,assign) BOOL pdfQuantity;
@property(nonatomic,assign) BOOL pdfRate;
@property(nonatomic,assign) BOOL pdfSubtotal;
@property(nonatomic,assign) BOOL pdfTotal;
@property(nonatomic,assign) BOOL pdfOptions;

@property(nonatomic, retain) NSString* templateTitleAssigned;
@property(nonatomic,retain) UITextView *templateTitleBox;
@property(nonatomic,retain) NSData *dataOfPDF;


- (IBAction) selectdateFromPicker:(id)sender;

@end
