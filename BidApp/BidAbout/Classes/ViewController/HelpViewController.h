//
//  HelpViewController.h
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UISegmentedControl *feedbackType;
@property (strong, nonatomic) IBOutlet UITextView *feedback;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextView *helpText;
@property (weak, nonatomic) IBOutlet UILabel *feedbackLabel;

//@property (weak, nonatomic) IBOutlet UILabel *version;
@property (weak, nonatomic) IBOutlet UIButton *submitBTN;


@property (strong, nonatomic) IBOutlet UILabel *version;

- (void)submitFeedback:(id)sender;

@end
