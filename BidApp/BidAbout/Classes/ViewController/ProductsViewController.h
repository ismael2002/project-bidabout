//
//  ProductsViewController.h
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductsViewController : BaseViewController
{
	__weak IBOutlet UITableView* _tableView;
    NSMutableArray* _itemList;
    NSArray* _results;
}

@end
