//
//  RequestsViewController.m
//  BidAbout
//
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import "RequestsViewController.h"
#import "RequestDetailViewController.h"
#import "Customers.h"

@interface RequestsViewController ()

@end

@implementation RequestsViewController

#pragma mark--ViewLife Cycle.

- (void) viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"Requests";
    
    UIColor *navColor = [UIColor colorWithRed:(20.0f/255.0f) green:(130.0f/255.0f) blue:(20.0f/255.0f) alpha:1.0f];
    self.navigationController.navigationBar.tintColor = navColor;
    
    // get revelButton
    UIButton *barButton = [UIUtils getRevelButtonItem:self];
    
    //Added revel button as nevegation button
    UIBarButtonItem *revealbuttonItem = [[UIBarButtonItem alloc] initWithCustomView:barButton];
    self.navigationItem.leftBarButtonItem = revealbuttonItem;
    
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:self
                                                                 action:@selector(editTable:)];
    [self.navigationItem setRightBarButtonItem:addButton];
    //uncommented the addition of the right bar button
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadRequests:) name:kCustomerCreateServiceSucess object:nil];
    
    _tableView.allowsMultipleSelectionDuringEditing = NO;
    
	//[_gAppData sendRequestForQuotes];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self getRequests];
    [self loadRequests:nil];
}

- (void) viewDidAppear:(BOOL)animated
{
    [_gAppPrefData numberOfPendingRequests:^(NSInteger requestCount) {} clear:YES];
	[super viewDidAppear:animated];
}

- (void)viewDidUnload
{
	_tableView = nil;
	[super viewDidUnload];
}

- (IBAction)deleteButtonAction:(id)sender
{
    
}

- (IBAction) editTable:(id)sender{
    if (self.editing) {
        [super setEditing:NO animated:NO];
        [_tableView setEditing:NO animated:NO];
        [_tableView reloadData];
        [self.navigationItem.leftBarButtonItem setTitle:@"Edit"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
    } else {
        [super setEditing:YES animated:YES];
        [_tableView setEditing:YES animated:YES];
        [_tableView reloadData];
        [self.navigationItem.leftBarButtonItem setTitle:@"Done"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStyleDone];
    }
}

#pragma mark - Parse

- (void)getRequests
{
    PFQuery *query = [PFQuery queryWithClassName:@"Quote"];
    [query whereKey:@"memberID" equalTo:_gAppPrefData.memberID];
    [query whereKey:@"isQuote" equalTo:@YES];
    
    NSArray *filter = [NSArray arrayWithArray:[_gAppData getCustomerDetails:YES andCustomerId:nil]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSMutableArray *customers = [[NSMutableArray alloc] init];
            for (PFObject *object in objects) {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"customer_external_id == %@", object.objectId];
                NSArray *filteredArray = [filter filteredArrayUsingPredicate:predicate];
                if ([filteredArray count] <= 0) {
                    NSString *dateString = [NSDateFormatter localizedStringFromDate:object.createdAt dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
                    NSDictionary *dict = @{@"CustomerMemberId":_gAppPrefData.memberID,
                                           @"CustomerFirstName":object[@"firstname"],
                                           @"CustomerLastName":object[@"lastname"],
                                           @"CustomerAddress1":object[@"address"],
                                           @"CustomerCity":object[@"city"],
                                           @"CustomerState":object[@"state"],
                                           @"CustomerZip":object[@"zip"],
                                           @"CustomerMobilePhone":object[@"phone"],
                                           @"CustomerEmail":object[@"email"],
                                           @"CustomerNotes":object[@"quote"],
                                           @"CustomerPendingQuote":object[@"isQuote"],
                                           @"CustomerExternalId": object.objectId,
                                           @"CustomerCreatedDate":dateString};
                    [customers addObject:dict];
                }
            }
            [_gAppData saveCustomerDeail:customers];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

- (void)loadRequests:(id)sender
{
    _requestCustomerList = [_gAppData getCustomerDetails:YES andCustomerId:nil];

    [_tableView reloadData];
}

#pragma mark --
#pragma mark TableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _requestCustomerList.count;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
	NSString* cellIdenifire  = @"My Identifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdenifire];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdenifire];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    }
	Customers *obj = [_requestCustomerList objectAtIndex:indexPath.row];
    if (obj)
    {
		NSString *name = [NSString stringWithFormat:@"%@ %@", obj.customer_firstname, obj.customer_lastname];
        cell.textLabel.text = name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Requested %@",obj.customer_createdDate];
    }
    
    return cell;
}

#pragma mark --
#pragma mark TableView Delegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RequestDetailViewController* reqDVC = [[RequestDetailViewController alloc] initWithNibName:@"RequestDetailView" bundle:nil];
    reqDVC.customerObj = [_requestCustomerList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:reqDVC animated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing == NO || !indexPath) {
        return UITableViewCellEditingStyleNone;
    }
    
    if (self.editing && indexPath.row == [_requestCustomerList count]) {
        return UITableViewCellEditingStyleNone;
    } else {
        return UITableViewCellEditingStyleDelete;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        Customers *obj = [_requestCustomerList objectAtIndex:indexPath.row];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Quote"];
        NSLog(@"%@", obj.customer_external_id);
        [query getObjectInBackgroundWithId:obj.customer_external_id block:^(PFObject *object, NSError *error) {
            if (!error) {
                object[@"isQuote"] = @NO;
                [object saveInBackground];
                
                NSMutableDictionary *customerDict = [[NSMutableDictionary alloc] init];
                [customerDict setObject:[NSString stringWithFormat:@"%ld",(long)[obj.customer_id integerValue]] forKey:@"CustomerID"];
                [customerDict setObject:obj.customer_company forKey:@"CustomerMemberCompany"];
                [customerDict setObject:obj.customer_firstname forKey:@"CustomerFirstName"];
                [customerDict setObject:obj.customer_lastname forKey:@"CustomerLastName"];
                [customerDict setObject:obj.customer_addres1 forKey:@"CustomerAddress1"];
                [customerDict setObject:obj.customer_addres2 forKey:@"CustomerAddress2"];
                [customerDict setObject:obj.customer_city forKey:@"CustomerCity"];
                [customerDict setObject:obj.customer_state forKey:@"CustomerState"];
                [customerDict setObject:obj.customer_zip forKey:@"CustomerZip"];
                [customerDict setObject:obj.customer_mobilePhone forKey:@"CustomerMobilePhone"];
                [customerDict setObject:obj.customer_email forKey:@"CustomerEmail"];
                [customerDict setObject:obj.customer_notes forKey:@"CustomerNotes"];
                [customerDict setObject:[NSString stringWithFormat:@"%ld",(long)[@NO integerValue]] forKey:@"CustomerQuotes"];
                
                [_gAppData updateCustomerDetail:customerDict];
                
                NSMutableArray *mutArray = [NSMutableArray arrayWithArray:_requestCustomerList];
                [mutArray removeObject:obj];
                _requestCustomerList = [NSArray arrayWithArray:mutArray];
                
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            }
        }];
        
        
    }
}
@end
