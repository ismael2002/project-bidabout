//
//  InvoiceViewController.h
//  BidAbout
//
//  Created by     on 11/07/13.
//  Copyright (c) 2013 BolderImage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoiceViewController : BaseViewController <UITableViewDelegate>
{
	__weak IBOutlet UITableView* _tableView;
    
    NSArray* _invoiceList;
    NSArray* _openOrPaidInvoices;
    
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property(nonatomic, retain) Invoice* invoiceObj;

@end
